var mongoose                  = require('mongoose');
var MongoClient               = require('mongodb').MongoClient;
// DB Config
const db                      = require('../config/keys').mongoURI;
const db_options              = require('../config/keys').db_options;
const database_name           = require('../config/keys').database_name;

var moment                    = require('moment');
var ObjectId                  = require('mongodb').ObjectId;

var activity = {};

/**
 * @method      : /getRecentAcitityLogs
 * @description : Fetching 5 recent activing logs and current user login log data for date manipulations 
 * @return      : activity_logs data
*/

activity.getRecentAcitityLogs = function (req, res, next) {
    
  MongoClient.connect(db, db_options).then(client => {
    const db          = client.db(database_name);

    (async () => {
      var user_o_id   = new ObjectId(req.user._id);
      const activity_logs_details  = await db.collection('activity_logs').find({'is_read':0}).project({ '_id': 0 }).limit(5).sort({'_id':-1}).toArray();
      const get_current_user_login = await db.collection('activity_logs').find({'type' : 'Login', 'created_by' : user_o_id}).sort({_id:-1}).limit(1).toArray(); console.log(get_current_user_login);
     
      var activity_data            =  activity_logs_details.map(function(data) {
                                        return data['log_time']   = moment(data['created_at'], "MM/DD/YYYY, h:mm:ss a").fromNow();
                                      });

      activity_logs_details['get_login_time'] = moment(get_current_user_login[0].created_at, "MM/DD/YYYY, h:mm:ss a").format("LT");
      activity_logs_details['get_login_date'] = moment(get_current_user_login[0].created_at, "MM/DD/YYYY, h:mm:ss a").format("DD MMM YYYY");
      req.activity_logs           = activity_logs_details; 
      client.close();
      next();
    })(); //async end
  });
}//getRecentAcitityLogs

/**
 * @method      : /getActivityTotalCount
 * @description : Getting count of unread logs 
 * @return      : count
*/

activity.getActivityTotalCount = function (req, res, next) {
  MongoClient.connect(db, db_options).then(client => {
    const db                   = client.db(database_name);
    (async () => {
      const activity_log_count = await db.collection('activity_logs').count({'is_read':0});
      req.activity_logs_count  = activity_log_count;
      client.close();
      next();
    })(); //async end
  });
}//getActivityTotalCount


module.exports = activity;