const db                      = require('../config/keys').mongoURI;
const db_options              = require('../config/keys').db_options;
const database_name           = require('../config/keys').database_name;
var MongoClient               = require('mongodb').MongoClient;
var ObjectId                  = require('mongodb').ObjectId;
var url                       = require('url');
var functions_handler         = require('../helpers/functions_handler');

var roles = {};

/*
method      : getRoleBasedAccess
description : Check whether given url have permissions or not, sends permission values to next request if url have permissions
params      : 
return      : Throws error if url is not exists, { req.user['role_based_access'], req.user['permission_access'] }
*/

roles.getRoleBasedAccess = function (req, res, next) {
  
  MongoClient.connect(db, db_options).then(client => {
    const db    = client.db(database_name);
    
    (async () => {
      
      var user_role_id         = req.user.role_id;
      var role_obj_id          = new ObjectId(user_role_id);

      const get_user_access    = await db.collection('roles').findOne({'_id':role_obj_id});
      var user_access_len      = Object.keys(get_user_access).length;
     
      if (user_access_len > 0) {
        var get_permissions    = get_user_access.permissions;
      }

      var menu_ids             = Object.keys(get_user_access.permissions); 
      var menu_o_ids           = menu_ids.map(function(o_id) {
        return new ObjectId(o_id);
      });
     
      const get_menus         = await db.collection('main_menus').find({'_id':{$in: menu_o_ids}}).sort({'menu_sort_no' : 1}).toArray();
      var current_url         = req.originalUrl;  

      if (Object.keys(req.params).length > 0) {
        current_url = current_url.substring(0, current_url.lastIndexOf("/") + 1);
      }
      current_url = current_url.replace(/\/$/, "");

      var final_arr           = [];
      var allowed_urls        = [];
      var pages_per_access    = [];
      
      for (i = 0; i < get_menus.length; i++) {
        var data    = {};
      
        if (menu_ids.includes(get_menus[i]._id.toString())) {
          
          get_menus[i].role_menu_type_id = get_user_access.menu_type_id;
          get_menus[i].permissions       = get_permissions[get_menus[i]._id];
          data[get_menus[i]._id]         = get_menus[i];
          if (!allowed_urls.includes(get_menus[i].menu_url)) 
            allowed_urls.push(get_menus[i].menu_url);

          var sub_menus                  = get_menus[i].sub_menus;
          if (sub_menus) {
          for (j = 0; j < Object.keys(sub_menus).length; j++) {
            var sub_menu_arr             = sub_menus[Object.keys(sub_menus)[j]];
            for (k = 0; k < sub_menu_arr.length; k++) {

              var sub_menu_url           = sub_menu_arr[k].sub_menu_url;
              if (!allowed_urls.includes(sub_menu_arr[k].sub_menu_url)) 
                allowed_urls.push(sub_menu_url);

              if (current_url == sub_menu_url && get_permissions[get_menus[i]._id][sub_menu_arr[k].sub_menu_id]) {

                var per_sub_data  = get_permissions[get_menus[i]._id][sub_menu_arr[k].sub_menu_id];
                 
                var conv_to_obj   = functions_handler.convertArrayToObj(per_sub_data);
                pages_per_access.push(conv_to_obj)
              }
            }
          }
        } else {
          if (current_url == get_menus[i].menu_url) {
            pages_per_access.push(get_permissions[get_menus[i]._id])
          }
        }
      }
      final_arr.push(data);
    }

    if (pages_per_access) {
      var page_per_obj  = functions_handler.convertArrayToObj(pages_per_access);
    }

    var check_urls  = await check_allowed_urls(allowed_urls, current_url);
     
    client.close();  
    if (req.user.role_name == 'Super Admin') {  
      req.user['role_based_access'] = final_arr;
      req.user['permission_access'] = {'create': '1', 'read' : '1', 'update' : '1', 'delete' : '1'};
      return next();
    } else {
      if (!check_allowed_urls) {
      res.send({'error': 'You do not have access to view this page'});
    } else {
      req.user['role_based_access'] = final_arr;
      req.user['permission_access'] = page_per_obj;
      return next();
    }  
    }
      
    
    })(); //async end
  });
}//getRecentAcitityLogs


/*
method      : check_allowed_urls
description : Check given url have permissions or not
params      : allowed_urls (contains all the urls that existed in db for logined user), current_url
return      : true, false
*/
async function check_allowed_urls(allowed_urls, current_url) {
  var return_status = true;
  if(!allowed_urls.includes(current_url)) {
    return_status  = false;
  } 
  return return_status;
}//check_allowed_urls


module.exports = roles;