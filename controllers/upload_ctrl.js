var mongoose               = require('mongoose');
var MongoClient            = require('mongodb').MongoClient;
// DB Config
const db                   = require('../config/keys').mongoURI;
const db_options           = require('../config/keys').db_options;
const database_name        = require('../config/keys').database_name;
const fs                   = require('fs');
var Hashids                = require('hashids');
var XLSX                   = require('xlsx')
const {convertArrayToCSV}  = require('convert-array-to-csv');

var ISODate                = require("isodate");
var csv_export             = require('csv-export');

// DB Config
var csv                    = require('fast-csv');
const csv_parser           = require('csv-parser');
const baseURL              = require('../config/keys').base_URL;
var functions_handler      = require('./../helpers/functions_handler'); 
var global_function        = require('./../helpers/global_func'); 
var path                   = require('path');
const shortid              = require('shortid');
var shortlink              = require('shortlink');

var upload_data            = {};

/**
* method: getInvalidNumbers
* description: Getting invalid numbers list, inserting contact numbers which are not in db and downloading invalid contacts to file
* return: filepath, valid numbers list(already existed numbers in database)
*/
upload_data.getInvalidNumbers  = function (req, res, next) {

  MongoClient.connect(db, db_options).then(client => {
    const db                    = client.db(database_name);      

    (async () => {

      var mbl_num_list          = req.mobile_number_list; 
      var mbl_json_list         = req.mbl_num_json_list; 
      var user_name             = req.body.user_name;
      const contact_num_details = await db.collection('contacts').find({'mobile_no':{$in:mbl_num_list}}, { 'fields': {'_id': 0, 'mobile_no':1}}).toArray();

      var array4                = await mbl_json_list.filter(o => !contact_num_details.find(o2 => o.mobile_no === o2.mobile_no)); //invalid numbers 
      var hashids               = new Hashids();
      if (array4.length > 0) {
        /* batch size dynamic count */
          var length            = array4.length - 1; 
          var each_arr_size     = 100000;
          var iteration_batch_min_and_max_values = [];

          if (length > 100000) {
            for (var size = 0; size < length;) {
              if (iteration_batch_min_and_max_values.length > 0) {
                var min_val       = length-size-each_arr_size; 
                var min_range_val = min_val;
                if (min_val < 0) {
                  min_range_val   = 0;
                }
                iteration_batch_min_and_max_values.push([length-size-1, min_range_val]);
              } else {
                iteration_batch_min_and_max_values.push([length, length-each_arr_size]);
              }
              size                = size + each_arr_size;
            }
          } else {
            iteration_batch_min_and_max_values.push([length, 0])
          }
        /* end batch size dynamcic count */
        var mobile_number_list    = [];
        var batch_array_data_prepartion = [];
        for (var i = 0; i < iteration_batch_min_and_max_values.length; i++) {
          var min_value           = iteration_batch_min_and_max_values[i][0]; 
          var max_value           = iteration_batch_min_and_max_values[i][1]; 
          for (var j = min_value; j >= max_value; j--) {
              var insert_invalid_nums = {};
              var new_id              = new mongoose.Types.ObjectId();

              var id                  = new_id.toString(),
                  ctr                 = 0;
              var timestamp           = parseInt(id.slice(ctr, (ctr += 8)), 16);
              var machineID           = parseInt(id.slice(ctr, (ctr += 6)), 16);
              var processID           = parseInt(id.slice(ctr, (ctr += 4)), 16);
              var counter             = parseInt(id.slice(ctr, (ctr += 6)), 16);
             
              var mobile_no           = array4[j].mobile_no;
             
              var filter_reg_exp_mobile_no = mobile_no;
              mobile_number_list.push(new RegExp('^'+filter_reg_exp_mobile_no));

              insert_invalid_nums['_id']                 = new mongoose.Types.ObjectId();
              insert_invalid_nums['raw_category_id']     = []; //category ID
              insert_invalid_nums['category_ids']        = []; //category ID
              insert_invalid_nums['campaign_ids']        = []; // Campaign ID
              insert_invalid_nums['campaign_content_id'] = []; // Content Label ID
              insert_invalid_nums['sender_category_ids'] = []; // sender_category_ids ID
              insert_invalid_nums['clicker_category_id'] = [];
              insert_invalid_nums['sender_status']       = [];
              insert_invalid_nums['cat_id']              = [];
              insert_invalid_nums['sub_cat_id']          = [];
              insert_invalid_nums['is_active']           = 0;
              insert_invalid_nums['unique_id']           = shortlink.generate(8);
              insert_invalid_nums['mobile_no']           = mobile_no;
              insert_invalid_nums['created_at']          = new Date().toLocaleString('en-US', {timeZone: 'Asia/Calcutta'});
              batch_array_data_prepartion.push(insert_invalid_nums);
            } //end for

            if (batch_array_data_prepartion.length > 0) {
           
              await db.collection('contacts').insertMany(batch_array_data_prepartion, {ordered: false}).then(result => {}).catch((error) => {
                  console.log('error')
                  console.log(error)
                });
            } //end if

        }// end for 

        var activity_data = {
          'type'              : 'Suppress',
          'Location'          : 'Admin Panel',
          'description'       : 'Hi ' + user_name +', suppress contacts inserted successfully <br/>(Total '+ batch_array_data_prepartion.length+' invalid contacts inserted and blocked)',
          'total_contacts'    : mobile_number_list.length,
          'user_name'         : user_name,
          'duplicate_contacts': 0,
          'created_by'        : '',
          'is_read'           : 0,
          'created_at'        : new Date().toLocaleString('en-US', {
            timeZone: 'Asia/Calcutta'
          })
        };
        await db.collection('activity_logs').insert(activity_data);
      } // if array4.length end

      if (req.invalid_contact_list.length > 0) {
        var filename           = 'invalid_contacts_' + functions_handler.getCurrentDateTime();
        var create_zip         = './public/invalid_contacts/'+ filename + '.zip';
        var createStream       = fs.createWriteStream(create_zip);
        createStream.end();
        var export_mobile_data = {};
        export_mobile_data[filename.toString()] = req.invalid_contact_list;
        await csv_export.export(export_mobile_data, function(buffer) {
          fs.writeFileSync(create_zip, buffer);
          //res.download(path.join(__dirname, '/../public/export/invalid_contacts/invalid_contacts_18_10_2019_10_31_am.zip'));
        });
        req.file_path         = create_zip;
      }
      req.valid_mbl_num_list  = contact_num_details;
      next();
    })(); //async end
  });
}//getRecentAcitityLogs

/**
* method: getMobileNumbers
* description: Getting mobile numbers from files(xls, xlsx, csv, txt) or textarea
* return: json list, arraylist mobilenumbers
*/
upload_data.getMobileNumbers = function (req, res, next) {

  MongoClient.connect(db, db_options).then(client => {
    const db           = client.db(database_name);      

    var smsDataCsvFile = req.file;
    var file_extension = req.file.originalname;
    var extension      = file_extension.substr(file_extension.lastIndexOf(".") + 1);
    // var mobile_number_list = [];
    // var mbl_num_json_list = [];
    (async () => {
    if (extension !== 'txt') {
      
        //console.log('CSV');
        /* Import -  CSV - Start */
        //var data = await fs.readFileSync(smsDataCsvFile.path); 
        /* Import -  CSV - Stop */
      if (extension == 'xls' || extension == 'xlsx' || extension == 'csv') {
        //console.log('Excel');
        /* Import -  Excel - Start */
        var workbook        =  XLSX.readFileSync(smsDataCsvFile.path);
        var sheet_name_list =  workbook.SheetNames;
        var data            =  XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]); 
        //convert Array To CSV
        //var data = convertArrayToCSV(excel_data);
        //var data =  XLSX.utils.sheet_to_csv(workbook.Sheets[sheet_name_list[0]]);
        /* Import -  Excel - Stop */
      } 

      var file_json_list        = Object.values(data);
      var mbl_num_json_list     = [];
      var invalid_contact_list  = [];
      var mobile_number_list    = file_json_list.map(function(data) {
        if (validateMobileNum(data.mobile_no)) {
          data.mobile_no        = (data.mobile_no.replace(/\D/g, '').slice(-10));
          mbl_num_json_list.push(data);
          return data.mobile_no;
        } else {
          console.log(data.mobile_no);
          invalid_contact_list.push({'mobile_no': data.mobile_no, 'reason': 'Mobile number must be valid 10 digit number'})
        }
      });
        
      // await csv.fromString(data, {
      //   headers: true,
      //   //strictColumnHandling: true,
      //   ignoreEmpty: true
      // }).on("data", function(data) {
      //   mbl_num_json_list.push({'mobile_no': data['mobile_no'].toString()});
      //   mobile_number_list.push(data['mobile_no'].toString());
      // })
    } else {
      var data = await fs.readFileSync(smsDataCsvFile.path, 'utf-8'); 

      var json_txt_data         = JSON.parse('[' + data.replace(/}{/g, '},{') + ']');
      
      var file_json_list        = Object.values(json_txt_data);
      var mbl_num_json_list     = [];
      var invalid_contact_list  = [];
      var mobile_number_list    =  file_json_list.forEach(function(data) {
        if (validateMobileNum(data.mobile_no)) {
          data.mobile_no        = (data.mobile_no.replace(/\D/g, '').slice(-10));
          mbl_num_json_list.push(data);
          return data.mobile_no;
        } else {
          invalid_contact_list.push({'mobile_no': data.mobile_no, 'reason': 'Mobile number must be valid 10 digit number'})
        }
      });
      // for (i=0; i < json_txt_data.length;i++) {
      //   var mobile_number = mbl_num_json_list.push({'mobile_no': json_txt_data[i].mobile_no});
      //   mobile_number_list.push(json_txt_data[i].mobile_no);
      // }
    }

    // console.log(mbl_num_json_list);
    // console.log(mobile_number_list);
    req.mbl_num_json_list    = mbl_num_json_list;
    req.mobile_number_list   = mobile_number_list;
    req.invalid_contact_list = invalid_contact_list;
    next();
  })(); //async end
  client.close();
  });
}//getMobileNumbers

upload_data.getNumbersRequest = function (req, res, next) {
  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);      

    (async () => {

      var mob_number_list    = req.body.block_contacts.split('\n'); 
        mob_number_list      = mob_number_list.filter(v=>v!=''); 
      var mobile_number_list = [];
      var mbl_num_json_list  = [];
      var invalid_contact_list  = [];
      for (i=0; i < mob_number_list.length;i++) {
        if (validateMobileNum(mob_number_list[i])) {
          mob_number_list[i] = (mob_number_list[i].replace(/\D/g, '').slice(-10));
          var mobile_number  = mbl_num_json_list.push({'mobile_no': mob_number_list[i]});
          mobile_number_list.push(mob_number_list[i]);
        } else {
          invalid_contact_list.push({'mobile_no': mob_number_list[i], 'reason': 'Mobile number must be valid 10 digit number'})
        }
      }
       
      req.mbl_num_json_list  = mbl_num_json_list;
      req.mobile_number_list = mobile_number_list;
      req.invalid_contact_list = invalid_contact_list;
      next();
    })(); //async end
    client.close();
  });
}//getRecentAcitityLogs



module.exports = upload_data;
