let express    = require('express');
let app        = express();
var MongoClient = require('mongodb').MongoClient;
var cron = require('node-cron');
const db = require('./config/keys').mongoURI;
const db_options = require('./config/keys').db_options;
const database_name = require('./config/keys').database_name;

/*const { cpus } = require('os');
const { fork } = require('child_process');

const numWorkers = cpus().length;
for (let i = 0; i < numWorkers; i += 1) {
  console.log('numWorkers');
  console.log(numWorkers);
  fork('./routes/queue/kue.js');
}

*/

app.use('/queue', require('./routes/queue/pending_queue.js'));

app.listen(5007);