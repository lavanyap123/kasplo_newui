const express            = require('express');
const expressLayouts     = require('express-ejs-layouts');
const mongoose           = require('mongoose');
const passport           = require('passport');
const flash              = require('connect-flash');
const session            = require('express-session');
var nodemailer           = require('nodemailer');
const app                = express();
var cookieParser         = require('cookie-parser');

require('./helpers/smpp');
// Passport Config
require('./config/passport')(passport);
const fs                 = require("fs");
const path               = require("path");
// DB Config
const db                 = require('./config/keys').mongoURI;
const db_options         = require('./config/keys').db_options;
const database_name      = require('./config/keys').database_name;
const mode               = require('./config/keys').mode;
const base_URL           = require('./config/keys').base_URL;
const mail_from          = require('./config/keys').mail_from;
const to_mail_ids        = require('./config/keys').to_mail_ids;
const smtp_credentials   = require('./config/keys').smtp_credentials;
var MongoStore           = require('connect-mongo')(session);

/*console.log(db);*/
// Connect to MongoDB
mongoose.connect(db, db_options)
.then(() => console.log('APP.js = MongoDB Connected from='+db))
.catch(err => console.log(err));


mongoose.connection.on('error', (err) => {
  //console.log(err);
  //sendEmail(err,'MongoDB error');
});

process.on('uncaughtException', (err) => {
  console.log(err);

  //sendEmail(err,'Uncaught Exception thrown');
});

process.on('unhandledRejection', (err) => {
  console.log(err);

  //sendEmail(err,'Unhandled Rejection at Promise');
});


function sendEmail(err, subject) {
  var transporter          = nodemailer.createTransport(smtp_credentials);

  if (mode == 'localhost') { 
    var subject            = "This error is " + mode + "(" + base_URL + ")";
  } else {
    var subject            = mode + "- Kasplo - Bulk SMS[app.js] - Critical alert -" + subject;
  }

  var mailOptions = {
    from       : mail_from, 
    to         : 'vinodhtamilvanan@adcanopus.com',        
    subject    : subject,// Subject line
    text       : err.stack.toString(), // plain text body
    html       : "<p><b style='color:red;'>" + err.stack.toString() + "</b></p>" // html body
  };

  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
    } else {
      console.log("Message sent: %s", info.messageId);
      console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    }
  });    
}
// EJS
//app.use(expressLayouts);
app.set('view engine', 'ejs');
// Routes
app.use("/public", express.static(__dirname + '/public/'));
// Express body parser
app.use(express.urlencoded({
    extended: true
}));


// Express session
app.use(
    session({
        secret: 'secret',
        resave: true,
        saveUninitialized: true,
        store: new MongoStore(
          {mongooseConnection:mongoose.connection}
        )   
    })
);

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

app.use(cookieParser());

// Connect flash
app.use(flash());

app.all('/*', function(req, res, next) {
  
  var fullUrl      = req.protocol + '://' + req.get('host') + req.originalUrl;
  var original_url = req.originalUrl.substr(0, 4);
  var domain_name  = req.get('host');

  /* var adcan_ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

  if(adcan_ip.substr(0, 7) == "::ffff:") {
      adcan_ip = adcan_ip.substr(7)
  }

  var target_ip=['182.75.91.58','125.99.253.162','106.51.76.101'];

  if(original_url=='/cro')
  {
    next();
  }
  else if(target_ip.includes(adcan_ip)==false&&original_url!='/tc/' || original_url!='/tc/' && domain_name=='www.txtby.me' || original_url!='/tc/' && domain_name=='http://txtby.me/'
    || original_url!='/tc/' && domain_name=='txtby.me' || original_url!='/tc/' && domain_name=='http://txtby.me'
    ){ 
    res.redirect('http://kasplo.com');
  } else {
    next();
  }*/

  next();
});

// Global variables
app.use(function(req, res, next) {
    res.locals.success_msg           = req.flash('success_msg');
    res.locals.import_success_msg    = req.flash('import_success_msg');
    res.locals.import_validation_msg = req.flash('import_validation_msg');
    res.locals.error_msg             = req.flash('error_msg');
    res.locals.error                 = req.flash('error');
    next();
});


app.use('/', require('./routes/index.js'));
app.use('/category', require('./routes/category.js'));
app.use('/campaign', require('./routes/campaign.js'));
app.use('/import', require('./routes/import.js'));
app.use('/export', require('./routes/export.js'));
app.use('/report', require('./routes/report.js'));
app.use('/report_export', require('./routes/report_export.js'));
app.use('/tc', require('./routes/tc.js'));
app.use('/users', require('./routes/users.js'));
app.use('/accounts', require('./routes/accounts.js'));
app.use('/draft', require('./routes/draft.js'));
//app.use('/cron', require('./routes/cron.js'));
app.use('/link', require('./routes/sms_link.js'));
app.use('/source', require('./routes/source.js'));
app.use('/operator', require('./routes/operator.js'));
app.use('/profession', require('./routes/profession.js'));
app.use('/settings', require('./routes/settings.js'));
app.use('/smpp', require('./routes/smpp.js'));
app.use('/sms', require('./routes/send_sms.js'));
app.use('/sender_id', require('./routes/sender_id.js'));
app.use('/roles', require('./routes/roles.js'));
app.use('/menus', require('./routes/menu.js'));
/***testing purpose ***/ 
app.use('/send', require('./routes/send.js'));
/***testing purpose ***/

/** conversion API **/
app.use('/conversion', require('./routes/conversion_url.js'));
/** End conversion API **/

require('./helpers/global_func');

app.use('/pending_queue', require('./routes/queue_resend.js'));

const PORT = process.env.PORT || 5006;

process.on('SIGINT', function() {
  mongoose.connection.close(function () {
    console.log('Mongoose disconnected on app termination');
    process.exit(0);
  });
});

// Handle 404
app.use(function(req, res) {
  
 res.render('404');
});
//app.listen(PORT, console.log(`Server started on port ${PORT}`));
const server   = app.listen(5006, console.log(`Server started on port ` + PORT));
// increase the timeout to 4 minutes
server.timeout = 3600000;  
/* ##################### UPLOAD PORT-2 Configuration - (Stop) ####################### */
