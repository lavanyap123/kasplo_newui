const role_controller   = require('./../controllers/role_ctrl');

/*
method      : ensureAuthenticated
description : checks whether the login users exists in db or not
params      : 
return      : authenticated_user to next middleware, error message
*/
var ensureAuthenticated = function(req, res, next) {
  if (req.isAuthenticated()) {
    next();
  } else {
    req.flash('error_msg', 'Please log in to view that resource');
    res.redirect('/users/login');
  }
} //ensureAuthenticated

/*
method      : roleAuthenticated
description : Assigns the permissions and page access to logined user based on the role
params      : 
return      : error message, next middleware
*/
var roleAuthenticated = function(req, res, next) {
  role_controller.getRoleBasedAccess(req, res, function() {
    next();
  });
} //roleAuthenticated

module.exports = {
  ensureAuthenticated : ensureAuthenticated,
  roleAuthenticated   : roleAuthenticated
};
