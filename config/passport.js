const LocalStrategy         = require('passport-local').Strategy;
const mongoose              = require('mongoose');
const bcrypt                = require('bcryptjs');

// Load User model
const User                  = require('../models/user');

module.exports              = function(passport) {
  passport.use(
    new LocalStrategy({ usernameField: 'email', passReqToCallback : true }, 
      function (req, email, password, done) {
      var response   = req.res;
      // Match user
      User.findOne({
        email: email
      }).then(user => {
        if (!user) {
          //return done(null, false, response.send({'status' : 400, message : 'Invalid credentials'}));
          response.send({'status' : 400, message : 'email not registered'});
        } else {
          // Match password
          bcrypt.compare(password, user.password, (err, isMatch) => {
            if (err) throw err;
            if (isMatch) {
              return done(null, user);
              
            } else {
              response.send({'status' : 400, message : 'Invalid credentials'});
              //done(null, false, ));
              //response.send({'status' : 400, message : 'Invalid credentials'});
            }
          });
        }
      });
  }));

  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });
};
