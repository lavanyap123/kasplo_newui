const ifaces             = require('os').networkInterfaces();
  let server_ip_address;

  Object.keys(ifaces).forEach(dev => {
    ifaces[dev].filter(details => {
      if (details.family === 'IPv4' && details.internal === false) {
        server_ip_address = details.address;
      }
    });
});

if (server_ip_address == '172.31.16.250') {

  /* ################ LIVE CREDENTIALS & Configuration - (Start) ######################### */

	DB_CONNECTION_STRING_URL = 'mongodb+srv://adcanadmin:gi0MFMlELjFopzAe@kasplo-db-bprde.mongodb.net/bulk_sms?readPreference=secondaryPreferred&w=majority';
	DATABASE_NAME            = 'bulk_sms';
	BASE_URL                 = 'http://panel.kasplo.com';
	UPLOAD_BASE_URL          = 'http://panel.kasplo.com:5001';
  RABBITMQ_URL             = 'amqp://wnwkkqbe:CvyrrhqBTcnbT4CfEtRLzCSXxcwhDpsc@grand-peacock.rmq.cloudamqp.com/wnwkkqbe';
  //'amqp://firdhjgf:FDOZLPfcHA6pwUX8HBWdsHMIpQ_YHwAZ@gull.rmq.cloudamqp.com/firdhjgf';
	MODE                     = 'live';
  DB_OPTIONS               = {
      useNewUrlParser     : true,
      //poolSize          : 30,
      //keepAlive         : true,
      //autoReconnect     : true,
      //reconnectTries    : Number.MAX_VALUE,
      //reconnectInterval : 1000,
      socketTimeoutMS     : 600000,
      connectTimeoutMS    : 600000
  };

  SMTP_CREDENTIALS        = {
        service           : 'smtp',
        host              : "tracecampaigns.com",
        port              : 587,
        secure            : false,
        auth              : {
          user            : 'mailclass-kasplo@notifications.kasplo.in',
          pass            : 'hbf78iufbjs'
        }
  };

  REDIS_CREDENTIALS       = {
      redis               : {
        host              : "kasplo.e2bp7t.ng.0001.aps1.cache.amazonaws.com",
        port              : 6379        
      },
      disableSearch       : false
  };

  MAIL_FROM               = 'Kasplo Notification <support@notifications.kasplo.in>';
  TO_MAIL_IDS             = 'sms.support@adcanopus.com,vinodhtamilvanan@adcanopus.com,lavanyap@adcanopus.com,atul@adcanopus.com,anil@adcanopus.com';
  //TO_MAIL_IDS           = 'vinodhtamilvanan@adcanopus.com';

  /* ################ LIVE CREDENTIALS - (Stop) ######################### */
  CRON_DIR                = '/var/www/kasplo.com/pull-data-api/psftft_data';
  CRON_CURL_DIR           = BASE_URL + '/cron/upload_psftft_file?upload_type = knowastro';
} else {

  /* ################ LOCALHOST CREDENTIALS - (Start) ######################### */

	DB_CONNECTION_STRING_URL = 'mongodb://localhost:27017/bulk_sms?readPreference=secondaryPreferred&w=majority';	
	DATABASE_NAME            = 'bulk_sms';
	MODE                     = 'localhost';
	BASE_URL                 = 'http://localhost:5000';
	UPLOAD_BASE_URL          = 'http://localhost:5001';
  DB_OPTIONS               = {
      useNewUrlParser      : true,
      socketTimeoutMS      : 600000,
      connectTimeoutMS     : 600000,
      autoReconnect        : true,
     /* poolSize           : 30,
      keepAlive            : true,
      autoReconnect        : true,
      reconnectTries       : Number.MAX_VALUE,
      reconnectInterval    : 1000,
      socketTimeoutMS      : 600000,
      connectTimeoutMS     : 600000*/
  };

  SMTP_CREDENTIALS         = {
        service            : 'smtp',
        host               : "smtp.gmail.com",
        port               : 587,
        secure             : false,
        auth               : {
          user             : 'realree.24@gmail.com',
          pass             : 'babluu002'
        }
  };


  REDIS_CREDENTIALS        = {
      redis                : {
        host               : "localhost",
        port               : 6379
      },
      disableSearch        : false

  };

  MAIL_FROM    = 'Kasplo Notification <realree.24@gmail.com>';
  TO_MAIL_IDS  = 'vinodhtamilvanan@adcanopus.com,Lavanyap@adcanopus.com';
  RABBITMQ_URL = 'amqp://localhost';
  //'amqp://firdhjgf:FDOZLPfcHA6pwUX8HBWdsHMIpQ_YHwAZ@gull.rmq.cloudamqp.com/firdhjgf';//'amqp://localhost';

  /* ################ LOCALHOST CREDENTIALS & Configuration - (Stop) ######################### */
  CRON_DIR      = '/../public/upload-cron-files/psftft_data/';
  CRON_CURL_DIR = BASE_URL + '/../public/upload-cron-files/psftft_data/';
}

var cron_path  = {
  'knowastro'     : '/var/www/kasplo.com/pull-data-api/astro_data',
  'other'         : CRON_DIR,
  'cron_curl_dir' : CRON_CURL_DIR
}

console.log('Database Connected from '+MODE);
module.exports = {
    mongoURI         : DB_CONNECTION_STRING_URL,
    database_name    : DATABASE_NAME,
    mode             : MODE,
    db_options       : DB_OPTIONS,
    base_URL         : BASE_URL,
    upload_baseURL   : UPLOAD_BASE_URL,
    smtp_credentials : SMTP_CREDENTIALS,
    mail_from        : MAIL_FROM,
    to_mail_ids      : TO_MAIL_IDS,
    redis_config     : REDIS_CREDENTIALS,
    rabbitmq_uri     : RABBITMQ_URL,
    cron_path        : cron_path
};
