/* constants */
var smpp_err_codes = {
  '-01': {
    'description': 'This status displays when the error code provided by the subscribers terminating operator is not mapped with the existing status.'
  },
  '000': {
    'description': 'The message is on SMSC queue i.e. the message has been inserted into the SMSC database but the status of the message is Yet to be received.'
  },
  '001': {
    'description': 'Successfully delivered.'
  },
  '002': {
    'description': 'The message is permanently failed due to Call Barred, Error in Destination Number, Error in TeleService Provider etc.'
  },
  '004': {
    'description': 'Failed Due to DND Registration'
  },
  '005': {
    'description': 'Blacklisted number. This list of numbers is provided by the Customer and includes numbers of CEO etc. A message will never go to a number in the black-list.'
  },
  '006': {
    'description': 'This error is received when a Opt-In account sends messages to a non-whitelisted number'
  },
  '007': {
    'description': 'This error is shown when a number series in the correct number format is in valid.'
  },
  '008': {
    'description': 'This error is shown when the messages are rejected due to insufficient credits.'
  },
  '009': {
    'description': 'Promotional messages blocked after 9PM'
  },
  '010': {
    'description': 'Invalid Source Address'
  },
  '011': {
    'description': 'Invalid Dest Addr'
  },
  '012': {
    'description': 'Message ID is invalid'
  },
  '013': {
    'description': 'Bind Failed'
  },
  '014': {
    'description': 'Invalid Password'
  },
  '015': {
    'description': 'Invalid System ID'
  },
  '016': {
    'description': 'Reserved'
  },
  '017': {
    'description': 'Cancel SM Failed'
  },
  '019': {
    'description': 'Replace SM Failed'
  },
  '020': {
    'description': 'Message Queue Full'
  },
  '021': {
    'description': 'Invalid Service Type'
  },
  '031': {
    'description': 'The message is rejected because there was no paging response,The IMSI record is marked detached, or the MS is subject to roaming restrictions.'
  },
  '032': {
    'description': 'Message rejected because the MS doesn\'t have enough memory.'
  },
  '033': {
    'description': 'Message rejected due to network failure.'
  },
  '034': {
    'description': 'Message rejected due to network or protocol failure.'
  },
  '035': {
    'description': 'Messagerejectedduetonetworkorprotocolfailure.'
  },
  '036': {
    'description': 'Themessageis rejectedbecauseofcongestionencounteredat thevisitedMSC.'
  },
  '037': {
    'description': 'MessagequeueexceededwhentherearetoomanymessagesforOnenumber.SMSCcandeliveronlyaparticularnumberof messagestoamobilenumber.Iftherearemoremessages,they getthiserrorcodetillthequeueclears.'
  },
  '051': {
    'description': 'Invalid number of destinations'
  },
  '052': {
    'description': 'Invalid Distribution List name'
  },
  '053': {
    'description': 'Reserved thru -0x0000003F'
  },
  '064': {
    'description': 'Destination flag is invalid (submit_multi)'
  },
  '066': {
    'description': 'Invalid submit with replace request; submit_sm with replace_if_present_flag set'
  },
  '067': {
    'description': 'Invalid esm_class field data'
  },
  '068': {
    'description': 'Cannot Submit to Distribution List'
  },
  '069': {
    'description': 'submit_sm or submit_multi failed'
  },
  '072': {
    'description': 'Invalid Source address TON'
  },
  '073': {
    'description': 'Invalid Source address NPI'
  },
  '080': {
    'description': 'Invalid Destination address TON'
  },
  '081': {
    'description': 'Invalid Destination address NPI'
  },
  '083': {
    'description': 'Invalid system_type field'
  },
  '084': {
    'description': 'Invalid replace_if_present flag'
  },
  '085': {
    'description': 'Invalid number of messages'
  },
  '088': {
    'description': 'Throttling error; ESME has exceeded allowed message limits.'
  },
  '089': {
    'description': 'Reserved thru -0x00000060'
  },
  '097': {
    'description': 'Invalid Scheduled Delivery Time'
  },
  '098': {
    'description': 'Invalid message validity period (Expiry time).'
  },
  '099': {
    'description': 'Predefined Message Invalid or Not Found'
  },
  '100': {
    'description': 'Predefined Message Invalid or Not Found'
  },
  '101': {
    'description': 'ESME Receiver Permanent App Error Code'
  },
  '102': {
    'description': 'ESME Receiver Reject Message Error Code'
  },
  '103': {
    'description': 'query_sm request failed'
  },
  '104': {
    'description': 'Reserved thru -0x000000BF'
  },
  '110': {
    'description': 'Themessageis rejectedbecausetherewasnopagingresponse, theIMSIrecordis markeddetached,other MSis subjecttoRoamingrestrictionsatthefirstattempt.' 
  },
  '120': {
    'description': 'MessagerejectedbecausetheMSdoesn\'thaveenoughmemory whenatthefirstattempt.' 
  },
  '130': {
    'description': 'Messagerejectedduetonetworkfailureatthefirstattempt.' 
  },
  '140': {
    'description': 'Messagerejectedduetonetworkorprotocolfailureatthefirst attempt.' 
  },
  '150': {
    'description': 'Messagerejectedduetonetworkorprotocolfailureatthefirst attempt.' 
  },
  '160': {
    'description': 'Themessageis rejectedbecauseofcongestionencounteredat thevisitedMSCatfirstattempt.' 
  },
  '190': {
    'description': 'TCAPProtocolabortreceivedfromoneofthenodeonthe network' 
  },
  '192': {
    'description': 'Error in the optional part of the PDU Body' 
  },
  '193': {
    'description': 'Optional Parameter not allowed' 
  },
  '194': {
    'description': 'Invalid Parameter Length' 
  },
  '195': {
    'description': 'Expected Optional Parameter missing.' 
  },
  '196': {
    'description': 'Invalid Optional Parameter Value' 
  },
  '200': {
    'description': 'RemoteHLRor VLRrouteisnotdefinedonadjacentnode' 
  },
  '210': {
    'description': 'TCAPProtocolabortreceivedfromoneofthenodeonthe network' 
  },
  '245': {
    'description': 'Delivery Failure, used for data_sm_resp' 
  },
  '255': {
    'description': 'Unknown Error' 
  },
  '507': {
    'description': 'Promotionalmessagesblocked'
  },
  '508': {
    'description': 'Message sent without whitelisted sender ID.' 
  },
  '517': {
    'description': 'Same message submitted twice within configured time frame as per account configuration' 
  },
  '519': {
    'description': 'Messages are blocked for MS to multiple NDNC violations' 
  },
  '522': {
    'description': 'Mobile number is invalid or not correct' 
  },
  '537': {
    'description': 'International service is disabled for the account' 
  },
  
  '999': {
    'description': 'Message got failed, and final error is not updated by error is not mappped.' 
  },
  '0': {
    'description': 'delivered'
  },
  '1': {
    'description': 'service-not-allowed'
  },
  '8': {
    'description': 'dlr-deleted-manually'
  },
  '8': {
    'description': 'dlr-deleted-manually'
  },
  '106': {
    'description': 'impossible-to-route'
  },
  '108': {
    'description': 'loop-detected'
  },
  '111': {
    'description': 'temporary-app-connection-closed'
  },
  '121': {
    'description': 'blocked-by-adaptive'
  },
  '187': {
    'description': 'spam-detected-statistical'
  },
  '188': {
    'description': 'spam-detected-keyword'
  },
  '189': {
    'description': 'spam-detected'
  },
  '201': {
    'description': 'temporary-rout-error-retries-exceeded'
  },
  '211': {
    'description': 'temporary-app-error-app-busy'
  },
  '220': {
    'description': 'temporary-store-error'
  },
  '231': {
    'description': 'discarded-concatenation-timeout'
  },
  '242': {
    'description': 'discarded-cannot-handle-udh-now'
  },
  '301': {
    'description': 'malformed-invalid-encoding'
  },
  '302': {
    'description': 'malformed-invalid-from-number'
  },
  '303': {
    'description': 'malformed-invalid-to-number'
  },
  '304': {
    'description': 'malformed-invalid-udh'
  },
  '305': {
    'description': 'malformed-invalid-udh'
  },
  '306': {
    'description': 'malformed-invalid-dlr-flag'
  },
  '350': {
    'description': 'malformed-for-destination'
  },
  '351': {
    'description': 'malformed-for-destination'
  },
  '352': {
    'description': 'malformed-too-large-for-destination'
  },
  '401': {
    'description': 'rejected-loop-detected'
  },
  '402': {
    'description': 'rejected-fail-with-code'
  },
  '403': {
    'description': 'rejected-forbidden-from-number'
  },
  '405': {
    'description': 'rejected-unallocated-from-number'
  },
  '406': {
    'description': 'rejected-unallocated-to-number'
  },
  '431': {
    'description': 'rejected-forbidden-shortcode'
  },
  '432': {
    'description': 'rejected-forbidden-country'
  },
  '433': {
    'description': 'rejected-forbidden-tollfree'
  },
  '434': {
    'description': 'rejected-forbidden-tollfree-for-recipient'
  },
   '434': {
    'description': 'rejected-forbidden-tollfree-for-recipient'
  },
   '434': {
    'description': 'rejected-forbidden-tollfree-for-recipient'
  },
   '434': {
    'description': 'rejected-forbidden-tollfree-for-recipient'
  },
   '434': {
    'description': 'rejected-forbidden-tollfree-for-recipient'
  },
   '451': {
    'description': 'rejected-wrong-user-id'
  },
   '452': {
    'description': 'rejected-wrong-application-id'
  },
   '470': {
    'description': 'rejected-spam-detected'
  },
   '481': {
    'description': 'rejected-from-number-in-blacklist'
  },
   '482': {
    'description': 'rejected-to-number-in-blacklist'
  },
   '491': {
    'description': 'rejected-unexpected-dlr'
  },
  '492': {
    'description': 'reject-emergency'
  },
  
  '493': { 
    'description' : 'rejected-unauthorized' 
  },
  '500': { 
    'description' : 'message-send-failed' 
  },
  '501': { 
    'description' : 'message-send-failed' 
  },
  '600': { 
    'description' : 'destination-carrier-queue-full'
     },
  '610': { 
    'description' : 'submit_ sm-or-submit_ multi-failed'
     },
  '620': { 
    'description' : 'destination-app-error'
     },
  '630': {
   'description' : 'message-not-acknowle' 
    },
  '650': {
   'description' : 'destination-failed' 
 },
  '700': { 
    'description' : 'invalid-service-type'
     },
  '720': {
   'description' : 'invalid-destination-address'
    },
  '740': { 
    'description' : 'invalid-source-address-address'
     },
  '750': { 
    'description' : 'destination-rejected-message' 
  },
  '751': { 
    'description' : 'destination-rejected-message-too-long' 
  },
  '770': { 
    'description' : 'destination-rejected-due-to-spam-detection'
     },
  '775': { 
    'description' : 'destination-rejected-due-to-user-opt-out'
     },
  '901': { 
    'description' : 'delivery-receipt-cleaned-manually' 
  },
  '902': { 
    'description' : 'delivery-receipt-expired'
     },
  '999': { 
    'description' : 'unknown-error' 
  },
}

var smpp_name = {
  '1': 'Karix', //live
  '2': 'Airtel', //live
  '3': 'Text-91', //live
  '4': 'YourSMSApp', //local+live
  '5': 'Route Mobile' //local+live
}

module.exports = {
    PER_PAGE_LIST_COUNT: parseInt(15),
    PER_BATCH_COUNT: parseInt(15),
    TRACKING_URL: 'txtby.me/tc/',
    SMPP_ERR_CODE: smpp_err_codes,
    SMPP_NAME: smpp_name
};
