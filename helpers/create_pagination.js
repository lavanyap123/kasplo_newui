var page              = {};
page.createPagination = function(pages, current, perPage, data_count, offset, is_display_gif = true) {

  var pagination_content = '';

  var show_entry_num     = current * perPage;
  if (data_count > 0) {
    pagination_content   += '<div class="countSec" id="pagination">';
    //console.log(offset); 
    var current_page     = parseInt(offset) + parseInt(1); //console.log(current_page);
    pagination_content   += '<p> Showing ' + current_page  + ' - <b>' + perPage * current + ' of ' +data_count + ' items </b> </p>';
    pagination_content   += '</div>';
  } 
  if (pages > 0) {
    pagination_content   += '<div class="paginationSec">';
    pagination_content   += '<div class="btn-group" role="group" aria-label="Basic example">';

    if (current == 1) {
      pagination_content += '<button type="button" class="btn btn-secondary" disabled>First</a></button>';
    } else {
      pagination_content += '<button type="button" class="btn btn-secondary page-link" data-value="1">First</button>';
    }

    var i = (Number(current) > 5 ? Number(current) - 4 : 1)
    if (i !== 1) {
      pagination_content += '<button type="button" class="btn btn-secondary page-link" disabled>...</button>';
    }

    for (; i <= (Number(current) + 4) && i <= pages; i++) {

      if (i == current) {
        pagination_content += '<button type="button" class="btn btn-primary page-item page-link" data-value="' + i + '">' + i + '</button>';
      } else {
        pagination_content += '<button type="button" class="btn btn-secondary page-item page-link" data-value="' + i + '"> ' + i + '</button>';
      }

      if (i == Number(current) + 4 && i < pages) {
        pagination_content += '<button type="button" class="btn btn-secondary page-item page-link" disabled>...</button>';
      }
    } //end for
    if (current == pages) {
      show_entry_num = data_count;
      pagination_content += '<button type="button" class="btn btn-secondary page-item page-link" disabled>Last</button>';
    } else {
      pagination_content += '<button type="button" class="btn btn-secondary page-item page-link" data-value="' + pages + '"> Last</button>';
    }
      pagination_content += '</div></div>';

  } //endif



  
  return pagination_content;
}






module.exports = page;