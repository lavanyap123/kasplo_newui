
var smpp          = require('smpp');

send_sms_smpp_old = function (ip_addr, port, sys_id, pwd, src_addr, dest_addr, sms_text) { 
  var session     = new smpp.Session({host: ip_addr, port: port});

  // We will track connection state for re-connecting
  var didConnect  = false; 

  session.on('connect', function() {
    didConnect    = true;

    session.bind_transceiver({
      system_id: sys_id,
      password: pwd,
    }, function(pdu) {
      if (pdu.command_status == 0) {
        //sendSMS(src_addr, dest_addr, sms_text, session); 
        session.submit_sm({ 
        channel: "sms",
        source_addr:     src_addr,
        destination_addr: dest_addr,
        short_message:    'testing smpp with in kasplo'
        }, function(pdu) {
          
          if (pdu.command_status == 0) {
           
          }
        });
      }
    });
  })

  session.on('pdu', function(pdu) {
    // incoming SMS from SMSC
    if (pdu.command == 'deliver_sm') {
      
      // no '+' here
      var fromNumber = pdu.source_addr.toString();
      var toNumber   = pdu.destination_addr.toString();
      
      var text       = '';
      if (pdu.short_message && pdu.short_message.message) {
        text         = pdu.short_message.message;
      }

      // Reply to SMSC that we received and processed the SMS
      session.deliver_sm_resp({ sequence_number: pdu.sequence_number });
    }
  })

  session.on('close', function(){
  console.log('smpp disconnected')
    if (didConnect) {
      connectSMPP(session);
    }
  })

  session.on('enquire_link', function(pdu) {
    console.log('enquire_link')
    session.send(pdu.response());
  });

  session.on('unbind', function(pdu) {
    console.log('unbind')
    session.send(pdu.response());
    session.close();
  });

  session.on('error', function(error){
    console.log('smpp error', error)
    didConnect = false;
  })

} //end send_sms_smpp


function lookupPDUStatusKey(pduCommandStatus) {
  for (var k in smpp.errors) {
    if (smpp.errors[k] == pduCommandStatus) {
      return k
    }
  }
}

function sendSMS(from, to, text, session) {

  // in this example, from & to are integers
  // We need to convert them to String
  // and add `+` before
  var result = false;
  //from = '+' + from.toString();
  //to   = '+' + to.toString();
  
  session.submit_sm({
      source_addr:      'PROVDT',
      destination_addr: '9121041187',
      short_message:    'testing in export'
  }, function(pdu) {
   
      if (pdu.command_status == 0) {
         
        result = pdu.message_id;
      }
  });

  console.log(result);
  return result;
}


function connectSMPP(session) {
  console.log('smpp reconnecting');
  session.connect();
}



