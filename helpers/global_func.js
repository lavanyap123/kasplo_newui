var moment        = require('moment');
/*  returns array data for filters section*/
getFilterSearchInput = function (filter_search, fil_search_input_data, is_regex = false) {
  var result         = ''; 
  if (Array.isArray(fil_search_input_data)) {
    result           = fil_search_input_data;
    if (is_regex == true) {
      var regex_data = [];
      for (var i = 0, len = fil_search_input_data.length; i < len; i++) {
        var value    = fil_search_input_data[i].replace(/\//ig,"");
        regex_data.push(new RegExp(value , 'i'));
      }
      result         = regex_data;
    }
  } else {
    result           = [fil_search_input_data];
    if (is_regex == true) {
      result         = [new RegExp(fil_search_input_data.replace(/\//ig,"") , 'i')];
    }
  }
 
  return result;
} //end filter 

/*  Sender or Nonsender filters section*/
getFilterArrayData = function (filter_id, optinal_parameters = '', is_include_zero = false) {
  var result       = [];                 
  if (filter_id && !is_include_zero) {

    if (Array.isArray(filter_id)) {

      for (var i = 0, len = filter_id.length; i < len; i++) {
        var value   = filter_id[i].replace(/\//ig,"");
        result.push(new RegExp('^'+value + optinal_parameters));
      }
    } else {
      var value   = filter_category_id.replace(/\//ig,"");
      result.push(new RegExp('^'+value + optinal_parameters));
    } 

  } 

  return result;
} //end filter 

/* clicker filters section*/
getFilterClickerArrayData = function (filter_id, optinal_parameters = '') {
  
  var result              = '';                 
  
  if (filter_id) {

    if (Array.isArray(filter_id)) {

      // Array ID's converting string to integer
      var filter_category_id = filter_id.map(function(i_category_id) {
        return parseInt(i_category_id, 10);
      });        

      var result            = filter_category_id;                 
       
    } else {
      var result            = [parseInt(filter_id)];
    } 

  } 

  return result;
} //end filter
// Return array of string values, or NULL if CSV string not well formed.
getCSVData = function (text) {
    var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
    var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
    // Return NULL if input string is not well formed CSV string.
    if (!re_valid.test(text)) return null;
    var a        = [];                     // Initialize array to receive values.
    text.replace(re_value, // "Walk" the string using replace with callback.
        function(m0, m1, m2, m3) {
            // Remove backslash from \' in single quoted values.
            if      (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
            // Remove backslash from \" in double quoted values.
            else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
            else if (m3 !== undefined) a.push(m3);
            return ''; // Return empty string.
        });
    // Handle special case of empty last value.
    if (/,\s*$/.test(text)) a.push('');
    return a;
};

//return true if data is empty/ undefined
isInValid = function (data) {
  return data == '' || typeof data == 'undefined' || isNaN(data);
}

//return true if data is not empty/ undefined
isValid = function (data) {
  return data !== '' && typeof data !== 'undefined';
}

//return true if data is not empty/ undefined and Invalid date
isValidDate = function (data) {
  return data !== '' && typeof data !== 'undefined' &&  !isNaN(data);
 }//isValidDate

getSMPPLabelName =  function (smpp_id) {

  var smpp_label = '<span class="badge badge-warning">'+smpp_id+'</span>';
  if (smpp_id == '1') {
    smpp_label   = '<span class="badge badge-success">Karix</span>';
  } else if (smpp_id == '2') {
    smpp_label   = '<span class="badge badge-danger">Airtel</span>';
  } else if (smpp_id == '3') {
    smpp_label   = '<span class="badge badge-primary">Text-91</span>';
  } else if (smpp_id == '4') {
    smpp_label   = '<span class="badge badge-info">YourSMSApp</span>';
  } else if (smpp_id == '5') {
    smpp_label   = '<span class="badge badge-dark">Route Mobile</span>';
  } else if (smpp_id == 'Karix') {
    smpp_label   = '<span class="badge badge-success">'+smpp_id+'</span>';
  } else if (smpp_id == 'Airtel') {
    smpp_label   = '<span class="badge badge-danger">'+smpp_id+'</span>';
  } else if (smpp_id == 'Text-91') {
    smpp_label   = '<span class="badge badge-primary">'+smpp_id+'</span>';
  } else if (smpp_id == 'YourSMSApp') {
    smpp_label   = '<span class="badge badge-info">'+smpp_id+'</span>';
  } else if (smpp_id == 'Route Mobile') {
    smpp_label   = '<span class="badge badge-dark">'+smpp_id+'</span>';
  }
  return smpp_label;
}

validateMobileNum = function(mobile_no) {
  //var mbl_regex = /^[0][1-9]\d{9}$|^[5-9]\d{9}$/g;
  var mbl_regex   = /^([0]|\+91)?[56789]\d{9}$/g;
  return mbl_regex.test(mobile_no);
}


// /* converts data inside array to array of string*/
isValueNull     = function (data) {
  console.log(data)
  var result  = data;                
  if (data == '' || typeof data == 'undefined' || isNaN(data) || data == null) {
    result = 0;
  }

  return result;
} //end 



//getting dates based on selection
//adding one to all the end_dates for getting in less than value
PastDaysList = function (num_of_days, type = 'array', given_frmt = 'DD MMM', created_dt_frmt = 'YYYY-MM-DD') {
  //console.log(num_of_days);

  var dates            = [];
  var dates_obj        = {};
  var created_at_dates = {};    
  switch(num_of_days) {
    case 'today':
      // code block
      var date = moment().format(given_frmt);
      dates.push(date);
      dates_obj[date] = 0;
      created_at_dates['start_date'] = moment().format(created_dt_frmt);
      created_at_dates['end_date']   = moment().add(1, 'day').format(created_dt_frmt);
      break;
    case 'week':
      for (let i = 0; i < 7; i++) {
        var date = moment().subtract(1, 'weeks').startOf('week').add(i, 'day').format(given_frmt);
        dates.push(date);
        dates_obj[date] = 0;
      }
      created_at_dates['start_date'] = moment().subtract(1, 'weeks').startOf('week').format(created_dt_frmt);
      created_at_dates['end_date']   = moment().subtract(1, 'weeks').endOf('week').add(1, 'day').format(created_dt_frmt);
      break;
    case 'last_month':
      num_of_days  = moment().subtract(1, 'months').startOf('month').daysInMonth();
      for (let i = 0; i < num_of_days; i++) {
        var date = moment().subtract(1, 'months').startOf('month').add(i, 'day').format(given_frmt);
        dates.push(date);
        dates_obj[date] = 0;
      }
      created_at_dates['start_date'] = moment().subtract(1, 'months').startOf('month').format(created_dt_frmt);
      created_at_dates['end_date']   = moment().subtract(1, 'months').endOf('month').add(1, 'day').format(created_dt_frmt);
      break;
    case 'this_month':
      num_of_days  = moment().daysInMonth(); 
      for (let i = 0; i < num_of_days; i++) {
        var date = moment().startOf('month').add(i, 'day').format(given_frmt);
        dates.push(date);
        dates_obj[date] = 0;
      }
      created_at_dates['start_date'] = moment().startOf('month').format(created_dt_frmt);
      created_at_dates['end_date']   = moment().endOf('month').add(1, 'day').format(created_dt_frmt);

      break;
    default:
      // code block
      for (let i = num_of_days; i > 0; i--) {
        var date = moment().subtract(i, 'day').format(given_frmt);
        dates.push(date);
        dates_obj[date] = 0;
      }
      created_at_dates['start_date'] = moment().subtract(num_of_days, 'day').format(created_dt_frmt);
      created_at_dates['end_date']   = moment().format(created_dt_frmt);
  }

  var result = {};
  result['dates_obj'] = dates_obj;

  //var result = dates_obj;
  if (type == 'array') {
    result = {};
    result['dates']   = dates; 
    result['created_dates'] = created_at_dates;
    dates_obj = {};
  }
  
  return result;
}


//getting current date with given format (default date format = 'YYYY-MM-DD')
getDateUserFormat   = function (given_date, default_frmt = 'YYYY-MM-DD', given_dt_fmt ='YYYY-MM-DD',) {
  return moment(given_date, given_dt_fmt).format(default_frmt); 
};