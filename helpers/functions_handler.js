var functions_handler = {};

// description: To get current indian time
functions_handler.getCurrentIndianDateTime = function(time = '') {
  var indian_time;
  indian_time = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                   });
  if (time != '') {
    var indian_time = new Date(time);
  }
  
  return indian_time;
};

// description: To get current month
functions_handler.getCurrentMonth = function(time = '') {
  var current_date_time = this.getCurrentIndianDateTime(time);
  var d     = new Date(current_date_time),
      month = '' + (d.getMonth() + 1);
  if (month.length < 2) month = '0' + month;
  return month;
};

// description: To get current year
functions_handler.getCurrentYear = function(time = '') {
  var current_date_time  = this.getCurrentIndianDateTime(time);
  var d                  = new Date(current_date_time),
      year               = d.getFullYear();
  
  return year;
};

// description: To get current day
functions_handler.getCurrentDay = function(time = '') {
  var current_date_time = this.getCurrentIndianDateTime(time);
  var d                 = new Date(current_date_time),
      day               = '' + d.getDate();

  if (day.length < 2) day = '0' + day;
  return day;
};


// description: To get current time (format: HH_MM_AM)
functions_handler.getCurrentTime = function() {
  var current_date_time = this.getCurrentIndianDateTime();
  var d                 = new Date(current_date_time);
  var hours             = d.getHours();
  var minutes           = d.getMinutes();
  var ampm              = hours >= 12 ? 'pm' : 'am';
  hours                 = hours % 12;
  hours                 = hours ? hours : 12; // the hour '0' should be '12'
  minutes               = minutes < 10 ? '0' + minutes : minutes;
  var strTime           = hours + '_' + minutes + '_' + ampm;
  return strTime;
};

//To get time with seconds (format: HH:MM:SS)
functions_handler.getCurrentTimeWithSec = function(time = '', am_pm = '') {
  var current_date_time = this.getCurrentIndianDateTime(time);
  var d                 = new Date(current_date_time);
  var hours             = d.getHours();
  var minutes           = d.getMinutes();
  var seconds           = d.getSeconds();
  var ampm              = hours >= 12 ? 'pm' : 'am';
  hours                 = hours % 12;
  hours                 = hours ? hours : 12; // the hour '0' should be '12'
  minutes               = minutes < 10 ? '0' + minutes : minutes;
  var strTime           = hours + ':' + minutes + ':' + seconds;
  if (am_pm !='') {
    strTime += ' ' + ampm;
  }
  return strTime;
};

// To get time with am pm (format: HH:MM am/pm)
functions_handler.getCurrentTimeAmPm = function(time = '') {
  var current_date_time = this.getCurrentIndianDateTime(time);
  var d                 = new Date(current_date_time);
  var hours             = d.getHours();
  var minutes           = d.getMinutes();
  var ampm              = hours >= 12 ? 'pm' : 'am';
  hours                 = hours % 12;
  hours                 = hours ? hours : 12; // the hour '0' should be '12'
  minutes               = minutes < 10 ? '0' + minutes : minutes;
  var strTime           = hours + ':' + minutes + ' ' + ampm;
  return strTime;
};

// description: To get current Date time (format: DD_MM_YYYY_HH_MM_AM)
functions_handler.getCurrentDateTime = function() {
  var date_time         = this.getCurrentDay() + '_' + this.getCurrentMonth() + '_' + this.getCurrentYear() + '_' + this.getCurrentTime();
  return date_time;
};

// description: To get current Date time (format:DD-MM-YYY HH:MM:SS am/pm)
functions_handler.getCurrentDateTimeForReport = function(time = '', ampm = '', pattern='_', time_pattern = ':') {
  //console.log(time)
  var date_time        = this.getCurrentDay(time) + pattern + this.getCurrentMonth(time) + pattern + this.getCurrentYear(time) + ' ' + this.getCurrentTimeWithSec(time, true);
  return date_time;
};

// description: To get current Date time with am/pm (format:DD-MM-YYY HH:MM am)
functions_handler.getCurrentDateTimeampm = function(time = '') {
  var date_time        = this.getCurrentDay(time) + '-' + this.getCurrentMonth(time) + '-' + this.getCurrentYear(time) + ' ' + this.getCurrentTimeAmPm(time);
  return date_time;
};

//remove double quotes from start and end of a string
functions_handler.stripEndQuotes= function (str) {
  var str_length       = str.length;
  if (str.charAt(0) == '"') str = str.substring(1,str_length--);
  if (str.charAt(--str_length) == '"') str = str.substring(0,str_length);
  return str;
}

//To check an array is empty or not
functions_handler.checkArrayEmpty = function (given_arr_data) {
  return typeof given_arr_data !== 'undefined' && given_arr_data.length > 0;
}

//To check an data given empty or not
functions_handler.checkEmptyVal = function (given_data) {
  if (typeof given_data === 'undefined') {
    return '';
  } else {
    return given_data;
  }
}

functions_handler.checkAllExists = function (data) {

  var result = true;
  if (data.toString() == 'all') {
    result = false;
  }

  return result;
}


functions_handler.getAllSelectedIDs = function (data) {

  if (data.toString() == 'all') {
      console.log('in all')
      var camp_values = [];
      
  }

  return camp_values;
}

/**
 * Function to sort alphabetically an array of objects by some specific key.
 * 
 * @param {String} property Key of the object to sort.
 */
functions_handler.dynamicSort = function (property) {
    var sortOrder = 1;

    if(property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }

    return function (a,b) {
      if(sortOrder == -1){
        return b[property].localeCompare(a[property]);
      }else{
        return a[property].localeCompare(b[property]);
      }        
    }
}

functions_handler.getDateArray = function(start, end) {

  var dates_arr = new Array(),
    dt          = new Date(start);
  var date_obj  = {};
  while (dt <= end) {
    var new_date = new Date(dt);
    dt.setDate(dt.getDate() + 1);
    new_date     = this.getCurrentDay(new_date) + '-' + this.getCurrentMonth(new_date) + '-' + this.getCurrentYear(new_date)
    dates_arr.push(new_date);
    date_obj[new_date] = 0;
  }
  return date_obj;
}

functions_handler.getTreeData = function(array_data) {
  var main_menu_len  = array_data.length;
  var final_arr = [];

  for (i = 0; i < main_menu_len; i++) {
    if (array_data[i].sub_menus) {
      var main_menu_name = array_data[i].menu_name;
      for (j = 1;j <= Object.keys(array_data[i].sub_menus).length; j++) {

        var key_name  =  Object.keys(array_data[i].sub_menus)[j-1];
        var submenus = array_data[i].sub_menus[key_name];  
        for (k = 0; k < submenus.length; k++) {
          submenus[k].menu_type_id = key_name;
          submenus[k].menu_id      = array_data[i]._id;
          submenus[k].main_menu_name = main_menu_name;
          final_arr.push({'sub_menus' : submenus[k]});
        }
      }
    }
  }

  const tree_list_dt = final_arr.reduce((r,{sub_menus}) => {
  
    r[sub_menus.sub_menu_id] = r[sub_menus.sub_menu_id] || [];
    r[sub_menus.sub_menu_id].push(sub_menus)
    return r;
  }, {});
  return tree_list_dt;
} //getTreeData

functions_handler.comparer = function(otherArray) {
  return function(current){
    return otherArray.filter(function(other){
      return other == current
    }).length == 0;
  }
}


/* Getting different array  value from two arrays */
functions_handler.arrayDiff = function(current_arr, fil_arr) {
  return fil_arr.filter(function(i) {return current_arr.indexOf(i) < 0;});
}; //arrayDiff

/* getting nested array list by merging the objects that have same menu_name and sub_menu */
functions_handler.getNestedMenuList = function(given_array) {
  console.log(JSON.stringify(given_array))
  const tree_list = given_array.reduce( (r,item) => {
    Object.keys(item).forEach(function(key){
    if(!r[key]) r[key] = {};
    
    var keys   = Object.keys(item[key]);
    var values = Object.values(item[key]); 
    if (values.includes('1')) {
        values = values.toString();
    if(!r[key][keys]) r[key][keys] = {};
      r[key][keys] = values;
    } else
    if(!r[key][keys]) r[key][keys] = [].concat(values);
    else {
      if(Array.isArray(r[key][keys]))
        r[key][keys].push(...values)
      else
        r[key][keys].push(values);
    }
  });
    return r;
  }, {});

  return tree_list;
} //getNestedMenuList

/* converting array to object */
functions_handler.convertArrayToObj = function (given_array) {
  return given_array.reduce((key, value) => Object.assign(key, value), {})
}//convertArrayToObj

/* Getting different object from given two array of objects  */
functions_handler.getDiffObject = function (arrayobj) {
  return function(current){
    return arrayobj.filter(function(other){
      return other.menu_type_id == current.menu_type_id && other.menu_id == current.menu_id
    }).length == 0;
  }
} //getDiffObject


/******** Export ***********/
module.exports = functions_handler;
