const express             = require('express');
const router              = express.Router();
const bcrypt              = require('bcryptjs');
const passport            = require('passport');
// Load User model
const User                = require('../models/user');
var MongoClient           = require('mongodb').MongoClient;
// DB Config
const db                  = require('../config/keys').mongoURI;
const db_options          = require('../config/keys').db_options;
const database_name       = require('../config/keys').database_name;

// Login Page
router.get('/login', (req, res) => {
  //console.log('User Login Details');
  //console.log(req.user);	
  if (typeof req.user !== "undefined") {
    res.redirect('/dashboard')
  } else {
    res.render('login')
  }
});

/**
 * @request     : {POST} /login
 * @description : Authenticating user with passport in middleware and storing login logs 
 * @return      : redirects to dashboard page on success
*/

router.post('/login', passport.authenticate('local', { failureRedirect: '/users/login', failureFlash: true, passReqToCallback : true }), (req, res) => {
  if (req.user) {
    MongoClient.connect(db, db_options).then(client => {
      const db      = client.db(database_name);
      var login_user_details = req.user;  
      (async () => {

        await db.collection('users').update({"email" : login_user_details.email}, {$set: { "last_login" : new Date().toLocaleString('en-US', {
              timeZone: 'Asia/Calcutta'
          })
        }});
        var login_activity_data = {
          'type'        : 'Login',
          'Location'    : 'Users - Login',
          'description' : 'Hi ' + login_user_details.name + ',You have successfully logged in.',
          'user_name'   : login_user_details.name,
          'created_by'  : req.user._id,
          'is_read'     : 0,
          'created_at'  : new Date().toLocaleString('en-US', {
            timeZone  : 'Asia/Calcutta'
          })
        };

        await db.collection('activity_logs').insert(login_activity_data);
        client.close();
        //res.redirect('/dashboard');
        res.send ({
          'status'   : 200,
          'message'  : 'Login successfull. Redirecting to dashboard',
          'redirect' : '/dashboard'
        })
      })(); //async
    });
  }
}); //login

/**
 * @request     : {GET} /logout
 * @description : Logout the user from session 
 * @return      : redirects to login page on success
*/

router.get('/logout', (req, res) => {

  MongoClient.connect(db, db_options).then(client => {
    const db               = client.db(database_name);

    var login_user_details = req.user;

    if (login_user_details) {
      var username         = login_user_details.name;
      var message          = 'Hi ' + login_user_details.name + ',You have successfully logged out.';
      var login_activity_data = {
      'type'        : 'Logout',
      'Location'    : 'Users - Logout',
      'description' : message,
      'total_contacts' : '',
      'duplicate_contacts' : '',
      'filter_params' : '',
      'user_name'     : username,
      'created_by'    : req.user._id,
      'is_read'       : 0,
      'created_at'    : new Date().toLocaleString('en-US', {
         timeZone    : 'Asia/Calcutta'
      })
    };

    db.collection('activity_logs').insert(login_activity_data)
      .catch((error) => {
          console.log('Acitivity Log is not inserted');
      });
    } else {
      var username = '';
      var message  = 'You have successfully logged out.';
    }

    req.logout();
    req.flash('success_msg', 'You are logged out');
    client.close();
    res.redirect('/users/login');
  });
}); //logout

module.exports = router;