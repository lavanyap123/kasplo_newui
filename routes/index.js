const express           = require('express');
const router            = express.Router();
const { ensureAuthenticated , roleAuthenticated}  = require('../config/auth');
var json2csv            = require('json2csv'); //not working
var csv_export          = require('csv-export');
const stringify         = require('csv-stringify');
var autoIncrement       = require("mongodb-autoincrement");


var mongoose            = require('mongoose');
var MongoClient         = require('mongodb').MongoClient;
// Dashboard
var await               = require("await");
var async               = require("async");
var Hashids             = require('hashids');

var functions_handler   = require('./../helpers/functions_handler'); 
var activityController  = require('./../controllers/activity_ctrl');
// DB Config
const db                = require('../config/keys').mongoURI;
const db_options        = require('../config/keys').db_options;
const database_name     = require('../config/keys').database_name;
const upload_baseURL    = require('../config/keys').upload_baseURL;

//helpers
var ObjectId            = require('mongodb').ObjectId;


/**
 * @request     : {GET} /
 * @description : Checking whether user logged in or not
 * @return      : redirects to dashboard/login page
*/

router.get('/', (req, res) => {   
  if (typeof req.user !== "undefined") {
    res.redirect('/dashboard');
  } else {
    res.render('login');
  }
});

router.get('/help', ensureAuthenticated, (req, res, next) => {

    res.render('help/index', {
        user: req.user
    });

});

router.get('/release-notes', ensureAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

    res.render('help/release_notes', {
        activity_logs: req.activity_logs,
        activity_logs_count: req.activity_logs_count,
        user: req.user
    });

});


/**
 * @request     : {GET} /dashboard
 * @description : Fetching all the dashboard data 
 * @return      : dashboard page with required data
*/

router.get('/dashboard', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db                  = client.db(database_name);
    const category_collection = db.collection('category');
    const campaign_collection = db.collection('campaign');
    const contacts_collection = db.collection('contacts');
    const clicks_collection   = db.collection('clicks');

    (async () => {


      

      

      var user_details = req.user;

      if (user_details.release_read_status == '0') {

      var o_id   = new ObjectId(user_details._id);

      db.collection('users').updateOne(
        {
          '_id' : o_id
        },
        {
          $set: {'release_read_status':1}
        }
        );
      }
      client.close();
      res.render('dashboard', {
        user                   : req.user,
        activity_logs          : req.activity_logs,
        activity_logs_count    : req.activity_logs_count
      });
    })();
  });
}); //dashboard

/**
 * @request     : {AJAX} {POST} /get_topten_camp
 * @description : Fetching top ten campaign data
 * @return      : {object} top ten campaign data
*/

router.post('/get_topten_camp', ensureAuthenticated, (req, res, next) => {
  MongoClient.connect(db, db_options).then(client => {
    (async () => {
      const db                  = client.db(database_name);
      const campaign_collection = db.collection('campaign');
      const campaign_summary    = await campaign_collection.aggregate([
        {
          $match: {
            draft_status: 1
          }
        },
        {
          $project: {
            name: "$name",
            campaign_id: "$_id"
          }
        },
        {
          $lookup: {
            from: 'campaign_content_labels',
            localField: '_id',
            foreignField: 'campaign_id',
            as: 'contentdetails'
          },
        },
        {
          $lookup: {
            from: 'clicks',
            localField: '_id',
            foreignField: 'campaign_id',
            as: 'clicksdetails'
          },
        },
        { $unwind: '$clicksdetails'},
        {
          $group: {
            "_id": {
              total_sender: {
                $sum: "$contentdetails.total_sender"
              },
              name: "$name",
              campaign_id: "$_id"
            },
            total_clicks: {
                $sum: "$clicksdetails.count"
              },
            },
        },
        {
          $sort: {
            "total_clicks": -1
          }
        },
        {
          $limit: 10
        },
      ], { allowDiskUse: true } ).toArray();

      res.send({
        'status' : 200,
        'data'   : campaign_summary
      })

    })();
  });
}); //get_topten_camp

/**
 * @request     : {AJAX} {POST} /get_dashboard_count
 * @description : Fetching all the dashboard counts
 * @return      : {object} dashboard counts
*/

router.post('/get_dashboard_count', ensureAuthenticated, (req, res, next) => {
  MongoClient.connect(db, db_options).then(client => {
    (async () => {
      const db                  = client.db(database_name);
      const category_collection = db.collection('category');
      const campaign_collection = db.collection('campaign');
      const contacts_collection = db.collection('contacts');
      const clicks_collection   = db.collection('clicks');

      const category_count      = await category_collection.count();
      const campaign_count      = await campaign_collection.count({'draft_status':1});
      const contact_count       = await contacts_collection.count();
      const contact_block_count = await contacts_collection.count({'is_active': { $in: [0] }});
      const clicks_count        = await clicks_collection.count();

      var count_data   = {
        'category_count'        : category_count,
        'campaign_count'        : campaign_count,
        'contact_count'         : contact_count,
        'clicks_count'          : clicks_count,
        'contact_block_count'   : contact_block_count,
      };

      res.send({
        'status' : 200,
        'data'   : count_data
      })

    })(); //async
  });
}); //get_topten_camp


/**
 * @request     : {AJAX} {POST} /get_volume_report
 * @description : Fetching the click report based on given date
 * @return      : {object} report
*/

router.post('/get_volume_report', ensureAuthenticated, (req, res, next) => {
  MongoClient.connect(db, db_options).then(client => {
    const db                    = client.db(database_name);
    const category_collection   = db.collection('category');
    const campaign_collection   = db.collection('campaign');
    const contacts_collection   = db.collection('contacts');
    const clicks_collection     = db.collection('clicks');
    var report_val              = req.body.report_val;
    (async () => {
      var date_condition   = {'created_at' : {}};
      
      if (report_val) {
        var get_dates             = PastDaysList(report_val);
        var get_dates_arr         = PastDaysList(report_val, 'object', 'DD-MM-YYYY'); 
        
      } else {
        var get_dates          = PastDaysList(7);
        var get_dates_arr      = PastDaysList(7, 'object', 'DD-MM-YYYY'); 
      }

      if (get_dates.created_dates.start_date) {
          date_condition['created_at']['$gt'] = new Date(get_dates.created_dates.start_date);
        }
        if (get_dates.created_dates.end_date) {
          date_condition['created_at']['$lt'] = new Date(get_dates.created_dates.end_date);
        }

      var last_seven_days           = get_dates.dates;
      var last_seven_days_array     = get_dates_arr.dates_obj;
      
     
      const last_seven_days_result = await db.collection('clicks').aggregate([
        {// Get only records created in the last 30 days
          $match : date_condition
        },

        { // Get the year, month and day from the createdTimeStamp
          $project   : {
            "year"   : {
              $year  : "$created_at"
            },
            "month"  : {
              $dateToString: { format: "%m", date: "$created_at" }, 
              //formatting date to get 2 digits for months
            },
            "day"    : {
              $dateToString: { format: "%d", date: "$created_at" }, 
              //formatting date to get 2 digits for date
            }
          }
        },

        // Group by year, month and day and get the count
        {
          $group : {
            _id  : {
              year  : "$year",
              month : "$month",
              day   : "$day"
            },
            "count"   : {
              $sum    : 1
            }
          }
        }
      ]).toArray();


      var arraySevanDaysLength = last_seven_days_result.length;

      if (arraySevanDaysLength > 0) {
        for (var i in last_seven_days_result) {
          var day        = last_seven_days_result[i]._id.day;
          var month      = last_seven_days_result[i]._id.month;
          var year       = last_seven_days_result[i]._id.year;
          last_seven_days_array[day + '-' + month + '-' + year] = last_seven_days_result[i].count;
        }
      }
      
      var report_data     = {
        'last_seven_days'       : last_seven_days,
        'last_seven_days_count' : Object.values(last_seven_days_array),
      }
      client.close();
      res.send({
        'status'  : 200,
        'data'    : report_data
      })
    })();
  });
}); //get_volume_report

/**
 * @request     : {AJAX} {POST} /get_recent_campaign
 * @description : Fetching the click report based on given date
 * @return      : {object} recent campaign data 
*/

router.post('/get_recent_campaign', ensureAuthenticated, (req, res, next) => {
  MongoClient.connect(db, db_options).then(client => {
    const db                    = client.db(database_name);
    const campaign_collection   = db.collection('campaign');
    (async () => {

      const recent_campaign     = await campaign_collection.find({'draft_status': 1}).sort({'created_at': -1}).limit(4).toArray();
      
      client.close();
      res.send({
        'status'  : 200,
        'data'    : recent_campaign
      })
    })();
  });
}); //get_recent_campaign

/**
 * @request     : {AJAX} {POST} /get_top_ten_category
 * @description : Getting top ten category for graph 
 * @return      : {object} top ten category
*/

router.post('/get_top_ten_category', ensureAuthenticated, (req, res, next) => {
  MongoClient.connect(db, db_options).then(client => {
    const db                    = client.db(database_name);
    (async () => {
      const category        = await db.collection('clicks').aggregate([
        {
          $group  : {
            _id   : "$category_id",
            count : {
                $sum : 1
            }
          },
        },

        {
          $sort: {
            "count": -1
          }
        },

        {
          $limit: 10
        },

      ]).toArray();

      var topten_category_array  = category.length;

      var top_ten_category_names = [];
      var top_ten_category_count = [];


      if (topten_category_array > 0) {

        for (var j in category) {

          var category_id          = category[j]._id;
          var count                = category[j].count;

          const cat_details      = await db.collection('category').find({
            '_id': parseInt(category_id)
          }).toArray();


          if (cat_details.length > 0) {
            var category_name = cat_details[0].name;
            top_ten_category_names.push(category_name);
            top_ten_category_count.push(count);
          }
        }
      }
      
      var top_ten_data = {
        'top_ten_category_names' : top_ten_category_names,
        'top_ten_category_count' : top_ten_category_count,
      }

      client.close();
      res.send({
        'status'  : 200,
        'data'    : top_ten_data
      })
    })();
  });
}); //get_top_ten_category

/**
 * @request     : {AJAX} {POST} /get_top_ten_campaign
 * @description : Getting top ten campaign for graph 
 * @return      : {object} top ten campaign
*/

router.post('/get_top_ten_campaign', ensureAuthenticated, (req, res, next) => {
  MongoClient.connect(db, db_options).then(client => {
    const db                    = client.db(database_name);
    (async () => {
      const campaign = await db.collection('clicks').aggregate([
        {
          $group: {
            _id: "$campaign_id",
            count: {
              $sum: 1
            }
          },
          },

          {
            $sort: {
              "count": -1
            }
          },

          {
            $limit: 10
          },

      ]).toArray();

      var topten_campaign_array  = campaign.length;

      var top_ten_campaign_names = [];
      var top_ten_campaign_count = [];


      if (topten_campaign_array > 0) {

        for (var k in campaign) {

          var campaign_id = campaign[k]._id;
          var count       = campaign[k].count;

          const campaign_details = await db.collection('campaign').find({
            '_id': parseInt(campaign_id)
          }).toArray();

          if (campaign_details.length > 0) {
            top_ten_campaign_names.push(campaign_details[0].name);
            top_ten_campaign_count.push(count);
          }
        }

      }
      
      var top_ten_camp_data = {
        'top_ten_campaign_names' : top_ten_campaign_names,
        'top_ten_campaign_count' : top_ten_campaign_count,
      }

      client.close();
      res.send({
        'status'  : 200,
        'data'    : top_ten_camp_data
      })
    })();
  });
}); //get_top_ten_campaign

function Last7Days() {
  return '6543210'.split('').map(function(n) {
    var d = new Date();
    d.setDate(d.getDate() - n);

    return (function(day, month, year) {
        return [day < 10 ? '0' + day : day, month < 10 ? '0' + month : month, year].join('-');
    })("\`" + d.getDate(), d.getMonth() + 1, d.getFullYear() + "\`");
  }).join(',');
}


function formatDate(date) {
    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = dd
    }
    if (mm < 10) {
        mm = mm
    }
    date = dd + '-' + mm + '-' + yyyy;
    return date
}



function Last7DaysCount() {
    var result = {};
    for (var i = 6; i >= 0; i--) {
        var d = new Date();
        d.setDate(d.getDate() - i);
        result[formatDate(d)] = 0;
    }

    return (result);
}


// campaign - List
router.get('/404', (req, res, next) => {
    res.render('404');
});


router.get('/users', ensureAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res) =>
    res.render('users/index', {
        activity_logs: req.activity_logs,
        activity_logs_count: req.activity_logs_count,
        user: req.user
    })
);

module.exports = router;
