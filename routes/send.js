const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
// Load User model
const User = require('../models/user');
var MongoClient = require('mongodb').MongoClient;
// DB Config
const db = require('../config/keys').mongoURI;
const db_options = require('../config/keys').db_options;
const database_name= require('../config/keys').database_name;

var mongoose = require('mongoose');
var Hashids = require('hashids');

const request = require('request');
var activityController = require('./../controllers/activity_ctrl');
var XLSX = require('xlsx')

const {
    ensureAuthenticated
} = require('../config/auth');

var smpp_handler = require('./../helpers/smpp'); 
var smpp = require('smpp');

const multer = require('multer');

const app = express();
const upload = multer();
// Import - Index
// router.get('/sms', ensureAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

//     MongoClient.connect(db, db_options).then(client => {
//             const db = client.db(database_name);

//             (async () => {

//                 var campaign_list = await db.collection('campaign').find({'draft_status':1}).toArray(); 
//                 res.render('campaign/direct_campaign/index', {
//                     user: req.user,
//                     campaign: campaign_list,
//                     activity_logs: req.activity_logs,
//                     activity_logs_count: req.activity_logs_count,
//                 });
//                 client.close();
//             })();

//         });      
// });


router.post('/testing_area', upload.single('csvFile'), async (req, res) => {
  try {

    const csvFile = req.file.buffer.toString();
    const rows = csvFile.split('\n');

    var params = req.body;
    var channel_name = params.channel;
    var sender_id = params.sender_id;
    var smpp_id = params.smpp_id;

    for (let row of rows) {
      const columns = row.replace(/"/g, '').split(',');
      //console.log(columns);
    }

    res.sendStatus(200);
  } catch (err) {
    console.log(err);
    res.sendStatus(400);
  }
});


router.post('/csv_data', upload.single('csvFile'), async (req, res) => {

    MongoClient.connect(db, db_options).then(client => {
        const db = client.db(database_name);

        (async () => {

          const sms_logs = db.collection('sms_logs');
          const smpp_collection = db.collection('smpp_details');

          var smsDataCsvFile = req.file.buffer.toString();
          var rows = smsDataCsvFile.split('\n');

          var params = req.body;
          var channel_name = params.channel;
          var sender_id = params.sender_id;
          var smpp_id = params.smpp_id;
          var campaign_id = params.campaign_id;
          var content_label_id = params.content_label_id;


          const smpp_info = await smpp_collection.findOne({
              'smpp_id': smpp_id                    
          });


          if(smpp_info.name=='Airtel')
          {

              var message_list = [];

              for (let row of rows) {

                  var columns = getCSVData(row); 
                  
                  if(columns[0]!=''&&columns[0]!='mobile_no'&&columns[1]!='sms_content'&&typeof columns[1] !== 'undefined' ||
                    columns[0]!=''&&columns[0]!='mobile_no'&&columns[1]!='content'&&typeof columns[1] !== 'undefined')
                  {
                    console.log('###Mobile Number###');
                    console.log(columns[0]);

                    console.log('###Content###');
                    console.log(columns[1]);

                    var mobile_no = columns[0];
                    var short_message = columns[1]; 
                    
                    //console.log(message);
                    //console.log(export_message);

                    var airtel_response = {};
                    airtel_response['smpp'] = smpp_info.name;
                    airtel_response['sender_id'] = sender_id;
                    airtel_response['channel'] = channel_name;
                    airtel_response['mobile_no'] = mobile_no;
                    airtel_response['message'] = short_message;
                    airtel_response['campaign_id'] = campaign_id;
                    airtel_response['content_label_id'] = content_label_id;
                    airtel_response['status'] = 'SUCCESS';
                    airtel_response['created_at'] = new Date().toLocaleString('en-US', {
                      timeZone: 'Asia/Calcutta'
                    });

                  sms_logs.insert(airtel_response);

                  if(channel_name=='transactional'){

                    request('http://digimate.airtel.in:15181/BULK_API/SendMessage?loginID=adcan_tppd&password=adcan@123&mobile='+mobile_no+'&text='+short_message+'&senderid='+sender_id+'&route_id=TRANSACTIONAL SMS&Unicode=0&camp_name=adcan_tppd', (error, response, body) => {

                      console.log('Transactional sms_reponse');
                      console.log(body);

                    });

                  }else if(channel_name=='promotional'){

                    request('http://digimate.airtel.in:15181/BULK_API/SendMessage?loginID=adcan_ppd&password=adcan@123&mobile='+mobile_no+'&text='+short_message+'&senderid='+sender_id+'&route_id=PROMOTIONAL SMS&Unicode=0&camp_name=adcan_tppd', (error, response, body) => {

                      console.log('Promotional - sms_reponse');
                      console.log(body);

                    });

                  }

                }

               }

            }else {

                var smpp_details = await smpp_collection.findOne({
                    'smpp_id': smpp_id
                });

                var session = new smpp.Session({host: smpp_details.ip_address, port: smpp_details.host[0]});

                delete smpp.encodings.ASCII;

                var didConnect = false; 



/*console.log('smpp_details');
console.log(smpp_details);*/

console.log('IP');
console.log(smpp_details.ip_address);

console.log('Port');
console.log(smpp_details.host[0]);

console.log('username');
console.log(smpp_details.system_id);

console.log('password');
console.log(smpp_details.password);

console.log('Sender ID');
console.log(sender_id);

                session.on('connect', function(){
                  didConnect = true;
                  session.bind_transceiver({
                      system_id: smpp_details.system_id, //SMPP - Settings Table
                      password: smpp_details.password,  //SMPP - Settings Table
                  }, function(pdu) {

                      if (pdu.command_status == 0) { 

                        var message_list = [];

                        for (let row of rows) {

                            //var columns = row.replace(/"/g, '').split(',');
                            var columns = getCSVData(row); 
                            if(columns[0]!=''&&columns[0]!='mobile_no'&&columns[1]!='sms_content'&&typeof columns[1] !== 'undefined' ||
                              columns[0]!=''&&columns[0]!='mobile_no'&&columns[1]!='content'&&typeof columns[1] !== 'undefined')
                            {
                              console.log('###Mobile Number###');
                              console.log(columns[0]);

                              console.log('###Content###');
                              console.log(columns[1]);


                              var msg_id = getRandom(1);

                              var pdu = new smpp.PDU('submit_sm', { 
                                source_addr: sender_id,
                                destination_addr:columns[0],
                                short_message:columns[1],
                              });  

                              console.log('Mobile No='+columns[0]);
                              console.log('Message='+columns[1]);


                              var pdus = session.send(pdu); 

/*console.log('sender pdus');
console.log(pdus);*/
                              var pdu_response = pdu.response();             

console.log('pdu_response');
console.log(pdu_response);

                              var response = {};
                              response['destination_addr'] = columns[0];
                              response['message'] = columns[1];
                              response['command_status'] = pdu.command_status;
                              response['campaign_id'] = campaign_id;
                              response['content_label_id'] = content_label_id;
                              response['smpp_id'] = smpp_info.name;
                              response['command'] = pdu.command;
                             // response['command_id'] = pdu_response.command_id;
                              response['message_id'] = pdu_response.message_id;
                              response['custom_message_id']='';
                              response['sequence_number'] = pdu.sequence_number;
                              response['source_addr'] =  sender_id;
                              response['message_status'] = 'PENDING'; //default message status
                              response['message_type'] = 'direct_campaign'; //default message status
                              response['submit_sm_data'] = pdu;
                              response['created_at'] = new Date().toLocaleString('en-US', {
                                timeZone: 'Asia/Calcutta'
                              });
                          
                            message_list.push(response);
                          }
                        }

                        saveMessages(message_list,sms_logs); //calling save messgae functions

                      }
                    });            
                    
                    /* First Session - Stop */

                    // After sending Delivery Report
                    session.on('pdu', function(pdu) {


                      if (pdu.command == 'deliver_sm') {
                        //console.log('inside deliver')
                        // no '+' here
                        var fromNumber = pdu.source_addr.toString();
                        var toNumber = pdu.destination_addr.toString();

                        var text = '';
                        var receipted_message_id = '';

                        if (pdu.short_message && pdu.short_message.message) {
                            text = pdu.short_message.message;
                            receipted_message_id = pdu.short_message.message_payload;

                        }

                        var parse_deliver_text_str = text.toString(); 
                        var receipted_id = receipted_message_id; 

                        const querystring = require('querystring');    
                        var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {decodeURIComponent: s =>  s.trim()});

                        var parse_test_json = querystring.parse(receipted_id, ' ', ':', {decodeURIComponent: s =>  s.trim()});
console.log('parse_test_json');
console.log(parse_test_json);           
                        session.deliver_sm_resp({ sequence_number: pdu.sequence_number });

                          var msg_id = parse_deliver_text_json.receipted_message_id;
                          //response['message_id'] = parse_deliver_text_json.id;
                          var delivery = {};
                          delivery['message_id'] = msg_id;
                          //delivery['custom_message_id'] = custom_message_id;
                          delivery['message_status'] = parse_deliver_text_json.stat;
                          delivery['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                          delivery['sub'] = parse_deliver_text_json.sub;
                          delivery['message_err'] = parse_deliver_text_json.err;
                          delivery['message_submit_date'] = parse_deliver_text_json.date[0];
                          delivery['message_delivered_date'] = parse_deliver_text_json.date[1];
                          delivery['short_message'] = parse_deliver_text_str; //it will store string data not buffer data
                          delivery['deliver_sm_data'] = pdu;
                          delivery['updated_at'] = new Date().toLocaleString('en-US', {
                            timeZone: 'Asia/Calcutta'
                          });

                          //Updating the each message ID wise the status
                          updatingMessageDeliveryStatus(delivery,msg_id,parse_deliver_text_json.stat,sms_logs)

                        }

                    });

                  /* Second Session - End */

                  session.on('close', function(){
                    console.log('SMPP CLOSE');
                   /* if(didConnect) {
                      connectSMPP(session);
                    }*/
                  });

                  session.on('enquire_link', function(pdu) {
                    console.log('enquire_link')
                    session.send(pdu.response());
                  });

                  session.on('unbind', function(pdu) {
                    console.log('unbind')
                    session.send(pdu.response());
                    session.close();
                  });

                  session.on('error', function(error){
                    console.log('smpp error', error)
                    didConnect = false;
                  });                  
              });
         
          }
          client.close();
         })();

          res.send({'status':200,'messages':'sms_sent_successfully'})
         
         /****** End sms functionality ********/

     
    });
});



function curlRequest(url) {
  return new Promise((resolve, reject) => {
    request(url, (error, response, body) => {
      if (error) {
        reject(error)
      }
      resolve({'body':body});
    });
  });
}

function getRandom(length) {

  return Math.floor(Math.pow(10, length-1) + Math.random() * 9 * Math.pow(10, length-1));

}

async function saveMessages(response,sms_logs) {

  sms_logs.insertMany(response); // Each SMS Logs stroing to Database
}

async function updatingMessageDeliveryStatus(delivery,msg_id,stat,sms_logs){

    console.log('Update-Code');

    sms_logs.findOneAndUpdate(
      { 
        'message_id': msg_id 
      },
      { 
        $set:{
          'message_status': stat, 
          'delivered_data': delivery 
        }
     
      });

}

// Login Page
router.get('/testdb', (req, res) => {
    MongoClient.connect(db, db_options).then(client => {
          
          const db = client.db(database_name);
          (async () => {

                const clicks_collection = db.collection('clicks');
                var mobile_number_list = [];
                var total_contacts = await clicks_collection.aggregate(
                                  {
                                "$match": { category_id: { '$in': [ 1, 2 ] } }
                            },
                            {
                                $skip: 0

                            },
                            {
                                $limit: 3
                            },
                            {
                              $group: {

                                _id: {
                                    contact_no: '$contact_no',
                                },
                              }
                            },
                            
                             

                            ).forEach(function(data) {
                              console.log('data'); console.log(data); 
                              var mobile_no = data.contact_no;
                              mobile_number_list.push(mobile_no);
                             
                            }); 
                    console.log(mobile_number_list);
                    client.close();
            })();
          });

});



router.get('/update_clicks', (req, res) => {

    MongoClient.connect(db, db_options).then(client => {
          const db = client.db(database_name);
          (async () => {
            const clicks_collection = db.collection('clicks');
            const contacts_collection = db.collection('contacts');
           
            var data = await clicks_collection.aggregate([
               {
                    $group : {
                       '_id' : {
                         'campaign_id':"$campaign_id",
                         'contact_no':"$contact_no",

                       }
                    }
               }
            ]).toArray(); 

            /* updating in bulk */
            var bulk = contacts_collection.initializeUnorderedBulkOp();

            for (i=0; i < data.length;i++) {

              var campaign_id = data[i]._id.campaign_id; 
              var contact_no = data[i]._id.contact_no;

              bulk.find({
                mobile_no: contact_no
              }).update({
                  $addToSet: {
                      clicker_sent_camp_ids: parseInt(campaign_id),
                  }
              }, {
                  upsert: false
              });
            } //for loop end

            bulk.execute(function(err, result) {
              console.log('Bulk Result:');
              console.log(result); 
              console.log(err)
            });

            res.send({ success: 'Records updated successfully' });

            client.close();
           
            })();
          
          });

});

function connectSMPP(session) {
  console.log('smpp reconnecting');
  session.connect();
}


module.exports = router;