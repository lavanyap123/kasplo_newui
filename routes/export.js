const express = require('express');
const router = express.Router();
const {
    ensureAuthenticated, roleAuthenticated
} = require('../config/auth');
var json2csv = require('json2csv'); //not working
var csv_export = require('csv-export');
const stringify = require('csv-stringify');
var autoIncrement = require("mongodb-autoincrement");
var functions_handler = require('./../helpers/functions_handler');
var smpp_handler = require('./../helpers/smpp');
var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
// Dashboard
var await = require("await");
var async = require("async");
var Hashids = require('hashids');
var http = require('http');
var url = require('url');
var nanoid = require('nanoid');
var fs = require('fs');
const path = require('path');
var ISODate = require("isodate");
// DB Config
const db = require('../config/keys').mongoURI;
const db_options = require('../config/keys').db_options;
const database_name= require('../config/keys').database_name;

var smpp = require('smpp');
const request = require('request');
var activityController = require('./../controllers/activity_ctrl');
var constants = require('./../config/constants');
// Export - Index
router.get('/index', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {
    let perPage = 10;
    let page = req.params.page || 1;
    MongoClient.connect(db, db_options).then(client => {
        const db = client.db(database_name);
        const contacts = db.collection('contacts');
        const category = db.collection('category');
        const campaign = db.collection('campaign');
        const campaign_content_labels = db.collection('campaign_content_labels');
        (async() => {
            var contacts_list = [];
            var category_list = [];
            var campaign_list = [];
            var campaign_content_list = [];
            let count;
            const items = await contacts.find().skip((perPage * page) - perPage).limit(perPage).forEach(function(contact_record) {
                contacts_list.push(contact_record);
            });
            const category_tbl = await category.find().forEach(function(category_record) {
                category_list.push(category_record);
            });
            const campaign_tbl = await campaign.find().forEach(function(campaign_record) {
                campaign_list.push(campaign_record);
            });
            const campaign_content_tbl = await campaign_content_labels.find().forEach(function(campaign_content_labels_rec) {
                campaign_content_list.push(campaign_content_labels_rec);
            });
            const contact_total = await contacts.count();
            res.render('export/index', {
                items,
                user: req.user,
                current: page,
                total: count,
                category_list: category_list,
                campaign_list: campaign_list,
                campaign_content_list: campaign_content_list,
                pages: Math.ceil(count / perPage),
                perpage: 10,
                activity_logs: req.activity_logs,
                activity_logs_count: req.activity_logs_count,
            });
            client.close();
        })();
    });
});

router.post('/get_dependent_dropdown', ensureAuthenticated, (req, res, next) => {
    MongoClient.connect(db, db_options).then(client => {
        const db = client.db(database_name);
        const campaign = db.collection('campaign');
        const campaign_content_labels = db.collection('campaign_content_labels');
        (async() => {
            var type = req.body.dropdown_type;
            var campaign_list = [];
            var campaign_content_list = [];
            var campaign_list_content = [];
            var campaign_list_html = '';
            var campaign_content_list_html = '';
            var category_id = req.body.category_id;
            if (type == 'campaign') {
                if (typeof category_id !== "undefined") {
                    var post_category_id = category_id;
                } else {
                    var post_category_id = [];
                }
                //console.log('post_category_id');
                //console.log(post_category_id);
                const campaign_tbl = await campaign.find({
                    'category_id': {
                        '$in': post_category_id
                    },
                    'draft_status': 1
                }).sort({_id:-1}).forEach(function(campaign_record) {
                    campaign_list.push(campaign_record);
                });
                if (campaign_list) {
                    for (var i = 0; i < campaign_list.length; i++) {
                        campaign_list_html += '<option value="' + campaign_list[i]._id + '" >' + campaign_list[i].name + '</option>';
                    }
                    res.send({
                        'msg': 'success',
                        'html': campaign_list_html
                    });
                }
            } else {
                var frm_campaign_id = req.body.campaign_id;
                if (typeof frm_campaign_id !== "undefined") {
                    var post_campaign_id = frm_campaign_id.map(function(campaign_id) {
                        return parseInt(campaign_id, 10);
                    });
                } else {
                    var post_campaign_id = [];
                }
                const campaign_content_tbl = await campaign_content_labels.find({
                    'campaign_id': {
                        '$in': post_campaign_id
                    }
                }).sort({_id:-1}).forEach(function(campaign_content_labels_rec) {
                    campaign_content_list.push(campaign_content_labels_rec);
                });
                //console.log(campaign_content_tbl)
                if (campaign_content_list) {
                    campaign_content_list_html += '<option value="0">All</option>';
                    for (var i = 0; i < campaign_content_list.length; i++) {
                        campaign_content_list_html += '<option value="' + campaign_content_list[i]._id + '" con_camp_id= "' + campaign_content_list[i].campaign_id + '" >' + campaign_content_list[i].label_name + '</option>';
                    }
                    res.send({
                        'msg': 'success',
                        'html': campaign_content_list_html
                    });
                }
            }
            client.close();
        })();
    });
});
router.post('/get_dependent_destination_dropdown', ensureAuthenticated, (req, res, next) => {
    MongoClient.connect(db, db_options).then(client => {
        const db = client.db(database_name);
        const campaign = db.collection('campaign');
        const campaign_content_labels = db.collection('campaign_content_labels');
        (async() => {
            var type = req.body.dropdown_type;
            var campaign_list = [];
            var campaign_content_list = [];
            var campaign_list_content = [];
            var campaign_list_html = '';
            var campaign_content_list_html = '';
            if (type == 'campaign') {
                const campaign_tbl = await campaign.find({
                    'category_id': req.body.campaign_id
                }).forEach(function(campaign_record) {
                    campaign_list.push(campaign_record);
                });
                if (campaign_list) {
                    campaign_list_html += '<option value="" selected>-- Select Campaign --</option>';
                    for (var i = 0; i < campaign_list.length; i++) {
                        campaign_list_html += '<option value="' + campaign_list[i]._id + '">' + campaign_list[i].name + '</option>';
                    }
                    res.send({
                        'msg': 'success',
                        'html': campaign_list_html
                    });
                }
            } else {
                const campaign_content_tbl = await campaign_content_labels.find({
                    'campaign_id': parseInt(req.body.campaign_id)
                }).forEach(function(campaign_content_labels_rec) {
                    campaign_content_list.push(campaign_content_labels_rec);
                });
                if (campaign_content_list) {
                    campaign_content_list_html += '<option value="" selected>---Select Content Label ---</option>';
                    for (var i = 0; i < campaign_content_list.length; i++) {
                        campaign_content_list_html += '<option value="' + campaign_content_list[i]._id + '">' + campaign_content_list[i].label_name + '</option>';
                    }
                    res.send({
                        'msg': 'success',
                        'html': campaign_content_list_html
                    });
                }
            }
            client.close();
        })();
    });
});

/*
method    : /get_all_campaign
description : Getting all the campaigns names, ids list 
return      : All campaign dropdown details 
*/

router.post('/get_all_campaign', ensureAuthenticated, (req, res, next) => {
  MongoClient.connect(db, db_options).then(client => {
    const db                      = client.db(database_name);
    const campaign                = db.collection('campaign');
    const campaign_content_labels = db.collection('campaign_content_labels');
    var status_code               = 400;
    var message                   = 'fail';
    (async() => {
      var campaign_list           = [];
      var campaign_content_list   = [];
      var campaign_list_content   = [];
      var campaign_list_html      = '';
      var campaign_content_list_html = '';
      const campaign_tbl          = await campaign.find({
                                      'draft_status': 1
                                    }).sort({_id:-1}).forEach(function(campaign_record) {
                                      campaign_list.push(campaign_record);
                                    });
      if (campaign_list.length > 0) {
        status_code        = 200;
        message            = 'success';
        campaign_list_html += '<option value="0">All</option>';
        for (var i = 0; i < campaign_list.length; i++) {
          campaign_list_html += '<option value="' + campaign_list[i]._id + '">' + campaign_list[i].name + '</option>';
        }
      }

      res.send({
        'status': status_code,
        'msg'   : message,
        'html'  : campaign_list_html
      });
      client.close();
    })();
  });
}); // get_all_campaign


router.post('/get_report_dependent_dropdown', ensureAuthenticated, (req, res, next) => {
    MongoClient.connect(db, db_options).then(client => {
        const db = client.db(database_name);
        const campaign = db.collection('campaign');
        const campaign_content_labels = db.collection('campaign_content_labels');
        (async() => {
            var type = req.body.dropdown_type;
            var campaign_list = [];
            var campaign_content_list = [];
            var campaign_list_content = [];
            var campaign_list_html = '';
            var campaign_content_list_html = '';
            var category_id = req.body.category_id;
            //console.log('category_id');
            //console.log(category_id);
            //var c_list = {};
            if (type == 'campaign') {
                const campaign_tbl = await campaign.find({
                    'category_id': category_id
                }).forEach(function(campaign_record) {
                    campaign_list.push(campaign_record);
                });
                if (campaign_list) {
                    campaign_list_html += '<option value="" selected>---Select Campaign ---</option>';
                    for (var i = 0; i < campaign_list.length; i++) {
                        campaign_list_html += '<option value="' + campaign_list[i]._id + '">' + campaign_list[i].name + '</option>';
                    }
                    //console.log('campaign_record');
                    //console.log(c_list);
                    res.send({
                        'msg': 'success',
                        'html': campaign_list_html
                    });
                }
            } else {
                var frm_campaign_id = req.body.campaign_id;
                const campaign_content_tbl = await campaign_content_labels.find({
                    'campaign_id': parseInt(frm_campaign_id)
                }).sort({
                  '_id': -1
                }).forEach(function(campaign_content_labels_rec) {
                    campaign_content_list.push(campaign_content_labels_rec);
                });
                if (campaign_content_list) {
                    campaign_content_list_html += '<option value="" selected>---Select Content Label ---</option>';
                    for (var i = 0; i < campaign_content_list.length; i++) {
                        campaign_content_list_html += '<option value="' + campaign_content_list[i]._id + '">' + campaign_content_list[i].label_name + '</option>';
                    }
                    res.send({
                        'msg': 'success',
                        'html': campaign_content_list_html
                    });
                }
            }
            client.close();
        })();
    });
});


module.exports = router;