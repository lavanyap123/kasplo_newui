const express = require('express');
var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
var autoIncrement = require("mongodb-autoincrement");
var nanoid = require('nanoid');
const router = express.Router();
const {
    ensureAuthenticated, roleAuthenticated
} = require('../config/auth');

// Dashboard
var await = require("await");
var async = require("async");
// DB Config
const db = require('../config/keys').mongoURI;
const db_options = require('../config/keys').db_options;
const database_name= require('../config/keys').database_name;

var activityController = require('./../controllers/activity_ctrl');
// campaign - List
router.get('/status', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const collection = db.collection('campaign');

            client.close();
            res.render('link/index', {
                user: req.user,
                activity_logs: req.activity_logs,
                activity_logs_count: req.activity_logs_count,
            });

        });

});

router.post('/test_fire_link', function(req, res, next) {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);

            var post_shortid = req.body.short_id;

            var contact_unique_id = post_shortid.substr(0, post_shortid.indexOf('_'));



            var p_ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

            if (p_ip.substr(0, 7) == "::ffff:") {
                p_ip = p_ip.substr(7)
            }

            (async () => {
                
                const test_clicks_collection = db.collection('test_clicks');

                var contacts_details = await db.collection('contacts').findOne({
                    'unique_id': contact_unique_id
                });


                /*console.log('contacts_details');
                console.log(contacts_details);*/

                if (contacts_details) {

                    var campaign_short_code = post_shortid.substr(post_shortid.indexOf("_") + 1)

                    /*console.log('Short ID:'+campaign_short_code);*/

                    var campaign_label_details = await db.collection('campaign_content_labels').findOne({
                        'url_shortcode': campaign_short_code.toString()
                    });

                    if (campaign_label_details) {

                        var campaign_details = await db.collection('campaign').findOne({
                            '_id': parseInt(campaign_label_details.campaign_id)
                        });

                        /*console.log('campaign_details');
                    console.log(campaign_details);
*/

                        if (campaign_details) {

                            var category_details = await db.collection('category').findOne({
                                '_id': parseInt(campaign_details.category_id)
                            });

                            /* Checking Unique Clicks or Gross Clicks */

                            var mobile_no = contacts_details.mobile_no;

                            var user_clicks = await db.collection('test_clicks').findOne({
                                'contact_no': mobile_no.toString(),
                                'content_label_id': parseInt(campaign_label_details._id),
                                'public_ip': p_ip.toString(),
                            });


                            if (user_clicks) {

                                /*  const user_clicks = await db.collection('clicks').findOne({'contact_no': mobile_no.toString(),'content_label_id':parseInt(campaign_label_details._id)});

                                  if(user_clicks)
                                  {*/

                                db.collection('test_clicks').updateOne({
                                    'contact_no': mobile_no.toString(),
                                    'content_label_id': parseInt(campaign_label_details._id)
                                }, {
                                    $inc: {
                                        gross_clicks: 1
                                    }
                                });

                                if (campaign_label_details.destination_url) {
                                    var destination_url = campaign_label_details.destination_url;
                                } else {
                                    var destination_url = 'http://kasplo.com/';
                                }
                                client.close();
                                res.send({
                                    'status': 200,
                                    'msg': 'valid',
                                    'destination_url': destination_url
                                });

                            } else {


                                /* Checking browser details -  (start) */

                                var user_agent = 'System';

                                /* Checking browser details -  (stop) */

                                var click_data = {
                                    'category_id': parseInt(campaign_details.category_id),
                                    'campaign_id': parseInt(campaign_label_details.campaign_id),
                                    'content_label_id': parseInt(campaign_label_details._id),
                                    'contact_no': contacts_details.mobile_no,
                                    'count': 1,
                                    'gross_clicks': 1,
                                    'contacts_details': contacts_details,
                                    'category_details': category_details,
                                    'campaign_details': campaign_details,
                                    'campaign_label_details': campaign_label_details,
                                    'public_ip': p_ip,
                                    'location': '',
                                    'browser_information': 'System',
                                    'created_at': new Date(),
                                    'click_date': new Date().toLocaleString('en-US', {
                                        timeZone: 'Asia/Calcutta'
                                    })
                                };

                                test_clicks_collection.insert(click_data);

                                if (campaign_label_details.destination_url) {
                                    var destination_url = campaign_label_details.destination_url;
                                } else {
                                    var destination_url = 'http://kasplo.com/';
                                }

                                client.close();
                                res.send({
                                    'status': 200,
                                    'msg': 'valid',
                                    'destination_url': destination_url
                                });

                            }

                        } else {
                            res.send({
                                'status': 400,
                                'msg': 'invalid_campaign'
                            });
                        }
                    } else {
                        res.send({
                            'status': 400,
                            'msg': 'invalid_campaign_label_details'
                        });
                    }
                } else {
                    res.send({
                        'status': 400,
                        'msg': 'invalid_unique_id'
                    });
                }

            })();
        });
});

module.exports = router;