const express               = require('express');
var mongoose                = require('mongoose');
var MongoClient             = require('mongodb').MongoClient;
var autoIncrement           = require("mongodb-autoincrement");

const router                = express.Router();
const {ensureAuthenticated, roleAuthenticated} = require('../config/auth');

// DB Config
const db                    = require('../config/keys').mongoURI;
const db_options            = require('../config/keys').db_options;
const database_name         = require('../config/keys').database_name;

var activityController      = require('./../controllers/activity_ctrl');
var ObjectId                = require('mongodb').ObjectId;

var functions_handler       = require('./../helpers/functions_handler');

/**
* route: /create_role
* method: GET
* description: getting create role page
* params: 
* return: renders create role page
*/
router.get('/create_role', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount,  activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db              = client.db(database_name);
    const collection      = db.collection('roles');
    const menu_type_coll  = db.collection('menu_types');
    
    (async() => {
      var get_menu_types  = await menu_type_coll.find({'menu_type_status':'1'}).toArray();
      
      res.render('settings/menu/roles/create_role', {
        permissions         : req.permissions,
        get_menu_types,
        user                : req.user,
        activity_logs       : req.activity_logs,
        activity_logs_count : req.activity_logs_count,
      });
      client.close();
    })();
  });
}); //create_role

/**
* route: /update_role/:id
* method: GET
* description: getting update role page
* params: id
* return: renders update role page
*/
router.get('/update_role/:id', ensureAuthenticated,  roleAuthenticated, activityController.getActivityTotalCount,  activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db         = client.db(database_name);
    const collection = db.collection('roles');
    const menu_type_coll   = db.collection('menu_types');
    
    (async() => {
      var get_menu_types = await menu_type_coll.find({'menu_type_status':'1'}).toArray();
      
        res.render('settings/menu/roles/update_role', {
          permissions         : req.permissions,
          get_menu_types,
          user                : req.user,
          activity_logs       : req.activity_logs,
          activity_logs_count : req.activity_logs_count,
        });
        client.close();
     
    })();
  });
}); //create_role

/**
* route: /gole_role_by_menu_type_id
* method: POST
* description: getting menus and sub menus when menu_type_id selected
* params: menu_type_id
* return: status_code, message, sub_menu_data
*/ 
router.post('/gole_role_by_menu_type_id', ensureAuthenticated, activityController.getActivityTotalCount,  activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db               = client.db(database_name);
    const roles_collection = db.collection('roles');
    const menus_collection = db.collection('main_menus');
    var menu_type_id       = req.body.menu_type_id; 
    var status_code        = 400;
    var message            = 'Error while fetching data';
    (async() => {
      var get_submenus_by_type = await menus_collection.find({'menu_type_id' : {$in: [menu_type_id]}}).toArray(); 
      var get_submenu_len      = get_submenus_by_type.length;
      var sub_menu_data        = [];
      
      for (var i = 0; i < get_submenu_len; i++) {
        var data   = {};
        data['menu_name']  = get_submenus_by_type[i].menu_name;
        data['menu_id']    = get_submenus_by_type[i]._id;
        if (get_submenus_by_type[i].sub_menus) {
          if (get_submenus_by_type[i].sub_menus[menu_type_id]) {
            data = {'menu_name' : get_submenus_by_type[i].menu_name, 'menu_id' : get_submenus_by_type[i]._id, 'sub_menus' : get_submenus_by_type[i].sub_menus[menu_type_id]};
           }
        }
        sub_menu_data.push(data);

      }

      if (sub_menu_data.length > 0) {
        status_code          = 200;
        message              = 'Successfully fetched data';   
      }
      res.send({
        'status'     : status_code,
        'message'    : message,
        'sub_menu_data' : sub_menu_data
      })

    client.close();
    })();
  });
}); //gole_role_by_menu_type_id



module.exports = router;