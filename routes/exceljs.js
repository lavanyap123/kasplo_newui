const express = require('express');
const router = express.Router();
const {
    ensureAuthenticated, roleAuthenticated
} = require('../config/auth');
var json2csv = require('json2csv'); //not working
var csv_export = require('csv-export');
const stringify = require('csv-stringify');
var autoIncrement = require("mongodb-autoincrement");

var constants = require('./../config/constants');

var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
// Dashboard
var await = require("await");
var async = require("async");

//helper
var helper          = require('./../helpers/functions_handler');

var ISODate         = require("isodate");

// DB Config
const db            = require('../config/keys').mongoURI;
const db_options    = require('../config/keys').db_options;
const database_name = require('../config/keys').database_name;


var Excel = require('exceljs');
const workbook = new Excel.Workbook({
    useStyles: true
})


const headers = [
    { key: 'is_active', width: 40 },
    { key: 'unique_id', width: 12 },
    {  key: 'created_at', width: 24 },
];


// async function setWorkSheet (name, responses, filename)  {
//   console.log("inside work sheet");
//   // console.log(responses);
//     // workbook.xlsx.readFile(filename)
//     //     .then(function () {
            
           
//             for (let i = 0; i < responses.length; i++) {
//                 await ;
//             }
            
//             return  workbook.xlsx.writeFile(`${filename}`)
//         // })
//         // .then(function () {
//             console.log("Done");
//             // res.send({'status':200});
//         // })
//         // .catch(function (err) {
//         //     console.log(err)
//         // });

// }


router.get('/store_bulk_excel', (req, res, next) => {
  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);

    (async () => {
      var file_name        = "./public/test_log_bulk/contact_dummy_report_exceljs" + helper.getCurrentTime() + ".xlsx";
      workbook.xlsx.writeFile(file_name);
      var contact_dummy = []
      //const contact_dummy  = await db.collection('contacts').find({}).limit(2000000).toArray();
      const get_data  = await db.collection('contacts_dummy').find({}).limit(100).forEach( function(myDoc) { contact_dummy.push({'is_active' : myDoc.is_active,
              'unique_id' : myDoc.unique_id,
              'created_at': myDoc.created_at}) } );


      var report_data = [];
              
      if (contact_dummy.length > 0) {

        var length = contact_dummy.length - 1; var each_arr_size = 1048575;
        iteration_batch_min_and_max_values = [];

        if (length > each_arr_size) {
          for (var size = 0; size < length;) {

            if (iteration_batch_min_and_max_values.length > 0) {
              var min_val       = length - size - each_arr_size; 
              var min_range_val = min_val;
              if (min_val < 0) {
                min_range_val = 0;
              }
              iteration_batch_min_and_max_values.push([length - size - 1, min_range_val]);
            } else {
              iteration_batch_min_and_max_values.push([length, length - each_arr_size]);
            }      
            size = size + each_arr_size;
          }
        } else {
          iteration_batch_min_and_max_values.push([length, 0])
        } 

        for (var i = 0; i < iteration_batch_min_and_max_values.length; i++) {
          var sheet_name     =  i.toString(); 
          const worksheet    = await workbook.addWorksheet('Sheet' + sheet_name);
          //var sheet      = await workbook.getWorksheet(i);
          /*TITLE*/
          worksheet.mergeCells('A1', 'C2');
          worksheet.getCell('A1').value = 'Client List';
          worksheet.getCell('A1').font  = {
            name: 'Comic Sans MS',
            family: 4,
            size: 11,
            underline: true,
            bold: true
          };
          worksheet.getCell('A1').alignment = { vertical: 'middle', horizontal: 'center' };
          worksheet.getRow(2).values = ['is_active', 'unique_id', 'created_at'];

          worksheet.state    = 'visible';
          worksheet.columns  = headers;

          var min_value      = iteration_batch_min_and_max_values[i][0]; 
          var max_value      = iteration_batch_min_and_max_values[i][1]; 
          for (var j = min_value; j >= max_value; j--) {
            
            await worksheet.addRow({
              'is_active' : contact_dummy[j].is_active,
              'unique_id' : contact_dummy[j].unique_id,
              'created_at': contact_dummy[j].created_at,
            }).commit();
          }
          //await worksheet.commit();
          await workbook.xlsx.writeFile(`${file_name}`);      
        }
      client.close();
      }
      res.send({'status': 200});
    })(); //end async 
  }); //end mongoclient
});

module.exports = router;