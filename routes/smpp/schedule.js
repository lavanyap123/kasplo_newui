const express = require('express');
const router = express.Router();
const {
    ensureAuthenticated
} = require('../../config/auth');
const bcrypt = require('bcryptjs');
const passport = require('passport');
var json2csv = require('json2csv'); //not working
var csv_export = require('csv-export');
const stringify = require('csv-stringify');
var autoIncrement = require("mongodb-autoincrement");
var smpp = require('smpp');
var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
// Dashboard
var await = require("await");
var async = require("async");
var Hashids = require('hashids');
var nodemailer = require('nodemailer');
var ObjectId = require('mongodb').ObjectId;

const crypto = require('crypto');
var algorithm = 'aes256';
var key = 'password';
var activityController = require('./../../controllers/activity_ctrl');
// Load User model
const User = require('../../models/user');
// DB Config
const db = require('../../config/keys').mongoURI;
const db_options = require('../../config/keys').db_options;
const database_name = require('../../config/keys').database_name;
var cron = require('node-cron');
var ISODate = require("isodate");

var constants = require('./../../config/constants');
var keys = require('./../../config/keys');
const mail_from = require('./../../config/keys').mail_from;
const to_mail_ids = require('./../../config/keys').to_mail_ids;
const smpp_credentials = require('./../../config/keys').smpp_credentials;



MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);

    (async () => {

        const sms_logs = db.collection('sms_logs');
        const smpp_collection = db.collection('smpp_details');
        const content_labels_collection = db.collection('campaign_content_labels');


        cron.schedule('0-59/1 * * * * *', () => {

        	console.log('Schedule Running');

            content_labels_collection.find({
                'schedule_status': "0"
            }).project({
                _id: 1,
                campaign_id: 1,
                label_name: 1,
                'segments.campaign_name': 1,
                'segments.schedule_date_and_time': 1,
                'segments.smpp_id': 1
            }).forEach(function(campaign_content) {


            if (campaign_content) {

                //campaign_content_labels.forEach(function(campaign_content) {

                    var today = new Date();
                    var current_date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
                    var current_time = today.getHours() + ":" + today.getMinutes();

                    var first_Time = current_date + ' ' + current_time;

                    var campaign_name = campaign_content.segments.campaign_name;
                    var label_name = campaign_content.label_name;

                    var campaign_id = campaign_content.campaign_id;
                    var campaign_content_id = campaign_content._id;

                    var schedule_dateTime = new Date(campaign_content.segments.schedule_date_and_time);

                    var schedule_date = schedule_dateTime.getDate() + '-' + (schedule_dateTime.getMonth() + 1) + '-' + schedule_dateTime.getFullYear();
                    var report_schedule_date = (schedule_dateTime.getMonth() + 1) + '-' + schedule_dateTime.getDate() + '-' + schedule_dateTime.getFullYear();

                    var schedule_time = schedule_dateTime.getHours() + ":" + schedule_dateTime.getMinutes();

                    var second_time = schedule_date + ' ' + schedule_time;
                    var report_end_time = report_schedule_date + ' ' + schedule_time;

                    today.setHours(today.getHours(), today.getMinutes(), 0, 0);

                    if (first_Time >= second_time && current_time === schedule_time) {

                        var conditions = {
                            'schedule_date_and_time': {
                                $eq: today
                            },
                            'message_status': 'SCHEDULE'
                        };

                        (async () => {

                            const total_sms_logs = await sms_logs.find(conditions).project({
                                _id: 0,
                                mobile_no: 1
                            }).toArray();

                            if (total_sms_logs) {
                                var mobile_no_list = [];

                                total_sms_logs.forEach(function(record) {
                                    mobile_no_list.push(new RegExp('^' + record.mobile_no));
                                }); //foreach - closed


                                sms_logs.updateMany({
                                    'campaign_id': parseInt(campaign_content.campaign_id),
                                    'campaign_content_label_id': parseInt(campaign_content._id),
                                    'message_status': "SCHEDULE"
                                }, {
                                    $set: {
                                        "message_status": "SUBMIT",
                                        "updated_at": new Date()
                                    }
                                });

                                /*  Scheduled Success Email - START */
                                sendEmailAfterSchedule(campaign_id, campaign_content_id, campaign_name, label_name, report_end_time, campaign_id, campaign_content_id, content_labels_collection);
                                /*  Scheduled Success Email - STOP */


                            }




                        })();


                    }
            	}

        		});
            });
    })();

});

async function sendEmailAfterSchedule(campaign_id, campaign_content_id, campaign_name, label_name, report_end_time, campaign_id, campaign_content_id, content_labels_collection) {
    var data = {
        'schedule_status': '1',
        'updated_at': new Date().toLocaleString('en-US', {
            timeZone: 'Asia/Calcutta'
        })
    };

    content_labels_collection.findOneAndUpdate({
        _id: parseInt(campaign_content_id)
    }, {
        $set: data
    });

    var mail_content = '';
    mail_content += '<div style="text-align: center; width: 100%;" data-mce-style="text-align: center; width: 100%;"> <div style="width: 740px; text-align: left; color: #000; background: #fff; border: 1px solid #f8f8f8; padding: 15px; margin: 0 auto;" data-mce-style="width: 600px; text-align: left; color: #000; background: #fff; border: 1px solid #f8f8f8; padding: 15px; margin: 0 auto;"><p>Dear Team,<br><br>Schedule sms sent successfully.<br><br><table border="1"><thead><tr><th style="width: 15%;">Campaign</th><th style="width:15%;">Label</th><th>Schedule date</th></tr></thead>';
    mail_content += '<tbody>';
    mail_content += '<tr>';
    mail_content += '<td>' + campaign_name + '</td>';
    mail_content += '<td>' + label_name + '</td>';
    mail_content += '<td>' + report_end_time + '</td>';
    mail_content += '</tr>';
    mail_content += '</tbody></table>';
    mail_content += '<p><br>Please click the following link to watch your campaign content sms report: </p><p><a href="http://panel.kasplo.com/report/sms_log_report?campaign_id=' + campaign_id + '&content_label_id=' + campaign_content_id + '"><blink style="animation: blinker 1s linear infinite;">Watch Your SMS Report</blink></a></p><br><br>Best Regards,<br>Kasplo<br></p></div><p><a href="#" style="margin-top: 15px;" data-mce-href="#" "="" data-mce-style="margin-top: 15px;"> <img width="100px" src="http://panel.kasplo.com/public/images/sms-logo.png"></a></p></div>';

    var transporter = nodemailer.createTransport(smpp_credentials);

    var mailOptions = {
        from: mail_from,
        to: to_mail_ids,
        text: '',
        subject: 'Schedule sms sent successfully',
        html: mail_content
    };

    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log("Message sent: %s", info.messageId);
            console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
        }
    });
}

module.exports = router;