const express = require('express');
const router = express.Router();
const {
    ensureAuthenticated
} = require('../../../config/auth');
const bcrypt = require('bcryptjs');
const passport = require('passport');
var autoIncrement = require("mongodb-autoincrement");
var smpp = require('smpp');
var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
// Dashboard
var await = require("await");
var async = require("async");

// DB Config
const db = require('../../../config/keys').mongoURI;
const db_options = require('../../../config/keys').db_options;
const database_name = require('../../../config/keys').database_name;
var keys = require('./../../../config/keys');

/* RabbitMQ Settings & Library - (Start) */

var connection = require('amqplib').connect(keys.rabbitmq_uri);

var QUEUE_NAME_1 = 'sms_logs';
var QUEUE_NAME_2 = 'sms_sent_logs';

/* Redis Settings & Library - (Stop) */

    MongoClient.connect(db, db_options).then(client => {

        // Consumer
        connection.then(function(conn) {
          //conn.basicQos(90); // Per consumer limit
          return conn.createChannel();
        }).then(function(ch) {

            (async () => {

                    const db = client.db(database_name);
                    const sms_logs = db.collection('sms_logs');
                    const smpp_collection = db.collection('smpp_details');

                   /* var smpp_details = await smpp_collection.findOne({
                        'smpp_id': "5" //Broadnet
                    });*/

                    var session = new smpp.Session({
                        host: '94.237.76.145',//smpp_details.ip_address,
                        port: '5050'//smpp_details.host[0]
                    });

                    delete smpp.encodings.ASCII;

                   session.on('connect', function() {
                    
                    session.bind_transmitter({
                        system_id: 'adcanopus',//smpp_details.system_id, //SMPP - Settings Table
                        password: '9xTKDWKc',//smpp_details.password, //SMPP - Settings Table
                        interface_version: 1,
                        system_type: '380666000600',
                        address_range: '+380666000600',
                        addr_ton: 1,
                        addr_npi: 1
                    }, (pdu) => {
                        if (pdu.command_status == 0) {
                            console.log('Transceiver-1 Successfully bound');

                              ch.assertQueue(QUEUE_NAME_1).then(function(ok) {

                                ch.consume(QUEUE_NAME_1, function(data) {

                                  if (data !== null) {

                                        /* Session Connected & RabbitMQ - Consumer - Start */
                                            var ack_msg_respons = JSON.parse(data.content);

                                        /* Worker - Start */

                                        session.submit_sm({
                                            source_addr: ack_msg_respons.sender_id, //SENDER ID
                                            destination_addr: ack_msg_respons.mobile_no, // Mobile Number
                                            short_message: ack_msg_respons.sms_content, // Text SMS
                                            registered_delivery: 1, //Mandatory Send Delivery Response
                                        }, function(pdu) {

                                            if (pdu.command_status == 0) {

console.log('######### smpp SMS are sent ######');
console.log(pdu);                                                    
                                               /* Add Queue - Submit Response - Start */

                                                var queue_pdu_response = {};
                                                queue_pdu_response['submit_log_id'] = ack_msg_respons.submit_log_id;
                                                queue_pdu_response['command_id'] = pdu.command_id;
                                                queue_pdu_response['command_status'] = pdu.command_status;
                                                queue_pdu_response['sequence_number'] = pdu.sequence_number;
                                                queue_pdu_response['message_id'] = pdu.message_id;
                                                queue_pdu_response['submit_sm_resp'] = pdu;
                                                queue_pdu_response['submit_session_name'] = 'session_1';
                                                queue_pdu_response['message_status'] = 'PENDING'; //default message status
                                                queue_pdu_response['message_type'] = 'Text SMS'; //default message status
                                                queue_pdu_response['updated_at'] = new Date();
                                                queue_pdu_response['message_update_date'] = new Date().toLocaleString('en-US', {
                                                    timeZone: 'Asia/Calcutta'
                                                });

                                                ch.assertQueue(QUEUE_NAME_2).then(function(ok) {
                                                    ch.sendToQueue(QUEUE_NAME_2, Buffer.from(JSON.stringify(queue_pdu_response)));
                                                });

                                            }


                                        }); //submit_sm session - closed
                                
                                
                                            ch.ack(data);
                                        }

                                        });
                                    });
                                }
                                /*  Worker - Stop */

                    });

                });

                

                session.on('close', function() {
                    console.log('Transceiver is now disconnected');
                    session.connect();
                });

                session.on('enquire_link', function(pdu) {
                    console.log('enquire_link')
                    session.send(pdu.response());
                });

                session.on('unbind', function(pdu) {
                    console.log('unbind')
                    session.send(pdu.response());
                    session.close();
                });

                session.on('error', function(error) {
                    console.log('smpp error', error);
                });


            })();



    }).catch(function(err) {
        return console.log("Message was rejected...  Boo!");
    });


 });

module.exports = router;