const express = require('express');
const router = express.Router();
const {
    ensureAuthenticated
} = require('../../config/auth');
const bcrypt = require('bcryptjs');
const passport = require('passport');
var autoIncrement = require("mongodb-autoincrement");
var smpp = require('smpp');
var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
// Dashboard
var await = require("await");
var async = require("async");

// DB Config
const db = require('../../config/keys').mongoURI;
const db_options = require('../../config/keys').db_options;
const database_name = require('../../config/keys').database_name;
var keys = require('./../../config/keys');


/* RabbitMQ Settings & Library - (Start) */
var connection = require('amqplib').connect(keys.rabbitmq_uri);
var QUEUE_NAME = 'broadnet_delivery_logs';


MongoClient.connect(db, db_options).then(client => {

    const db = client.db(database_name);
    const sms_logs = db.collection('sms_logs');
    const smpp_collection = db.collection('smpp_details');

    (async () => {

    connection.then(function(conn) {
      return conn.createChannel();
    }).then(function(ch) {

        ch.assertQueue(QUEUE_NAME).then(function(ok) {

            /*
            var smpp_details = await smpp_collection.findOne({
                'smpp_id': "1" //Broadnet
            });
            */

            /* Broadnet Bind Receiver Session -1 ( Start ) */

            var bind_receiver_session_1 = new smpp.Session({
                host: '145.239.245.15',//smpp_details.ip_address,
                port: '8892'//smpp_details.host[0]
            });

            bind_receiver_session_1.on('connect', function() {

                bind_receiver_session_1.bind_receiver({
                    system_id: 'AdcanOPEN',//smpp_details.system_id, //SMPP - Settings Table
                    password: 'Ad7n37k1S',//smpp_details.password, //SMPP - Settings Table
                }, (pdu) => {
                    if (pdu.command_status == 0) {
                        console.log('Session-1 Received Successfully bound')

                    }
                });

            });

            bind_receiver_session_1.on('pdu', function(pdu) {

                // incoming SMS from SMSC
                if (pdu.command == 'deliver_sm') {

                    var delivery_response = {};

                    var fromNumber = pdu.source_addr.toString();
                    var toNumber = pdu.destination_addr.toString();

                    var text = '';
                    var receipted_message_id = '';
                    var user_message_reference = '';

                    if (pdu.short_message && pdu.short_message.message) {
                        text = pdu.short_message.message;
                        receipted_message_id = pdu.short_message.message_payload;
                        user_message_reference = pdu.short_message.user_message_reference;
                    }

                    console.log('Delivery Response-1');
                    //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                    var parse_deliver_text_str = text.toString();
                    var receipted_id = receipted_message_id;

                    const querystring = require('querystring');
                    var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });

                    var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });
                    var sequence_number = pdu.sequence_number;

                    var msg_id = parse_deliver_text_json.id;
                    delivery_response['message_id'] = msg_id;
                    delivery_response['service_type'] = pdu.service_type;
                    delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                    delivery_response['protocol_id'] = pdu.protocol_id;
                    delivery_response['priority_flag'] = pdu.priority_flag;
                    delivery_response['message_status'] = parse_deliver_text_json.stat;
                    delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                    delivery_response['sub'] = parse_deliver_text_json.sub;
                    delivery_response['message_err'] = parse_deliver_text_json.err;
                    delivery_response['deliver_sm_data'] = pdu;
                    delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                    delivery_response['receiver_session_name'] = 'session_1';
                    delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                    delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Calcutta'
                    });
                  
                    console.log("Message was sent-delivery_response!")
                    ch.sendToQueue(QUEUE_NAME, Buffer.from(JSON.stringify(delivery_response)));

                    // Reply to SMSC that we received and processed the SMS
                    bind_receiver_session_1.deliver_sm_resp({
                        'sequence_number': sequence_number
                    });
                    console.log('Reply to SMSC=' + sequence_number);

                }

            });

            bind_receiver_session_1.on('close', function() {
                //console.log('Receiver is now disconnected');
                bind_receiver_session_1.connect();
            });

            bind_receiver_session_1.on('enquire_link', function(pdu) {
                console.log('enquire_link')
                bind_receiver_session_1.send(pdu.response());
            });

            bind_receiver_session_1.on('unbind', function(pdu) {
                console.log('unbind')
                bind_receiver_session_1.send(pdu.response());
                bind_receiver_session_1.close();
            });

            bind_receiver_session_1.on('error', function(error) {
                console.log('smpp error', error)
            });

            /* Yoursmsapp Bind Receiver Session -1 ( Stop ) */

            /* Yoursmsapp Bind Receiver Session -2 ( Start ) */

            var bind_receiver_session_2 = new smpp.Session({
                host: '145.239.245.15',//smpp_details.ip_address,
                port: '8892'//smpp_details.host[0]            
            });

            bind_receiver_session_2.on('connect', function() {

                bind_receiver_session_2.bind_receiver({
                    system_id: 'AdcanOPEN',//smpp_details.system_id, //SMPP - Settings Table
                    password: 'Ad7n37k1S',//smpp_details.password, //SMPP - Settings Table
                }, (pdu) => {
                    if (pdu.command_status == 0) {
                        console.log('Session-2 Received Successfully bound')
                    }
                });

            });

            bind_receiver_session_2.on('pdu', function(pdu) {

                // incoming SMS from SMSC
                if (pdu.command == 'deliver_sm') {

                    var delivery_response = {};

                    var fromNumber = pdu.source_addr.toString();
                    var toNumber = pdu.destination_addr.toString();

                    var text = '';
                    var receipted_message_id = '';
                    var user_message_reference = '';

                    if (pdu.short_message && pdu.short_message.message) {
                        text = pdu.short_message.message;
                        receipted_message_id = pdu.short_message.message_payload;
                        user_message_reference = pdu.short_message.user_message_reference;
                    }

                    console.log('Delivery Response-2');
                    //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                    var parse_deliver_text_str = text.toString();
                    var receipted_id = receipted_message_id;

                    const querystring = require('querystring');
                    var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });

                    var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });
                    var sequence_number = pdu.sequence_number;

                    var msg_id = parse_deliver_text_json.id;
                    delivery_response['message_id'] = msg_id;
                    delivery_response['service_type'] = pdu.service_type;
                    delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                    delivery_response['protocol_id'] = pdu.protocol_id;
                    delivery_response['priority_flag'] = pdu.priority_flag;
                    delivery_response['message_status'] = parse_deliver_text_json.stat;
                    delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                    delivery_response['sub'] = parse_deliver_text_json.sub;
                    delivery_response['message_err'] = parse_deliver_text_json.err;
                    delivery_response['deliver_sm_data'] = pdu;
                    delivery_response['receiver_session_name'] = 'session_2';
                    delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                    delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];                     
                    delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Calcutta'
                    });

                    console.log("Message was sent-delivery_response!")
                    ch.sendToQueue(QUEUE_NAME, Buffer.from(JSON.stringify(delivery_response)));

                    // Reply to SMSC that we received and processed the SMS
                    bind_receiver_session_2.deliver_sm_resp({
                        'sequence_number': sequence_number
                    });
                    console.log('Reply to SMSC=' + sequence_number);
                }

            });

            bind_receiver_session_2.on('close', function() {
                //console.log('Receiver is now disconnected');
                bind_receiver_session_2.connect();
            });

            bind_receiver_session_2.on('enquire_link', function(pdu) {
                console.log('enquire_link')
                bind_receiver_session_2.send(pdu.response());
            });

            bind_receiver_session_2.on('unbind', function(pdu) {
                console.log('unbind')
                bind_receiver_session_2.send(pdu.response());
                bind_receiver_session_2.close();
            });

            bind_receiver_session_2.on('error', function(error) {
                console.log('smpp error', error)
            });

            /* Yoursmsapp Bind Receiver Session -2 ( Stop ) */

            /* Yoursmsapp Bind Receiver Session -3 ( Start ) */

            var bind_receiver_session_3 = new smpp.Session({
                host: '145.239.245.15',//smpp_details.ip_address,
                port: '8892'//smpp_details.host[0]            
            });

            bind_receiver_session_3.on('connect', function() {

                bind_receiver_session_3.bind_receiver({
                    system_id: 'AdcanOPEN',//smpp_details.system_id, //SMPP - Settings Table
                    password: 'Ad7n37k1S',//smpp_details.password, //SMPP - Settings Table
                }, (pdu) => {
                    if (pdu.command_status == 0) {
                        console.log('Session-3 Received Successfully bound')
                    }
                });

            });

            bind_receiver_session_3.on('pdu', function(pdu) {

                // incoming SMS from SMSC
                if (pdu.command == 'deliver_sm') {

                    var delivery_response = {};

                    var fromNumber = pdu.source_addr.toString();
                    var toNumber = pdu.destination_addr.toString();

                    var text = '';
                    var receipted_message_id = '';
                    var user_message_reference = '';

                    if (pdu.short_message && pdu.short_message.message) {
                        text = pdu.short_message.message;
                        receipted_message_id = pdu.short_message.message_payload;
                        user_message_reference = pdu.short_message.user_message_reference;
                    }

                    console.log('Delivery Response-3');
                    //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                    var parse_deliver_text_str = text.toString();
                    var receipted_id = receipted_message_id;

                    const querystring = require('querystring');
                    var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });

                    var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });
                    var sequence_number = pdu.sequence_number;

                    var msg_id = parse_deliver_text_json.id;
                    delivery_response['message_id'] = msg_id;
                    delivery_response['service_type'] = pdu.service_type;
                    delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                    delivery_response['protocol_id'] = pdu.protocol_id;
                    delivery_response['priority_flag'] = pdu.priority_flag;
                    delivery_response['message_status'] = parse_deliver_text_json.stat;
                    delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                    delivery_response['sub'] = parse_deliver_text_json.sub;
                    delivery_response['message_err'] = parse_deliver_text_json.err;
                    delivery_response['deliver_sm_data'] = pdu;
                    delivery_response['receiver_session_name'] = 'session_3';
                    delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                    delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];                     
                    delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Calcutta'
                    });


                    console.log("Message was sent-delivery_response!")
                    ch.sendToQueue(QUEUE_NAME, Buffer.from(JSON.stringify(delivery_response)));
                   
                    // Reply to SMSC that we received and processed the SMS
                    bind_receiver_session_3.deliver_sm_resp({
                        'sequence_number': sequence_number
                    });
                    console.log('Reply to SMSC=' + sequence_number);

                }

            });

            bind_receiver_session_3.on('close', function() {
                //console.log('Receiver is now disconnected');
                bind_receiver_session_3.connect();
            });

            bind_receiver_session_3.on('enquire_link', function(pdu) {
                console.log('enquire_link')
                bind_receiver_session_3.send(pdu.response());
            });

            bind_receiver_session_3.on('unbind', function(pdu) {
                console.log('unbind')
                bind_receiver_session_3.send(pdu.response());
                bind_receiver_session_3.close();
            });

            bind_receiver_session_3.on('error', function(error) {
                console.log('smpp error', error)
            });

            /* Yoursmsapp Bind Receiver Session -3 ( Stop ) */


            /* Yoursmsapp Bind Receiver Session -4 ( Start ) */

            var bind_receiver_session_4 = new smpp.Session({
                host: '145.239.245.15',//smpp_details.ip_address,
                port: '8892'//smpp_details.host[0]            
            });

            bind_receiver_session_4.on('connect', function() {

                bind_receiver_session_4.bind_receiver({
                    system_id: 'AdcanOPEN',//smpp_details.system_id, //SMPP - Settings Table
                    password: 'Ad7n37k1S',//smpp_details.password, //SMPP - Settings Table
                }, (pdu) => {
                    if (pdu.command_status == 0) {
                        console.log('Session-4 Received Successfully bound')
                    }
                });

            });

            bind_receiver_session_4.on('pdu', function(pdu) {

                // incoming SMS from SMSC
                if (pdu.command == 'deliver_sm') {

                    var delivery_response = {};

                    var fromNumber = pdu.source_addr.toString();
                    var toNumber = pdu.destination_addr.toString();

                    var text = '';
                    var receipted_message_id = '';
                    var user_message_reference = '';

                    if (pdu.short_message && pdu.short_message.message) {
                        text = pdu.short_message.message;
                        receipted_message_id = pdu.short_message.message_payload;
                        user_message_reference = pdu.short_message.user_message_reference;
                    }

                    console.log('Delivery Response-4');
                    //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                    var parse_deliver_text_str = text.toString();
                    var receipted_id = receipted_message_id;

                    const querystring = require('querystring');
                    var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });

                    var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });
                    var sequence_number = pdu.sequence_number;

                    var msg_id = parse_deliver_text_json.id;
                    delivery_response['message_id'] = msg_id;
                    delivery_response['service_type'] = pdu.service_type;
                    delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                    delivery_response['protocol_id'] = pdu.protocol_id;
                    delivery_response['priority_flag'] = pdu.priority_flag;
                    delivery_response['message_status'] = parse_deliver_text_json.stat;
                    delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                    delivery_response['sub'] = parse_deliver_text_json.sub;
                    delivery_response['message_err'] = parse_deliver_text_json.err;
                    delivery_response['deliver_sm_data'] = pdu;
                    delivery_response['receiver_session_name'] = 'session_4';
                    delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                    delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];                     
                    delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Calcutta'
                    });

                    console.log("Message was sent-delivery_response!")
                    ch.sendToQueue(QUEUE_NAME, Buffer.from(JSON.stringify(delivery_response)));

                    // Reply to SMSC that we received and processed the SMS
                    bind_receiver_session_4.deliver_sm_resp({
                        'sequence_number': sequence_number
                    });
                    console.log('Reply to SMSC=' + sequence_number);

                }

            });

            bind_receiver_session_4.on('close', function() {
                //console.log('Receiver is now disconnected');
                bind_receiver_session_4.connect();
            });

            bind_receiver_session_4.on('enquire_link', function(pdu) {
                console.log('enquire_link')
                bind_receiver_session_4.send(pdu.response());
            });

            bind_receiver_session_4.on('unbind', function(pdu) {
                console.log('unbind')
                bind_receiver_session_4.send(pdu.response());
                bind_receiver_session_4.close();
            });

            bind_receiver_session_4.on('error', function(error) {
                console.log('smpp error', error)
            });

            /* Yoursmsapp Bind Receiver Session -4 ( Stop ) */


            /* Yoursmsapp Bind Receiver Session -5 ( Start ) */

            var bind_receiver_session_5 = new smpp.Session({
                host: '145.239.245.15',//smpp_details.ip_address,
                port: '8892'//smpp_details.host[0]
            });

            bind_receiver_session_5.on('connect', function() {

                bind_receiver_session_5.bind_receiver({
                    system_id: 'AdcanOPEN',//smpp_details.system_id, //SMPP - Settings Table
                    password: 'Ad7n37k1S',//smpp_details.password, //SMPP - Settings Table
                }, (pdu) => {
                    if (pdu.command_status == 0) {
                        console.log('Session-5 Received Successfully bound')

                    }
                });

            });

            bind_receiver_session_5.on('pdu', function(pdu) {

                // incoming SMS from SMSC
                if (pdu.command == 'deliver_sm') {

                    var delivery_response = {};

                    var fromNumber = pdu.source_addr.toString();
                    var toNumber = pdu.destination_addr.toString();

                    var text = '';
                    var receipted_message_id = '';
                    var user_message_reference = '';

                    if (pdu.short_message && pdu.short_message.message) {
                        text = pdu.short_message.message;
                        receipted_message_id = pdu.short_message.message_payload;
                        user_message_reference = pdu.short_message.user_message_reference;
                    }

                    console.log('Delivery Response-1');
                    //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                    var parse_deliver_text_str = text.toString();
                    var receipted_id = receipted_message_id;

                    const querystring = require('querystring');
                    var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });

                    var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });
                    var sequence_number = pdu.sequence_number;

                    var msg_id = parse_deliver_text_json.id;
                    delivery_response['message_id'] = msg_id;
                    delivery_response['service_type'] = pdu.service_type;
                    delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                    delivery_response['protocol_id'] = pdu.protocol_id;
                    delivery_response['priority_flag'] = pdu.priority_flag;
                    delivery_response['message_status'] = parse_deliver_text_json.stat;
                    delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                    delivery_response['sub'] = parse_deliver_text_json.sub;
                    delivery_response['message_err'] = parse_deliver_text_json.err;
                    delivery_response['deliver_sm_data'] = pdu;
                    delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                    delivery_response['receiver_session_name'] = 'session_1';
                    delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];                     
                    delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Calcutta'
                    });
                  
                    console.log("Message was sent-delivery_response!")
                    ch.sendToQueue(QUEUE_NAME, Buffer.from(JSON.stringify(delivery_response)));

                    // Reply to SMSC that we received and processed the SMS
                    bind_receiver_session_5.deliver_sm_resp({
                        'sequence_number': sequence_number
                    });
                    console.log('Reply to SMSC=' + sequence_number);

                }

            });

            bind_receiver_session_5.on('close', function() {
                //console.log('Receiver is now disconnected');
                bind_receiver_session_5.connect();
            });

            bind_receiver_session_5.on('enquire_link', function(pdu) {
                console.log('enquire_link')
                bind_receiver_session_5.send(pdu.response());
            });

            bind_receiver_session_5.on('unbind', function(pdu) {
                console.log('unbind')
                bind_receiver_session_5.send(pdu.response());
                bind_receiver_session_5.close();
            });

            bind_receiver_session_5.on('error', function(error) {
                console.log('smpp error', error)
            });

            /* Yoursmsapp Bind Receiver Session -5 ( Stop ) */

            /* Yoursmsapp Bind Receiver Session -6 ( Start ) */

            var bind_receiver_session_6 = new smpp.Session({
                host: '145.239.245.15',//smpp_details.ip_address,
                port: '8892'//smpp_details.host[0]
            });

            bind_receiver_session_6.on('connect', function() {

                bind_receiver_session_6.bind_receiver({
                    system_id: 'AdcanOPEN',//smpp_details.system_id, //SMPP - Settings Table
                    password: 'Ad7n37k1S',//smpp_details.password, //SMPP - Settings Table
                }, (pdu) => {
                    if (pdu.command_status == 0) {
                        console.log('Session-6 Received Successfully bound')

                    }
                });

            });

            bind_receiver_session_6.on('pdu', function(pdu) {

                // incoming SMS from SMSC
                if (pdu.command == 'deliver_sm') {

                    var delivery_response = {};

                    var fromNumber = pdu.source_addr.toString();
                    var toNumber = pdu.destination_addr.toString();

                    var text = '';
                    var receipted_message_id = '';
                    var user_message_reference = '';

                    if (pdu.short_message && pdu.short_message.message) {
                        text = pdu.short_message.message;
                        receipted_message_id = pdu.short_message.message_payload;
                        user_message_reference = pdu.short_message.user_message_reference;
                    }

                    console.log('Delivery Response-1');
                    //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                    var parse_deliver_text_str = text.toString();
                    var receipted_id = receipted_message_id;

                    const querystring = require('querystring');
                    var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });

                    var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });
                    var sequence_number = pdu.sequence_number;

                    var msg_id = parse_deliver_text_json.id;
                    delivery_response['message_id'] = msg_id;
                    delivery_response['service_type'] = pdu.service_type;
                    delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                    delivery_response['protocol_id'] = pdu.protocol_id;
                    delivery_response['priority_flag'] = pdu.priority_flag;
                    delivery_response['message_status'] = parse_deliver_text_json.stat;
                    delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                    delivery_response['sub'] = parse_deliver_text_json.sub;
                    delivery_response['message_err'] = parse_deliver_text_json.err;
                    delivery_response['deliver_sm_data'] = pdu;
                    delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                    delivery_response['receiver_session_name'] = 'session_1';
                    delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];                     
                    delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Calcutta'
                    });
                  
                    console.log("Message was sent-delivery_response!")
                    ch.sendToQueue(QUEUE_NAME, Buffer.from(JSON.stringify(delivery_response)));

                    // Reply to SMSC that we received and processed the SMS
                    bind_receiver_session_6.deliver_sm_resp({
                        'sequence_number': sequence_number
                    });
                    console.log('Reply to SMSC=' + sequence_number);

                }

            });

            bind_receiver_session_6.on('close', function() {
                //console.log('Receiver is now disconnected');
                bind_receiver_session_6.connect();
            });

            bind_receiver_session_6.on('enquire_link', function(pdu) {
                console.log('enquire_link')
                bind_receiver_session_6.send(pdu.response());
            });

            bind_receiver_session_6.on('unbind', function(pdu) {
                console.log('unbind')
                bind_receiver_session_6.send(pdu.response());
                bind_receiver_session_6.close();
            });

            bind_receiver_session_6.on('error', function(error) {
                console.log('smpp error', error)
            });

            /* Yoursmsapp Bind Receiver Session -6 ( Stop ) */


           /* Yoursmsapp Bind Receiver Session -7 ( Start ) */

            var bind_receiver_session_7 = new smpp.Session({
                host: '145.239.245.15',//smpp_details.ip_address,
                port: '8892'//smpp_details.host[0]
            });

            bind_receiver_session_7.on('connect', function() {

                bind_receiver_session_7.bind_receiver({
                    system_id: 'AdcanOPEN',//smpp_details.system_id, //SMPP - Settings Table
                    password: 'Ad7n37k1S',//smpp_details.password, //SMPP - Settings Table
                }, (pdu) => {
                    if (pdu.command_status == 0) {
                        console.log('Session-7 Received Successfully bound')

                    }
                });

            });

            bind_receiver_session_7.on('pdu', function(pdu) {

                // incoming SMS from SMSC
                if (pdu.command == 'deliver_sm') {

                    var delivery_response = {};

                    var fromNumber = pdu.source_addr.toString();
                    var toNumber = pdu.destination_addr.toString();

                    var text = '';
                    var receipted_message_id = '';
                    var user_message_reference = '';

                    if (pdu.short_message && pdu.short_message.message) {
                        text = pdu.short_message.message;
                        receipted_message_id = pdu.short_message.message_payload;
                        user_message_reference = pdu.short_message.user_message_reference;
                    }

                    console.log('Delivery Response-1');
                    //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                    var parse_deliver_text_str = text.toString();
                    var receipted_id = receipted_message_id;

                    const querystring = require('querystring');
                    var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });

                    var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });
                    var sequence_number = pdu.sequence_number;

                    var msg_id = parse_deliver_text_json.id;
                    delivery_response['message_id'] = msg_id;
                    delivery_response['service_type'] = pdu.service_type;
                    delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                    delivery_response['protocol_id'] = pdu.protocol_id;
                    delivery_response['priority_flag'] = pdu.priority_flag;
                    delivery_response['message_status'] = parse_deliver_text_json.stat;
                    delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                    delivery_response['sub'] = parse_deliver_text_json.sub;
                    delivery_response['message_err'] = parse_deliver_text_json.err;
                    delivery_response['deliver_sm_data'] = pdu;
                    delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                    delivery_response['receiver_session_name'] = 'session_1';
                    delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];                     
                    delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Calcutta'
                    });
                  
                    console.log("Message was sent-delivery_response!")
                    ch.sendToQueue(QUEUE_NAME, Buffer.from(JSON.stringify(delivery_response)));

                    // Reply to SMSC that we received and processed the SMS
                    bind_receiver_session_7.deliver_sm_resp({
                        'sequence_number': sequence_number
                    });
                    console.log('Reply to SMSC=' + sequence_number);

                }

            });

            bind_receiver_session_7.on('close', function() {
                //console.log('Receiver is now disconnected');
                bind_receiver_session_7.connect();
            });

            bind_receiver_session_7.on('enquire_link', function(pdu) {
                console.log('enquire_link')
                bind_receiver_session_7.send(pdu.response());
            });

            bind_receiver_session_7.on('unbind', function(pdu) {
                console.log('unbind')
                bind_receiver_session_7.send(pdu.response());
                bind_receiver_session_7.close();
            });

            bind_receiver_session_7.on('error', function(error) {
                console.log('smpp error', error)
            });

            /* Yoursmsapp Bind Receiver Session -7 ( Stop ) */


           /* Yoursmsapp Bind Receiver Session -8 ( Start ) */

            var bind_receiver_session_8 = new smpp.Session({
                host: '145.239.245.15',//smpp_details.ip_address,
                port: '8892'//smpp_details.host[0]
            });

            bind_receiver_session_8.on('connect', function() {

                bind_receiver_session_8.bind_receiver({
                    system_id: 'AdcanOPEN',//smpp_details.system_id, //SMPP - Settings Table
                    password: 'Ad7n37k1S',//smpp_details.password, //SMPP - Settings Table
                }, (pdu) => {
                    if (pdu.command_status == 0) {
                        console.log('Session-8 Received Successfully bound')

                    }
                });

            });

            bind_receiver_session_8.on('pdu', function(pdu) {

                // incoming SMS from SMSC
                if (pdu.command == 'deliver_sm') {

                    var delivery_response = {};

                    var fromNumber = pdu.source_addr.toString();
                    var toNumber = pdu.destination_addr.toString();

                    var text = '';
                    var receipted_message_id = '';
                    var user_message_reference = '';

                    if (pdu.short_message && pdu.short_message.message) {
                        text = pdu.short_message.message;
                        receipted_message_id = pdu.short_message.message_payload;
                        user_message_reference = pdu.short_message.user_message_reference;
                    }

                    console.log('Delivery Response-1');
                    //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                    var parse_deliver_text_str = text.toString();
                    var receipted_id = receipted_message_id;

                    const querystring = require('querystring');
                    var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });

                    var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });
                    var sequence_number = pdu.sequence_number;

                    var msg_id = parse_deliver_text_json.id;
                    delivery_response['message_id'] = msg_id;
                    delivery_response['service_type'] = pdu.service_type;
                    delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                    delivery_response['protocol_id'] = pdu.protocol_id;
                    delivery_response['priority_flag'] = pdu.priority_flag;
                    delivery_response['message_status'] = parse_deliver_text_json.stat;
                    delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                    delivery_response['sub'] = parse_deliver_text_json.sub;
                    delivery_response['message_err'] = parse_deliver_text_json.err;
                    delivery_response['deliver_sm_data'] = pdu;
                    delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                    delivery_response['receiver_session_name'] = 'session_1';
                    delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];                     
                    delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Calcutta'
                    });
                  
                    console.log("Message was sent-delivery_response!")
                    ch.sendToQueue(QUEUE_NAME, Buffer.from(JSON.stringify(delivery_response)));

                    // Reply to SMSC that we received and processed the SMS
                    bind_receiver_session_8.deliver_sm_resp({
                        'sequence_number': sequence_number
                    });
                    console.log('Reply to SMSC=' + sequence_number);

                }

            });

            bind_receiver_session_8.on('close', function() {
                //console.log('Receiver is now disconnected');
                bind_receiver_session_8.connect();
            });

            bind_receiver_session_8.on('enquire_link', function(pdu) {
                console.log('enquire_link')
                bind_receiver_session_8.send(pdu.response());
            });

            bind_receiver_session_8.on('unbind', function(pdu) {
                console.log('unbind')
                bind_receiver_session_8.send(pdu.response());
                bind_receiver_session_8.close();
            });

            bind_receiver_session_8.on('error', function(error) {
                console.log('smpp error', error)
            });

            /* Yoursmsapp Bind Receiver Session - 8 ( Stop ) */

           /* Yoursmsapp Bind Receiver Session -9 ( Start ) */

            var bind_receiver_session_9 = new smpp.Session({
                host: '145.239.245.15',//smpp_details.ip_address,
                port: '8892'//smpp_details.host[0]
            });

            bind_receiver_session_9.on('connect', function() {

                bind_receiver_session_9.bind_receiver({
                    system_id: 'AdcanOPEN',//smpp_details.system_id, //SMPP - Settings Table
                    password: 'Ad7n37k1S',//smpp_details.password, //SMPP - Settings Table
                }, (pdu) => {
                    if (pdu.command_status == 0) {
                        console.log('Session-9 Received Successfully bound')

                    }
                });

            });

            bind_receiver_session_9.on('pdu', function(pdu) {

                // incoming SMS from SMSC
                if (pdu.command == 'deliver_sm') {

                    var delivery_response = {};

                    var fromNumber = pdu.source_addr.toString();
                    var toNumber = pdu.destination_addr.toString();

                    var text = '';
                    var receipted_message_id = '';
                    var user_message_reference = '';

                    if (pdu.short_message && pdu.short_message.message) {
                        text = pdu.short_message.message;
                        receipted_message_id = pdu.short_message.message_payload;
                        user_message_reference = pdu.short_message.user_message_reference;
                    }

                    console.log('Delivery Response-1');
                    //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                    var parse_deliver_text_str = text.toString();
                    var receipted_id = receipted_message_id;

                    const querystring = require('querystring');
                    var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });

                    var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });
                    var sequence_number = pdu.sequence_number;

                    var msg_id = parse_deliver_text_json.id;
                    delivery_response['message_id'] = msg_id;
                    delivery_response['service_type'] = pdu.service_type;
                    delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                    delivery_response['protocol_id'] = pdu.protocol_id;
                    delivery_response['priority_flag'] = pdu.priority_flag;
                    delivery_response['message_status'] = parse_deliver_text_json.stat;
                    delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                    delivery_response['sub'] = parse_deliver_text_json.sub;
                    delivery_response['message_err'] = parse_deliver_text_json.err;
                    delivery_response['deliver_sm_data'] = pdu;
                    delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                    delivery_response['receiver_session_name'] = 'session_1';
                    delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];                     
                    delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Calcutta'
                    });
                  
                    console.log("Message was sent-delivery_response!")
                    ch.sendToQueue(QUEUE_NAME, Buffer.from(JSON.stringify(delivery_response)));

                    // Reply to SMSC that we received and processed the SMS
                    bind_receiver_session_9.deliver_sm_resp({
                        'sequence_number': sequence_number
                    });
                    console.log('Reply to SMSC=' + sequence_number);

                }

            });

            bind_receiver_session_9.on('close', function() {
                //console.log('Receiver is now disconnected');
                bind_receiver_session_9.connect();
            });

            bind_receiver_session_9.on('enquire_link', function(pdu) {
                console.log('enquire_link')
                bind_receiver_session_9.send(pdu.response());
            });

            bind_receiver_session_9.on('unbind', function(pdu) {
                console.log('unbind')
                bind_receiver_session_9.send(pdu.response());
                bind_receiver_session_9.close();
            });

            bind_receiver_session_9.on('error', function(error) {
                console.log('smpp error', error)
            });

            /* Yoursmsapp Bind Receiver Session - 9 ( Stop ) */

           /* Yoursmsapp Bind Receiver Session - 10 ( Start ) */

            var bind_receiver_session_10 = new smpp.Session({
                host: '145.239.245.15',//smpp_details.ip_address,
                port: '8892'//smpp_details.host[0]
            });

            bind_receiver_session_10.on('connect', function() {

                bind_receiver_session_10.bind_receiver({
                    system_id: 'AdcanOPEN',//smpp_details.system_id, //SMPP - Settings Table
                    password: 'Ad7n37k1S',//smpp_details.password, //SMPP - Settings Table
                }, (pdu) => {
                    if (pdu.command_status == 0) {
                        console.log('Session-10 Received Successfully bound')

                    }
                });

            });

            bind_receiver_session_10.on('pdu', function(pdu) {

                // incoming SMS from SMSC
                if (pdu.command == 'deliver_sm') {

                    var delivery_response = {};

                    var fromNumber = pdu.source_addr.toString();
                    var toNumber = pdu.destination_addr.toString();

                    var text = '';
                    var receipted_message_id = '';
                    var user_message_reference = '';

                    if (pdu.short_message && pdu.short_message.message) {
                        text = pdu.short_message.message;
                        receipted_message_id = pdu.short_message.message_payload;
                        user_message_reference = pdu.short_message.user_message_reference;
                    }

                    console.log('Delivery Response-1');
                    //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                    var parse_deliver_text_str = text.toString();
                    var receipted_id = receipted_message_id;

                    const querystring = require('querystring');
                    var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });

                    var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                        decodeURIComponent: s => s.trim()
                    });
                    var sequence_number = pdu.sequence_number;

                    var msg_id = parse_deliver_text_json.id;
                    delivery_response['message_id'] = msg_id;
                    delivery_response['service_type'] = pdu.service_type;
                    delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                    delivery_response['protocol_id'] = pdu.protocol_id;
                    delivery_response['priority_flag'] = pdu.priority_flag;
                    delivery_response['message_status'] = parse_deliver_text_json.stat;
                    delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                    delivery_response['sub'] = parse_deliver_text_json.sub;
                    delivery_response['message_err'] = parse_deliver_text_json.err;
                    delivery_response['deliver_sm_data'] = pdu;
                    delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                    delivery_response['receiver_session_name'] = 'session_1';
                    delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];                     
                    delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Calcutta'
                    });
                  
                    console.log("Message was sent-delivery_response!")
                    ch.sendToQueue(QUEUE_NAME, Buffer.from(JSON.stringify(delivery_response)));

                    // Reply to SMSC that we received and processed the SMS
                    bind_receiver_session_10.deliver_sm_resp({
                        'sequence_number': sequence_number
                    });
                    console.log('Reply to SMSC=' + sequence_number);

                }

            });

            bind_receiver_session_10.on('close', function() {
                //console.log('Receiver is now disconnected');
                bind_receiver_session_10.connect();
            });

            bind_receiver_session_10.on('enquire_link', function(pdu) {
                console.log('enquire_link')
                bind_receiver_session_10.send(pdu.response());
            });

            bind_receiver_session_10.on('unbind', function(pdu) {
                console.log('unbind')
                bind_receiver_session_10.send(pdu.response());
                bind_receiver_session_10.close();
            });

            bind_receiver_session_10.on('error', function(error) {
                console.log('smpp error', error)
            });

            /* Yoursmsapp Bind Receiver Session -10 ( Stop ) */
          });

        }).catch(function(err) {
            return console.log("Delivery Message was rejected...  delivery_response!");
        });

    })();

});


module.exports = router;