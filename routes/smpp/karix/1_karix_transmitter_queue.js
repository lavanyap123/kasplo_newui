const express = require('express');
const router = express.Router();
const {
    ensureAuthenticated
} = require('../../../config/auth');
const bcrypt = require('bcryptjs');
const passport = require('passport');
var json2csv = require('json2csv'); //not working
var csv_export = require('csv-export');
const stringify = require('csv-stringify');
var autoIncrement = require("mongodb-autoincrement");
var smpp = require('smpp');
var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
// Dashboard
var await = require("await");
var async = require("async");
var Hashids = require('hashids');
var nodemailer = require('nodemailer');
var ObjectID = require('mongodb').ObjectId;

const crypto = require('crypto');
var algorithm = 'aes256';
var key = 'password';
var activityController = require('./../../../controllers/activity_ctrl');
// Load User model
const User = require('../../../models/user');
// DB Config
const db = require('../../../config/keys').mongoURI;
const db_options = require('../../../config/keys').db_options;
const database_name = require('../../../config/keys').database_name;
var cron = require('node-cron');
var ISODate = require("isodate");

var constants = require('./../../../config/constants');
var keys = require('./../../../config/keys');
var redis_config= require('./../../../config/keys').redis_config;
let kue   = require('kue');
let queue = kue.createQueue(redis_config);

console.log('1_karix_transmitter_queue');
console.log(redis_config);

queue.setMaxListeners(2000000); // <- golden method
queue.watchStuckJobs(100);

MongoClient.connect(db, db_options).then(client => {

    const db = client.db(database_name);

    (async () => {

        const sms_logs = db.collection('sms_logs');
        const smpp_collection = db.collection('smpp_details');

        var smpp_details = await smpp_collection.findOne({
            'smpp_id': "1" //Karix
        });

        var karix_tps = smpp_details.tps;
        var tps_limit = karix_tps - 2;


        /* Karix Bind Transmitter Session -1 ( Start ) */

        var session_1 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        delete smpp.encodings.ASCII;

        session_1.on('connect', function() {
            session_1.bind_transmitter({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password, //SMPP - Settings Table
                interface_version: 1,
                system_type: '380666000600',
                address_range: '+380666000600',
                addr_ton: 1,
                addr_npi: 1
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    console.log('Transceiver-1 Successfully bound');
                    /* Kue Job - Worker - Start */
                    sendSMS(session_1, sms_logs)
                    /* Kue Job - Worker - Stop */

                }
            });

        });


        session_1.on('close', function() {
            console.log('Transceiver is now disconnected');
            session_1.connect();
        });

        session_1.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            session_1.send(pdu.response());
        });

        session_1.on('unbind', function(pdu) {
            console.log('unbind')
            session_1.send(pdu.response());
            session_1.close();
        });

        session_1.on('error', function(error) {
            console.log('smpp error', error);
        });


        /* Karix Bind Transmitter Session -1 ( Stop ) */


    })();

});

function sendSMS(session, sms_logs) {

    queue.process('sms_logs',98, function(queue_document, done){
       
        console.log('queue_document');
        console.log(queue_document);

        var find_query = {
            '_id': ObjectID(queue_document.data._id)
        };

       //console.log('sms_logs-Queue-Inserting Running');
       //console.log(find_query);

        session.submit_sm({
            source_addr: queue_document.data.sender_id, //SENDER ID
            destination_addr: queue_document.data.mobile_no, // Mobile Number
            short_message: queue_document.data.sms_content, // Text SMS
            registered_delivery: 1, //Mandatory Send Delivery Response
        }, function(pdu) {

            if (pdu.command_status == 0) {

            /* Add Queue - Submit Response - Start */

                var queue_pdu_response = {};
                queue_pdu_response['submit_log_id'] = queue_document.data.campaign_id+'-'+queue_document.data.campaign_content_label_id+'-'+queue_document.data.mobile_no;
                queue_pdu_response['queue_job_id'] = queue_document.id;
                queue_pdu_response['command_id'] = pdu.command_id;
                queue_pdu_response['command_status'] = pdu.command_status;
                queue_pdu_response['sequence_number'] = pdu.sequence_number;
                queue_pdu_response['message_id'] = pdu.message_id;
                queue_pdu_response['submit_sm_resp'] = pdu;
                queue_pdu_response['submit_session_name'] = 'session_1';
                queue_pdu_response['message_status'] = 'PENDING'; //default message status
                queue_pdu_response['message_type'] = 'Text SMS'; //default message status
                queue_pdu_response['updated_at'] = new Date();
                queue_pdu_response['message_update_date'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });

                queue.create('sms_submit_logs',queue_pdu_response)
                //.removeOnComplete(true)                
                .searchKeys(['submit_log_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

            /* Add Queue - Submit Response - Start */

                // Message successfully sent
                var pdu_response = {};
                pdu_response['queue_job_id'] = queue_document.id;
                pdu_response['submit_log_id'] = queue_document.data.campaign_id+'-'+queue_document.data.campaign_content_label_id+'-'+queue_document.data.mobile_no;
                pdu_response['command_id'] = pdu.command_id;
                pdu_response['command_status'] = pdu.command_status;
                pdu_response['sequence_number'] = pdu.sequence_number;
                pdu_response['message_id'] = pdu.message_id;
                pdu_response['submit_sm_resp'] = pdu;
                pdu_response['submit_session_name'] = 'session_1';
                pdu_response['message_status'] = 'PENDING'; //default message status
                pdu_response['message_type'] = 'Text SMS'; //default message status
                pdu_response['updated_at'] = new Date();
                pdu_response['message_update_date'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });

                /* Update - Message Status & Delivery Response - Start */
                updatingSubmitResponse(
                    find_query,
                    pdu_response,
                    sms_logs
                );
                /* Update - Message Status & Delivery Response - Stop */

            }

        }); //submit_sm session - closed

        /* Queue Job Done - Completed - (Start) */
        done();
        /* Queue Job Done  - Completed - (Stop) */

    });
}

function updatingSubmitResponse(find_query, update_data, sms_logs) {

    sms_logs.findOneAndUpdate(
        find_query, {
            $set: update_data
        }, {
            multi: true,
            returnOriginal: false,
            writeConcern: {
                w: 0
            }
        });
}


module.exports = router;