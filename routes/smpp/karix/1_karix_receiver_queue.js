const express = require('express');
const router = express.Router();
const {
    ensureAuthenticated
} = require('../../../config/auth');
const bcrypt = require('bcryptjs');
const passport = require('passport');
var json2csv = require('json2csv'); //not working
var csv_export = require('csv-export');
const stringify = require('csv-stringify');
var autoIncrement = require("mongodb-autoincrement");
var smpp = require('smpp');
var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
// Dashboard
var await = require("await");
var async = require("async");
var Hashids = require('hashids');
var nodemailer = require('nodemailer');
var ObjectID = require('mongodb').ObjectId;

const crypto = require('crypto');
var algorithm = 'aes256';
var key = 'password';
var activityController = require('./../../../controllers/activity_ctrl');
// Load User model
const User = require('../../../models/user');
// DB Config
const db = require('../../../config/keys').mongoURI;
const db_options = require('../../../config/keys').db_options;
const database_name = require('../../../config/keys').database_name;
var redis_config= require('./../../../config/keys').redis_config;
var cron = require('node-cron');
var ISODate = require("isodate");

var constants = require('./../../../config/constants');
var keys = require('./../../../config/keys');

/* Queue & Adding to Redis Cache - (Start) */
let kue   = require('kue');
let queue = kue.createQueue(redis_config);
/* Queue & Adding to Redis Cache - (Stop) */
queue.setMaxListeners(1000) // <- golden method
queue.watchStuckJobs(100);
//event handler. Called when job is saved to the Redis.
queue.on('job enqueue', function(){
    console.log(`Job Submitted in the Queue.`);
    //process.exit(0);
});


MongoClient.connect(db, db_options).then(client => {

    const db = client.db(database_name);
    const sms_logs = db.collection('sms_logs');
    const dlr_logs = db.collection('dlr_logs');
    const smpp_collection = db.collection('smpp_details');

    (async () => {

 
        var smpp_details = await smpp_collection.findOne({
            'smpp_id': "1" //Karix
        });


        /* Karix Bind Receiver Session -1 ( Start ) */

        var bind_receiver_session_1 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_1.on('connect', function() {

            bind_receiver_session_1.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    console.log('Session-1 Received Successfully bound')

                }
            });

        });

        bind_receiver_session_1.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-1');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['receiver_session_name'] = 'session_1';
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });

                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_1.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);

            }

        });

        bind_receiver_session_1.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_1.connect();
        });

        bind_receiver_session_1.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_1.send(pdu.response());
        });

        bind_receiver_session_1.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_1.send(pdu.response());
            bind_receiver_session_1.close();
        });

        bind_receiver_session_1.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -1 ( Stop ) */




        /* Karix Bind Receiver Session -2 ( Start ) */

        var bind_receiver_session_2 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_2.on('connect', function() {

            bind_receiver_session_2.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    console.log('Session-2 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_2.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-2');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_2';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });


                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_2.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);
            }

        });

        bind_receiver_session_2.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_2.connect();
        });

        bind_receiver_session_2.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_2.send(pdu.response());
        });

        bind_receiver_session_2.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_2.send(pdu.response());
            bind_receiver_session_2.close();
        });

        bind_receiver_session_2.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -2 ( Stop ) */



        /* Karix Bind Receiver Session -3 ( Start ) */

        var bind_receiver_session_3 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_3.on('connect', function() {

            bind_receiver_session_3.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    console.log('Session-3 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_3.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-3');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_3';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });


                queue.create('delivery_logs',delivery_response)
                .searchKeys(['message_id'])
                //.removeOnComplete(true)
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_3.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);

            }

        });

        bind_receiver_session_3.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_3.connect();
        });

        bind_receiver_session_3.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_3.send(pdu.response());
        });

        bind_receiver_session_3.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_3.send(pdu.response());
            bind_receiver_session_3.close();
        });

        bind_receiver_session_3.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -3 ( Stop ) */


        /* Karix Bind Receiver Session -4 ( Start ) */

        var bind_receiver_session_4 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_4.on('connect', function() {

            bind_receiver_session_4.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    console.log('Session-4 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_4.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-4');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_4';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });

                queue.create('delivery_logs',delivery_response)
                .searchKeys(['message_id'])
                //.removeOnComplete(true)
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_4.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);

            }

        });

        bind_receiver_session_4.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_4.connect();
        });

        bind_receiver_session_4.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_4.send(pdu.response());
        });

        bind_receiver_session_4.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_4.send(pdu.response());
            bind_receiver_session_4.close();
        });

        bind_receiver_session_4.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -4 ( Stop ) */


        /* Karix Bind Receiver Session -5 ( Start ) */

        var bind_receiver_session_5 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_5.on('connect', function() {

            bind_receiver_session_5.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    console.log('Session-5 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_5.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-5');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_5';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });

                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();


                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_5.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);
            }

        });

        bind_receiver_session_5.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_5.connect();
        });

        bind_receiver_session_5.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_5.send(pdu.response());
        });

        bind_receiver_session_5.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_5.send(pdu.response());
            bind_receiver_session_5.close();
        });

        bind_receiver_session_5.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -5 ( Stop ) */


        /* Karix Bind Receiver Session -6 ( Start ) */

        var bind_receiver_session_6 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_6.on('connect', function() {

            bind_receiver_session_6.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    console.log('Session-6 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_6.on('pdu', function(pdu) {

            console.log(pdu.command);
            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-6');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_6';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });

                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_6.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);
            }

        });

        bind_receiver_session_6.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_6.connect();
        });

        bind_receiver_session_6.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_6.send(pdu.response());
        });

        bind_receiver_session_6.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_6.send(pdu.response());
            bind_receiver_session_6.close();
        });

        bind_receiver_session_6.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -6 ( Stop ) */

        /* Karix Bind Receiver Session -7 ( Start ) */

        var bind_receiver_session_7 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_7.on('connect', function() {

            bind_receiver_session_7.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    //console.log('Received Successfully bound')
                    console.log('Session-7 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_7.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-7');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_7';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });

                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_7.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);

            }

        });

        bind_receiver_session_7.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_7.connect();
        });

        bind_receiver_session_7.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_7.send(pdu.response());
        });

        bind_receiver_session_7.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_7.send(pdu.response());
            bind_receiver_session_7.close();
        });

        bind_receiver_session_7.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -7 ( Stop ) */


        /* Karix Bind Receiver Session -8 ( Start ) */

        var bind_receiver_session_8 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_8.on('connect', function() {

            bind_receiver_session_8.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    //console.log('Received Successfully bound')
                    console.log('Session-8 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_8.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-8');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_8';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });


                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_8.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);

            }

        });

        bind_receiver_session_8.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_8.connect();
        });

        bind_receiver_session_8.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_8.send(pdu.response());
        });

        bind_receiver_session_8.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_8.send(pdu.response());
            bind_receiver_session_8.close();
        });

        bind_receiver_session_8.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -8 ( Stop ) */



        /* Karix Bind Receiver Session -9 ( Start ) */

        var bind_receiver_session_9 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_9.on('connect', function() {

            bind_receiver_session_9.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    //console.log('Received Successfully bound')
                    console.log('Session-9 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_9.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-9');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_9';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });

                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_9.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);

            }

        });

        bind_receiver_session_9.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_9.connect();
        });

        bind_receiver_session_9.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_9.send(pdu.response());
        });

        bind_receiver_session_9.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_9.send(pdu.response());
            bind_receiver_session_9.close();
        });

        bind_receiver_session_9.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -9 ( Stop ) */




        /* Karix Bind Receiver Session -10 ( Start ) */

        var bind_receiver_session_10 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_10.on('connect', function() {

            bind_receiver_session_10.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    //console.log('Received Successfully bound')
                    console.log('Session-10 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_10.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-10');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_10';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });


                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_10.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);

            }

        });

        bind_receiver_session_10.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_10.connect();
        });

        bind_receiver_session_10.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_10.send(pdu.response());
        });

        bind_receiver_session_10.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_10.send(pdu.response());
            bind_receiver_session_10.close();
        });

        bind_receiver_session_10.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -10 ( Stop ) */        


        /* Karix Bind Receiver Session -11 ( Start ) */

        var bind_receiver_session_11 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_11.on('connect', function() {

            bind_receiver_session_11.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    //console.log('Received Successfully bound')
                    console.log('Session-11 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_11.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-11');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_11';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });

                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();


                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_11.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);
            }

        });

        bind_receiver_session_11.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_11.connect();
        });

        bind_receiver_session_11.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_11.send(pdu.response());
        });

        bind_receiver_session_11.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_11.send(pdu.response());
            bind_receiver_session_11.close();
        });

        bind_receiver_session_11.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -11 ( Stop ) */  


        /* Karix Bind Receiver Session -12 ( Start ) */

        var bind_receiver_session_12 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_12.on('connect', function() {

            bind_receiver_session_12.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    //console.log('Received Successfully bound')
                    console.log('Session-12 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_12.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-12');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_12';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });


                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_12.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);

            }

        });

        bind_receiver_session_12.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_12.connect();
        });

        bind_receiver_session_12.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_12.send(pdu.response());
        });

        bind_receiver_session_12.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_12.send(pdu.response());
            bind_receiver_session_12.close();
        });

        bind_receiver_session_12.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -12 ( Stop ) */  


        /* Karix Bind Receiver Session -13 ( Start ) */

        var bind_receiver_session_13 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_13.on('connect', function() {

            bind_receiver_session_13.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    //console.log('Received Successfully bound')
                    console.log('Session-13 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_13.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-13');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_13';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });

                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_13.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);
            }

        });

        bind_receiver_session_13.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_13.connect();
        });

        bind_receiver_session_13.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_13.send(pdu.response());
        });

        bind_receiver_session_13.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_13.send(pdu.response());
            bind_receiver_session_13.close();
        });

        bind_receiver_session_13.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -13 ( Stop ) */  



        /* Karix Bind Receiver Session -14 ( Start ) */

        var bind_receiver_session_14 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_14.on('connect', function() {

            bind_receiver_session_14.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    //console.log('Received Successfully bound')
                    console.log('Session-14 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_14.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-14');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_14';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });

                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_14.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);

            }

        });

        bind_receiver_session_14.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_14.connect();
        });

        bind_receiver_session_14.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_14.send(pdu.response());
        });

        bind_receiver_session_14.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_14.send(pdu.response());
            bind_receiver_session_14.close();
        });

        bind_receiver_session_14.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -14 ( Stop ) */  


        /* Karix Bind Receiver Session -15 ( Start ) */

        var bind_receiver_session_15 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_15.on('connect', function() {

            bind_receiver_session_15.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    //console.log('Received Successfully bound')
                    console.log('Session-15 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_15.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-15');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_15';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });

                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)                
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_15.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);

            }

        });

        bind_receiver_session_15.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_15.connect();
        });

        bind_receiver_session_15.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_15.send(pdu.response());
        });

        bind_receiver_session_15.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_15.send(pdu.response());
            bind_receiver_session_15.close();
        });

        bind_receiver_session_15.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -15 ( Stop ) */  

        /* Karix Bind Receiver Session -16 ( Start ) */

        var bind_receiver_session_16 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_16.on('connect', function() {

            bind_receiver_session_16.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    //console.log('Received Successfully bound')
                    console.log('Session-16 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_16.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-16');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_16';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });


                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();


                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_16.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);
            }

        });

        bind_receiver_session_16.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_16.connect();
        });

        bind_receiver_session_16.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_16.send(pdu.response());
        });

        bind_receiver_session_16.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_16.send(pdu.response());
            bind_receiver_session_16.close();
        });

        bind_receiver_session_16.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -16 ( Stop ) */  

        /* Karix Bind Receiver Session -17 ( Start ) */

        var bind_receiver_session_17 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_17.on('connect', function() {

            bind_receiver_session_17.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    //console.log('Received Successfully bound')
                    console.log('Session-17 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_17.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-17');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_17';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });

                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_17.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);
            }

        });

        bind_receiver_session_17.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_17.connect();
        });

        bind_receiver_session_17.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_17.send(pdu.response());
        });

        bind_receiver_session_17.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_17.send(pdu.response());
            bind_receiver_session_17.close();
        });

        bind_receiver_session_17.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -17 ( Stop ) */  

        /* Karix Bind Receiver Session -18 ( Start ) */

        var bind_receiver_session_18 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_18.on('connect', function() {

            bind_receiver_session_18.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    //console.log('Received Successfully bound')
                    console.log('Session-18 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_18.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-18');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_18';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });


                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_18.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);


            }

        });

        bind_receiver_session_18.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_18.connect();
        });

        bind_receiver_session_18.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_18.send(pdu.response());
        });

        bind_receiver_session_18.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_18.send(pdu.response());
            bind_receiver_session_18.close();
        });

        bind_receiver_session_18.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -18 ( Stop ) */  

        /* Karix Bind Receiver Session -19 ( Start ) */

        var bind_receiver_session_19 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_19.on('connect', function() {

            bind_receiver_session_19.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    //console.log('Received Successfully bound')
                    console.log('Session-19 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_19.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-19');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_19';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });

                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_19.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);


            }

        });

        bind_receiver_session_19.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_19.connect();
        });

        bind_receiver_session_19.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_19.send(pdu.response());
        });

        bind_receiver_session_19.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_19.send(pdu.response());
            bind_receiver_session_19.close();
        });

        bind_receiver_session_19.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -19 ( Stop ) */  

        /* Karix Bind Receiver Session -20 ( Start ) */

        var bind_receiver_session_20 = new smpp.Session({
            host: smpp_details.ip_address,
            port: smpp_details.host[0]
        });

        bind_receiver_session_20.on('connect', function() {

            bind_receiver_session_20.bind_receiver({
                system_id: smpp_details.system_id, //SMPP - Settings Table
                password: smpp_details.password,  //SMPP - Settings Table
            }, (pdu) => {
                if (pdu.command_status == 0) {
                    //console.log('Received Successfully bound')
                    console.log('Session-20 Received Successfully bound')
                }
            });

        });

        bind_receiver_session_20.on('pdu', function(pdu) {

            // incoming SMS from SMSC
            if (pdu.command == 'deliver_sm') {

                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference = '';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference = pdu.short_message.user_message_reference;
                }

                console.log('Delivery Response-20');
                //console.log('SMS ' + fromNumber + ' -> ' + toNumber + ': ' + text);

                var parse_deliver_text_str = text.toString();
                var receipted_id = receipted_message_id;

                const querystring = require('querystring');
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {
                    decodeURIComponent: s => s.trim()
                });
                var sequence_number = pdu.sequence_number;

                var msg_id = parse_deliver_text_json.id;
                delivery_response['message_id'] = msg_id;
                delivery_response['service_type'] = pdu.service_type;
                delivery_response['delivery_sequence_number'] = pdu.sequence_number;
                delivery_response['protocol_id'] = pdu.protocol_id;
                delivery_response['priority_flag'] = pdu.priority_flag;
                delivery_response['message_status'] = parse_deliver_text_json.stat;
                delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                delivery_response['sub'] = parse_deliver_text_json.sub;
                delivery_response['message_err'] = parse_deliver_text_json.err;
                delivery_response['deliver_sm_data'] = pdu;
                delivery_response['receiver_session_name'] = 'session_20';
                delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                });

                queue.create('delivery_logs',delivery_response)
                //.removeOnComplete(true)
                .searchKeys(['message_id'])
                .attempts(3) // if job fails retry 3 times
                .backoff({delay: 60 * 1000}) // wait 60s before retry
                .save();

                // Reply to SMSC that we received and processed the SMS
                bind_receiver_session_20.deliver_sm_resp({
                    'sequence_number': sequence_number
                });
                console.log('Reply to SMSC=' + sequence_number);

            }

        });

        bind_receiver_session_20.on('close', function() {
            //console.log('Receiver is now disconnected');
            bind_receiver_session_20.connect();
        });

        bind_receiver_session_20.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            bind_receiver_session_20.send(pdu.response());
        });

        bind_receiver_session_20.on('unbind', function(pdu) {
            console.log('unbind')
            bind_receiver_session_20.send(pdu.response());
            bind_receiver_session_20.close();
        });

        bind_receiver_session_20.on('error', function(error) {
            console.log('smpp error', error)
        });

        /* Karix Bind Receiver Session -20 ( Stop ) */  


    })();
});


module.exports = router;