const express             = require('express');
var mongoose              = require('mongoose');
var MongoClient           = require('mongodb').MongoClient;
var autoIncrement         = require("mongodb-autoincrement");

const router              = express.Router();
const {
    ensureAuthenticated, roleAuthenticated
} = require('../config/auth');

// DB Config
const db                  = require('../config/keys').mongoURI;
const db_options          = require('../config/keys').db_options;
const database_name       = require('../config/keys').database_name;

var activityController    = require('./../controllers/activity_ctrl');
var ObjectId              = require('mongodb').ObjectId;

var functions_handler     = require('./../helpers/functions_handler');
var shortlink             = require('shortlink');

// city - List
router.get('/city', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount,  activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db         = client.db(database_name);
    const collection = db.collection('city');

    collection.find().toArray(function(err, city) {
    res.render('settings/city/index', {
      permissions   : req.permissions,
      city,
      user          : req.user,
      activity_logs : req.activity_logs,
      activity_logs_count : req.activity_logs_count,
    });
    client.close();
    });
  });
});

// state - List
router.get('/state', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount,  activityController.getRecentAcitityLogs, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const collection = db.collection('state');

            collection.find().toArray(function(err, state) {
                res.render('settings/state/index', {
                    permissions  : req.permissions,
                    state,
                    user: req.user,
                    activity_logs: req.activity_logs,
                    activity_logs_count: req.activity_logs_count,
                });
                client.close();
            });

        });

});

// country - List
router.get('/country', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {


    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const collection = db.collection('country');

            collection.find().toArray(function(err, country) {
                res.render('settings/country/index', {
                    country,
                    user: req.user,
                    activity_logs: req.activity_logs,
                    activity_logs_count: req.activity_logs_count,
                });
                client.close();
            });


        });

});

// nationalities - List
router.get('/nationalities', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {


    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const collection = db.collection('nationalities');

            collection.find().toArray(function(err, nationalities) {
                res.render('settings/nationalities/index', {
                    nationalities,
                    user: req.user,
                    activity_logs: req.activity_logs,
                    activity_logs_count: req.activity_logs_count,
                });
                client.close();
            });

            //
        });

});

// test sms - List
router.get('/tst_sm', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);
    const collection = db.collection('add_mobile_numbers');

    collection.find().toArray(function(err, numbers) {
      res.render('settings/test_sms', {
        mobile_numbers:numbers,
        user: req.user,
        activity_logs: req.activity_logs,
        activity_logs_count: req.activity_logs_count,
      });
    });

    client.close();
  });

});

router.post('/edit_test_sms_popup', ensureAuthenticated, (req, res) => {

    var type = req.body.type;
    var id = req.body.id;
    var o_id = new ObjectId(id);

    MongoClient.connect(db, db_options).then(client => {
                const db = client.db(database_name);
                const collection = db.collection('add_mobile_numbers');

                collection.findOne({
                    '_id': o_id
                }).then(function(doc) {


                    if (!doc) {
                        throw new Error('No record found.');
                    } else {
                        
                        var test_sms_html_content = '<div class="modal-header"> <h5 class="modal-title" id="'+ id +'">Update Test SMS </h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><div id="sm_notification"></div> <form id="update_tst_sm" type="post"><input type="hidden" id="update_test_sms_id" value="' + id + '"> <div class="row"> <div class="col-12"> <div class="form-group"> <label for="exampleInputEmail1">Name</label> <input type="text" class="form-control" name="test_sms_name" id="test_sms_name" placeholder="Enter Name" value="' + doc.name + '"> </div><div class="form-group"> <label for="exampleInputEmail1">Mobile Number</label><input type="text" class="form-control" name="mbl_num" id="mbl_num" placeholder="Enter Mobile Number" value="' + doc.mobile_number + '">  </div></div></div><div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> <button type="submit" class="btn btn-primary btn_update_test_sms" id="'+ id +'">Update</button><img src="/public/images/loader_form.gif" id="loader" class="img-responsive" style="width: 25px;display:none;"/> </form> </div></div></div>';

                      }

                    res.send({
                        'popup_content': test_sms_html_content
                    });

                });

                client.close();

            });
});

router.post('/update_test_sms', ensureAuthenticated, (req, res) => {
   
    var id = req.body.id;
    var o_id = new ObjectId(id);
    var name = req.body.name;
    var mobile_number = req.body.mobile_number;

    MongoClient.connect(db, db_options).then(client => {
      const db = client.db(database_name);
      const collection = db.collection('add_mobile_numbers');
      
      const update_mbl_num = collection.updateOne({'_id':o_id}, {$set:{'name':name, 'mobile_number':mobile_number}});

      res.send({
        'status': 200,
        'message': 'success'
      });
      client.close();
    });
});

// test sms - Delete
router.get('/delete_test_sms/:id', ensureAuthenticated, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            var id = req.params.id;
            var o_id = new ObjectId(id);
            db.collection('add_mobile_numbers', function(err, collection) {
                collection.deleteOne({
                    _id: o_id
                });
            });
            res.json({
                'status': 200,
                'msg':'success'
            });
            client.close();
        });

});


/******************** Role implementation methods *****************/
// role - List
router.get('/roles', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount,  activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db               = client.db(database_name);
    const roles_collection = db.collection('roles');
    const menu_type_coll   = db.collection('menu_types');

    (async() => {
      var get_roles        = await roles_collection.find({}).sort({'_id' : -1}).toArray(); 
      var get_menu_types   = await menu_type_coll.find({'menu_type_status':'1'}).toArray();
    
      res.render('settings/menu/roles', {
        permissions         : req.permissions,
        user                : req.user,
        get_roles           : get_roles,
        get_menu_types,
        activity_logs       : req.activity_logs,
        activity_logs_count : req.activity_logs_count,
      });

      client.close();
    })();
    
  });
}); //roles


/**
* route: /role_insert_update
* method: POST
* description: Inserting/updating/deleting  roles data to database
* params: role_name, role_status
* return: status_code, message
*/
router.post('/role_insert_update',ensureAuthenticated, function(req, res, next) {

  MongoClient.connect(db, db_options).then(client => {
    const db         = client.db(database_name);
    const collection = db.collection('roles');
    var role_name    = req.body.role_name;
    var role_status  = req.body.role_status;
    var menu_type    = req.body.menu_type;
    var role_desc    = req.body.role_desc;
    var id           = req.body.id;
    var status_code  = 400;
    var message      = 'Error while submitting data';
    var user_id      = req.user._id;

    var permissions  = req.body.permissions;

    var get_nested_per = functions_handler.getNestedMenuList(permissions);
   
    (async () => {

      var data      = {
        'role_name'        : role_name,
        'role_status'      : role_status,
        'menu_type_id'     : menu_type,
        'role_description' : role_desc,
      }

      if (id) {
        data['permissions']   = get_nested_per;
        data['updated_by']    = user_id;
        data['updated_at']    = functions_handler.getCurrentIndianDateTime();

        var o_id        = new ObjectId(id);
        const role_data = await collection.find({'_id': o_id}).toArray(); 

        if (role_data) {
          collection.updateOne({_id: o_id}, // Filter
                    {
                      $set: data
                    }, // Update
                    {
                      upsert: false
                    }
                );
          status_code = 200;
          message     = 'Data successfully updated';
        } //role_data 
      } else {
        data['permissions']  = get_nested_per;
        data['created_by']   = user_id;
        data['created_at']   = functions_handler.getCurrentIndianDateTime();

        collection.insertOne(data); // Filter      
        status_code = 200; 
        message     = 'Data successfully added';             
      }//o_id 
      
      res.send({
        'status'        : status_code, 
        'message'       : message
      });

      client.close();
      })();
    });
}); //role_insert_update


/**
* route: /get_role_data_by_id
* method: POST
* description: Getting the role data, menus, sub menus data by id
* params: role_id
* return: status_code, message, roles_data
*/
router.post('/get_role_data_by_id', ensureAuthenticated, activityController.getActivityTotalCount,  activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db               = client.db(database_name);
    const roles_collection = db.collection('roles');
    var id                 = req.body.id;
    var o_id               = new ObjectId(id);
    var status_code        = 400;
    var message            = 'Error while fetching data';
    (async() => {
      var get_roles_by_id    = await roles_collection.find({'_id' : o_id}).toArray(); 
      console.log('==== get role data by id ====');
      console.log(get_roles_by_id);

      if (get_roles_by_id.length > 0) {
        status_code          = 200;
        message              = 'Successfully fetched data';   
      }
      res.send({
        'status'     : status_code,
        'message'    : message,
        'roles_data' : get_roles_by_id
      })

    client.close();
    })();
  });
}); // get_role_data_by_id

/* ============ dynamic menu methods starts =========== */
/**
* route: /menu
* method: POST
* description: Getting menu page
* return: menu page with required params
*/
router.get('/menu', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount,  activityController.getRecentAcitityLogs, (req, res, next) => {
  //console.log(req)
  MongoClient.connect(db, db_options).then(client => {
    const db               = client.db(database_name);
    const menus_collection = db.collection('main_menus');
    const menu_types_collection = db.collection('menu_types');
   
    (async() => {
      var get_menus          = await menus_collection.find({}).sort({'_id' : -1}).toArray(); 
      var get_menu_types     = await menu_types_collection.find({'menu_type_status' : '1'}).sort({'_id' : -1}).toArray(); 
      

      res.render('settings/menu/main_menus', {
        
        permissions         : req.permissions,
        user                : req.user,
        get_menus           : get_menus,
        get_menu_types      : get_menu_types,
        activity_logs       : req.activity_logs,
        activity_logs_count : req.activity_logs_count,
      });
      client.close();
    })();
  });
}); // roles


/**
* route: /main_menu_insert_update
* method: POST
* description: Inserting/updating/deleting  main menus data to database
* params: id, menu_name, menu_type_id, menu_type_name, menu_url, menu_icon, menu_sort_no, menu_status
* return: status_code, message
*/
router.post('/main_menu_insert_update', ensureAuthenticated, function(req, res, next) {

  MongoClient.connect(db, db_options).then(client => {
    const db         = client.db(database_name);
    const collection = db.collection('main_menus');
    var id           = req.body.main_menu_id; 
    var menu_name    = req.body.menu_name;
    var menu_type_id = req.body.menu_type_id;
    var menu_type_name = req.body.menu_type_name;
    var menu_url     = req.body.menu_url;
    var menu_icon    = req.body.menu_icon;
    var menu_sort_no = req.body.menu_sort_no;
    var menu_status  = req.body.menu_status;

    var status_code  = 400;
    var message      = 'Error while submitting data';
    var user_id      = req.user._id;
    menu_url         = menu_url.replace(/\/$/, ""); //removing last slash from url if exists

    (async () => {

      var data      = {
        'menu_name'    : menu_name,
        'menu_type_id'   : menu_type_id,
        'menu_type_name' : menu_type_name,
        'menu_url'     : menu_url,
        'menu_icon'    : menu_icon,
        'menu_sort_no' : menu_sort_no,
        'menu_status'  : menu_status
      }

      if (id) {
        data['updated_by']    = user_id;
        data['updated_at']    = functions_handler.getCurrentIndianDateTime();
        var bulk_del_ops      = [];
        var o_id              = new ObjectId(id);

        const menu_data = await collection.findOne({
          '_id': o_id
        }); 
        var get_prev_menu_type_ids      = menu_data.menu_type_id; console.log(get_prev_menu_type_ids);
        var current_selec_menu_type_ids = menu_type_id; console.log(current_selec_menu_type_ids);
        
        var get_array_diff   = functions_handler.arrayDiff(current_selec_menu_type_ids, get_prev_menu_type_ids); 
       
        if (menu_data) {
          await collection.updateOne({_id: o_id}, 
                    {
                      $set: data
                    }, 
                    {
                      upsert: false
                    }
                );

          if (get_array_diff.length > 0) {
            for (i = 0; i < get_array_diff.length; i++) {
              var delete_sub_menu_field   = 'sub_menus.' + get_array_diff[i];
              bulk_del_ops.push({ 
                "updateOne" : {
                  "filter" : { "_id" :  o_id},
                  "update" : { "$unset" : {[delete_sub_menu_field] : "" } }
                }
              });  // $unset for deleting object, $pull for deleting array
            }
            if (bulk_del_ops.length > 0) {
              await collection.bulkWrite(bulk_del_ops, { ordered: false});
            }
          }
          
          status_code = 200;
          message     = 'Data successfully updated';
        } //role_data 

      } else {
        data['created_by']    = user_id;
        data['created_at']    = functions_handler.getCurrentIndianDateTime();

        collection.insertOne(data); // Filter      
        status_code = 200; 
        message     = 'Data successfully added';             
      }//o_id 
      
      res.send({
        'status'        : status_code, 
        'message'       : message
      });

      client.close();
      })();
    });
}); //main_menu_insert_update

/**
* route: /get_menu_data_by_id
* method: POST
* description: Getting menu data based on id
* params: menu_id (_id)
* return: status_code, message, menus_data
*/
router.post('/get_menu_data_by_id', ensureAuthenticated, activityController.getActivityTotalCount,  activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db               = client.db(database_name);
    const menus_collection = db.collection('main_menus');
    var id                 = req.body.id;
    var o_id               = new ObjectId(id);
    var status_code        = 400;
    var message            = 'Error while fetching data';
    (async() => {
    var get_menus_by_id    = await menus_collection.find({'_id' : o_id}).toArray(); 

    if (get_menus_by_id.length > 0) {
      status_code          = 200;
      message              = 'Successfully fetched data';   
    }
    res.send({
      'status'     : status_code,
      'message'    : message,
      'menus_data' : get_menus_by_id
    })

    client.close();
    })();
  });
}); // get_menu_data_by_id

/**
* route: /checkMenuNameExists
* method: POST
* description: Checking menu name given already exists or not based on update/add 
* params: id
* return: true/false
*/
router.post('/checkMenuNameExists', ensureAuthenticated, function(req, res, next) {

  MongoClient.connect(db, db_options).then(client => {
    const db         = client.db(database_name);
    const collection = db.collection('main_menus');
    var id           = req.body.id; 
    
    var status_code  = 400;
    var message      = 'Error while submitting data';
    var name         = req.body.menu_name;
    (async () => {

      if (id) {
        var o_id         = new ObjectId(id);
        console.log('o_id');
        console.log(id);
        var name_status = await collection.findOne({'menu_name':new RegExp('^' + name + '$', 'i'), '_id': {$ne: o_id}}); 
      } else {
        var name_status = await collection.findOne({'menu_name':new RegExp('^' + name + '$', 'i')});
      }

      if (name_status) {               
        res.send('false'); // name already exists 
        //return false;
      } else {
        res.send('true');     
        //return true;
      }
      client.close();
      })();
    });
}); //checkMenuNameExists

/**
* route: /menu_types
* method: GET
* description: Getting menu type page
* params: 
* return: renders menu_type page with required data
*/
router.get('/menu_types', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount,  activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db                     = client.db(database_name);
    const menus_types_collection = db.collection('menu_types');
   
    (async() => {
      var get_menu_types         = await menus_types_collection.find({}).sort({'_id' : -1}).toArray(); 

      res.render('settings/menu/menu_types', {
        permissions         : req.permissions,
        user                : req.user,
        get_menu_types,
        activity_logs       : req.activity_logs,
        activity_logs_count : req.activity_logs_count,
      });
      client.close();
    })();
  });
}); // menu_types

/**
* route: /get_menu_type_by_id
* method: POST
* description: Getting menu type by id
* params: _id
* return: status_code, message, menu types data
*/
router.post('/get_menu_type_by_id', ensureAuthenticated, activityController.getActivityTotalCount,  activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db               = client.db(database_name);
    const menu_types_collection = db.collection('menu_types');
    var id                 = req.body.id;
    var o_id               = new ObjectId(id);
    var status_code        = 400;
    var message            = 'Error while fetching data';
    (async() => {
    var get_menu_types_by_id  = await menu_types_collection.find({'_id' : o_id}).toArray(); 

    if (get_menu_types_by_id.length > 0) {
      status_code          = 200;
      message              = 'Successfully fetched data';   
    }
    res.send({
      'status'     : status_code,
      'message'    : message,
      'menu_types_data' : get_menu_types_by_id
    })

    client.close();
    })();
  });
}); // get_menu_data_by_id

/**
* route: /menu_type_insert_update
* method: POST
* description: Inserting/updating  main menus data to database
* params: menu_type_name, menu_type_status
* return: status_code, message
*/
router.post('/menu_type_insert_update', ensureAuthenticated, function(req, res, next) {

  MongoClient.connect(db, db_options).then(client => {
    console.log()
    const db         = client.db(database_name);
    const collection = db.collection('menu_types');
    var id           = req.body.id; 
    var menu_type_name    = req.body.menu_type_name;
    var menu_type_status  = req.body.menu_type_status;
    var status_code  = 400;
    var message      = 'Error while submitting data';
    var user_name    = req.user._id;

    (async () => {

      var data      = {
        'menu_type_name'    : menu_type_name,
        'menu_type_status'  : menu_type_status
      }

      if (id) {
        data['updated_by']    = user_name;
        data['updated_at']    = functions_handler.getCurrentIndianDateTime();

        var o_id             = new ObjectId(id);
        const menu_type_data = await collection.find({
          '_id': o_id
        }).toArray(); 

        if (menu_type_data.length > 0) {
          collection.updateOne({_id: o_id}, 
                    {
                      $set: data
                    }, 
                    {
                      upsert: false
                    }
                );
          status_code = 200;
          message     = 'Data successfully updated';
        } //role_data 
      } else {
        data['created_by']    = user_name;
        data['created_at']    = functions_handler.getCurrentIndianDateTime();

        collection.insertOne(data); // Filter      
        status_code = 200; 
        message     = 'Data successfully added';             
      }//o_id 
      
      res.send({
        'status'        : status_code, 
        'message'       : message
      });

      client.close();
      })();
    });
}); //main_menu_insert_update

/**
* route: /sub_menus
* method: GET
* description: Getting sub menu page
* params: 
* return: renders sub menu page
*/
router.get('/sub_menus', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount,  activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db                     = client.db(database_name);
    const main_menus_collection  = db.collection('main_menus');
   
    (async() => {
      var get_main_menus         = await main_menus_collection.find({'menu_status' : '1'}).sort({'_id' : -1}).toArray(); 
      var main_menu_len          = get_main_menus.length;
      var final_sub_menu_list    = '';
      if (main_menu_len > 0) {
        var tree_list_dt         = functions_handler.getTreeData(get_main_menus); 
        final_sub_menu_list      = Object.values(tree_list_dt);
      }
      //console.log('== tree list =='); console.log(get_main_menus)
      res.render('settings/menu/sub_menus', {
        permissions         : req.permissions,
        user                : req.user,
        get_main_menus,
        final_sub_menu_list,
        activity_logs       : req.activity_logs,
        activity_logs_count : req.activity_logs_count,
      });
      client.close();
    })();
  });
}); //sub_menus

/**
* route: /sub_menu_insert_update
* method: POST
* description: Inserting/updating/deleting sub menus data to database
* params: role_name, role_status
* return: status_code, message
*/
router.post('/sub_menu_insert_update', ensureAuthenticated, function(req, res, next) {

  MongoClient.connect(db, db_options).then(client => {
    const db             = client.db(database_name);
    const collection     = db.collection('main_menus');
    var sub_menu_id      = req.body.sub_menu_id; 
    var sub_menu_name    = req.body.sub_menu_name;
    var menu_type_id     = req.body.menu_type_id;
    var sub_menu_url     = req.body.sub_menu_url;
    var sub_menu_icon    = req.body.sub_menu_icon;
    var sub_menu_status  = req.body.sub_menu_status;
    var selected_menu_id = req.body.selected_menu_id; 
    var selected_json    = req.body.selected_json; 
    var main_menu_id     = req.body.main_menu_id;

    var status_code      = 400;
    var message          = 'Error while submitting data';
    var user_id          = req.user._id;
    sub_menu_url         = sub_menu_url.replace(/\/$/, ""); //removing last slash from url if exists

    (async () => {
      
      var data = {
        'sub_menu_name'   : sub_menu_name,
        'sub_menu_url'    : sub_menu_url,
        'sub_menu_icon'   : sub_menu_icon,
        'sub_menu_status' : sub_menu_status,
      };

       
      var bulk_update_ops = [];
      var diff_array      = [];
      var bulk_del_ops    = [];
      if (main_menu_id) {
                
        var main_menu_id_split       = main_menu_id.split(',');
        var selected_menu_split      = selected_menu_id.split(',');
        var main_menu_id_arr         = [];
        var current_selected_menu_id = [];
        for (i = 0; i < main_menu_id_split.length; i++) {
          main_menu_id_arr.push(ObjectId(main_menu_id_split[i]));
        }
        for (i = 0; i < selected_menu_split.length; i++) {
          current_selected_menu_id.push(ObjectId(selected_menu_split[i]));
        }
        data['updated_by']    = user_id;
        data['sub_menu_id']   = sub_menu_id;
        data['updated_at']    = functions_handler.getCurrentIndianDateTime();
     
        const get_menu_by_id        = await collection.find({'_id': {$in: main_menu_id_arr} }).toArray(); 

        var main_menu_len           = get_menu_by_id.length;
        var previous_sub_menu_list  = '';
        var previous_selected_hson_cnvrt = [];
        if (main_menu_len > 0) {
          var tree_list_dt          = functions_handler.getTreeData(get_menu_by_id); 
          previous_sub_menu_list    = tree_list_dt[sub_menu_id];
          for (x = 0;x < previous_sub_menu_list.length;x++) {
            var prev_selc_data = {};
            prev_selc_data['menu_id']      =  previous_sub_menu_list[x].menu_id.toString();
            prev_selc_data['menu_type_id'] = previous_sub_menu_list[x].menu_type_id;
            previous_selected_hson_cnvrt.push(prev_selc_data);
          }
        }

        var prev_menu_type_id       = req.body.prev_menu_type_id.split(',');
        if (prev_menu_type_id.length !== menu_type_id.length) {
          diff_array = functions_handler.arrayDiff(menu_type_id, prev_menu_type_id);
        }
      
        var diff_mnu_id_array = []; 

        var selected_js_convrt   = [];

        for (i = 0; i < selected_json.length; i++) {
          var selc_data = {};

          var selected_menu_key   = Object.keys(selected_json[i])[0]; 
          selc_data['menu_id']         = selected_menu_key;
          selc_data['menu_type_id']    = selected_json[i][selected_menu_key].menu_type_id;
          var selected_menu_o_id  = new ObjectId(selected_menu_key); 
          var sub_menu_field      = 'sub_menus.' + selected_json[i][selected_menu_key].menu_type_id; 
          var sub_menu_id_field   = selected_json[i][selected_menu_key].menu_type_id;
          var check_query_updated = sub_menu_field + '.$';
          var query               = sub_menu_field + '.$[elem]';
          var query_with_field    = query + '.sub_menu_id';
          selected_js_convrt.push(selc_data)
          
          bulk_update_ops.push(
            { "updateOne": {
              "filter": { 
                "_id":  selected_menu_o_id , 
                [sub_menu_field]: { 
                  "$elemMatch": { "sub_menu_id": sub_menu_id }
                }
              },
              "update": { 
                "$set": { [check_query_updated]: data}
              }
            }},
            
            { "updateOne": {
              "filter": {
                "_id": selected_menu_o_id,
                [sub_menu_field]: {
                  "$not": {
                    "$elemMatch": { "sub_menu_id": sub_menu_id }
                  }
                }
              },
              "update": {
                "$push": { [sub_menu_field]: data }
              }
            }},
          ); // updates the sub_menus that matches menu type id & adds new array data that matches sub_menus.menu_type_id       
         }

         var diff_prev_array_del  = previous_selected_hson_cnvrt.filter(functions_handler.getDiffObject(selected_js_convrt));
         
         if (diff_prev_array_del.length > 0) {

          for (i = 0;i < diff_prev_array_del.length;i++) {
            var delete_menu_id        = diff_prev_array_del[i].menu_id;
            var delete_menu_object_id = new ObjectId(delete_menu_id);
           
            var delete_menu_type_id   = diff_prev_array_del[i].menu_type_id; 
            var delete_sub_menu_field = 'sub_menus.' + delete_menu_type_id; 
            
             if (typeof delete_menu_type_id !== 'undefined') {
               bulk_del_ops.push({ 
                "updateOne" : {
                  "filter" : { "_id" :  delete_menu_object_id},
                  "update" : { "$pull" : {[delete_sub_menu_field]: { "sub_menu_id": sub_menu_id } } }
                }
              }); //removes matched array element from submenus.menu_type_id             
            }
          }
        }

        status_code = 200;
        message     = 'Data successfully updated';
       
        } else {
          data['sub_menu_id']   = shortlink.generate(8);
          data['created_by']    = user_id;
          data['created_at']    = functions_handler.getCurrentIndianDateTime();
          for (i = 0; i < selected_json.length; i++) {
            var selected_menu_key   = Object.keys(selected_json[i])[0]; 
            var selected_menu_o_id  = new ObjectId(selected_menu_key); 
            var sub_menu_field     = 'sub_menus.' + selected_json[i][selected_menu_key].menu_type_id; 
             await bulk_update_ops.push({ 
              "updateOne": { 
                "filter" : { "_id": selected_menu_o_id },  
                "update" : { $addToSet: {[sub_menu_field]: data } },
              },
            }); // adds every json data object to array in submenus.menu_type_id
          }

          status_code   = 200; 
          message       = 'Data successfully added';      

        }//o_id 

        if (bulk_update_ops.length > 0) {
          await collection.bulkWrite(bulk_update_ops, { ordered: false});
        }

        if (bulk_del_ops.length > 0) {
          await collection.bulkWrite(bulk_del_ops, { ordered: false});
        }

        res.send({
          'status'        : status_code, 
          'message'       : message
        });

        client.close();
      })();
    });
}); //sub_menu_insert_update

/**
* route: /get_submenu_data_by_id
* method: POST
* description: Inserting/updating/deleting sub menus data to database
* params: role_name, role_status
* return: status_code, message
*/
router.post('/get_submenu_data_by_id', ensureAuthenticated, activityController.getActivityTotalCount,  activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db               = client.db(database_name);
    const menu_types_collection = db.collection('main_menus');
    var id                 = req.body.id; 
    var sub_menu_id        = req.body.sub_menu_id;
    var status_code        = 400;
    var message            = 'Error while fetching data';
    (async() => {
    var split_id = id.split(',');
    var id_arr   = [];
    for (i = 0; i < split_id.length; i++) {
      id_arr.push(ObjectId(split_id[i]));
    }
    
    var get_menu_by_id = await menu_types_collection.find({'_id' : {$in : id_arr}}).toArray(); 
    var main_menu_len  = get_menu_by_id.length;
    var final_sub_menu_list    = [];
    if (main_menu_len > 0) {
      var tree_list_dt          = functions_handler.getTreeData(get_menu_by_id);
      final_sub_menu_list       = tree_list_dt[sub_menu_id];
    }
      
    var menu_type_arr = [];
    var menu_arr     = []; 

    for (var i = 0; i < final_sub_menu_list.length; i++) {               
      menu_arr.push(final_sub_menu_list[i].menu_id);
      menu_type_arr.push(final_sub_menu_list[i].menu_type_id);
    }

    if (get_menu_by_id.length > 0) {
      status_code        = 200;
      message            = 'Successfully fetched data';   
    }
    res.send({
      'status'          : status_code,
      'message'         : message,
      'get_menu_by_id'  : get_menu_by_id,
      final_sub_menu_list,
      menu_arr,
      menu_type_arr
    })

    client.close();
    })();
  });
}); // get_submenu_data_by_id
/*============ dynamic menu method ends   ===========*/


/*============ team method starts   ===========*/
/**
* route: /team
* method: GET
* description: Getting team page
* params: 
* return: renders team page
*/
router.get('/team', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount,  activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db               = client.db(database_name);
    const teams_collection = db.collection('teams');
    (async() => {
      var get_teams        = await teams_collection.find({}).sort({'_id' : -1}).toArray(); 

      res.render('settings/menu/team', {
        permissions         : req.permissions,
        user                : req.user,
        get_teams           : get_teams,
        activity_logs       : req.activity_logs,
        activity_logs_count : req.activity_logs_count,
      });

      client.close();
    })();
  });
}); // roles


/**
* route: /team_insert_update
* method: POST
* description: Inserting/updating  teams data to database
* params: team_name, team_status, team_desc, team_id
* return: status_code, message
*/
router.post('/team_insert_update', function(req, res, next) {

  MongoClient.connect(db, db_options).then(client => {
    const db         = client.db(database_name);
    const collection = db.collection('teams');
    var team_name    = req.body.team_name;
    var team_status  = req.body.team_status;
    var team_desc    = req.body.team_desc;
    var id           = req.body.id;
    var status_code  = 400;
    var message      = 'Error while submitting data';
    var user_name    = req.user.name;

    (async () => {

      var data      = {
        'id'          : id,
        'team_name'   : team_name,
        'team_status' : team_status,  
        'team_description'   : team_desc,

      }

      if (id) {
        data['updated_by']    = user_name;
        data['updated_at']    = functions_handler.getCurrentIndianDateTime();
        var o_id        = new ObjectId(id);

        const role_data = await collection.find({
          '_id': o_id
        }).toArray(); 
        if (role_data) {
          collection.updateOne({_id: o_id}, // Filter
                    {
                      $set: data
                    }, // Update
                    {
                      upsert: false
                    }
                );
          status_code = 200;
          message     = 'Data successfully updated';
        } //role_data 
      } else {
        data['created_by']    = user_name;
        data['created_at']    = functions_handler.getCurrentIndianDateTime();
        collection.insertOne(data); // Filter      
        status_code = 200; 
        message     = 'Data successfully added';             
      }//o_id 
      
      res.send({
        'status'        : status_code, 
        'message'       : message
      });

      client.close();
      })();
    });
}); //popup_content


/**
* route: /get_team_data_by_id
* method: POST
* description: Getting team data by id
* params: id
* return: status_code, message, team_data
*/
router.post('/get_team_data_by_id', ensureAuthenticated, activityController.getActivityTotalCount,  activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db               = client.db(database_name);
    const teams_collection = db.collection('teams');
    var id                 = req.body.id;
    var o_id               = new ObjectId(id);
    var status_code        = 400;
    var message            = 'Error while fetching data';
    (async() => {
    var get_teams_by_id    = await teams_collection.find({'_id' : o_id}).toArray(); 

    if (get_teams_by_id.length > 0) {
      status_code          = 200;
      message              = 'Successfully fetched data';   
    }
    res.send({
      'status'     : status_code,
      'message'    : message,
      'teams_data' : get_teams_by_id
    })

    client.close();
    })();
  });
}); // get_team_data_by_id

/*============ team method ends   ===========*/

/******************** Role implementation methods end *****************/

module.exports = router;