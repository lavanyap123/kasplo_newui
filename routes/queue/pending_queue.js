const express = require('express');
const router = express.Router();
var cron = require('node-cron');
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectId;
const db = require('../../config/keys').mongoURI;
const db_options = require('../../config/keys').db_options;
const database_name = require('../../config/keys').database_name;
var redis_config= require('../../config/keys').redis_config;
var async = require("async");
var keys = require('../../config/keys');

/* RabbitMQ Settings & Library - (Start) */
var connection1 = require('amqplib').connect(keys.rabbitmq_uri);

console.log('Rabbitmq URL');
console.log(keys.rabbitmq_uri);

var SMS_LOG_QUEUE_NAME = 'sms_logs';
/* RabbitMQ Settings & Library - (Stop) */

	MongoClient.connect(db, db_options).then(client => {
	    const db = client.db(database_name);
		const sms_logs = db.collection('sms_logs');

	    (async () => {

		    var start_date = new Date();
		    start_date.setHours(0, 0, 0, 0);


		    var end_date = new Date();
		    end_date.setHours(23, 59, 59, 59);

		    var pending_queue = await sms_logs.find({
                'created_at': {
                    $gt: start_date,
                    $lte: end_date
                },		    	
                "message_id" : {"$exists" : false}
		    }).toArray();

		    console.log('pending_queue');
		    console.log(pending_queue.length);

			connection1.then(function(conn) {
			  return conn.createChannel();
			}).then(function(ch) {

			   ch.assertQueue(SMS_LOG_QUEUE_NAME).then(function(ok) {

				    pending_queue.forEach(function(state_record) {
				    	delete state_record['_id'];
					    console.log("Message was sent!");
					    //console.log(state_record);
					    
					    ch.sendToQueue(SMS_LOG_QUEUE_NAME, Buffer.from(JSON.stringify(state_record)));
					});

			    });


			}).catch(function(err) {
			    console.log(err);
			    return console.log("Message was rejected...  Boo!");
			});

        })();

});

module.exports = router;
