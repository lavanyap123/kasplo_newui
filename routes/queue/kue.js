const express = require('express');
const router = express.Router();
var cron = require('node-cron');
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectId;
const db = require('../../config/keys').mongoURI;
const db_options = require('../../config/keys').db_options;
const database_name = require('../../config/keys').database_name;
var redis_config= require('../../config/keys').redis_config;
var async = require("async");
var keys = require('../../config/keys');

/* RabbitMQ Settings & Library - (Start) */

var connection = require('amqplib').connect(keys.rabbitmq_uri);
var connection2 = require('amqplib').connect(keys.rabbitmq_uri);

var QUEUE_NAME_1 = 'sms_sent_logs';
var QUEUE_NAME_2 = 'delivery_logs';

/* RabbitMQ Settings & Library - (Stop) */

	MongoClient.connect(db, db_options).then(client => {
	    const db = client.db(database_name);
		const sms_logs = db.collection('sms_logs');
		const deliverylogs = db.collection('delivery_logs');
		const sms_submit_logs = db.collection('sms_submit_logs');

	    (async () => {


			// Consumer
			connection.then(function(conn) {
			  return conn.createChannel();
			}).then(function(ch) {
			   ch.prefetch(900);
			   ch.assertQueue(QUEUE_NAME_1).then(function(ok) {
			    ch.consume(QUEUE_NAME_1, function(data) {
			      //

			      //setTimeout(function(){
			      		//console.log('5 Seconds once running the SMS Logs');
				      if (data !== null) {
				      	//console.log('Kue-Message Sent Log='+QUEUE_NAME_1);
				       
				    	var response = JSON.parse(data.content.toString());
				    	delete response['_id'];
				    	response['created_at'] = new Date();
				    	//console.log('SMS Sent Logs - response');
				    	//console.log(response.message_id);

						sms_logs.findOneAndUpdate(
							{"submit_log_id" : {"$exists" : true, $ne: null },'submit_log_id':response.submit_log_id },
							{ $set: response }, (err, doc) => {
							sms_submit_logs.insert(response);
						    if (err) {
						        console.log("SMS submit Logs -Something wrong when updating data!");
						    }else{
						    	console.log('Submitlog Queue - Outside Loop');
						    	ch.ack(data);						    	
						    }
					    	
						});

				      }

			      //}, 5000);
			    });
			  });
			}).catch(console.warn);


			// Consumer
			connection2.then(function(connection_2) {
			  return connection_2.createChannel();
			}).then(function(channel_2) {
			   channel_2.prefetch(900);
			   channel_2.assertQueue(QUEUE_NAME_2).then(function(ok) {
			     channel_2.consume(QUEUE_NAME_2, function(delivery_data) {
			     	
					      if (delivery_data !== null) {
					      	//console.log('Kue-Message Receive Log='+QUEUE_NAME_2);


					    	var response = JSON.parse(delivery_data.content.toString());
					    	delete response['_id'];
					    	response['created_at'] = new Date();
					    	
					    	//console.log('Delivery Response Message ID - Update');
					    	console.log(response.message_id);
						    
							sms_logs.findOneAndUpdate(
								{"message_id" : {"$exists" : true, $ne: null },'message_id':response.message_id },
								{ $set: response }, (err, doc) => {
							    
							    deliverylogs.insert(response);

							    if (err) {
							    	console.log(err);
							        console.log("Delivery Logs -Something wrong when updating data!");
							    }else{
								    //var result_json_arr = JSON.stringify(doc);
								    //var response = JSON.parse(result_json_arr);
							    	console.log('Deliveryslog Queue - Outside Loop');
							    	channel_2.ack(delivery_data);							    	
							    }
							    

							});			       

							

					        
					      }

					//}, 5000);
			    });
			  });
			}).catch(console.warn);		  

    })();

});

module.exports = router;
