const express = require('express');
const router = express.Router();

var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
// Dashboard
var await = require("await");
var async = require("async");

// DB Config
const db = require('../config/keys').mongoURI;
const db_options = require('../config/keys').db_options;
const database_name= require('../config/keys').database_name;

var ObjectId = require('mongodb').ObjectId;
const request = require('request');
/*
method: get_conversion_url
params: click_id {click_id}
descripton: getting click_id from given click_id and update count for conversion field in db 
*/
router.get('/get_conversion_url', function(req, res, next) {
  var click_id = req.query.click_id; 
  if (click_id) {
    MongoClient.connect(db, db_options).then(client => {
      (async () => {
      var response = {};
      const db = client.db(database_name);
      const clicks_collection = db.collection('clicks'); 
      const camp_collection = db.collection('campaign');
      const conv_report_collection = db.collection('conversion_report');

      var check_id_valid = mongoose.Types.ObjectId.isValid(click_id);

      response['status'] = 400;
      response['is_valid'] = false;
      response['message'] = 'Given click id is invalid';

      if (check_id_valid) {

        var o_id = new ObjectId(click_id); 
        const check_click_id =  await clicks_collection.findOne({'_id': o_id});

        if (check_click_id) {
          if (!check_click_id.conversion_count) {
            await clicks_collection.updateOne({'_id': o_id}, { $set: { conversion_count: 1,  conversion_gross_count: 1, updated_at:new Date()}});
             
          } else {
            await clicks_collection.updateOne(
             {'_id': o_id}
             , { $set: 
               { 
                 updated_at: new Date() 
               },
                  $inc: {
                      conversion_gross_count: 1
                  }
              });

          }// end conversion if

          var click_camp_id = parseInt(check_click_id.campaign_id);
          if (click_camp_id) {
            const get_adv_url = await camp_collection.findOne({'_id': click_camp_id}, {adv_postback_url:1});
            try {
              const get_request = await request(get_adv_url.adv_postback_url);
              (async () => {
                await conv_report_collection.insert({'response': 'success'});
              })();
            } catch(e) {
              //console.log(e);
            }
          }

          response['status'] = 200;
          response['is_valid'] = true;
          response['message'] = 'Successfully updated conversion count';

        }// end if check click id
      } //end if check id valid
      res.send(response);
      await client.close();
    })();//async end
     
  });
  } else {
    res.send({'status': 400, 'is_valid':false, 'message': 'click id not given'});
  }
  // end click id
}); //end get_conversion_url 


module.exports = router;