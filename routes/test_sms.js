onst express = require('express');
var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
var autoIncrement = require("mongodb-autoincrement");

const router = express.Router();
const {
    ensureAuthenticated, roleAuthenticated
} = require('../config/auth');
var activityController = require('./../controllers/activity_ctrl');
// DB Config
const db = require('../config/keys').mongoURI;
const db_options = require('../config/keys').db_options;
const database_name= require('../config/keys').database_name;

// test sms - List
router.get('/', ensureAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);
    const collection = db.collection('add_mobile_numbers');

    collection.find().toArray(function(err, profession) {
      res.render('settings/test_sms', {
        user: req.user,
        activity_logs: req.activity_logs,
        activity_logs_count: req.activity_logs_count,
      });
    });

    client.close();
  });

});