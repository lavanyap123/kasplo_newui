const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
// Load User model
const User = require('../models/user');
var MongoClient = require('mongodb').MongoClient;
// DB Config
const db = require('../config/keys').mongoURI;
const db_options = require('../config/keys').db_options;
const database_name= require('../config/keys').database_name;

var mongoose = require('mongoose');

const { ensureAuthenticated, roleAuthenticated } = require('../config/auth');

const app = express();

router.post('/checkvalid', ensureAuthenticated, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
        const db = client.db(database_name);

    	(async () => {

	        const smpp_collection = db.collection('smpp_details');

	        var post_parameter = req.body;

	        var smpp_id = post_parameter.smpp;
	        var sender_id = post_parameter.sender_id;


		    var smpp_details = await smpp_collection.findOne({
		        'smpp_id': smpp_id
		    });

		    if(smpp_details){

		    	var sender_id_list_arr = smpp_details.sender_address;

		    	if(existInArr(sender_id, sender_id_list_arr)){

	                res.json({
	                    'status':500,
	                    'msg':'already sender id found',
	                    'type':'already_exist'
	                });		    		

		    	}else{

	                res.json({
	                    'status':500,
	                    'msg':'This sender id allowed to create',
	                    'type':'allowed'
	                });		
		    	}

		    }else{

                res.json({
                    'status':404,
                    'msg':'smpp not found',
	                'type':'invalid_smpp'

                });
		    }
        client.close();
		})();

    });
});

router.post('/save', ensureAuthenticated, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
        const db = client.db(database_name);

    	(async () => {
    		
	        const smpp_collection = db.collection('smpp_details');

	        var post_parameter = req.body;

	        var smpp_id = post_parameter.smpp_id;
	        var sender_id = post_parameter.sender_id;

	        var update_sender_id = await smpp_collection.update(
			   { 'smpp_id': smpp_id.toString() },
			   { $addToSet: { sender_address: sender_id.toUpperCase() } }
			);

                res.json({
                    'status':200,
                    'msg':'successfully saved',
                    'smpp_id':smpp_id.toString(),
                    'sender_id':sender_id.toUpperCase()

                });
           client.close();     
		})();

    });	        
});

function existInArr(val, arr){
    var retVal = false;

    if(arr.length){
        for(var i=0; arr.length > i; i++ ){
            if(val.toLowerCase() == arr[i].toLowerCase()){
                retVal = true;
                break;
            }
        }
    }

    return retVal;
}
module.exports = router;