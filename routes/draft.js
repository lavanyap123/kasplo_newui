const express                     = require('express');
const router                      = express.Router();
const { ensureAuthenticated, roleAuthenticated }     = require('../config/auth');
var json2csv                      = require('json2csv'); //not working
var csv_export                    = require('csv-export');
const stringify                   = require('csv-stringify');
var autoIncrement                 = require("mongodb-autoincrement");
var mongoose                      = require('mongoose');
var MongoClient                   = require('mongodb').MongoClient;

// Dashboard
var await                         = require("await");
var async                         = require("async");
var Hashids                       = require('hashids');
var http                          = require('http');
var url                           = require('url');
var functions_handler             = require('./../helpers/functions_handler'); 
var activityController            = require('./../controllers/activity_ctrl');
var nanoid                        = require('nanoid');
var fs                            = require('fs');
const path                        = require('path');

// DB Config
const db                          = require('../config/keys').mongoURI;
const db_options                  = require('../config/keys').db_options;
const database_name               = require('../config/keys').database_name;

// Export - Index
router.get('/list/:id', ensureAuthenticated, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db          = client.db(database_name);
    const campaign    = db.collection('campaign');
    var campaignIndex = req.params.id;

    campaign.findOne({
      '_id': campaignIndex
    }).then(function (doc) {

    res.json({
      'data' : doc
    });
    client.close();
    });
  });
});

// campaign - create draft
router.post('/create_draft', ensureAuthenticated, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);
    (async () => {
      const counters        = await db.collection('counters').findOne({
        "_id": "campaign"
      });
      if (counters) {
        var camp_counter_id = counters.seq + 1;
      } else {
        var camp_counter_id = 1;
      }

      var campaign_id       = camp_counter_id + nanoid(3);
      var campaign_data     = {
        '_id'             : campaign_id,
        'category_id'     : '',
        'name'            : '',
        'sms_content'     : '',
        'destination_url' : '',
        'status'          : '',
        'draft_status'    : 0,
        'draft_segments'  : '',
        'created_by'      : req.user.name,
        'created_at'      : new Date().toLocaleString('en-US', {
            timeZone : 'Asia/Calcutta'
        }),
        'date_added'      : new Date(),
      };

      //db.collection('campaign').insert(campaign_data);
      db.collection('campaign').insert(campaign_data);
      // Object inserted successfully.
      var campaignIndex   = campaign_id; // this will return the id of object inserted

      res.send({
        'succcess '       : 200,
        'campaignIndex'   : campaignIndex
      }); 
      client.close();
    })();
  });
});

// Export - Index
router.post('/save', ensureAuthenticated, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db                         = client.db(database_name);
    const campaign                   = db.collection('campaign');
    const draft                      = db.collection('draft');
    var params                       = req.body; 
    var campaign_name                = params.campaign_name;
    var detination_url               = params.detination_url;
    var text_message                 = params.message; 
    var replace_message              = text_message.replace(/(\r\n|\n|\r)/gm, "");
    var message                      = "\"" + replace_message + "\""; 
    var content_label_name           = params.content_label_name;
    var category_id                  = params.category_id;
    var filter_category_id           = params.filter_category_id;
    var filter_campaign_ids_obj      = params.filter_campaign_ids_obj;
    var filter_content_label_ids_obj = params.filter_content_label_ids_obj;
    var filter_topper_id             = params.filter_topper_id;
    var filter_search                = params.filter_search;
    var filter_modes_obj             = params.filter_modes_obj; 
    var minRecord                    = params.minRecord;
    var maxRecord                    = params.maxRecord;
    var minAge                       = params.minAge;
    var maxAge                       = params.maxAge;
    var draft_id                     = params.draft_id;
    var form_type                    = params.type;
    var remove_quotes_message        = functions_handler.stripEndQuotes(message);  
    var filter_selected              = new Array();
    var filter_obj                   = {};

    if (filter_category_id) {
      filter_selected.push('filter_category_id');
      filter_obj['filter_category_id'] = filter_category_id;
    }

    /* multiple filters in advanced search starts */
    if (filter_campaign_ids_obj) {
      filter_obj['filter_campaign_ids']      = filter_campaign_ids_obj;
    }

    if (filter_content_label_ids_obj) {
      filter_obj['filter_content_label_ids'] = filter_content_label_ids_obj;
    }

    if (filter_modes_obj) {
      filter_obj['filter_modes']            = filter_modes_obj;
    }
    
    /* multiple filters in advanced search ends */
    var form_data = {
      'content_label'    : content_label_name,
      'filters'          : filter_obj,
      'filter_topper_id' : filter_topper_id,
      'filter_search'    : filter_search,
      'minRecord'        : minRecord,
      'maxRecord'        : maxRecord,
      'minAge'           : minAge,
      'maxAge'           : maxAge,
      'smpp_id'          : params.smpp_id,
      'smpp_sender_name' : params.smpp_sender_name
    }; 
    
    if (draft_id) {
      var campaign_name    = params.campaign_name;
      var detination_url   = params.detination_url;
      campaign.updateOne(
        {
          _id  : draft_id
        }, // Filter
        {
          $set: {
            'name'            : campaign_name,
            'sms_content'     : text_message,
            'destination_url' : detination_url,
            'category_id'     : category_id,
            'draft_segments'  : form_data,
            'created_by'      : req.user.name
          }
        }, // Update
        {
          upsert: false
        }
      ).then((obj) => {
        res.json({
        'msg'     : 'success',
        'data'    : obj,
        'status'  : 200
        });
      }).catch((err) => {
        res.json({
          'msg'    : err,
          'status' : 400
        });
      });
    } else {
      res.json({
        'msg'    : 'success',
        'status' : 200
      });
    }
    client.close();
  });
});

module.exports = router;