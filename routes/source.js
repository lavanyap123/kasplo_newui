const express = require('express');
var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
var autoIncrement = require("mongodb-autoincrement");

const router = express.Router();
const {
    ensureAuthenticated, roleAuthenticated
} = require('../config/auth');

// DB Config
const db = require('../config/keys').mongoURI;
const db_options = require('../config/keys').db_options;
const database_name= require('../config/keys').database_name;

var activityController = require('./../controllers/activity_ctrl');

// Source - List
router.get('/manage', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const collection = db.collection('source');

            collection.find().sort({
                'date_added': -1
            }).toArray(function(err, source) {
                res.render('source/manage', {
                    source,
                    user: req.user,
                    activity_logs: req.activity_logs,
                    activity_logs_count: req.activity_logs_count,
                });
            });

            client.close();
        });

});

/**
 * @request     : {GET} /get_source_dropdown
 * @description : Fetching data from source table for dropdown listing 
 * @param       : {Int} _limit
 * @param       : {Int} _page               
 * @return      : source_data, source_count, success
*/
router.get('/get_source_dropdown', ensureAuthenticated,  activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    
    const db              = client.db(database_name);
    const collection      = db.collection('source');
    
    (async () => {
      var limit           = parseInt(req.query._limit);
      var current_page    = parseInt(req.query._page);
      var offset          = parseInt((current_page - 1) * limit); 
      var source_count    = await collection.count();
      var source_data     = [];
      if (current_page == 1) {
        source_data.push({'_id' : '0', 'name' : 'None'});
        source_count += 1;
      }
      const source_list   = await collection.find({'status' : '1'}).sort({'_id' : 1}).skip(offset).limit(limit).forEach(function (src_record) {;
        source_data.push(src_record);
      });

      res.send({
        'status'      : 200,
        'total_count' : source_count,
        'source_data' : source_data
      });

      client.close();
    })();
  });
           
}); //get_source_dropdown


/**
 function: checkCategoryExists
 method  : POST
 description: checking if source name given already exists or not in db
 params: {string} name $name
         {int} id $id
 return: true or false
*/
router.post('/checkSourceExists', ensureAuthenticated, (req, res) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            var name = req.body.name;
            const collection = db.collection('source');

            (async () => {
              var check_type = req.body.check_type;
              if (check_type == 'add') {
                var name_status = await collection.findOne({'name':new RegExp('^' + name + '$', 'i')});
              } else {
                var source_id = parseInt(req.body.source_id); 
                var name_status = await collection.findOne({'name': new RegExp('^' + name + '$', 'i'), '_id': {$ne: source_id}});
              }
              if (name_status) {               
                res.send('false');     
                return false;
              } else {
                res.send('true');     
                return true;
              }
              client.close();
            })();
    });
});

router.post('/popup_content', ensureAuthenticated, (req, res) => {

    var type = req.body.type;
    var id = req.body.id;


    if (type == 'add') {

        var source_html_content = '<div class="modal-header"> <h5 class="modal-title" id="createSourceModal">Create Source</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><div id="notification"></div> <form id="add_source" type="post"> <div class="row"> <div class="col-12"> <div class="form-group"> <label for="exampleInputEmail1">Source Name</label> <input type="text" class="form-control" name="name" id="source_name" placeholder="Enter Source"> </div><div class="form-group"> <label for="exampleInputEmail1">Source Description</label> <textarea class="form-control" name="description" id="source_description" rows="2" placeholder="Enter Source Description"></textarea> </div><div class="form-group"> <label class=" col-form-label">Status</label> <div class=""> <select class="form-control" name="status" id="status"> <option value="">---Status ---</option> <option value="1">Active</option> <option value="0">De-active</option> </select> </div></div></div><div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> <button type="submit" class="btn btn-primary" id="save_source">Save</button><img src="/public/images/loader_form.gif" class="img-responsive" id="loader" style="width: 25px;display:none;"/> </form> </div></div></div>';
        res.send({
            'popup_content': source_html_content
        });

    } else {


        MongoClient.connect(db, db_options).then(client => {
                const db = client.db(database_name);
                const collection = db.collection('source');

                collection.findOne({
                    '_id': parseInt(id)
                }).then(function(doc) {


                    if (!doc) {
                        throw new Error('No record found.');
                    } else {
                        var status = doc.status;
                        if (status == '1') {
                            var active_selected = 'selected';
                            var de_active_selected = '';

                        } else {
                            var active_selected = '';
                            var de_active_selected = 'selected';
                        }

                        var source_html_content = '<div class="modal-header"> <h5 class="modal-title" id="createSourceModal">Update Source - #' + id + ' </h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><div id="notification"></div> <form id="add_source" type="post"><input type="hidden" id="update_source_id" value="' + id + '"> <div class="row"> <div class="col-12"> <div class="form-group"> <label for="exampleInputEmail1">Source Name</label> <input type="text" class="form-control" name="name" id="source_name" placeholder="Enter Source" value="' + doc.name + '"> </div><div class="form-group"> <label for="exampleInputEmail1">Source Description</label> <textarea class="form-control" name="description" id="source_description" rows="2">' + doc.description + '</textarea> </div><div class="form-group"> <label class=" col-form-label">Status</label> <div class=""> <select class="form-control" name="status" id="status"> <option value="">---Status ---</option> <option value="1" ' + active_selected + '>Active</option> <option value="0"  ' + de_active_selected + '>De-active</option> </select> </div></div></div><div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> <button type="submit" class="btn btn-primary" id="btn_update_source">Update</button><img src="/public/images/loader_form.gif" id="loader" class="img-responsive" style="width: 25px;display:none;"/> </form> </div></div></div>';

                    }

                    res.send({
                        'popup_content': source_html_content
                    });

                });

                client.close();

            });
    }


});

router.post('/add', ensureAuthenticated, (req, res) => {

        MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const collection = db.collection('source');

            autoIncrement.getNextSequence(db, 'source', function(err, autoIndex) {

                var data = {
                    '_id': autoIndex,
                    'name': req.body.cat_name,
                    'description': req.body.description,
                    'status': req.body.status,
                    'created_at': new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Calcutta'
                    }),
                    'date_added': new Date()
                };

                collection.insert(data);
                client.close();


            });

            res.send({
                'msg': 'success',
                'status': 200
            });


        });

});

router.post('/update', ensureAuthenticated, (req, res) => {

        MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const collection = db.collection('source');

            var data = {
                'name': req.body.cat_name,
                'description': req.body.description,
                'status': req.body.status,
                'updated_at': new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                })


            };

            var id = req.body.source_id;

            collection.updateOne({
                        _id: parseInt(id)
                    }, // Filter
                    {
                        $set: data
                    }, // Update
                    {
                        upsert: false
                    }
                )
                .then((obj) => {
                    res.send({
                        'msg': 'success',
                        'status': 200
                    });
                })
                .catch((err) => {
                    res.send({
                        'msg': err,
                        'status': 400
                    });
                });


            client.close();
        });

});

module.exports = router;