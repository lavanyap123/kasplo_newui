const express          = require('express');
const router           = express.Router();

var mongoose           = require('mongoose');
var MongoClient        = require('mongodb').MongoClient;
var activityController = require('./../controllers/activity_ctrl');

router.get('/activity_logs', activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res) => {   
  res.render('logs/activity_logs', {activity_logs : req.activity_logs, activity_logs_count : req.activity_logs_count});
});

router.get('/server_logs', activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res) => {  
  res.render('logs/server_logs', {activity_logs : req.activity_logs, activity_logs_count : req.activity_logs_count});
});
module.exports = router;