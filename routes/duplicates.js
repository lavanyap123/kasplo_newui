 const express = require('express');
const router = express.Router();
const {
    ensureAuthenticated
} = require('../config/auth');
var json2csv = require('json2csv'); //not working
var csv_export = require('csv-export');
const stringify = require('csv-stringify');
var autoIncrement = require("mongodb-autoincrement");

var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
// Dashboard
var await = require("await");
var async = require("async");
var Hashids = require('hashids');
// DB Config
const db = require('../config/keys').mongoURI;

const UAParser = require('ua-parser-js');
const request = require('request');
const useragent = require('express-useragent');
var activityController = require('./../controllers/activity_ctrl');
router.get('/remove', (req, res, next) => {

        MongoClient.connect(db, {
                useNewUrlParser: true
            })
            .then(client => {
                const db = client.db('bulk_sms');

                db.collection('clicks').aggregate([
                         // discard selection criteria, You can remove "$match" section if you want
                         { $match: {
                           contact_no: { "$ne": '' }
                         }},
                         { $group: {
                           _id: { 
                            contact_no: "$contact_no",
                            content_label_id: "$content_label_id"
                           }, // can be grouped on multiple properties
                           dups: { "$addToSet": "$_id" },
                           count: { "$sum": 1 }
                         }},
                         { $match: {
                           count: { "$gt": 1 }    // Duplicates considered as count greater than one
                         }}
                       ])               // You can display result until this and check duplicates
                .forEach(function(doc) {
                  
                   doc.dups.shift();      // First element skipped for deleting
                   db.collection('clicks').remove({_id : {$in: doc.dups }});  // Delete remaining duplicates
               });

              res.send({'status':'successfully deleted duplicated records'});
       });
});
module.exports = router;