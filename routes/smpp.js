const express          = require('express');
const router           = express.Router();
const { ensureAuthenticated } = require('../config/auth');
var mongoose           = require('mongoose');
var MongoClient        = require('mongodb').MongoClient;
// Dashboard
var await              = require("await");
var async              = require("async");
// DB Config
const db               = require('../config/keys').mongoURI;
const db_options       = require('../config/keys').db_options;
const database_name    = require('../config/keys').database_name;
var activityController = require('./../controllers/activity_ctrl');
var ObjectId           = require('mongodb').ObjectId;

router.post('/get_smpp_name', ensureAuthenticated, (req, res, next) => {
   
    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const smpp = db.collection('smpp_details');
            (async () => {
                var smpp_name_list = [];
                var smpp_list_html = '';
                const smpp_tbl = await smpp.find({}).forEach(function(smpp_record) {
                    smpp_name_list.push(smpp_record);
                });

                if (smpp_name_list.length > 0) {
                  smpp_list_html += '<option disabled selected>---Please select--</option>';
                  for (var i = 0; i < smpp_name_list.length; i++) {
                    smpp_list_html += '<option value="' + smpp_name_list[i].smpp_id + '">' + smpp_name_list[i].name + '</option>';
                  }

                  res.send({
                      'msg': 'success',
                      'html': smpp_list_html
                  });
                }

                client.close();
            })();

        });
});


router.post('/get_sender_addr', ensureAuthenticated, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db         = client.db(database_name);
    const smpp       = db.collection('smpp_details');

    (async () => {

      var smpp_sender_list_html = '';
      var smpp_id    = req.body.smpp_id; 
      var type       = req.body.type;
      const smpp_tbl = await smpp.findOne({'smpp_id': smpp_id                    });

      var smpp_sender_list  = smpp_tbl.sender_address;
      smpp_sender_list_html += '<option disabled selected>---Please select--</option>';                
      if (smpp_sender_list) {
        for (var i = 0; i < smpp_sender_list.length; i++) {
          smpp_sender_list_html += '<option value="' + smpp_sender_list[i].sender_id  + '">' + smpp_sender_list[i].sender_id + '</option>';
        } //for end
      } //if end
      var response      = {'msg' : 'success'};
      if (type == "table_list") {
        response['status']      = 200;
        response['sender_data'] = smpp_tbl;
      } else {
        response['html']        =  smpp_sender_list_html;
      }
      res.send(response);

      client.close();
    })(); //async end
  }); //mongoconnection end
}); // get_sender_addr

/**
method:   senderid
description: getting sender id page
params :
return : sender id page
*/
router.get('/senderid', ensureAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db   = client.db(database_name);
    const smpp = db.collection('smpp_details');

    (async () => {
      var smpp_list   = [];

      //get smpp data
      const smpp_data = await db.collection('smpp_details').find().forEach(function(smpp_record) {
        smpp_list.push(smpp_record);
      });

      res.render('settings/smpp/senderid', {
        user                : req.user,
        activity_logs       : req.activity_logs,
        activity_logs_count : req.activity_logs_count,
        smpp_list           : smpp_list
      });

      client.close();
    })(); //async end
  }); //mongoconnection end
}); // senderid


/**
* route: /sender_id_insert_update
* method: POST
* description: Inserting/updating sender id data to database
* params: smpp_id, sender_id_name, sender_id_status
* return: status_code, message
*/
router.post('/sender_id_insert_update', ensureAuthenticated, function(req, res, next) {

  MongoClient.connect(db, db_options).then(client => {
    const db             = client.db(database_name);
    const collection     = db.collection('smpp_details');
    var smpp_obj_id      = req.body.smpp_obj_id; 
    var sender_id_name   = req.body.sender_id_name;
    var sender_id_status = req.body.sender_id_status;
    var data_type        = req.body.data_type;

    var status_code      = 400;
    var message          = 'Error while submitting data';
  
    (async () => {
      var o_id           = new ObjectId(smpp_obj_id); 
      var insert_update_obj = {
        'sender_id'        : sender_id_name,
        'sender_id_status' : parseInt(sender_id_status)
      };
      
      if (data_type == 'Update') {
        var prev_sender_id_val = req.body.prev_sender_id_val; 
        await collection.findOneAndUpdate({ _id: o_id, sender_address: { "$elemMatch": { "sender_id": prev_sender_id_val }}  }, { $set: { "sender_address.$" : insert_update_obj } });
        message     = 'Sender id updated successfully';
       
      } else {
        await collection.findOneAndUpdate({ _id: o_id  }, { $addToSet: { "sender_address" : insert_update_obj } });
        message     = 'Sender id added successfully';
      }
      

      status_code = 200;
      
      res.send({
        'status' : status_code,
        'message' : message
      })

     
    })();
}); //sub_menu_insert_update

});

/**
* route: /check_sender_id_exists
* method: POST
* description: checking sender id given already exists in that respective smpp id or not
* params: smpp_id, sender_id
* return: status_code, true/false
*/
router.post('/check_sender_id_exists', ensureAuthenticated, function(req, res, next) {
 
  MongoClient.connect(db, db_options).then(client => {
    const db             = client.db(database_name);
    const collection     = db.collection('smpp_details');
    var smpp_id          = req.body.smpp_id; 
    var sender_id        = req.body.sender_id_name;  
    var data_type        = req.body.data_type;

    var status_code      = 200;
    var message          = 'Sender id not exists';
  
    (async () => {
      var o_id           = new ObjectId(smpp_id); 
      if (data_type == "Update") {
        var prev_sender_id_val = req.body.prev_sender_id_val; 
        if (prev_sender_id_val.toString() === sender_id.toString()) {
         
          var check_sender_exists = null;
        } else {
        
           var check_sender_exists = await collection.findOne({ "_id": o_id,  "sender_address": { $elemMatch: { "sender_id": sender_id } } });
        }
        
      } else {
        var check_sender_exists = await collection.findOne({ "_id": o_id,  "sender_address": { $elemMatch: { "sender_id": sender_id } } });
      }
      
      if (check_sender_exists !== null) {  
     
        res.send('false'); // name already exists 
        //return false;
      } else {
        res.send('true');     
        //return true;
      }
    })();
  }); //sub_menu_insert_update
});

/**
* route: /get_sender_by_ids
* method: POST
* description: Getting sender data by smpp id and sender id
* params: smpp_id, sender_id
* return: status_code, message, sender_data
*/
router.post('/get_sender_by_ids', ensureAuthenticated, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db      = client.db(database_name);
    const smpp    = db.collection('smpp_details');
    var status_code = 400;
    var message     = "Error while fetching data";
    (async () => {

      var smpp_sender_list_html = '';
      var smpp_id           = req.body.smpp_id; 
      var sender_id         = req.body.sender_id;
      var o_id              = new ObjectId(smpp_id); 
      const get_data_by_id  = await smpp.findOne({ "_id": o_id, "sender_address": { $elemMatch: { "sender_id": sender_id } } });

       status_code  = 200;
       message      = "Data successfully fetched";

      res.send({
        'status' : status_code,
        'message' : message,
        'sender_data' : get_data_by_id
      });

      client.close();
    })(); //async end
  }); //mongoconnection end
}); // get_sender_addr

/**
* route: /delete_sender_by_ids
* method: POST
* description: deleting sender id based on smpp id and sender id
* params: smpp_id, sender_id
* return: status_code, true/false
*/
router.post('/delete_sender_by_ids', ensureAuthenticated, function(req, res, next) {

  MongoClient.connect(db, db_options).then(client => {
    const db             = client.db(database_name);
    const collection     = db.collection('smpp_details');
    var smpp_id          = req.body.smpp_id;  
    var sender_id        = req.body.sender_id;  
    var data_type        = req.body.data_type;

    var status_code      = 200;
    var message          = 'Sender id deleted successfully';
  
    (async () => {
      var o_id           = new ObjectId(smpp_id);
     
      var delete_sender_ids = await collection.findOneAndUpdate(
        { "_id" : o_id},
        { $pull: { "sender_address" : {'sender_id' : sender_id} } },
      )

      if (check_sender_exists !== null) {     
        res.send('false'); // name already exists 
        //return false;
      } else {
        res.send('true');     
        //return true;
      }
    })();
  }); //sub_menu_insert_update
});

module.exports = router;