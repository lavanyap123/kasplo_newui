const express              = require('express');
const router               = express.Router();
const {
    ensureAuthenticated, roleAuthenticated
} = require('../config/auth');
var autoIncrement          = require("mongodb-autoincrement");
var functions_handler      = require('./../helpers/functions_handler');
var smpp_handler           = require('./../helpers/smpp');
var mongoose               = require('mongoose');
var MongoClient            = require('mongodb').MongoClient;
// Dashboard
var await                  = require("await");
var async                  = require("async");
var Hashids                = require('hashids');
var nanoid                 = require('nanoid');
var fs                     = require('fs');
const path                 = require('path');
var ISODate                = require("isodate");
// DB Config
const db                   = require('../config/keys').mongoURI;
const db_options           = require('../config/keys').db_options;
const database_name        = require('../config/keys').database_name;
var redis_config           = require('../config/keys').redis_config;

var smpp                   = require('smpp');
const request              = require('request');
var nodemailer             = require('nodemailer');

var constants              = require('./../config/constants');
var keys                   = require('./../config/keys');
const mail_from            = require('../config/keys').mail_from;
const to_mail_ids          = require('../config/keys').to_mail_ids;
const smpp_credentials     = require('../config/keys').smtp_credentials;

/* RabbitMQ Settings & Library - (Start) */

var QUEUE_NAME             = 'sms_logs';
var connection             = require('amqplib').connect(keys.rabbitmq_uri);

/* RabbitMQ Settings & Library - (Stop) */

router.post('/cancel_schedule', ensureAuthenticated, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);
    const contacts = db.collection('contacts');
    const campaign_content_collection = db.collection('campaign_content_labels');
    const sms_logs_collection = db.collection('sms_logs');

    var post_parameter = req.body;
    var campaign_id = post_parameter.campaign_id;
    var content_id = post_parameter.content_id;
    var schedule_canceled_date = new Date().toLocaleString('en-US', {
            timeZone: 'Asia/Calcutta'
    });

    campaign_content_collection.updateOne(
      {
        '_id': parseInt(content_id)
      },
      {
        $set: {
          'schedule_status':'2',
          'schedule_canceled_date':schedule_canceled_date
          
        }
      }
    );

    contacts.update({},
      {$pull:{'campaign_content_id': { "$in":[content_id.toString()] }}},
      {multi:true}
    );


    var bulk = sms_logs_collection.initializeUnorderedBulkOp();

      bulk.find({
          campaign_id: {
              $in: [parseInt(campaign_id)]
          },
          campaign_content_label_id: {
              $in: [parseInt(content_id)]
          },
      }).update({
          $set: {
              message_status: 'SCHEDULE CANCELLED',
              updated_at:new Date()
          }
      }, {
          upsert: true
      });

      bulk.execute(function(err, result) {
          //console.log('Bulk Result:');
          //console.log(result); 
      });

    /*  Scheduled Success Email - START */
      sendEmailAfterScheduleChanges(campaign_content_collection,campaign_id,content_id,schedule_canceled_date,'cancelschedule',req.user.name);
    /*  Scheduled Success Email - STOP */

    client.close();
    res.send({'status':200});

  });
});

router.post('/reschedule', ensureAuthenticated, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
        const db = client.db(database_name);
        var sms_logs_collection = db.collection('sms_logs');
        var campaign_content_collection = db.collection('campaign_content_labels');

        var post_parameter = req.body;
        var campaign_id = post_parameter.campaign_id;
        var content_id = post_parameter.content_id;
        var campaign_id = post_parameter.campaign_id;
        var sch_date_time = post_parameter.schedule_date_and_time;

        var schedule_date_and_time=new Date(sch_date_time);
        var schedule_date = (schedule_date_and_time.getMonth()+1)+'-'+schedule_date_and_time.getDate()+'-'+schedule_date_and_time.getFullYear();
        var schedule_time = schedule_date_and_time.getHours() + ":" + schedule_date_and_time.getMinutes();
        var schedule = schedule_date+' '+schedule_time;

        var schedule_date_and_time=new Date(schedule);
        var indian_schedule_date_and_time=new Date(schedule_date_and_time).toLocaleString('en-US', {
          timeZone: 'Asia/Calcutta'
        }); 

        campaign_content_collection.updateOne(
          {
            '_id': parseInt(content_id)
          },
          {
            $set: {
              'segments.schedule_date_and_time':sch_date_time
            }
          }
        );

        var bulk = sms_logs_collection.initializeUnorderedBulkOp();

          bulk.find({
              campaign_id: {
                  $in: [parseInt(campaign_id)]
              },
              campaign_content_label_id: {
                  $in: [parseInt(content_id)]
              },
          }).update({
              $set: {
                  indian_schedule_date_and_time: indian_schedule_date_and_time,
                  schedule_date_and_time:schedule_date_and_time,
                  updated_at:new Date()
              }
          }, {
              upsert: true
          });

          bulk.execute(function(err, result) {
              //console.log('Bulk Result:');
              //console.log(result); 
          });

          /*  Scheduled Success Email - START */
            sendEmailAfterScheduleChanges(campaign_content_collection,campaign_id,content_id,indian_schedule_date_and_time,'reschedule',req.user.name);
          /*  Scheduled Success Email - STOP */

          client.close();
          res.send({'status':200});

    });
});


router.post('/test_sms', ensureAuthenticated, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
        const db = client.db(database_name);
        const test_sms_logs = db.collection('sms_logs');
        const smpp_collection = db.collection('smpp_details');
        var post_parameter = req.body;

        var smpp_id = post_parameter.smpp_id;
        var sender_id = post_parameter.sender_id;
        var mobile_no = post_parameter.mobile_no;
        var sms_content = post_parameter.message;
        var destination_url = post_parameter.destination_url;

        var url_shortcode = nanoid(3);
        var shortcode = 'kasplo_'+url_shortcode;
        var tracking_url = constants.TRACKING_URL+shortcode;
        var sms_content = sms_content.replace("{url}", tracking_url);

        testingSMSFormat(smpp_id,sender_id,mobile_no,sms_content,url_shortcode,destination_url,test_sms_logs,smpp_collection);
        res.json({'status':200});
    });
});

async function testingSMSFormat(smpp_id,sender_id,mobile_no,sms_content,url_shortcode,destination_url,test_sms_logs,smpp_collection)
{

    var smpp_details = await smpp_collection.findOne({
        'smpp_id': smpp_id
    });

    if(smpp_id=='6'){
      var send_mobile_no = '91'+mobile_no;
    }else{
      var send_mobile_no = mobile_no;      
    }

    var ins_response = {};
    ins_response['mobile_no'] = mobile_no;
    ins_response['sms_content'] = sms_content;
    ins_response['smpp_id'] = smpp_id;
    ins_response['sender_id'] =  sender_id;
    ins_response['url_shortcode'] = url_shortcode;
    ins_response['destination_url'] = destination_url;
    ins_response['mode'] = '0';
    ins_response['mode_type'] = 'test';
    ins_response['message_status'] = 'PENDING'; //default message status
    ins_response['message_type'] = 'Text SMS'; //default message status
    ins_response['created_at'] = new Date(); 
    saveCollectionDocuments(test_sms_logs,ins_response);

    var session = new smpp.Session({host: smpp_details.ip_address, port: smpp_details.host[0]});
    delete smpp.encodings.ASCII;

    session.on('connect', function(){

        session.bind_transmitter({
              system_id: smpp_details.system_id, //SMPP - Settings Table
              password: smpp_details.password,  //SMPP - Settings Table
              interface_version: 1,
              system_type: '380666000600',
              address_range: '+380666000600',
              addr_ton: 1,
              addr_npi: 1
        }, function(pdu) {

           if(pdu.command_status == 0) { 

              //Successfully bound
              session.submit_sm({
                  source_addr: sender_id, //SENDER ID
                  destination_addr:send_mobile_no, // Mobile Number
                  short_message:sms_content, // Text SMS
                  registered_delivery:1, //Mandatory Send Delivery Response
              }, function(pdu) {

                  // Message successfully sent
                  var response = {};
                  response['command_id'] = pdu.command_id;
                  response['command_status'] = pdu.command_status;
                  response['sequence_number'] = pdu.sequence_number;
                  response['message_id'] = pdu.message_id;
                  response['submit_sm_resp'] = pdu;
                  response['message_submit_date'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                  }); 
                  updatingMessageCollection({'mobile_no':mobile_no,'url_shortcode':url_shortcode},response,test_sms_logs);

              });
            }
        });


        session.on('close', function(){
            console.log('SMPP CLOSE');
        });

        session.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            session.send(pdu.response());
        });

        session.on('unbind', function(pdu) {
            console.log('unbind')
            session.send(pdu.response());
            session.close();
        });

        session.on('error', function(error){
            console.log('smpp error', error);
        }); 

 });

}

/**
 * method: uni_submit
 * description: shows total number of sms count based on data passed from and sends sms 
 * params: filter_params, filter_search
 * return: returns total count and sends sms 
*/

router.post('/uni_submit', ensureAuthenticated, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db                      = client.db(database_name);
    const contacts_collection     = db.collection('contacts');
    const clicks_collection       = db.collection('clicks');
    const campaign_collection     = db.collection('campaign');
    const campaign_content_labels = db.collection('campaign_content_labels');
    const counters_collection     = db.collection('counters');
    const smpp_collection         = db.collection('smpp_details');
    const activity_logs           = db.collection('activity_logs');
    const sms_logs                = db.collection('sms_logs');

    var post_parameter            = req.body; 

    if (post_parameter.button_type == 'search') {
      var filter_params           = getCampaignGeneralSearchParameters(post_parameter);
    } else {
      var filter_params           = getCampaignSaveParameters(post_parameter,campaign_collection);                       
    }
    // console.log('=== filter params');
    // console.log(filter_params);

    var filter_category_id        = filter_params.filter_category_id;
    var category_id               = filter_params.category_id;
    var page_name                 = filter_params.page_name;
    var filter_search             = filter_params.filter_search;
    var filter_mode               = filter_params.filter_mode;
    var filter_topper             = filter_params.filter_topper;
    // var filter_campaign_id        = filter_params.filter_campaign_id;
    // var filter_content_label_id   = filter_params.filter_content_label_id;
    var minAge                    = filter_params.minAge;
    var maxAge                    = filter_params.maxAge;
    var minRecord                 = filter_params.minRecord;
    var maxRecord                 = filter_params.maxRecord;
    var campaign_id_filter        = filter_params.campaign_id_filter;
    var user_name                 = req.user.name;
    var user_id                   = req.user._id;
    var button_type               = filter_params.button_type;
    var schedule_date_and_time    = filter_params.schedule_date_and_time;

    var include_camp_ids          = filter_params.include_camp_ids; 
    var exclude_camp_ids          = filter_params.exclude_camp_ids;
    var include_cont_lbl_ids      = filter_params.include_cont_lbl_ids; 
    var exclude_cont_lbl_ids      = filter_params.exclude_cont_lbl_ids;

    var find_paramas_query        = [];
    var exclude                   = [];

    var exclude_camp_ids_arr      = [];
    var include_camp_ids_arr      = [];

    var clic_contact_include_obj   = {};
    var clic_contact_exclude_obj   = [];
    var clic_contact_exclude_obj_arr   = [];
    var and_exclude_obj_arr            = [];
    /* Filter by Active Contacts - start */

    if (filter_topper != 'clicker') {
      find_paramas_query.push({
        is_active : {
          $in     : [1]
        },
      });
    }

    /* Filter by Active Contacts - stop */

    if ('filter_category_id' in filter_params) {
      //Get Category id
      if (filter_topper == 'clicker') {   // Array string value converting to integer
        var cat_id_filter       = getFilterClickerArrayData(filter_category_id);
      } else {
        //let it be
        var cat_id_filter       = getFilterArrayData(filter_category_id);
      }
    }
            
    /* ############### Step-1 - "Sender Query" - Start ###############*/

    if (filter_topper == 'sender') {
      /* Filter by Category ID wise - start */
      find_paramas_query.push({
        sender_category_ids: {
          $in: cat_id_filter
        }
      });            

      /* Filter by Category ID wise - Stop */

      if (page_name != 'add' && campaign_id_filter != '' && typeof campaign_id_filter != 'undefined') {
        

        find_paramas_query.push(
           
          {"$or": [
            { "sender_sent_camp_ids" : { "$exists" : false } },
            {  "$nor" : [{"sender_sent_camp_ids": {$in: [parseInt(campaign_id_filter)]}}] },
            ]
          }
        );

        
        // find_paramas_query.push({
        //   sender_sent_camp_ids: {
        //     $nin: [parseInt(campaign_id_filter)], $exists: false
        //   }
        // }); 

        // exclude.push({
        //   sender_sent_camp_ids: {
        //     $in: [parseInt(campaign_id_filter)], $exists: true
        //   }, 
        // })
      }

      /* Get Sender ID's - Filter Category ID - Start */
      var filter_sender_category_ids = getFilterArrayData(filter_category_id,'-Yes');

      find_paramas_query.push({
        sender_status: {
          $in: filter_sender_category_ids
        },
      });
      /* Get Sender ID's - Filter Category ID - Stop */
      //console.log('page name   ' + page_name); console.log('campaign_id_filter   ' + campaign_id_filter);
      if (page_name !== 'add' && campaign_id_filter !== '' && typeof campaign_id_filter !== 'undefined' && button_type == 'search') {

        /*
          (i). This find paramater only running in add new content and copy content
          (ii). Don't check new campaign
        */

        /* Campaign ID wise targeting sender - Checking - Start */
        include_camp_ids_arr.push(campaign_id_filter);

        find_paramas_query.push({
          campaign_ids: {
            $in: [campaign_id_filter]
          },
        });
        /* Campaign ID wise targettting sender - Checking - Stop */
      }
    }
    console.log('====filter parasm query');
    console.log(find_paramas_query);
    /* ############### Step-1 - "Sender Query" - Stop ###############*/
    /* ############### Step-2 - "Non-Sender Query" - Start ############### */

    if (filter_topper == 'non-sender') {
      /* Target Specific category ID */

      find_paramas_query.push({
        category_ids: {
          $in: cat_id_filter
        }
    });

    /* Target Specific category ID */

    /*
      (i). This find paramater only running in add new content and copy content
      (ii). Don't check new campaign
    */


    /*  Category wise and Campaign ID targettting sender - Checking - Start */
    if (page_name !== 'add' && campaign_id_filter != '' && typeof campaign_id_filter !== 'undefined') { 
      var replace_category_id        = category_id.replace(/\//ig,"");
      var replace_campaign_id_filter = campaign_id_filter.replace(/\//ig,""); 
      //Category ID
      exclude.push({
        sender_status : {
          $in : [new RegExp('^'+replace_category_id + '-Yes')]
        }
      });

      exclude_camp_ids_arr.push(new RegExp('^'+replace_campaign_id_filter));

      //Campaign ID
      exclude.push({
        campaign_ids: {
          $in: [new RegExp('^'+replace_campaign_id_filter)]
        }
      });

    }
    /* Campaign ID wise targettting sender - Checking - Stop */
  }
        
  /* ############### Step-2 - "Non-Sender Query" - Stop ###############*/

  /* ############### Step-3 - "Clicker Query" - Start ###############*/

  if (filter_topper == 'clicker') {

    find_paramas_query.push({
      category_id: {
        $in: cat_id_filter
      }
    });

    if (page_name != 'add' && campaign_id_filter != '' && typeof campaign_id_filter != 'undefined') {
      // clic_contact_exclude_obj['clicker_sent_camp_ids'] = {
      //             '$in': [parseInt(campaign_id_filter)]
      //           };
      clic_contact_exclude_obj.push({'clicker_sent_camp_ids': {'$in': [parseInt(campaign_id_filter)]}})
    }
  }
  /* ############### Step-3 - "Clicker Query" - Stop ###############*/


  /*  
      ############### Step 4 - Include and Exclude - START ###############
      It's Common For all toppers - Like Sender,non-sender,clicker
  */
  if (typeof include_camp_ids !== 'undefined' && include_camp_ids !== '') {

    /*  ############### Include Mode - Start ###############*/

    //if ('filter_campaign_id' in filter_params) {

      //Filter by campaignwise target
      if (include_camp_ids.length > 0) {

        if (filter_topper == 'clicker') {   // Array string value converting to integer
          var camp_id_arr     = getFilterClickerArrayData(include_camp_ids);  
          find_paramas_query.push({
            campaign_id: {
              $in: camp_id_arr
            }
          });

          // clic_contact_include_arr.push({
          //   campaign_ids: {
          //     $in: camp_id_arr
          //   }
          // })

          clic_contact_include_obj['campaign_ids'] = {$in: include_camp_ids};  //sending include_camp_ids because ids are strings

        } else {

          var camp_id_arr     = getFilterArrayData(include_camp_ids); 

          find_paramas_query.push({
            campaign_ids: {
              $in: include_camp_ids_arr.concat(camp_id_arr)
            }
          });
        }
      }
    //}
                
    //Filter by campaign and contentlavelwise target
    //if ('filter_content_label_id' in filter_params) {
    if (typeof include_cont_lbl_ids !== 'undefined' && include_cont_lbl_ids !== '') {
      //if (filter_content_label_id.length > 0) {
        if (include_cont_lbl_ids.length > 0) {

        if (filter_topper == 'clicker') {   // Array string value converting to integer
          var camp_content_id_arr = getFilterClickerArrayData(include_cont_lbl_ids); 
          find_paramas_query.push({
            content_label_id: {
              $in: camp_content_id_arr
            }
          });  

          // clic_contact_include_arr.push({
          //   content_label_id: {
          //     $in: camp_content_id_arr
          //   }
          // });
          clic_contact_include_obj['campaign_content_id'] = {$in: include_cont_lbl_ids};
        } else {
          var camp_content_id_arr = getFilterArrayData(include_cont_lbl_ids);
          find_paramas_query.push({
            campaign_content_id: {
              $in: camp_content_id_arr
            }
          });  
        }                                   
      }
    }
    /*  ############### Include Mode - Stop ###############*/

  } //end include_camp
  /*  ############### Exclude Mode - Start ###############*/
console.log('==== exclude camp ids===');
console.log(exclude_camp_ids);

  if (typeof exclude_camp_ids !== 'undefined' && exclude_camp_ids !== '') {
    
    //Filter by campaignwise target
    //if ('filter_campaign_id' in filter_params) {
         
      if (exclude_camp_ids.length > 0) {

        if (filter_topper == 'clicker') {   // Array string value converting to integer
          
          var camp_id_arr        = getFilterClickerArrayData(exclude_camp_ids); /*console.log('camp_id_arr');console.log(camp_id_arr);*/
          exclude.push({
            campaign_id: {
              $in: camp_id_arr
            }
          });

          // clicker_contact_exclude_arr.push({
          //   campaign_ids: {
          //     $in: camp_id_arr
          //   }
          // })
          
          //clic_contact_exclude_obj['campaign_ids'] = {$in: exclude_camp_ids};

          clic_contact_exclude_obj_arr.push({'campaign_ids' : {$in: exclude_camp_ids}});
          
          //click_arr.push({'campaign_ids' : {$in: exclude_camp_ids}});
        } else {

          var camp_id_arr       = getFilterArrayData(exclude_camp_ids);
          //camp_id_arr.toString(); 
       
          //exclude_camp_ids_arr.concat(camp_id_arr); console.log(exclude_camp_ids_arr);

          // exclude.push({
          //   campaign_ids: {
          //     $in: exclude_camp_ids_arr.concat(camp_id_arr)
          //   }
          // });
          and_exclude_obj_arr.push({'campaign_ids' : {$in: exclude_camp_ids_arr.concat(camp_id_arr)}});

        }                            
      }
    //}

    //Filter by campaign and contentlavelwise target
    //if('filter_content_label_id' in filter_params) {
      if (typeof exclude_cont_lbl_ids !== 'undefined' && exclude_cont_lbl_ids !== '') {
      //if (filter_content_label_id.length > 0) {
         if (exclude_cont_lbl_ids.length > 0) {
        if (filter_topper == 'clicker') {   // Array string value converting to integer
          var camp_content_id_arr    = getFilterClickerArrayData(exclude_cont_lbl_ids); 
          exclude.push({
            content_label_id: {
              $in: camp_content_id_arr
            }
          });

          // clicker_contact_exclude_arr.push({
          //   content_label_id: {
          //     $in: camp_content_id_arr
          //   }
          // });

          //clic_contact_exclude_obj['content_label_id'] = {$in: exclude_cont_lbl_ids};
          clic_contact_exclude_obj_arr.push({'campaign_content_id' : {$in: exclude_cont_lbl_ids}});

          //click_arr.push({'campaign_content_id' : {$in: exclude_cont_lbl_ids}});
        } else {
          var camp_content_id_arr    = getFilterArrayData(exclude_cont_lbl_ids);
          // exclude.push({
          //   campaign_content_id: {
          //     $in: camp_content_id_arr
          //   }
          // });

          and_exclude_obj_arr.push({'campaign_content_id' : {$in: camp_content_id_arr}});
        }
      }
    }

    /*  ############### Exclude Mode - Stop ###############*/

    if (Array.isArray(clic_contact_exclude_obj_arr) && clic_contact_exclude_obj_arr.length > 0) {
      
      clic_contact_exclude_obj.push({'$and': clic_contact_exclude_obj_arr});
    }
  } //end include_mode

//console.log(' camp_id arr ' + camp_id_arr); console.log(' exclude_camp_ids_arr arr ' + exclude_camp_ids_arr);
// if (exclude_camp_id_arr) {
//    exclude.push({
//     campaign_ids: {
//       $in: exclude_camp_ids_arr.concat(exclude_camp_id_arr)
//     }
//   });
// }

// if (include_camp_id_arr) {
//    find_paramas_query.push({
//       campaign_ids: {
//         $in: include_camp_ids_arr.concat(include_camp_id_arr)
//       }
//     });
// }
  /*  
    ############### Step 4 - Include and Exclude - STOP ###############
    It's Common For all toppers - Like Sender,non-sender,clicker
  */
if (Array.isArray(and_exclude_obj_arr) && and_exclude_obj_arr.length > 0) {
      exclude.push({'$and': and_exclude_obj_arr});
    }
  /* ############### Step-5- All Not in conditions exclude array assign with $nor condions ############### */
  console.log('exclude query'); console.log(and_exclude_obj_arr);
  if (exclude.length > 0) {
    
    //var exclude_filter_query      = exclude.reduce((key, value) => Object.assign(key, value), {}); 
    console.log('exclude query'); console.log(exclude);
    find_paramas_query.push({
      $nor: exclude
    });

  }
  /* ############### Step-5- All Not in conditions exclude array assign with $nor condions ############### */
  /* ############### Step-6- Basic Filters - START ############### */
  var basic_filter_search         = getBasicFilterFindParams(filter_topper,filter_search,minAge,maxAge);

  if (basic_filter_search.length > 0) {
    var basic_filter_array_to_object = basic_filter_search.reduce((basic_filter_key, basic_filter_value) => Object.assign(basic_filter_key, basic_filter_value), {});          
    find_paramas_query.push(basic_filter_array_to_object);
  }

  /* ############### Step-6- Basic Filters - STOP ############### */

  /* ############### Step-8- Filter - Get Total Count - START ############### */

  //Step 1: declare promise
  var search_query_promises       = getFilterQueryTotalCount(filter_topper,post_parameter,find_paramas_query,page_name,campaign_id_filter,filter_mode,contacts_collection,clicks_collection, clic_contact_include_obj, clic_contact_exclude_obj);

  //Step 2: async promise handler
  var totalCountPromise = async () => {
    
    var result = await (search_query_promises); 
    //anything here is executed after result is resolved
    return result;
  };

            
  if (filter_params.button_type == 'search') {
    //Step 3: make the call
    totalCountPromise().then(function(result) {
      client.close();
      res.json({
        'status': 200,
        'total_contacts': result
      });
    });

  } else {

    /* ############### Step-8- Filter - Get Total Count - START ############### */
    /* ############### Step-9- Campaign Save & Send SMS - START ############### */
    if (filter_params.button_type != 'search') {
      if (filter_params.button_type == 'new_campaign') {
        /* Fresh New Campaign - Creating Campaign & Content - Start*/
          var campaign_seq         = getNextSequence('campaign',counters_collection);
          var campaign_content_seq = getNextSequence('campaign_content_labels',counters_collection);
        /* Fresh New Campaign - Creating Campaign & Content - Start*/
      } else {
        var campaign_content_seq   = getNextSequence('campaign_content_labels',counters_collection);
      }
                    
      var campaignPromise = async () => {              

        if (filter_params.button_type == 'new_campaign') {

          /* Sequence ID - Generation Campaign and Content Label - Start */
          var campaign_collection_promise         = await (campaign_seq);
          var campaign_content_collection_promise = await (campaign_content_seq);

          return {'campaign_counter' : campaign_collection_promise,'campaign_content_counter':campaign_content_collection_promise};
          /* Sequence ID - Generation Campaign and Content Label - Stop */
        } else {

          /* Sequence ID - Generation Content Label - Start */
          var campaign_content_collection_promise = await (campaign_content_seq);
          return {'campaign_content_counter':campaign_content_collection_promise};
          /* Sequence ID - Generation Content Label - Start */
        }
      };

      campaignPromise().then(function(result) {    

        if (filter_params.button_type == 'new_campaign') {

          /* Return - Sequence ID(Campaign and Content Label) - Start */
          var campaign_object_id                = result.campaign_counter.value.seq; 
          var campaign_content_label_object_id  = result.campaign_content_counter.value.seq; 
          /* Return - Sequence ID(Campaign and Content Label) - Stop */

        } else {
          /* Return - Sequence ID(Content Label) - Start */
          var campaign_object_id                = filter_params.campaign_id;
          var campaign_content_label_object_id  = result.campaign_content_counter.value.seq; 
          /* Return - Sequence ID(Content Label) - Stop */
        }


        var total_range_contacts                = filter_params.total_contacts;

        var skip_val                            = parseInt(filter_params.minRecord); 
        if (skip_val == '' || isNaN(skip_val)) {
          skip_val = 0;
        } else {
          skip_val = skip_val - 1;
        }

        var max_range_val  = parseInt(filter_params.maxRecord);
        if (max_range_val == '' || isNaN(max_range_val)) {
          max_range_val   = total_range_contacts;
        } 

        var limit_val     = max_range_val - skip_val;
        var url_shortcode = nanoid(3);

        if (filter_params.button_type == 'new_campaign') {

          /* ############# New - Campaign Save - (Start) ############# */

          var object_campaign_data = {
            '_id'             : campaign_object_id,
            'category_id'     : category_id,
            'name'            : filter_params.campaign_name,
            'sms_content'     : filter_params.message,
            'destination_url' : filter_params.destination_url,
            'status'          : "1",
            'draft_status'    : 1,
            'created_by'      : req.user.name,
            'created_at'      : new Date().toLocaleString('en-US', {
                                  timeZone: 'Asia/Calcutta'
                                }),
            'date_added'      : new Date()
          };
                            
          saveCollectionDocuments(campaign_collection,object_campaign_data);
         
         

          // console.log('======filter parasm ======');
          // console.log(filter_params)
          /* ############# New - Campaign Save - (Stop) ############# */

          /* ############# New - Campaign Content Label Save - (Start) ############# */

          var object_campaign_lable_data = {
            '_id'             : campaign_content_label_object_id,
            'campaign_id'     : parseInt(campaign_object_id),
            'url_shortcode'   : url_shortcode,
            'label_name'      : filter_params.content_label_name,
            'sms_content'     : filter_params.message,
            'destination_url' : filter_params.destination_url,
            'segments'        : filter_params,
            'total_sender'    : limit_val,
            'status'          : "1",
            'draft_status'    : 1,
            'created_by'      : user_name,
            'user_id'         : user_id,
            'schedule_status' : filter_params.schedule_status,
            'created_at'      : new Date().toLocaleString('en-US', {
                                  timeZone: 'Asia/Calcutta'
                                }),
            'date_added'      : new Date()
          };

          saveCollectionDocuments(campaign_content_labels,object_campaign_lable_data);

          /* ############# New - Campaign Content Label Save - (STOP) #############*/

        } else {

          /*
              1. Add new content
              2. Copy Content
          */
          //console.log('fillllldlggdg');console.log(filter_params);
          var object_campaign_data = {
            'name': filter_params.campaign_name,
          };
          /* ############# New - Campaign Content Label Save - (Start) ############# */

          var object_campaign_lable_data = {
            '_id'             : campaign_content_label_object_id,
            'campaign_id'     : parseInt(campaign_object_id),
            'url_shortcode'   : url_shortcode,
            'label_name'      : filter_params.content_label_name,
            'sms_content'     : filter_params.message,
            'destination_url' : filter_params.destination_url,
            'segments'        : filter_params,
            'total_sender'    : limit_val,
            'status'          : "1",
            'draft_status'    : 1,
            'created_by'      : user_name,
            'user_id'         : user_id,
            'schedule_status' : filter_params.schedule_status,
            'created_at'      : new Date().toLocaleString('en-US', {
                                  timeZone : 'Asia/Calcutta'
                                }),
            'date_added'      : new Date()
          };


          saveCollectionDocuments(campaign_content_labels,object_campaign_lable_data);

          /* ############# New - Campaign Content Label Save - (STOP) #############*/
        }

        /* ############# Find Total Contacts and SEND SMS - START ############# */

        var saved_ids           = {
          'campaign_id'         : campaign_object_id,
          'campaign_content_id' : campaign_content_label_object_id
        };


        findTotalContacts(filter_topper,find_paramas_query,saved_ids,skip_val,limit_val,url_shortcode,filter_params,user_name,user_id,clicks_collection,contacts_collection,smpp_collection,campaign_content_labels,activity_logs,sms_logs, object_campaign_data, object_campaign_lable_data, clic_contact_include_obj, clic_contact_exclude_obj);

        /* ############# Find Total Contacts and SEND SMS - STOP ############# */
        res.json({
          'status'              : 200,
          'campaign_id'         : campaign_object_id,
          'campaign_content_id' : campaign_content_label_object_id
        });
      });
    }
    /* ############### Step-9- Campaign Save - START ############### */
    }
  }); //mongoDB connection brackets closed
}); //uni_submit


//Step-1-Functions
function getCampaignGeneralSearchParameters(filter_params) {
  //console.log(filter_params);
  var category_id             = filter_params.category_id;
  var filter_category_id      = filter_params.filter_category_id; //category (filter-category)
  // var filter_campaign_id      = filter_params.filter_campaign_id; //campaign (filter-campaign)

  // if (typeof filter_campaign_id == 'undefined') {
  //   filter_campaign_id        = [];
  // }
  // var filter_content_label_id = filter_params.filter_content_label_id; //campaign content label

  // if (typeof filter_content_label_id == 'undefined') {
  //   filter_content_label_id   = [];
  // }

  var filter_topper           = filter_params.filter_topper_id; // sender [or] non-sender [or] clicker
  var filter_mode             = filter_params.filter_mode; // on-Include [or] off-Exclude
  var minRecord               = filter_params.minRecord; // Record Selection
  var maxRecord               = filter_params.maxRecord; // Record Selection
  var minAge                  = filter_params.minAge; // Min Age
  var maxAge                  = filter_params.maxAge; // Max Age
  if (maxAge == '100') {
    var maxAge                = '99';
  }
  var filter_search           = filter_params.filter_search;
  var page_name               = filter_params.page_name; //console.log(page_name)
  var campaign_id_filter      = filter_params.stored_camp_id;
  var button_type             = filter_params.button_type;
  var action                  = filter_params.action;
  var include_camp_ids        = filter_params.include_camp_ids;
  var exclude_camp_ids        = filter_params.exclude_camp_ids;
  var include_cont_lbl_ids    = filter_params.include_cont_lbl_ids;
  var exclude_cont_lbl_ids    = filter_params.exclude_cont_lbl_ids;

  var result                  = {
    'category_id'             : category_id,
    'filter_category_id'      : filter_category_id,
    // 'filter_campaign_id'      : filter_campaign_id,
    // 'filter_content_label_id' : filter_content_label_id,
    'filter_topper'           : filter_topper,
    'filter_mode'             : filter_mode,
    'minRecord'               : minRecord,
    'maxRecord'               : maxRecord,
    'minAge'                  : minAge,
    'maxAge'                  : maxAge,
    'filter_search'           : filter_search,
    'page_name'               : page_name,
    'campaign_id_filter'      : campaign_id_filter,
    'button_type'             : button_type,
    'action'                  : action,
    'include_camp_ids'        : include_camp_ids,
    'exclude_camp_ids'        : exclude_camp_ids,
    'include_cont_lbl_ids'    : include_cont_lbl_ids,
    'exclude_cont_lbl_ids'    : exclude_cont_lbl_ids,
  };
  return result;
} //getCampaignGeneralSearchParameters


function getCampaignSaveParameters (filter_params, campaign_collection) {

  /* STEP-1 */
  var camp_temp_id               = filter_params.camp_id; //Draft Temporary ID
  if (camp_temp_id) {
    campaign_collection.deleteOne({
      _id: camp_temp_id.toString()
    });
  }
  var campaign_name             = filter_params.get_campaign_name;
  var category_id               = filter_params.category_id;

  /* STEP-2 */
  var content_label_name        = filter_params.content_label_name;
  var text_message              = filter_params.hide_text;
  var message                   = text_message.replace(/(\r\n|\n|\r)/gm, "");
  var export_message            = "\"" + message + "\"";
  var destination_url           = filter_params.destination_url;
  /* STEP-3 */
  var filter_category_id        = filter_params.filter_category_id; //category (filter-category)

  // var filter_campaign_id        = filter_params.filter_campaign_id; //campaign (filter-campaign)
  // if (typeof filter_campaign_id == 'undefined') {
  //     filter_campaign_id        = [];
  // }
  // /* checking filter campaign contains all option*/
  // var filter_all_camp_sel_ids   = filter_params.fil_camp_all_ids;
  // if (filter_all_camp_sel_ids !== '') {
  //   var fil_all_camp_arr        = filter_all_camp_sel_ids.split(",");
  //   filter_campaign_id          = fil_all_camp_arr;
  // }
  // /* checking filter campaign contains all option end*/
  // var filter_content_label_id   = filter_params.filter_content_label_id; //campaign content label
  //  checking filter content label contains all option
  // var filter_all_cont_sel_ids   = filter_params.fil_content_label_all_ids;
  // if (filter_all_cont_sel_ids !== '') {
  //   var fil_all_cont_lab_arr    = filter_all_cont_sel_ids.split(",");
  //   filter_content_label_id     = fil_all_cont_lab_arr;
  // }


  // if (typeof filter_content_label_id == 'undefined') {
  //     filter_content_label_id    = [];
  // }

  // /* checking filter content label contains all option end*/
  // var check_non_selected_con_ids = filter_params.non_selected_con_ids;
  // if (check_non_selected_con_ids !== '') {
  //   filter_content_label_id      = check_non_selected_con_ids.split(",");
  // }
  
  var filter_topper_id     = filter_params.filter_topper_id; // sender [or] non-sender [or] clicker
  var filter_mode          = filter_params.filter_mode; // on-Include [or] off-Exclude
  var minRecord            = filter_params.minRecord; // Record Selection
  var maxRecord            = filter_params.maxRecord; // Record Selection
  var minAge               = filter_params.minAge; // Min Age
  var maxAge               = filter_params.maxAge; // Max Age
  var page_name            = filter_params.page_name;
  var campaign_id_filter   = filter_params.stored_camp_id;
  var button_type          = filter_params.button_type;
  var filter_search        = filter_params.filter_search;
  var sender_id            = filter_params.sender_id;
  var smpp_id              = filter_params.smpp_id;
  var total_contacts       = filter_params.total_contacts;
  var original_sms_content = filter_params.original_sms_content;
  var action               = filter_params.action;

  if (action=='confirm_schedule') {

    var schedule_date_and_time = new Date(filter_params.schedule_date_and_time);
    var schedule_date      = (schedule_date_and_time.getMonth()+1)+'-'+schedule_date_and_time.getDate()+'-'+schedule_date_and_time.getFullYear();
    var schedule_time      = schedule_date_and_time.getHours() + ":" + schedule_date_and_time.getMinutes();
    var schedule           = schedule_date+' '+schedule_time;
    var schedule_status    = '0';

  } else {
    var schedule           = '';
    var schedule_status    = '1';
  }

  if (maxAge == '100') {
      var maxAge           = '99';
  }

  var total_sms_sent       = filter_params.total_sms_sent; 

  var button_type          = filter_params.button_type;

  if (button_type == 'new_content_label' || button_type == 'copy_new_content') {
    var campaign_id        = filter_params.campaign_id;
  } else {
    var campaign_id        = '';
  }

  var include_camp_ids     = filter_params.include_camp_ids;
  var exclude_camp_ids     = filter_params.exclude_camp_ids;
  var include_cont_lbl_ids = filter_params.include_cont_lbl_ids;
  var exclude_cont_lbl_ids = filter_params.exclude_cont_lbl_ids;
  var filter_campaign_ids_obj      = filter_params.filter_campaign_ids_obj;
  var filter_content_label_ids_obj = filter_params.filter_content_label_ids_obj; 
  var filter_modes_obj             = filter_params.filter_modes_obj;

  var result               = {
    'button_type'            : button_type,
    'campaign_name'          : campaign_name,
    'campaign_id'            : campaign_id,
    'category_id'            : category_id,
    'content_label_name'     : content_label_name,
    'text_message'           : text_message,
    'message'                : message,
    'original_sms_content'   : original_sms_content,
    'export_message'         : export_message,
    'destination_url'        : destination_url,
    'filter_category_id'     : filter_category_id,
    // 'filter_campaign_id'     : filter_campaign_id,
    // 'filter_all_camp_sel_ids'    : filter_all_camp_sel_ids,
    // 'filter_content_label_id'    : filter_content_label_id,
    // 'check_non_selected_con_ids' : check_non_selected_con_ids,
    'filter_topper'          : filter_topper_id,
    'filter_mode'            : filter_mode,
    'minRecord'              : minRecord,
    'maxRecord'              : maxRecord,
    'minAge'                 : minAge,
    'maxAge'                 : maxAge,
    'smpp_id'                : smpp_id,
    'total_contacts'         : total_contacts,
    'sender_id'              : sender_id,
    'page_name'              : page_name,
    'campaign_id_filter'     : campaign_id_filter,
    'filter_search'          : filter_search,
    'button_type'            : button_type,
    'total_sms_sent'         : total_sms_sent,
    'schedule_date_and_time' : schedule,
    'action'                 : action,
    'schedule_status'        : schedule_status,
    'include_camp_ids'       : include_camp_ids,
    'exclude_camp_ids'       : exclude_camp_ids,
    'include_cont_lbl_ids'   : include_cont_lbl_ids,
    'exclude_cont_lbl_ids'   : exclude_cont_lbl_ids,
    'filter_campaign_ids'      : filter_campaign_ids_obj,
    'filter_content_label_ids' : filter_content_label_ids_obj,
    'filter_modes'             : filter_modes_obj,
  }
  return result;
} //getCampaignSaveParameters end

/**
 * method: getBasicFilterFindParams
 * description: Getting array data for search filters 
 * params: filter_topper, filter_search(lead_from_date, lead_to_date, source, country, state, city, nationality, pincode, gender,locality, profession, salary,  operator_name, minAge, maxAge)
 * return: returns passed filter search params array data 
*/

function getBasicFilterFindParams(filter_topper, filter_search,minAge,maxAge) {
    
  var basic_filters_array = [];

  /** Lead Date filter Start */
  if ('lead_from_date' in filter_search && 'lead_to_date' in filter_search) {
    var lead_from_date = filter_search.lead_from_date;
    var lead_to_date = filter_search.lead_to_date;
    if (lead_from_date && lead_to_date) {
      var start_date = new Date(lead_from_date);
      //start_date.setHours(0, 0, 0, 0);
      var end_date = new Date(lead_to_date);
      end_date.setHours(23, 59, 59, 59);
      basic_filters_array.push({
        'lead_date': {
          $gt: ISODate(start_date),
          $lte: ISODate(end_date)
        }
      });
    }
    var conditions = {
      'lead_date': {
        $gt: ISODate(start_date),
        $lte: ISODate(end_date)
      }
    }
  }
  /** Lead Date filter stop */

  /** source filter start */
  if ('source' in filter_search) {
    var source = getFilterSearchInput(filter_search, filter_search.source);
    if (source) {
      basic_filters_array.push({
        'source': {
          $in: source
        }
      });
    }
  }
  /** source filter stop */

  /** country filter start */
  if ('country' in filter_search) {
    var country = getFilterSearchInput(filter_search, filter_search.country); 

    var country_column = 'country';

    if (filter_topper == 'clicker') {
      country_column = 'location.country_name';
    }

    if (country) {
      basic_filters_array.push({
        [country_column]: {
          $in: country
        }
      });
    }
  }
  /** country filter stop */

  /** state filter start */
  if ('state' in filter_search) {
    var state = getFilterSearchInput(filter_search, filter_search.state, is_regex = true); 
    var state_column = 'state';

    if (filter_topper == 'clicker') {
      state_column = 'location.region';
    }

    if (state) {
      basic_filters_array.push({
        [state_column]: {
          $in: state
        }
      });
    }
  } 
  /** state filter stop */
//console.log(new RegExp('^'+state));
  /** city filter start */
  if ('city' in filter_search) {
    var city = getFilterSearchInput(filter_search, filter_search.city);

    var city_column = 'city';

    if (filter_topper == 'clicker') {
      city_column = 'location.city';
    }

    if (city) {
      basic_filters_array.push({
        [city_column]: {
          $in: city
        }
      });
    }
  }
  /** city filter stop */

  /** nationality filter start */
  if ('nationality' in filter_search) {
    var nationality = getFilterSearchInput(filter_search, filter_search.nationality);
    if (nationality) {
      basic_filters_array.push({
        'nationality': {
          $in: nationality
        }
      });
    }
  }
  /** nationality filter stop */

  /** pincode filter start */
  if ('pincode' in filter_search) {
    var pin_code = filter_search.pincode;

    var pin_code_column = 'pin_code';

    if (filter_topper == 'clicker') {
      pin_code_column = 'location.postal';
    }

    basic_filters_array.push({
      [pin_code_column]: pin_code
    });
  }
  /** pincode filter stop */

  /** gender filter start */
  if ('gender' in filter_search) {
    var gender = getFilterSearchInput(filter_search, filter_search.gender);
    if (gender) {
      basic_filters_array.push({
        'gender': {
          $in: gender
        }
      });
    }
  }
  /** gender filter stop */

  /** Locality filter start */
  if ('locality' in filter_search) {
    var locality = filter_search.locality;
    if (locality) {
      basic_filters_array.push({
        'locality': locality
      });
    }
  }
  /** Locality filter stop */

  /** profession filter stop */
  if ('profession' in filter_search) {
    var profession = getFilterSearchInput(filter_search, filter_search.profession);
    if (profession) {
      basic_filters_array.push({
        'profession': {
          $in: profession
        }
      });
    }
  }
  /** profession filter stop */

  /** Salary filter start */
  if ('salary' in filter_search) {
    var salary = filter_search.salary;
    if (salary) {
      basic_filters_array.push({
        'salary': salary
      });
    }
  }
  /** Salary filter stop */

  /** operator filter start */
  if ('operator_name' in filter_search) {
    var operator = getFilterSearchInput(filter_search, filter_search.operator_name); 
    var op_column = 'operator';

    if (filter_topper == 'clicker') {
      op_column = 'location.carrier.name';
    }
    if (operator) {
      basic_filters_array.push({
        [op_column]: {
          $in: operator
        }
      });
    }
  }
  /** operator filter stop */

  /** min age and max age filter start */
  if (minAge != '0' || maxAge != '99') {
    basic_filters_array.push({
      'age': {
        $gte: minAge,
        $lte: maxAge
      }
    });
  }
  /** min age and max age filter stop */
  return basic_filters_array;
} //end getBasicFilterFindParams

/**
 * method: getFilterQueryTotalCount
 * description: Getting total contacts count from database based on data passed in query
 * params: filter_topper,filter_params,filter_query,page_name,campaign_id_filter,filter_mode,contacts_collection,clicks_collection
 * return: returns total numbers of contacts
*/

async function getFilterQueryTotalCount(filter_topper, filter_params, filter_query, page_name, campaign_id_filter, filter_mode,contacts_collection, clicks_collection, clicker_contact_include_obj, clicker_contact_exclude_obj) {
  var query = filter_query.reduce((key, value) => Object.assign(key, value), {}); console.log(query);
  
  console.log('==========clicker_contact_include_obj====');
  console.log(clicker_contact_include_obj); console.log(clicker_contact_exclude_obj)
  query  = {$and: [query]}; 
  var mobile_number_list = [];
 
  if (filter_params.button_type == 'search') {
    if (filter_topper == 'sender' || filter_topper == 'non-sender') {
        
      if (filter_topper == 'non-sender') {
          /* ######## Non-sender - Always batch limit is 5 lacks */
        var total_contacts = await contacts_collection.count(query, {limit: 500000});
      } else {
        // if (filter_params.filter_mode == 'include') {
        //   var total_contacts = await contacts_collection.count(query);
        // } else {
          var total_contacts = await contacts_collection.count(query, {limit: 500000});
        //}
        console.log('total contacts' + total_contacts);
      }

    } else {

      if (filter_params.filter_mode == 'include') {
        var total_contacts = await clicks_collection.aggregate({
            $match: query
        }, {
          $group: {
            _id: {
              contact_no: '$contact_no',
            },
          }
        }).limit(500000).forEach(function(data) {
          var mobile_no = data.contact_no;
          mobile_number_list.push(mobile_no.toString());
        });
      } else {
        var total_contacts = await clicks_collection.aggregate({
            $match: query
        }, {
          $group: {
            _id: {
              contact_no: '$contact_no',
            },
          }
        }).forEach(function(data) {
          var mobile_no = data.contact_no;
          mobile_number_list.push(mobile_no.toString());
        });
      }
        /* ######## Clicker Find & fetch count ######## */
        //var mobile_number_list = [];
        
        

        var clicker_mbl_no_lst      = [];
       
        var count_tot_contacts      = mobile_number_list;
         
        if (mobile_number_list.length > 0) {

          var remove_dup_mbl_json_arr = [];
          var remove_dup_mbl_no_json = {};
          
          remove_dup_mbl_no_json['mobile_no'] = {'$in': mobile_number_list};
          remove_dup_mbl_no_json['is_active'] = {'$in': [1]};
 
          remove_dup_mbl_json_arr.push(remove_dup_mbl_no_json);

          if (Object.keys(clicker_contact_include_obj).length > 0) {
             remove_dup_mbl_json_arr.push(clicker_contact_include_obj);
           
          }
          
          if (Object.keys(clicker_contact_exclude_obj).length > 0) {
            //remove_dup_mbl_no_json.push({'$nor': clicker_contact_exclude_obj});
            remove_dup_mbl_json_arr.push({'$nor': clicker_contact_exclude_obj});

          }
          
          
          console.log("================ remove_dup_mblno_query ========"); console.log(mobile_number_list)
          console.log(remove_dup_mbl_json_arr);
          //remove_dup_mblno_query['is_active']['$in'] = 1;

          var export_details = await contacts_collection.find({'$and':remove_dup_mbl_json_arr}).forEach(function(record) {
            var mobile_no = record.mobile_no;
            clicker_mbl_no_lst.push(mobile_no);
          }); console.log('/****************** export_details**************/'); console.log(export_details);
          count_tot_contacts = clicker_mbl_no_lst;

          var total_contacts = count_tot_contacts.length;
        } else {
          var total_contacts = mobile_number_list.length;
        }
    }
  } else {
    var total_contacts = filter_params.total_contacts;
  }

  return total_contacts;
}

async function getNextSequence(name,counters_collection) {

    return new Promise((resolve, reject) => {
        counters_collection.findOneAndUpdate({ _id: name },{$inc: { seq: 1 }}, {returnOriginal: false, new:true, upsert:true}, function(err, response) {
            err ? reject(err): resolve(response);
        });
    });
}

async function saveCollectionDocuments(collection_name, object_data) {
  collection_name.insert(object_data);
}

async function findTotalContacts(filter_topper,filter_query,saved_ids,skip_val,limit_val,url_shortcode,filter_params,user_name,user_id,clicks_collection,contacts_collection,smpp_collection,campaign_content_labels,activity_logs,sms_logs, object_campaign_data, object_campaign_lable_data, clicker_contact_include_obj, clicker_contact_exclude_obj) {

  var smpp_id   = filter_params.smpp_id;
  var sender_id = filter_params.sender_id;
  var message   = filter_params.message;
  var action    = filter_params.action;

  var query     = filter_query.reduce((key, value) => Object.assign(key, value), {});
  query = {$and: [query]};

  if (filter_topper == 'sender' || filter_topper == 'non-sender') {
      var total_contacts = await contacts_collection.find(query).project({_id:0,mobile_no:1,unique_id:1}).skip(skip_val).limit(limit_val).toArray();

      if (total_contacts) {

        if (action=='confirm_schedule') {
          /* ########## CAMPAIGN SCHEDULE - Start ########## */
          scheduleCampaignSMS(smpp_id,sender_id,message,total_contacts,url_shortcode,saved_ids,filter_params,user_name,user_id,smpp_collection,contacts_collection,activity_logs,sms_logs,'SCHEDULE');
          /* ########## CAMPAIGN SCHEDULE - Stop ########## */

          /* ########## Update to total_sender - Campaign Content - Start ########## */
          updateToCampaignContentLabel(saved_ids,total_contacts.length,campaign_content_labels);
          /* ########## Update to total_sender - Campaign Content - Stop ########## */

        } else {

          /* ########## SEND SMS - Sender or Nonsender - Start ########## */
          //sendSMSThoroughSMPP(smpp_id,sender_id,message,total_contacts,url_shortcode,saved_ids,filter_params,user_name,user_id,smpp_collection,contacts_collection,activity_logs,sms_logs);
          /* ########## SEND SMS - Sender or Nonsender - Stop ########## */

          /* ########## CAMPAIGN SCHEDULE - Start ########## */
          scheduleCampaignSMS(smpp_id,sender_id,message,total_contacts,url_shortcode,saved_ids,filter_params,user_name,user_id,smpp_collection,contacts_collection,activity_logs,sms_logs,'SUBMIT');
          /* ########## CAMPAIGN SCHEDULE - Stop ########## */

          /* ########## Update to total_sender - Campaign Content - Start ########## */
          updateToCampaignContentLabel(saved_ids,total_contacts.length,campaign_content_labels);
          /* ########## Update to total_sender - Campaign Content - Stop ########## */

        }

      }

  } else {

  /* ############### Clicker - Find and Send SMS - (Start) ############### */

  var mobile_number_list        = [];

  var find_contacts             = await clicks_collection.aggregate({
                                      $match: query
                                  },
                                  
                                  {
                                    $group: {

                                        _id: {
                                            contact_no: '$contact_no',
                                        },
                                    }
                                  }
                                 
                              ).limit(500000).forEach(function(data) {
                                  var mobile_no             = data.contact_no;
                                  mobile_number_list.push(mobile_no.toString());
                              }); 

  var clicker_mbl_no_lst        = [];
       
  var count_tot_contacts        = mobile_number_list;
         
  console.log(mobile_number_list)
  if (mobile_number_list.length > 0) {

    var remove_dup_mbl_json_arr = [];
    var remove_dup_mbl_no_json  = {};
    
    remove_dup_mbl_no_json['mobile_no'] = {'$in': mobile_number_list};
    remove_dup_mbl_no_json['is_active'] = {'$in': [1]};

    remove_dup_mbl_json_arr.push(remove_dup_mbl_no_json);

    if (Object.keys(clicker_contact_include_obj).length > 0) {
       remove_dup_mbl_json_arr.push(clicker_contact_include_obj);
   
    }
    
    if (Object.keys(clicker_contact_exclude_obj).length > 0) {
      //remove_dup_mbl_no_json.push({'$nor': clicker_contact_exclude_obj});
      remove_dup_mbl_json_arr.push({'$nor': clicker_contact_exclude_obj});

    }

    var total_contacts = await contacts_collection.find({'$and':remove_dup_mbl_json_arr}).skip(skip_val).limit(limit_val).toArray(); console.log(total_contacts)
    }

    if (total_contacts) {

          if (action == 'confirm_schedule') {
              /* ##########  CAMPAIGN SCHEDULE - Start ########## */
              scheduleCampaignSMS(smpp_id,sender_id,message,total_contacts,url_shortcode,saved_ids,filter_params,user_name,user_id,smpp_collection,contacts_collection,activity_logs,sms_logs,'SCHEDULE');
              /* ##########  CAMPAIGN SCHEDULE - Stop ########## */

              /* ########## Update to total_sender - Campaign Content - Start ########## */
              updateToCampaignContentLabel(saved_ids,total_contacts.length,campaign_content_labels);
              /* ########## Update to total_sender - Campaign Content - Stop ########## */

          } else {

            /* ########## SEND SMS - Immediatly - Start ########## */
            //sendSMSThoroughSMPP(smpp_id,sender_id,message,total_contacts,url_shortcode,saved_ids,filter_params,user_name,user_id,smpp_collection,contacts_collection,activity_logs,sms_logs);
            /* ########## SEND SMS  - Immediatly - Stop ########## */

            /* ##########  SUBMIT & SEND LATER - Start ########## */
              scheduleCampaignSMS(smpp_id,sender_id,message,total_contacts,url_shortcode,saved_ids,filter_params,user_name,user_id,smpp_collection,contacts_collection,activity_logs,sms_logs,'SUBMIT');
            /* ########## SUBMIT & SEND LATER- Start ########## */

            /* ########## Update to total_sender - Campaign Content - Start ########## */
            updateToCampaignContentLabel(saved_ids,total_contacts.length,campaign_content_labels);
            /* ########## Update to total_sender - Campaign Content - Stop ########## */

          }
        }

        /* ############### Clicker - Find and Send SMS - (Stop) ############### */

    }

     /**** send sms after creating campaign *****/
     sendEmailAfterCampaign(object_campaign_data, object_campaign_lable_data,total_contacts.length);
     /**** end send sms ***/
}

async function scheduleCampaignSMS(smpp_id,sender_id,message,total_contacts,url_shortcode,saved_ids,filter_params,user_name,user_id,smpp_collection,contacts_collection,activity_logs,sms_logs,message_status) {

    var category_id=filter_params.category_id;
    var filter_category_id=filter_params.filter_category_id;
    var campaign_object_id=saved_ids.campaign_id;
    var campaign_content_object_id=saved_ids.campaign_content_id;    
    var filter_topper=filter_params.filter_topper;
    var total_range_contacts = filter_params.total_contacts;
    var original_sms_content = filter_params.original_sms_content;
    var schedule_date_and_time = filter_params.schedule_date_and_time;

    // Send some messages to the queue.  If we're not currently connected, these will be queued up in memory
    // until we connect.  Note that `sendToQueue()` and `publish()` return a Promise which is fulfilled or rejected
    // when the message is actually sent (or not sent.)
    // Publisher
    // connection.then(function(conn) {
    //   return conn.createChannel();
    // }).then(function(ch) {

    //   ch.assertQueue(QUEUE_NAME).then(function(ok) {

          var mobile_number_list = [];
          var schedule_bulk_list = [];

          total_contacts.forEach(function(record) {

              var mobile_no = record.mobile_no;
              var shortcode = record.unique_id + '_' + url_shortcode;
              var tracking_url = constants.TRACKING_URL+shortcode;
              var sms_content = original_sms_content.replace("{url}", tracking_url);
              mobile_number_list.push(mobile_no);

              var response = {};
              response['campaign_id'] = parseInt(campaign_object_id);
              response['campaign_content_label_id'] = parseInt(campaign_content_object_id);
              response['mobile_no'] = mobile_no;
              response['sms_content'] = sms_content;
              response['smpp_id'] = smpp_id;
              response['sender_id'] =  sender_id;
              response['message_status'] = message_status; //default message status
              response['message_type'] = 'Text SMS'; //default message status
              response['mode'] = '1';
              response['mode_type'] = 'live';

              if(message_status=='SCHEDULE'){
                response['schedule_date_and_time']=new Date(schedule_date_and_time);
                response['indian_schedule_date_and_time']=new Date(schedule_date_and_time).toLocaleString('en-US', {
                  timeZone: 'Asia/Calcutta'
                });           
                response['created_at'] = new Date(schedule_date_and_time); 
              }else{
                response['created_at'] = new Date(); 
              }
              
              response['message_submit_date'] = new Date().toLocaleString('en-US', {
                timeZone: 'Asia/Calcutta'
              }); 

              // console.log("Message was sent!")
              // ch.sendToQueue(QUEUE_NAME, Buffer.from(JSON.stringify(response)));

              schedule_bulk_list.push(response);

          });

          /* ########## Bulk - Insert Schedule SMS Logs - Start ########## */
           saveMessages(schedule_bulk_list,sms_logs);
          /* ########## Bulk - Insert Schedule SMS Logs - Stop ########## */

          /* ########## Update to Campaign ID's and content Label ID's - Start ########## */
            bulkUpdateContactCollectionMappingValues(filter_topper,mobile_number_list,contacts_collection,category_id,filter_category_id,campaign_object_id,campaign_content_object_id,total_contacts,total_range_contacts,filter_params,user_name,user_id,activity_logs);
          /* ########## Update to Campaign ID's and content Label ID's - Stop ########## */
  //     });

  // }).catch(function(err) {
  //     console.log(err);
  //     return console.log("Message was rejected...  Boo!");
  // });

}

async function sendSMSThoroughSMPP(smpp_id,sender_id,message,total_contacts,url_shortcode,saved_ids,filter_params,user_name,user_id,smpp_collection,contacts_collection,activity_logs,sms_logs) {

    var category_id=filter_params.category_id;
    var filter_category_id=filter_params.filter_category_id;
    var campaign_object_id=saved_ids.campaign_id;
    var campaign_content_object_id=saved_ids.campaign_content_id;    
    var filter_topper=filter_params.filter_topper;
    var total_range_contacts = filter_params.total_contacts;
    var original_sms_content = filter_params.original_sms_content;
    
    var smpp_details = await smpp_collection.findOne({
        'smpp_id': smpp_id
    });

    var session = new smpp.Session({host: smpp_details.ip_address, port: smpp_details.host[0]});


    delete smpp.encodings.ASCII;

    var didConnect = false; 

    session.on('connect', function(){

        didConnect = true;

        session.bind_transceiver({
              system_id: smpp_details.system_id, //SMPP - Settings Table
              password: smpp_details.password,  //SMPP - Settings Table
        }, function(pdu) {

           if(pdu.command_status == 0) { 

                var mobile_number_list = [];
                var save_sms_response = [];

                total_contacts.forEach(function(record) {

                    var mobile_no = record.mobile_no;
                    var shortcode = record.unique_id + '_' + url_shortcode;
                    var tracking_url = constants.TRACKING_URL+shortcode;
                    var sms_content = original_sms_content.replace("{url}", tracking_url);
                    mobile_number_list.push(mobile_no);

                    //Successfully bound
                    session.submit_sm({
                        source_addr: sender_id, //SENDER ID
                        destination_addr:mobile_no, // Mobile Number
                        short_message:sms_content, // Text SMS
                        registered_delivery:1, //Mandatory Send Delivery Response
                    }, function(pdu) {

                       
                        var message_list = [];

                        if (pdu.command_status == 0) {
                            // Message successfully sent
                            var response = {};
                            response['campaign_id'] = parseInt(campaign_object_id);
                            response['campaign_content_label_id'] = parseInt(campaign_content_object_id);
                            response['mobile_no'] = mobile_no;
                            response['sms_content'] = sms_content;

                            response['smpp_id'] = smpp_details.name;
                            response['sender_id'] =  sender_id;

                            response['command_id'] = pdu.command_id;
                            response['command_status'] = pdu.command_status;
                            response['sequence_number'] = pdu.sequence_number;
                            response['message_id'] = pdu.message_id;
                            response['submit_sm_resp'] = pdu;

                            response['message_status'] = 'PENDING'; //default message status
                            response['message_type'] = 'Text SMS'; //default message status
                            response['message_submit_date'] = new Date().toLocaleString('en-US', {
                              timeZone: 'Asia/Calcutta'
                            }); 

                            response['created_at'] = new Date(); 
                            saveCollectionDocuments(sms_logs,response);
                            
                        }


                    }); //submit_sm session - closed
                    
                 
                }); //foreach - closed


                /* ########## Update to Campaign ID's and content Label ID's - Start ########## */
                  bulkUpdateContactCollectionMappingValues(filter_topper,mobile_number_list,contacts_collection,category_id,filter_category_id,campaign_object_id,campaign_content_object_id,total_contacts,total_range_contacts,filter_params,user_name,user_id,activity_logs);
                /* ########## Update to Campaign ID's and content Label ID's - Stop ########## */

            }//PDU-Command Status Condtions - Closing

         }); //session-bind - closed


        // After sending Delivery Report
        session.on('pdu', function(pdu) {

            if(pdu.command=='query_sm')
            {
                var query_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference ='';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference=pdu.short_message.user_message_reference;
                }


                var parse_deliver_text_str = text.toString(); 
                var receipted_id = receipted_message_id; 

                const querystring = require('querystring');    
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {decodeURIComponent: s =>  s.trim()});

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {decodeURIComponent: s =>  s.trim()});

                  var msg_id = parse_deliver_text_json.id;
                  query_response['service_type'] = pdu.service_type;
                  query_response['protocol_id'] = pdu.protocol_id;
                  query_response['priority_flag'] = pdu.priority_flag;
                  query_response['message_status'] = parse_deliver_text_json.stat;
                  query_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                  query_response['sub'] = parse_deliver_text_json.sub;
                  query_response['message_err'] = parse_deliver_text_json.err;
                  query_response['deliver_sm_data'] = pdu;
                  query_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                  query_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                  query_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                  });
              
                 session.deliver_sm_resp({ sequence_number: pdu.sequence_number });

                /* Update - Message Status & Delivery Response - Start */
                    updatingMessageCollection({'message_id':msg_id},delivery_response,sms_logs);
                /* Update - Message Status & Delivery Response - Stop */

            }



            if(pdu.command == 'deliver_sm') {

              /*
                console.log('Delivery pdu');
                console.log(pdu);

                console.log('Delivery - pdu response');
                console.log(pdu.response());*/



                var delivery_response = {};

                var fromNumber = pdu.source_addr.toString();
                var toNumber = pdu.destination_addr.toString();

                var text = '';
                var receipted_message_id = '';
                var user_message_reference ='';

                if (pdu.short_message && pdu.short_message.message) {
                    text = pdu.short_message.message;
                    receipted_message_id = pdu.short_message.message_payload;
                    user_message_reference=pdu.short_message.user_message_reference;
                }


                var parse_deliver_text_str = text.toString(); 
                var receipted_id = receipted_message_id; 

                const querystring = require('querystring');    
                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {decodeURIComponent: s =>  s.trim()});

                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {decodeURIComponent: s =>  s.trim()});

                  var msg_id = parse_deliver_text_json.id;
                  delivery_response['service_type'] = pdu.service_type;
                  delivery_response['protocol_id'] = pdu.protocol_id;
                  delivery_response['priority_flag'] = pdu.priority_flag;
                  delivery_response['message_status'] = parse_deliver_text_json.stat;
                  delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
                  delivery_response['sub'] = parse_deliver_text_json.sub;
                  delivery_response['message_err'] = parse_deliver_text_json.err;
                  delivery_response['deliver_sm_data'] = pdu;
                  delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
                  delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
                  delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                  });
              
                session.deliver_sm_resp({ sequence_number: pdu.sequence_number });

                /* Update - Message Status & Delivery Response - Start */
                updatingMessageCollection({'message_id':msg_id},delivery_response,sms_logs);
                /* Update - Message Status & Delivery Response - Stop */

            }
        });

        /* Second Session - End */



        session.on('close', function(){
            console.log('SMPP CLOSE');
            /*if(didConnect) {
              connectSMPP(session);
            }*/
        });

        session.on('enquire_link', function(pdu) {
            console.log('enquire_link')
            session.send(pdu.response());
        });

        session.on('unbind', function(pdu) {
            console.log('unbind')
            session.send(pdu.response());
            session.close();
        });

        session.on('error', function(error){
            console.log('smpp error', error)
            didConnect = false;
        }); 

    });
}

async function bulkUpdateContactCollectionMappingValues(filter_topper,mobile_number_list,contacts_collection,category_id,filter_category_id,campaign_object_id,campaign_content_object_id,total_contacts,total_range_contacts,filter_params,user_name,user_id,activity_logs) {

  if (mobile_number_list.length > 0) {
           //console.log(mobile_number_list);
            var bulk = contacts_collection.initializeUnorderedBulkOp();
            var scat_id_arr  = [];
            var check_in_arr = [];
            scat_id_arr.push(category_id.toString() + '-Yes');
            check_in_arr.push(category_id.toString() + '-Yes');

            if (filter_category_id) {
              if (Array.isArray(filter_category_id)) {
                for (var i = 0, len = filter_category_id.length; i < len; i++) {
                  if (filter_category_id[i] == 0) {
                    scat_id_arr.push(filter_category_id[i] + '-Yes');
                  }
                }
              } else {
                if (filter_category_id == 0) {
                  scat_id_arr.push(filter_category_id + '-Yes');
                }
              }
            }
            var sender_category_ids = {
              $each: scat_id_arr
            };


            /* STEP- 1 - Sender Status Update - populateContactsToSenderStatus - START */
            if (scat_id_arr) {
              bulk.find({
                mobile_no: {
                  $in: mobile_number_list
                },
                $nor: [{
                  sender_status: {
                    $elemMatch: {
                      $in: check_in_arr
                    }
                  }
                }]
              }).update({
                $addToSet: {
                  sender_status: sender_category_ids
                }
              }, {
                  upsert: true
              });
            }
            /* STEP- 1 - Sender Status Update - populateContactsToCategory - STOP */
            /* STEP -2 - Category - populateContactsToCategory - STOP */
            if (category_id) {
              bulk.find({
                mobile_no: {
                  $in: mobile_number_list
                },
                $nor: [{
                  sender_category_ids: {
                    $elemMatch: {
                      $in: [category_id.toString()]
                    }
                  }
                }]
              }).update({
                $addToSet: {
                  sender_category_ids: category_id.toString()
                }
              }, {
                upsert: true
              });
            }
            /* STEP -2 - Category - populateContactsToCategory - STOP */
            
            /* STEP -3 - Campaign - Sender Campaign IDs - Start */
            if (campaign_object_id) {
                bulk.find({
                    mobile_no: {
                        $in: mobile_number_list
                    },
                    $nor: [{
                        campaign_ids: {
                            $elemMatch: {
                                $in: [campaign_object_id.toString()]
                            }
                        }
                    }]
                }).update({
                    $addToSet: {
                        campaign_ids: campaign_object_id.toString()
                    }
                }, {
                    upsert: true
                });
            }
            /* STEP -3 - Campaign - Sender Campaign IDs - Stop */
           
            /* STEP -4 - Campaign content Label IDs - Start */
            
            if (campaign_content_object_id) {
                bulk.find({
                    mobile_no: {
                        $in: mobile_number_list
                    },
                    $nor: [{
                        campaign_content_id: {
                            $elemMatch: {
                                $in: [campaign_content_object_id.toString()]
                            }
                        }
                    }]
                }).update({
                    $addToSet: {
                        campaign_content_id: campaign_content_object_id.toString()
                    }
                }, {
                    upsert: true
                });
            }

            if (filter_topper == 'clicker') {
              
                var click_camp_id_arr = parseInt(campaign_object_id); 

                /* clicker campaign Status Update - populate campaign sent status - START */
                if (click_camp_id_arr) {
                    bulk.find({
                      mobile_no: {
                        $in: mobile_number_list
                      }
                    }).update({
                      $addToSet: {
                        clicker_sent_camp_ids: click_camp_id_arr
                      }
                    }, {
                      upsert: false
                    });
                }
                /* clicker campaign Status Update - populate campaign sent status - STOP */
            } //end updating clicker ender data in db

            if (filter_topper == 'sender') {
             
              /* sender campaign Status Update - populate sender campaign sent status - START */
              if (campaign_object_id) {
                bulk.find({
                  mobile_no: {
                    $in: mobile_number_list
                  }
                }).update({
                  $addToSet: {
                    sender_sent_camp_ids: parseInt(campaign_object_id)
                  }
                }, {
                  upsert: false
                });
              }
              /* sender campaign Status Update - populate sender campaign sent status - STOP */
            } //end updating sender data in db

            // Execute the operations
            bulk.execute(function(err, result) {
              //console.log('Bulk Result:');
              //console.log(result); 
            });

            var duplicate_contacts = parseInt(total_range_contacts) - parseInt(total_contacts.length);

            var activity_object_data = {
              'type': 'Send Sms',
              'Location': 'Campaign',
              'description': 'Total ' + filter_params.total_sms_sent + ' contacts sent sms successfully.',
              'total_contacts': total_range_contacts,
              'duplicate_contacts': duplicate_contacts,
              'filter_params': filter_params,
              'user_name': user_name,
              'created_by': user_id,
              'is_read': 0,
              'created_at': new Date().toLocaleString('en-US', {
                  timeZone: 'Asia/Calcutta'
              })
            };

            saveCollectionDocuments(activity_logs,activity_object_data);
        }  
}
async function updateToCampaignContentLabel(saved_ids,total_contacts,campaign_content_labels){

        campaign_content_labels.findOneAndUpdate(
        {
            '_id':saved_ids.campaign_content_id, 
            'campaign_id': saved_ids.campaign_id
        },
        {$set: { total_sender: total_contacts }}, 
        {upsert:true}, function(err, response) {
            //console.log('campaign label-response');
            //console.log(response);
            //console.log('updated contacts='+total_contacts);
        });

}


async function sendEmailAfterCampaign(object_campaign_data, object_campaign_lable_data, total_contacts)
{


    var mail_content = '';

    if(object_campaign_lable_data.schedule_status=='0')
    {
      mail_content += '<div style="text-align: center; width: 100%;" data-mce-style="text-align: center; width: 100%;"> <div style="width: 740px; text-align: left; color: #000; background: #fff; border: 1px solid #f8f8f8; padding: 15px; margin: 0 auto;" data-mce-style="width: 600px; text-align: left; color: #000; background: #fff; border: 1px solid #f8f8f8; padding: 15px; margin: 0 auto;"><p>Dear Team,<br><br>'+ object_campaign_lable_data.created_by +' created a Campaign Content for kasplo. You have successfully scheduled an campaign content to be sent at the perfect time and day. <br><br><table border="1"><thead><tr><th style="width: 15%;">Campaign</th><th style="width:15%;">Label</th><th>Content</th><th>Destination</th><th>Total Sent</th><th>Schedule date</th><th>Created by</th><th>Sent date</th></tr></thead>';
    }else{
      mail_content += '<div style="text-align: center; width: 100%;" data-mce-style="text-align: center; width: 100%;"> <div style="width: 740px; text-align: left; color: #000; background: #fff; border: 1px solid #f8f8f8; padding: 15px; margin: 0 auto;" data-mce-style="width: 600px; text-align: left; color: #000; background: #fff; border: 1px solid #f8f8f8; padding: 15px; margin: 0 auto;"><p>Dear Team,<br><br>'+ object_campaign_lable_data.created_by +' created a Campaign Content for kasplo.<br><br><table border="1"><thead><tr><th style="width: 15%;">Campaign</th><th style="width:15%;">Label</th><th>Content</th><th>Destination</th><th>Total Sent</th><th>Created by</th><th>Sent date</th></tr></thead>';
    }
    mail_content += '<tbody>';
    mail_content += '<tr>';
    mail_content += '<td>' + object_campaign_data.name + '</td>';
    mail_content += '<td>' + object_campaign_lable_data.label_name + '</td>';
    mail_content += '<td><textarea class="form-control" id="sms_content_val" readonly="" style="height: 100px;">' + object_campaign_lable_data.segments.original_sms_content + '</textarea></td>';
    mail_content += '<td><a href="'+ object_campaign_lable_data.destination_url +'" target="_blank">URL</a></td>';
    mail_content += '<td>'+ total_contacts +'</td>';
    if(object_campaign_lable_data.schedule_status=='0')
    {
      mail_content += '<td>'+ object_campaign_lable_data.segments.schedule_date_and_time +'</td>';
    }
    mail_content += '<td>'+ object_campaign_lable_data.created_by +'</td>';
    mail_content += '<td>'+ object_campaign_lable_data.created_at +'</td>';
    mail_content += '</tr>';
    mail_content += '</tbody></table>';
    mail_content += '<p><br>Please click the following link to watch your campaign content sms report: </p><p><a href="http://panel.kasplo.com/report/sms_log_report?campaign_id='+ object_campaign_lable_data.campaign_id +'&content_label_id='+ object_campaign_lable_data._id +'"><blink style="animation: blinker 1s linear infinite;">Watch Your SMS Report</blink></a></p><br><br>Best Regards,<br>Kasplo<br></p></div><p><a href="#" style="margin-top: 15px;" data-mce-href="#" "="" data-mce-style="margin-top: 15px;"> <img width="100px" src="http://panel.kasplo.com/public/images/sms-logo.png"></a></p></div>';

    var transporter = nodemailer.createTransport(smpp_credentials);

    var mailOptions = {
       from: mail_from, 
       to: to_mail_ids,  
       text:'',
       subject:'Notification of Campaign Content-New',
       html:mail_content
   };

    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
          console.log("Message sent: %s", info.messageId);
          console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
      }
    });    
}



async function sendEmailAfterScheduleChanges(campaign_content_collection,campaign_id,content_id,date_and_time,type,changes_made_by)
{
    
    var content_details = await campaign_content_collection.findOne(
      {
        _id: parseInt(content_id)
    });

    var mail_content = '';
    if(type === 'reschedule'){
      mail_content += '<div style="text-align: center; width: 100%;" data-mce-style="text-align: center; width: 100%;"> <div style="width: 740px; text-align: left; color: #000; background: #fff; border: 1px solid #f8f8f8; padding: 15px; margin: 0 auto;" data-mce-style="width: 600px; text-align: left; color: #000; background: #fff; border: 1px solid #f8f8f8; padding: 15px; margin: 0 auto;"><p><p>Dear Team,<br><br>'+ changes_made_by +' updated a Campaign Content for kasplo.<br><br><table border="1"><thead><tr><th style="width: 15%;">Campaign</th><th style="width:15%;">Label</th><th>Reschedule date</th></tr></thead>';
    }else{
      mail_content += '<div style="text-align: center; width: 100%;" data-mce-style="text-align: center; width: 100%;"> <div style="width: 740px; text-align: left; color: #000; background: #fff; border: 1px solid #f8f8f8; padding: 15px; margin: 0 auto;" data-mce-style="width: 600px; text-align: left; color: #000; background: #fff; border: 1px solid #f8f8f8; padding: 15px; margin: 0 auto;"><p>Dear Team,<br><br>'+ changes_made_by +' updated a Campaign Content for kasplo.<br><br><table border="1"><thead><tr><th style="width: 15%;">Campaign</th><th style="width:15%;">Label</th><th>Schedule Cancelled date</th></tr></thead>';
    }
    mail_content += '<tbody>';
    mail_content += '<tr>';
    mail_content += '<td>' + content_details.segments.campaign_name + '</td>';
    mail_content += '<td>' + content_details.label_name + '</td>';
    mail_content += '<td>'+ date_and_time +'</td>';
    mail_content += '</tr>';
    mail_content += '</tbody></table>';

    var transporter = nodemailer.createTransport({
      service: 'smtp',
      host: "smtp.gmail.com",
        port: 587,
        secure: false,
      auth: {
        user:'realree.24@gmail.com',
        pass: 'babluu002'
      }
    });

  if (type === 'reschedule') {
    var subject = 'Successfully Rescheduled campaign content';
  } else {
    var subject = 'Successfully deleted campaign content';

  }
    var mailOptions = {
       from: 'Kasplo-Team <realree.24@gmail.com>', // sender address ,atul@adcanopus.com
       to: keys.TO_MAIL_IDS, // list of receivers //,atul@adcanopus.com
       text:'',
       subject:subject,
       html:mail_content
   };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {
        console.log("Message sent: %s", info.messageId);
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    }
  });    
}

async function saveCollectionDocuments(collection_name, object_data) {
    collection_name.insert(object_data);
}

async function saveMessages(sms_response,sms_logs) {
  sms_logs.insertMany(sms_response); // Each SMS Logs stroing to Database
}

async function updatingMessageCollection(find_query, update_data,sms_logs) {

    sms_logs.findOneAndUpdate(
      find_query,
      { 
        $set:update_data
      });

}


/*
 type: API
 description: updating sender column for the existing data (data which are sent using sender topper then updating that contact field to respective campaign id).
* Identifying filter topper in campaign content labels and if sender tooper then getting campaign id and updating to contact table
*/
router.get('/update_sender_column',  (req, res, next) => {
  
  MongoClient.connect(db).then(client => {
    const db = client.db(database_name);
    (async () => {
      const contacts_collection = db.collection('contacts');
      const content_collection  = db.collection('campaign_content_labels');
      const sender_skip_collection = db.collection('sender_skip');

      var get_skip_val = await sender_skip_collection.findOne({}); console.log(get_skip_val);
      
      if (get_skip_val == null) {
      	console.log('if');
      	var skip_value_last = 0;
      } else {
      	var skip_value_last = parseInt(get_skip_val.skip_value);
      }

      var get_total_contacts_count = await contacts_collection.count();
      if ( skip_value_last <= get_total_contacts_count) {

      var get_filter_topper        = await content_collection.find({'segments.filter_topper': 'sender'}).project({"campaign_id":1, _id:1}).skip(skip_value_last).limit(500000).toArray();
      console.log(get_filter_topper);

      //var check_sender_updated = await contacts_collection.find()
      if (get_filter_topper.length > 0) {
        /* batch size dynamic count */
        var length = get_filter_topper.length-1; var each_arr_size = 100000;
        var iteration_batch_min_and_max_values = [];
        if (length > 100000) {
          for (var size = 0; size < length;) {

            if (iteration_batch_min_and_max_values.length > 0) {
              var min_val = length-size-each_arr_size; 
              var min_range_val = min_val;
              if (min_val < 0) {
                min_range_val = 0;
              }
              iteration_batch_min_and_max_values.push([length-size-1, min_range_val]);
            } else {
              iteration_batch_min_and_max_values.push([length, length-each_arr_size]);
            } //end if
            size = size + each_arr_size;
          } //end for
        } else {
          iteration_batch_min_and_max_values.push([length, 0])
        } //end if 
      
      	var bulkOps = [];

        for (var i = 0; i < iteration_batch_min_and_max_values.length; i++) {

          var min_value = iteration_batch_min_and_max_values[i][0];
          var max_value = iteration_batch_min_and_max_values[i][1];
          for (var j = min_value; j >= max_value; j--) {
            var camp_id = get_filter_topper[j].campaign_id.toString();
            var content_lbl_id = get_filter_topper[j]._id.toString();
            bulkOps.push({ 
              "updateOne": { 
                "filter": { "campaign_ids": {$in: [camp_id] }, "campaign_content_id": {$in: [content_lbl_id] }} ,              
                "update": { "$addToSet": { 'sender_sent_camp_ids':  parseInt(get_filter_topper[j].campaign_id)}} 
              }         
            });
          }
          await contacts_collection.bulkWrite(bulkOps);
        }
              
    	}
    	var skip_value = skip_value_last + 500000;  
      await sender_skip_collection.update({}, {$set:{'skip_value':skip_value}}, { multi: false, upsert: true});
      await client.close();
      var status = 200;
      var message = 'data updated successfully';
       } else {
			var status = 400;
      var message = 'No data is available to update';
       }
       res.send({'status':status, 'message':message}); 
    })();
  });
});

module.exports = router;