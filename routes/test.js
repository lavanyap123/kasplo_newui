const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
// Load User model
const User = require('../models/user');
var MongoClient = require('mongodb').MongoClient;
// DB Config
const db = require('../config/keys').mongoURI;

// Login Page
router.get('/testdb', (req, res) => {
  MongoClient.connect(db, {
            useNewUrlParser: true,
        })
        .then(client => {
          (async () => {
            const db = client.db('bulk_sms');
            const clicks_collection = db.collection('clicks');
          var mobile_number_list = [];
          var total_contacts = await clicks_collection.aggregate(
                            {
                                "$match": { category_id: { '$in': [ 1, 2 ] } }
                            },
                            {
                                $skip: 0

                            },
                            {
                                $limit: 3
                            },
                            {
                              $group: {

                                _id: {
                                    contact_no: '$contact_no',
                                },
                              }
                            },
                            
                             

                            ).forEach(function(data) {
                              console.log('data'); console.log(data); 
                              var mobile_no = data.contact_no;
                              mobile_number_list.push(mobile_no);
                             
                            }); 
                    console.log(mobile_number_list);
                    client.close();
            })();
          });

});


// Login Page
router.get('/readfile', (req, res) => {
  MongoClient.connect(db, {
            useNewUrlParser: true,
        })
        .then(client => {
          (async () => {
            var fs = require('fs');

            fs.readFile("tmp.txt", "utf-8", (err, data) => {
              console.log(data);
              var bodyupdateJson = JSON.parse('[' + data.replace(/}{/g, '},{')+ ']');
               console.log('bodyupdateJson');
               console.log(bodyupdateJson);
            });

            client.close();
          })();
        });

});


router.get('/update_clicks', (req, res) => {
  MongoClient.connect(db, {
            useNewUrlParser: true,
        })
        .then(client => {
          (async () => {
            const db = client.db('bulk_sms');
            const clicks_collection = db.collection('clicks');
            const contacts_collection = db.collection('contacts');
           
            var data = await clicks_collection.aggregate([
               {
                    $group : {
                       '_id' : {
                         'campaign_id':"$campaign_id",
                         'contact_no':"$contact_no",

                       }
                    }
               }
            ]).toArray(); 

            /* updating in bulk */
            var bulk = contacts_collection.initializeUnorderedBulkOp();

            for (i=0; i < data.length;i++) {

              var campaign_id = data[i]._id.campaign_id; 
              var contact_no = data[i]._id.contact_no;

              bulk.find({
                mobile_no: contact_no
              }).update({
                  $addToSet: {
                      clicker_sent_camp_ids: parseInt(campaign_id),
                  }
              }, {
                  upsert: false
              });
            } //for loop end

            bulk.execute(function(err, result) {
              console.log('Bulk Result:');
              console.log(result); 
              console.log(err)
            });

            res.send({ success: 'Records updated successfully' });

            client.close();
           
            })();
          
          });

});

module.exports = router;