const express = require('express');
const router = express.Router();

var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectId;
const db = require('../config/keys').mongoURI;
const db_options = require('../config/keys').db_options;
const database_name = require('../config/keys').database_name;
var redis_config= require('../config/keys').redis_config;

var async = require("async");
/* Queue & Adding to Redis Cache - (Start) */
let kue   = require('kue');
let queue = kue.createQueue(redis_config);
let reds =  require('reds');
/* Queue & Adding to Redis Cache - (Stop) */
queue.setMaxListeners(10000000) // <- golden method

router.get('/', (req, res) => {
    res.redirect('/kue/jobs');
 
});

console.log('redis_config');
console.log(redis_config);


router.get('/active_to_inactive_queue', (req, res) => {

	queue.active(function (err, ids) {

	    // for each id we're going to see how long ago the job was last "updated"
	    async.map(ids, function(id, cb) {
	      // we get the job info from redis
	      kue.Job.get(id, function(err, job) {
	        if (err) { throw err; } // let's think about what makes sense here

	        // we compare the updated_at to current time.
	        //var lastUpdate = +Date.now() - job.updated_at;
	        //if (lastUpdate > 2000) {
	          console.log('job ' + job.id);
	          job.inactive();
	           //rescheduleJob(job, cb);  // either reschedule (re-attempt?) or remove the job.
	        //} 

	      });
	    });

	});
    res.json({'status':200,'message':'Active Queue rescheduled and status changed as complete'});
});

router.get('/active_queue', (req, res) => {

	queue.active(function (err, ids) {

	    // for each id we're going to see how long ago the job was last "updated"
	    async.map(ids, function(id, cb) {
	      // we get the job info from redis
	      kue.Job.get(id, function(err, job) {
	        if (err) { throw err; } // let's think about what makes sense here

	        // we compare the updated_at to current time.
	        var lastUpdate = +Date.now() - job.updated_at;
	        if (lastUpdate > 2000) {
	          //console.log('job ' + job.id + 'hasnt been updated in' + lastUpdate);
	           job.complete();
	           //rescheduleJob(job, cb);  // either reschedule (re-attempt?) or remove the job.
	        } 

	      });
	    });

	});
    res.json({'status':200,'message':'Active Queue rescheduled and status changed as complete'});
});


router.get('/inactive_queue', (req, res) => {

	queue.inactive(function (err, ids) {
	    // for each id we're going to see how long ago the job was last "updated"
	    async.map(ids, function(id, cb) {
	      // we get the job info from redis
	      kue.Job.get(id, function(err, job) {
	        if (err) { throw err; } // let's think about what makes sense here

	        // we compare the updated_at to current time.
	        var lastUpdate = +Date.now() - job.updated_at;
	        if (lastUpdate > 2000) {
	          //console.log('job ' + job.id + 'hasnt been updated in' + lastUpdate);
	           job.complete();
	           //rescheduleJob(job, cb);  // either reschedule (re-attempt?) or remove the job.
	        } 

	      });
	    });

	});

    res.json({'status':200,'message':'Inactive Queue rescheduled and status changed as complete'});

});

router.get('/inactive_queue', (req, res) => {

	queue.inactive(function (err, ids) {
	    // for each id we're going to see how long ago the job was last "updated"
	    async.map(ids, function(id, cb) {
	      // we get the job info from redis
	      kue.Job.get(id, function(err, job) {
	        if (err) { throw err; } // let's think about what makes sense here

	        // we compare the updated_at to current time.
	        var lastUpdate = +Date.now() - job.updated_at;
	        if (lastUpdate > 2000) {
	          //console.log('job ' + job.id + 'hasnt been updated in' + lastUpdate);
	           job.complete();
	           //rescheduleJob(job, cb);  // either reschedule (re-attempt?) or remove the job.
	        } 

	      });
	    });

	});

    res.json({'status':200,'message':'Inactive Queue rescheduled and status changed as complete'});

});

router.get('/failed', (req, res) => {

	queue.failed(function (err, ids) {
	    // for each id we're going to see how long ago the job was last "updated"
	    async.map(ids, function(id, cb) {
	      // we get the job info from redis
	      kue.Job.get(id, function(err, job) {
	        if (err) { throw err; } // let's think about what makes sense here

	        // we compare the updated_at to current time.
	        var lastUpdate = +Date.now() - job.updated_at;
	        if (lastUpdate > 2000) {
	          //console.log('job ' + job.id + 'hasnt been updated in' + lastUpdate);
	           job.complete();
	           //rescheduleJob(job, cb);  // either reschedule (re-attempt?) or remove the job.
	        } 

	      });
	    });

	});

    res.json({'status':200,'message':'Failed Queue rescheduled and status changed as complete'});
});

router.get('/clear', (req, res) => {

	kue.Job.rangeByState( 'complete', 0, -1, 'asc', function( err, jobs ) {
	  jobs.forEach( function( job ) {
	    job.remove( function(){
	      console.log( 'removed ', job.id );
	    });
	  });
	});

	kue.Job.rangeByState( 'inactive', 0, -1, 'asc', function( err, jobs ) {
	  jobs.forEach( function( job ) {
	    job.remove( function(){
	      console.log( 'removed ', job.id );
	    });
	  });
	});


	kue.Job.rangeByState( 'active', 0, -1, 'asc', function( err, jobs ) {
	  jobs.forEach( function( job ) {
	    job.remove( function(){
	      console.log( 'removed ', job.id );
	    });
	  });
	});

	kue.Job.rangeByState( 'delayed', 0, -1, 'asc', function( err, jobs ) {
	  jobs.forEach( function( job ) {
	    job.remove( function(){
	      console.log( 'removed ', job.id );
	    });
	  });
	});

	kue.Job.rangeByState( 'failed', 0, -1, 'asc', function( err, jobs ) {
	  jobs.forEach( function( job ) {
	    job.remove( function(){
	      console.log( 'removed ', job.id );
	    });
	  });
	});

    res.json({'status':200,'message':'All completed Queue was removed from redis server '});
});

router.get('/submit_log_search', (req, res) => {


MongoClient.connect(db, db_options).then(client => {

    const db = client.db(database_name);

    (async () => {


		    var start_date = new Date();
		    start_date.setHours(0, 0, 0, 0);

		    var end_date = new Date();
		    end_date.setHours(23, 59, 59, 59);

        	const sms_logs = db.collection('sms_logs');

        	var search_params = {
                'created_at': {
                    $gt: start_date,
                    $lte: end_date
                },
	            'message_status': "SUBMIT",
	            'message_id': {"$exists" : false },
/*			   "campaign_content_label_id" : { '$in': [491,492,493,494] },
			   'message_status': 'SUBMIT',
			   'message_id': { '$exists': false }*/

	        };


	        const cursor =  await sms_logs.find(search_params);

	        while (await cursor.hasNext()) {
	        	
	        	const submit_non_updated_doc = await cursor.next();

	        	if(submit_non_updated_doc) {

	        		var campaign_id = submit_non_updated_doc.campaign_id;
	        		var campaign_content_label_id = submit_non_updated_doc.campaign_content_label_id;
	        		var mobile_no = submit_non_updated_doc.mobile_no;

	        		var id_prepartion = campaign_id+'-'+campaign_content_label_id+'-'+mobile_no;
	        		id_prepartion.toString();
	        		var search_log_id = [id_prepartion];

	        		console.log('search_log_id');
	        		console.log(search_log_id);

					const sms_submit_logs_search= reds.createSearch(queue.client.getKey('search'))
					  sms_submit_logs_search.query(search_log_id).end((err, ids) => {
				   
					    if(err){
					      return reject({ error: err.message })
					    }else{

					    	var job_length = ids.length;

					    	if(job_length > 0)
					    	{
						    	console.log('job_length');
						    	console.log(job_length);

					    		for (var i = 0; i < job_length; i++) {

					    			var job_id = ids[i];
					    			console.log('job_id='+job_id);
									kue.Job.get(job_id, function(err, job_records) {

										if(typeof job_records!=="undefined")
										{

									        var find_query = {
									            '_id': ObjectID(submit_non_updated_doc._id)
									        };

									        var update_data = job_records.data;

									        console.log('job data');
								   	        console.log(job_records.data);


										    sms_logs.findOneAndUpdate(
										        find_query, 
										        {
										            $set: update_data
										        }, 
										        {
										            multi: true,
										            returnOriginal: false,
										            writeConcern: {
										                w: 0
										            }
										   }, function(err, response) {

										   		job_records.remove();
										   });
										}

							   	    });

					    		}

					    	}
					    } 

					});

	        	}
	        }

			res.json({'status':200,'message':'All Submit Logs Queue was updated in mongodb'});

	    })();

	});
});


router.get('/clear', (req, res) => {

	kue.Job.rangeByState( 'complete', 0, -1, 'asc', function( err, jobs ) {
	  jobs.forEach( function( job ) {
	    job.remove( function(){
	      console.log( 'removed ', job.id );
	    });
	  });
	});

});

router.get('/dlr_log_search', (req, res) => {


MongoClient.connect(db, db_options).then(client => {

    const db = client.db(database_name);

    (async () => {


		    var start_date = new Date();
		    start_date.setHours(0, 0, 0, 0);

		    var end_date = new Date();
		    end_date.setHours(23, 59, 59, 59);

        	const sms_logs = db.collection('sms_logs');

        	var search_params = {
                'created_at': {
                    $gt: start_date,
                    $lte: end_date
                },
	            'message_status': "PENDING"
			  
	        };

/*console.log('search_params');
console.log(search_params);
*/
	        const cursor =  await sms_logs.find(search_params);

	        while (await cursor.hasNext()) {
	        	
	        	const submit_non_updated_doc = await cursor.next();

	        	if(submit_non_updated_doc) {

	        		var message_id = submit_non_updated_doc.message_id;
	        		message_id.toString();
	        		var search_dlr_log_id = [message_id];

	        		console.log('search_dlr_log_id');
	        		console.log(search_dlr_log_id);

					const sms_dlr_logs_search= reds.createSearch(queue.client.getKey('search'))
					  sms_dlr_logs_search.query(search_dlr_log_id).end((err, ids) => {
				   
					    if(err){
					      return reject({ error: err.message })
					    }else{

					    	var job_length = ids.length;

					    	if(job_length > 0)
					    	{
						    	console.log('job_length');
						    	console.log(job_length);

					    		for (var i = 0; i < job_length; i++) {

					    			var job_id = ids[i];
					    			console.log('job_id='+job_id);
									kue.Job.get(job_id, function(err, job_records) {

										if(typeof job_records!=="undefined")
										{

									        var find_query = {
									            '_id': ObjectID(submit_non_updated_doc._id)
									        };

									        var update_data = job_records.data;

									        console.log('job data');
								   	        console.log(job_records.data);


										    sms_logs.findOneAndUpdate(
										        find_query, 
										        {
										            $set: update_data
										        }, 
										        {
										            multi: true,
										            returnOriginal: false,
										            writeConcern: {
										                w: 0
										            }
										   }, function(err, response) {

										   		job_records.remove();
										   });
										}

							   	    });

					    		}

					    	}
					    } 

					});

	        	}
	        }

			res.json({'status':200,'message':'All DLR Queue was updated in mongodb '});

	    })();

	});
});

module.exports = router;
