const express                    = require('express');
const router                     = express.Router();
const { ensureAuthenticated, roleAuthenticated }    = require('../config/auth');
const bcrypt                     = require('bcryptjs');
const passport                   = require('passport');
var json2csv                     = require('json2csv'); //not working
var csv_export                   = require('csv-export');
const stringify                  = require('csv-stringify');
var autoIncrement                = require("mongodb-autoincrement");

var mongoose                     = require('mongoose');
var MongoClient                  = require('mongodb').MongoClient;
// Dashboard
var await                        = require("await");
var async                        = require("async");
var Hashids                      = require('hashids');
var ObjectId                     = require('mongodb').ObjectId;
const crypto                     = require('crypto');
var algorithm                    = 'aes256';
var key                          = 'password';
var activityController           = require('./../controllers/activity_ctrl');
// Load User model
const User                       = require('../models/user');
// DB Config
const db                         = require('../config/keys').mongoURI;
const db_options                 = require('../config/keys').db_options;
const database_name              = require('../config/keys').database_name;

/**
* route: /users
* method: GET
* description: Getting all the registerd users list
* params: 
* return: users data
*/
router.get('/users', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    (async () => {
      const db         = client.db(database_name);
      const collection = db.collection('users');
      const roles_collection = db.collection('roles');
      const teams_collection   = db.collection('teams');
      const country_collection = db.collection('country');

      var get_roles          = await roles_collection.find({'role_status' : '1'}).sort({'_id' : -1}).toArray();
      var get_teams          = await teams_collection.find({'team_status': '1'}).sort({'_id' : -1}).toArray(); 
      var get_countries      = await country_collection.find({}).sort({'_id' : -1}).toArray(); 

      collection.find().sort({
        'created_at': -1
      }).sort({'_id': -1}).toArray(function(err, users) {
        res.render('users/index', {
          users,
          user                : req.user,
          get_roles,
          get_teams,
          get_countries,           
          activity_logs       : req.activity_logs,
          activity_logs_count : req.activity_logs_count
        });
      });
      client.close();
    })();
  });
}); //users

/**
* route: /update
* method: POST
* description: Updating users data based on user id
* params: id
* return: status_code, message
*/
router.post('/update', ensureAuthenticated, (req, res) => {

  MongoClient.connect(db, db_options).then(client => {
    const db         = client.db(database_name);
    const collection = db.collection('users');
    var id           = req.body.id;
    const {
      first_name,
      last_name,
      mobile,
      country_id,
      im_id,
      im_account,
      address,
      team_id,
      team_leader,
      role_id,
      password,
      password2,
      role_name,
    } = req.body;
    
   
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(req.body.password, salt, (err, hash) => {

            var password = req.body.password;
            var cipher = crypto.createCipher(algorithm, key);
            var encrypted = cipher.update(password, 'utf8', 'hex') + cipher.final('hex');

            var data = {
                first_name,
                last_name,
                mobile,
                country_id,
                im_id,
                im_account,
                address,
                team_id,
                team_leader,
                role_id,
                role_name,
                'password'   : hash,
                'pcrypto'    : encrypted,
                'updated_at' : new Date(Date.now()).toISOString()
            };

            var o_id = new ObjectId(id);


            collection.updateOne({
                        '_id': o_id
                    }, // Filter
                    {
                        $set: data
                    }
                )
                .then((obj) => {
                    res.send({
                        'msg': 'success',
                        'status': 200
                    });
                })
                .catch((err) => {
                    res.send({
                        'msg': err,
                        'status': 400
                    });
                });

            client.close();

        });
    });

  });

}); //update

/**
* route: /popup_content
* method: POST
* description: Getting users data based on user id
* params: id
* return: status_code, message
*/
router.post('/popup_content', function(req, res, next) {
  MongoClient.connect(db, db_options).then(client => {

    const db               = client.db(database_name);
    const collection       = db.collection('users');
    const roles_collection = db.collection('roles');
    var type               = req.body.type;
    var user_id            = req.body.id;
    var status_code        = 400;
    var message            = 'Error while fetching data';

    (async () => {
      var user_content     = '';
      var get_roles        = await roles_collection.find({'role_status' : '1'}).sort({'_id' : -1}).toArray();

      var o_id             = new ObjectId(user_id);
      const user           = await collection.find({
                              '_id': o_id
                            }).toArray();
      if (user.length > 0) {
        status_code        = 200;
        message            = 'Successfully fetched data';   
        var decipher       = crypto.createDecipher(algorithm, key);
        var password       = decipher.update(user[0].pcrypto, 'hex', 'utf8') + decipher.final('utf8');
        user[0].password   = password;
      } 
   
      res.send({
        'status'    : status_code,
        'message'   : message,
        'user_data' : user
      });
      client.close();
    })();
  });

}); //popup_content


/**
* route: /add
* method: GET
* description: Getting add page
* params: 
* return: 
*/
router.get('/add', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {
   MongoClient.connect(db, db_options).then(client => {  
    const db                 = client.db(database_name);
    const roles_collection   = db.collection('roles');
    const teams_collection   = db.collection('teams');
    const country_collection = db.collection('country');

    (async() => {
      var get_roles          = await roles_collection.find({'role_status' : '1'}).sort({'_id' : -1}).toArray(); 
      var get_teams          = await teams_collection.find({'team_status': '1'}).sort({'_id' : -1}).toArray(); 
      var get_countries      = await country_collection.find({}).sort({'_id' : -1}).toArray(); 

      res.render('users/register', {
          user                : req.user,
          get_roles           : get_roles,
          get_teams           : get_teams,
          get_countries       : get_countries,
          activity_logs       : req.activity_logs,
          activity_logs_count : req.activity_logs_count
      });
    })();
  });
}); //add

/**
* route: /register
* method: POST
* description: Adding data to users collection
* params: 
* return: success message, error message
*/
router.post('/register', ensureAuthenticated, activityController.getRecentAcitityLogs, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => { 
    const db                 = client.db(database_name);
    const roles_collection   = db.collection('roles');
    (async() => {
      var get_roles          = await roles_collection.find({}).sort({'_id' : -1}).toArray();  
      const {
        first_name,
        last_name,
        email,
        mobile,
        country_id,
        im_id,
        im_account,
        address,
        team_id,
        team_leader,
        role_id,
        password,
        password2,
        role_name,
    } = req.body;
    var message     = 'Error while submitting data';
    var status_code = {'status' : 400};

  
    let errors = {};
    errors['message'] = 'Error while submitting data';

    await User.findOne({
            email: email
        }).then(user => {
            if (user) {
                errors['message'] = 'Email already exists';
                
            } else {
              (async () => {
                status_code['status']  = 200;
                errors['message']      = 'You are now registered and can log in';
                var cipher             = await crypto.createCipher(algorithm, key);
                var pcrypto            = await cipher.update(password, 'utf8', 'hex') + cipher.final('hex');
                var name               = first_name + ' ' + last_name;

                const newUser = new User({
                    first_name,
                    last_name,
                    name,
                    email,
                    mobile,
                    country_id,
                    im_id,
                    im_account,
                    address,
                    team_id,
                    team_leader,
                    role_id,
                    password,
                    pcrypto,
                    role_name
                });
              
              await bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                  if (err) throw err;
                  newUser.password = hash;
                  (async () => {
                    await newUser
                      .save()
                      .then(user => {

                      })
                      .catch(err => console.log(err));
                  })();
                });
              });
            })();
          }

          res.send({
            'status'  : status_code.status,
            'message' : errors.message
          });
        });
    })();
  });
}); //register

module.exports = router;