const express                 = require('express');
const router                  = express.Router();
const { ensureAuthenticated } = require('../config/auth');
var json2csv                  = require('json2csv'); //not working
var csv_export                = require('csv-export');
const stringify               = require('csv-stringify');
var autoIncrement             = require("mongodb-autoincrement");

var mongoose                  = require('mongoose');
var MongoClient               = require('mongodb').MongoClient;
// Dashboard
var await                     = require("await");
var async                     = require("async");
var Hashids                   = require('hashids');
var activityController        = require('./../controllers/activity_ctrl');
// DB Config
const db                      = require('../config/keys').mongoURI;
const db_options              = require('../config/keys').db_options;
const database_name           = require('../config/keys').database_name;
const upload_baseURL          = require('../config/keys').upload_baseURL;
var constants                 = require('./../config/constants');
var helper                    = require('./../helpers/create_pagination');
var functions_handler         = require('./../helpers/functions_handler');
var uploadController          = require('./../controllers/upload_ctrl');
// Import - Index
router.get('/index', ensureAuthenticated,  activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
      const db = client.db(database_name);

      (async () => {

        var category_list = await db.collection('category').find().toArray();
        var source_list = await db.collection('source').find().toArray();

        res.render('import/index', {
          category_list,
          source_list,
          user: req.user,
          activity_logs: req.activity_logs,
          upload_base_url:upload_baseURL,
          activity_logs_count: req.activity_logs_count,
        });

      client.close();
      })();

  });
});

// Import - Index
router.get('/data_aggregation', ensureAuthenticated,  activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
      const db = client.db(database_name);

      (async () => {

        var category_list = await db.collection('category').find().toArray();
        var source_list   = await db.collection('source').find().toArray();
        var campaign_list = await db.collection('campaign').find({'draft_status':1}).sort({'date_added': -1}).toArray();

        res.render('import/past_campaign_upload', {
          category_list,
          source_list,
          user: req.user,
          campaign_list:campaign_list,
          activity_logs: req.activity_logs,
          upload_base_url:upload_baseURL,
          activity_logs_count: req.activity_logs_count,
        });

      client.close();
      })();

  });
});
// suppress contacts
router.get('/suppress_contacts', ensureAuthenticated,  activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {
   
    MongoClient.connect(db, db_options).then(client => {
      const db = client.db(database_name);

    (async () => {

      var category_list = await db.collection('category').find().toArray();
      var source_list   = await db.collection('source').find().toArray();

      res.render('import/suppress_contacts', {
        category_list,
        source_list,
        user: req.user,
        activity_logs: req.activity_logs,
        upload_base_url:upload_baseURL,
        activity_logs_count: req.activity_logs_count,
      });

      client.close();
    })();
  });
});


/** method: /block_contact
*   description: Updating given contact numbers is_active field to 0 
*   return: status_code, filepath
*/
router.post('/block_contact', ensureAuthenticated,  uploadController.getNumbersRequest, uploadController.getInvalidNumbers, (req, res) => {
    
    MongoClient.connect(db, db_options).then(client => {
      const db = client.db(database_name);

    (async () => {

      const activity_logs = db.collection('activity_logs');
      const contacts_collection = db.collection('contacts');
      var cat_id = parseInt(req.body.category_id); 

      var mobile_number_list = req.body.block_contacts.split('\n'); 
      mobile_number_list = mobile_number_list.filter(v=>v!='');

      var user_name=req.body.user_name;
      var user_id=req.body.user_id;
      
      /* Total Contacts - START */
      var existing_contacts = await contacts_collection.count();

      var hashids = new Hashids();

      var bulk = contacts_collection.initializeUnorderedBulkOp();

      if (req.valid_mbl_num_list.length > 0) {
        let mbl_nms = req.valid_mbl_num_list.map(a => a.mobile_no); 

        bulk.find({
          mobile_no: {
            $in: mbl_nms
          }
        }).update({
         $set: { is_active: 0, updated_by: user_id,  updated_at: functions_handler.getCurrentIndianDateTime()}
        }, {
          upsert: false
        });

        // // Execute the operations
        bulk.execute(function(err, data) {
            //console.log('Bulk Result:');
            console.log(data);
            var status = 400;
              var duplicates = parseInt(req.valid_mbl_num_list.length) - parseInt(data.result.nModified); 
              if (parseInt(data.result.nModified) > 0) {
                status = 200;
              }
              var activity_data = {
                'type': 'Block Contacts',
                'Location': 'Admin Panel',
                'description': 'Total ' + data.result.nModified + ' records have been blocked successfully.',
                'total_contacts': req.valid_mbl_num_list.length,
                //'category_id': cat_id,
                'user_name': req.user.name,
                'duplicate_contacts': parseInt(duplicates),
                'created_by': user_id,
                'is_read': 0,
                'created_at': functions_handler.getCurrentIndianDateTime()
              };

              activity_logs.insert(activity_data).catch((error) => {
                //console.log('Acitivity Log is not inserted');
              });
              client.close();
              
        });
               }

               res.send({'status':200, 'download_file_name': req.file_path});
      })();
      });
});

// suppress contacts list
router.get('/supress', ensureAuthenticated,  activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {
   
    MongoClient.connect(db, db_options).then(client => {
      const db = client.db(database_name);

    (async () => {

      var category_list = await db.collection('category').find().toArray();
      var source_list   = await db.collection('source').find().toArray();

      res.render('import/suppress_list', {
        
        user: req.user,
        activity_logs: req.activity_logs,
        activity_logs_count: req.activity_logs_count,
      });

      client.close();
    })();
  });
});


/**
  description: Fetching suppress contact list based on is active 0 
  return: list table data with pagination
**/

router.post('/suppress_contact_list', (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
      const db = client.db(database_name);
      (async () => {
                var perPage = constants.PER_PAGE_LIST_COUNT;
                var page = parseInt(req.body.page_no) || 1;

                var current = req.body.page_no;
                var searched = req.body.search;
                var is_searched = req.body.is_search; 
               
                if (is_searched == 'search' && searched != '') {
                  var contact_details_count = await db.collection('contacts').find({
                        'is_active':0, 'mobile_no':new RegExp(searched)
                    }).count(); 
                  var contact_details = await db.collection('contacts').find({'is_active':0, 'mobile_no':new RegExp(searched)}).skip((perPage * page) - perPage).limit(perPage).sort({'_id': -1 }).toArray(); 
                  
                } else {
                   var contact_details_count = await db.collection('contacts').find({
                        'is_active':0,
                    }).count();

                  var contact_details = await db.collection('contacts').find({'is_active':0}).skip((perPage * page) - perPage).limit(perPage).sort({'_id': -1 }).toArray();
                }
                
                var offset = parseInt((perPage * page) - perPage);
                //var content_html = '';
                var content_html = '<table class="table table-bordered">';
                content_html += '<thead><tr><th>ID</th><th>Contact Name</th><th>Contact No</th><th>Status</th><th>Blocked Date</th></tr>';
                content_html += '</thead>';
                content_html += '<tbody>';

                    if (contact_details.length > 0) {

                        for (var i = 0; i < contact_details.length; i++) {
                            var s_no = offset + i + 1;
                            content_html += '<tr>';
                            content_html += '<td>' + s_no + '</td>';
                            if(typeof contact_details[i].name!=='undefined'){
                              content_html += '<td>' + contact_details[i].name + '</td>';
                            }else{
                              content_html += '<td></td>';
                            }
                            content_html += '<td>' + contact_details[i].mobile_no + '</td>';
                        
                            content_html += '<td><label class="badge badge-danger">' + 'blocked' + '</label></td>';
                            content_html += '<td>' + functions_handler.getCurrentDateTimeampm(contact_details[i].updated_at) + '</td>';
                            content_html += '</tr>';
                        } //endfor
                    } //endif
                    content_html += '</tbody>';
                    content_html += '</table>';

                    //var contact_details_count = contact_details.length; console.log(contact_details_count);
                    var pages = Math.ceil(contact_details_count / perPage); 

                    var pagination_data = helper.createPagination(pages, current, perPage, contact_details_count, offset);
                    res.send({
                        pagination_content: pagination_data,
                        content_html: content_html
                    });

                    client.close();
            })();

        });
});
module.exports = router;
