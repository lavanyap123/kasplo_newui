const express = require('express');
const router = express.Router();
const {
    ensureAuthenticated, roleAuthenticated
} = require('../config/auth');
var json2csv = require('json2csv'); //not working
var csv_export = require('csv-export');
const stringify = require('csv-stringify');
var autoIncrement = require("mongodb-autoincrement");

var constants = require('./../config/constants');

var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
// Dashboard
var await = require("await");
var async = require("async");

//helper
var helper = require('./../helpers/functions_handler');

var ISODate = require("isodate");

// DB Config
const db = require('../config/keys').mongoURI;
const db_options = require('../config/keys').db_options;
const database_name= require('../config/keys').database_name;

router.post('/cat_export_as_csv', ensureAuthenticated, (req, res, next) => {

    var fromdate = req.body.fromdate;
    var todate = req.body.todate;

    var start_date = new Date(fromdate + ' 00:00:00');
    var end_date = new Date(todate);
    var add_one_day_to_date = new Date(end_date.getTime() + 1 * 24 * 60 * 60000);

    var category_id = req.body.category_id;
    var cat_name = req.body.category_name; 

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);

            (async () => {

                let count;

                if (category_id > 0) {
                    var conditions = {
                        'category_id': parseInt(category_id),
                        'created_at': {
                            $gte: ISODate(start_date),
                            $lte: ISODate(add_one_day_to_date)
                        }
                    };
                } else {

                    var conditions = {
                        'created_at': {
                            $gte: ISODate(start_date),
                            $lte: ISODate(add_one_day_to_date)
                        }
                    };
                } 

                /*    
                 console.log('conditions');
                 console.log(conditions);*/

                const category = await db.collection('clicks').aggregate([

                    {
                        $match: conditions
                    },
                    // / "created_at" : { '$gte' : todate},
                    // Get the year, month and day from the createdTimeStamp
                    {
                        $project: {
                            "year": {
                                $year: "$created_at"
                            },
                            "month": {
                                $month: "$created_at"
                            },
                            "day": {
                                $dayOfMonth: "$created_at"
                            },
                            "category_id": "$category_id",
                            //"created_at":"$created_at",
                            category_details: "$category_details"

                        }
                    },


                    {
                        $group: {
                            "_id": {
                                category_id: "$category_id",
                                year: "$year",
                                month: "$month",
                                day: "$day",
                                category_details: "$category_details",
                                //created_at:"$created_at",

                            },
                            count: {
                                $sum: 1
                            }
                        },
                    },

                    {
                        $sort: {
                            "_id.day": -1,
                            "_id.month": -1,
                            "_id.year": -1
                        }
                    },

                ]).toArray(); 


                var report_data = [];

                if (category.length > 0) {

                    for (var i = 0; i < category.length; i++) {

                        var date = category[i]._id.day + '-' + category[i]._id.month + '-' + category[i]._id.year;
                        var clicks = category[i].count;
                        var category_name = category[i]._id.category_details.name;

                        report_data.push({
                            'Date': date,
                            'Category Name': category_name,
                            'Clicks': clicks
                        });
                    }


                }

                //export data
                var fs = require('fs');
                var date = helper.getCurrentDateTime(); 
                var file_name = cat_name + '_' + date ;

                //export category data
                var export_category_data = {};
                export_category_data[file_name.toString()] = report_data;
                csv_export.export(export_category_data, function(buffer) {

                    //this module returns a buffer for the csv files already compressed into a single zip.
                    //save the zip or force file download via express or other server
                    fs.writeFileSync('./public/export/' + file_name + '.zip', buffer);
                    //res.download('./contacts_list_csv.zip'); // Set disposition and send it.   

                });

                res.send({
                    'status': 200,
                    'msg': 'successfully exported is csv',
                    'file_name': file_name
                });
                client.close();


            })();

        });

});

router.post('/campaign_export_as_csv', ensureAuthenticated, (req, res, next) => {

    let category_id = req.body.category_id;
    let campaign_id = parseInt(req.body.campaign_id);

    var fromdate = req.body.fromdate;
    var todate = req.body.todate;

    var start_date = new Date(fromdate + ' 00:00:00');
    var end_date = new Date(todate);
    var add_one_day_to_date = new Date(end_date.getTime() + 1 * 24 * 60 * 60000);
    var cat_name = req.body.category_name;
    var camp_name = req.body.campaign_name;

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);

            (async () => {

                let count;

                if (category_id > 0 && campaign_id > 0) {
                    var conditios = {
                        'category_id': parseInt(category_id),
                        'campaign_id': parseInt(req.body.campaign_id),
                        'created_at': {
                            $gte: ISODate(start_date),
                            $lte: ISODate(add_one_day_to_date)
                        }
                    };

                } else if (category_id > 0) {
                    var conditios = {
                        'category_id': parseInt(category_id),
                        'created_at': {
                            $gte: ISODate(start_date),
                            $lte: ISODate(add_one_day_to_date)
                        }
                    };

                } else {

                    var conditios = {
                        'created_at': {
                            $gte: ISODate(start_date),
                            $lte: ISODate(add_one_day_to_date)
                        }
                    };
                }


                const campaign = await db.collection('clicks').aggregate([

                    {
                        $match: conditios
                    },
                    // / "created_at" : { '$gte' : todate},
                    // Get the year, month and day from the createdTimeStamp
                    {
                        $project: {
                            "year": {
                                $year: "$created_at"
                            },
                            "month": {
                                $month: "$created_at"
                            },
                            "day": {
                                $dayOfMonth: "$created_at"
                            },
                            campaign_id: "$campaign_id",
                            campaign_details: "$campaign_details",
                            campaign_label_details: "$campaign_label_details",
                            //created_at:"$created_at",

                        }
                    },

                    {
                        $group: {
                            "_id": {
                                campaign_id: "$campaign_id",
                                year: "$year",
                                month: "$month",
                                day: "$day",
                                campaign_details: "$campaign_details",
                                campaign_label_details: "$campaign_label_details",
                                //created_at:"$created_at",
                            },
                            count: {
                                $sum: 1
                            }
                        },
                    },
                    {
                        $sort: {
                            "_id.day": -1,
                            "_id.month": -1,
                            "_id.year": -1
                        }
                    },

                ]).toArray();

                var report_data = [];

                if (campaign.length > 0) {

                    for (var i = 0; i < campaign.length; i++) {

                        var date = campaign[i]._id.day + '-' + campaign[i]._id.month + '-' + campaign[i]._id.year;
                        var clicks = campaign[i].count;
                        if (campaign[i]._id.campaign_details)
                            var campaign_name = campaign[i]._id.campaign_details.name;
                        else
                            var campaign_name = '#';

                        var campaign_label_name = campaign[i]._id.campaign_label_details.label_name;

                        report_data.push({
                            'Date': date,
                            'Campaign Name': campaign_name,
                            'Campaign Label Name': campaign_label_name,
                            'Clicks': clicks
                        });

                    }


                }

                //export data
                var fs = require('fs');
                var date = helper.getCurrentDateTime(); 
                var file_name = cat_name + '_' + camp_name + '_'+ date ;

                //export category and campaign data
                var export_category_campaigndata = {};
                export_category_campaigndata[file_name.toString()] = report_data;
                csv_export.export(export_category_campaigndata, function(buffer) {

                    //this module returns a buffer for the csv files already compressed into a single zip.
                    //save the zip or force file download via express or other server
                    fs.writeFileSync('./public/export/' + file_name + '.zip', buffer);
                    //res.download('./contacts_list_csv.zip'); // Set disposition and send it.   

                });

                res.send({
                    'status': 200,
                    'msg': 'successfully exported as csv',
                    'file_name': file_name
                });

                client.close();
            })();

        });


});

router.post('/general_report_export_as_csv', ensureAuthenticated, (req, res, next) => {

  let category_id      = req.body.category_id;
  let campaign_id      = parseInt(req.body.campaign_id);
  let content_label_id = parseInt(req.body.content_label_id);

  var fromdate         = req.body.fromdate;
  var todate           = req.body.todate;
  var start_date       = new Date(fromdate + ' 00:00:00');
  var end_date         = new Date(todate);
  var add_one_day_to_date = new Date(end_date.getTime() + 1 * 24 * 60 * 60000);
  var cat_name         = req.body.category_name;
  var camp_name        = req.body.campaign_name;
  var cont_label_name  = req.body.content_label_name;
  var status_code      = 400;
  MongoClient.connect(db, db_options).then(client => {
    const db                  = client.db(database_name);
    (async () => {

      let count;

      if (category_id > 0 && campaign_id > 0 && content_label_id > 0) {
        var conditios        = {
          'category_id'      : parseInt(category_id),
          'campaign_id'      : parseInt(req.body.campaign_id),
          'content_label_id' : parseInt(content_label_id),
          'created_at'       : {
            $gte             : ISODate(start_date),
            $lte             : ISODate(add_one_day_to_date)
          }
        };

      } else if (category_id > 0 && campaign_id > 0) {
        var conditios        = {
          'category_id'      : parseInt(category_id),
          'campaign_id'      : parseInt(req.body.campaign_id),
          'created_at'       : {
            $gte             : ISODate(start_date),
            $lte             : ISODate(add_one_day_to_date)
          }
        };

      } else if (category_id > 0) {
        var conditios       = {
          'category_id'     : parseInt(category_id),
          'created_at'      : {
            $gte            : ISODate(start_date),
            $lte            : ISODate(add_one_day_to_date)
          }
        };

      } else {
        var conditios = {
          'created_at': {
            $gte      : ISODate(start_date),
            $lte      : ISODate(add_one_day_to_date)
          }
        };
      }

      const campaign    = await db.collection('clicks').aggregate([
        {
            $match: conditios
        },
        // Get the year, month and day from the createdTimeStamp
        {
          $project : {
            "year" : {
              $year                : "$created_at"
            },
            "month" : {
              $month               : "$created_at"
            },
            "day" : {
              $dayOfMonth          : "$created_at"
            },
            campaign_id            : "$campaign_id",
            campaign_details       : "$campaign_details",
            campaign_label_details : "$campaign_label_details",
            category_details       : "$category_details",
            location               : "$location",
            browser_information    : "$browser_information",
            public_ip              : "$public_ip",
          }
        },

        {
          $group : {
            "_id": {
              category_id         : "$category_id",
              campaign_id         : "$campaign_id",
              content_label_id    : "$content_label_id",
              year                : "$year",
              month               : "$month",
              day                 : "$day",
              campaign_details    : "$campaign_details",
              campaign_label_details : "$campaign_label_details",
              category_details    : "$category_details",
              //created_at:"$created_at",
            },
            count  : {
              $sum : 1
            }
          },
        },
        {
          $sort: {
            "_id.day"     : -1,
            "_id.month"   : -1,
            "_id.year"    : -1
          }
        },
      ]).toArray();

      var report_data       = [];

      if (campaign.length > 0) {
        status_code         = 200;
      
        for (var i = 0; i < campaign.length; i++) {
          var date          = campaign[i]._id.day + '-' + campaign[i]._id.month + '-' + campaign[i]._id.year;
          var clicks        = campaign[i].count;

          var category_name = campaign[i]._id.category_details.name;
          var campaign_name = campaign[i]._id.campaign_details.name;
          var campaign_label_name = campaign[i]._id.campaign_label_details.label_name;
          var ip            = campaign[i].public_ip;
          var location      = campaign[i].location;
          var browser_information = campaign[i].browser_information;   
          var city          = '';
          var state         = '';
          var country       = '';
          var latitude      = '';
          var longitude     = '';
          var postal        = '';
          var network       = '';
          var browser       = '';
          var os            = '';
          var platform      = '';
          var source        = '';

          if (ip != '::1') {
            if (location) {
              var city      = location.city;
              var state     = location.region;
              var country   = location.country_name;
              var latitude  = location.latitude;
              var longitude = location.longitude;
              var postal    = location.postal;
              var network   = location.organisation;
            }
          }

          report_data.push({
            'Date'          : date,
            'Category Name' : category_name,
            'Campaign Name' : campaign_name,
            'Campaign Label Name' : campaign_label_name,
            'Clicks'        : clicks,
            'City'          : city,
            'State'         : state,
            'Country'       : country,
            'Latitude'      : latitude,
            'Longitude'     : longitude,
            'Pin Code'      : postal,
            'Network'       : network,
            'Browser'       : helper.checkEmptyVal(browser),
            'Os'            : helper.checkEmptyVal(os),
            'Platform'      : helper.checkEmptyVal(platform),
            'Source'        : helper.checkEmptyVal(source)
          });

          if (isValid(browser_information)) {
            var browser     = browser_information.browser;
            var os          = browser_information.os;
            var platform    = browser_information.platform;
            var source      = browser_information.source;
          }
        }
      }

      //export data
      var fs               = require('fs');
      var date             = helper.getCurrentDateTime(); 
      var file_name        = cat_name + '_' + camp_name + '_'+ cont_label_name + '_' + date ;
      //export category and campaign data
      var export_category_campaign_label_data                   = {};
      export_category_campaign_label_data[file_name.toString()] = report_data;
      csv_export.export(export_category_campaign_label_data, function(buffer) {
        //this module returns a buffer for the csv files already compressed into a single zip.
        //save the zip or force file download via express or other server
        fs.writeFileSync('./public/export/' + file_name + '.zip', buffer);
        //res.download('./contacts_list_csv.zip'); // Set disposition and send it.   
      });
      res.send({
          'status'    : status_code,
          'msg'       : 'successfully exported as csv',
          'file_name' : file_name
      });
      client.close();
    })(); // async
  }); //mongoclient
});

router.post('/kx_report_export_as_csv', ensureAuthenticated, (req, res, next) => {

    var fromdate = req.body.fromdate;
    var todate = req.body.todate;

    var start_date = new Date(fromdate + ' 00:00:00');
    var end_date = new Date(todate);
    var add_one_day_to_date = new Date(end_date.getTime() + 1 * 24 * 60 * 60000);
    var search_campaign=req.body.search_campaign;

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);

            (async () => {

                let count;

                if(search_campaign!=''){
                    var conditios = {
                        'created_at': {
                            $gte: ISODate(start_date),
                            $lte: ISODate(add_one_day_to_date)
                        },
                        'Campaign':{$regex:'^'+search_campaign+'$','$options' : 'i'}
                    };
                }else{
                    var conditios = {
                        'created_at': {
                            $gte: ISODate(start_date),
                            $lte: ISODate(add_one_day_to_date)
                        }
                    };
                }

                const karix_clickers = await db.collection('karix_clickers').aggregate([

                    {
                        $match: conditios
                    },

                    {
                        $project: {
                            "year": {
                                $year: "$created_at"
                            },
                            "month": {
                                $month: "$created_at"
                            },
                            "day": {
                                $dayOfMonth: "$created_at"
                            },
                            mobile:"$mobile",
                            city:"$city",
                            IP:"$IP",
                            Campaign:"$Campaign",
                            Device:"$Device",
                            Operating:"$Operating",
                            Platform:"$Platform",
                            Browser:"$Browser",
                            BrowVersion:"$BrowVersion",
                            Engine:"$Engine",
                            Ename:"$Ename",
                            Country:"$Country",
                            Region:"$Region",
                            Latitude:"$Latitude",
                            Longitud:"$Longitud",
                            DeviceMake:"$DeviceMake",
                            clickeddate:"$clickeddate",
                            clickedtime:"$clickedtime",
                            MID:"$MID"

                        }
                    },

                    {
                        $sort: {
                            "_id.day": -1,
                            "_id.month": -1,
                            "_id.year": -1
                        }
                    },

                ]).toArray();

                var report_data = [];

                if (karix_clickers.length > 0) {



                    for (var i = 0; i < karix_clickers.length; i++) {

                        var date = karix_clickers[i].day + '-' + karix_clickers[i].month + '-' + karix_clickers[i].year;
                        var click_count = 1; 

                        var mobile = karix_clickers[i].mobile;
                        var city = karix_clickers[i].city;
                        var IP = karix_clickers[i].IP;
                        var Campaign = karix_clickers[i].Campaign;
                        var Device = karix_clickers[i].Device;
                        var Operating = karix_clickers[i].Operating;
                        var Platform = karix_clickers[i].Platform;
                        var Browser = karix_clickers[i].Browser;
                        var BrowVersion = karix_clickers[i].BrowVersion;
                        var Engine = karix_clickers[i].Engine;
                        var Ename = karix_clickers[i].Ename;
                        var Country = karix_clickers[i].Country;
                        var Region = karix_clickers[i].Region;
                        var Latitude = karix_clickers[i].Latitude;
                        var Longitude = karix_clickers[i].Longitude;
                        var DeviceMake = karix_clickers[i].DeviceMake;
                        var clickeddate = karix_clickers[i].clickeddate;
                        var clickedtime = karix_clickers[i].clickedtime;
                        var MID = karix_clickers[i].MID;

                        report_data.push({
                            'Date': date,
                            'mobile': mobile,
                            'city': city,
                            'IP': IP,
                            'Campaign': Campaign,
                            'Device': Device,
                            'Operating': Operating,
                            'Platform': Platform,
                            'Browser': Browser,
                            'BrowVersion': BrowVersion,
                            'Engine': Engine,
                            'Ename': Ename,
                            'Country': Country,
                            'Region': Region,
                            'Latitude': Latitude,
                            'Longitude': Longitude,
                            'DeviceMake': DeviceMake,
                            'clickeddate': clickeddate,
                            'clickedtime': clickedtime,
                            'MID ': MID
                        });

                    }


                }

                //export data
                var fs = require('fs');
                var date = helper.getCurrentDateTime(); 
                var file_name = 'karix_click_report' ;

                //export category and campaign data
                var export_category_campaign_label_data = {};
                export_category_campaign_label_data[file_name.toString()] = report_data;
                csv_export.export(export_category_campaign_label_data, function(buffer) {

                    //this module returns a buffer for the csv files already compressed into a single zip.
                    //save the zip or force file download via express or other server
                    fs.writeFileSync('./public/export/karix_click_report.zip', buffer);
                    //res.download('./contacts_list_csv.zip'); // Set disposition and send it.   

                });

                res.send({
                    'status': 200,
                    'msg': 'successfully exported as csv',
                    'file_name': file_name
                });
                client.close();
            })();

        });


});

/*
 method      : /sms_report_export_as_csv
 description : Exporting sms report in .xlsx file 
 return      : file_path 
*/
router.post('/sms_report_export_as_csv', ensureAuthenticated, (req, res, next) => {

  let perPage      = 20;
  let page         = req.body.page_no || 1;
  var current      = req.body.page_no;
  var status_code  = 400;

  var fromdate     = req.body.fromdate;
  var todate       = req.body.todate;

  var start_date   = new Date(fromdate);
  start_date.setHours(0, 0, 0, 0);
  //var limit_msgs_val = parseInt(req.body.limit_msgs_val);

  var end_date     = new Date(todate);
  var add_one_day_to_date = new Date(end_date.getTime() + 1 * 24 * 60 * 60000);


  MongoClient.connect(db, db_options).then(client => {
    const db       = client.db(database_name);

    (async () => {

      let count;

      var campaign_id      = parseInt(req.body.campaign_id);
      var content_label_id = parseInt(req.body.content_label_id);
      var smpp_id          = req.body.smpp_id;  
      var smpp_name        = req.body.smpp_name;

      var conditions = {
        'created_at': {
          $gt: start_date,
          $lte: add_one_day_to_date
        }, 
        'mode': { $in: ['1'] },
      };

      if (content_label_id > 0) {
        conditions['campaign_content_label_id'] = parseInt(content_label_id);
      }

      if (campaign_id > 0) {
        conditions['campaign_id']               = parseInt(campaign_id)
      }
    
      if (smpp_id) {
        conditions['smpp_id']                   = smpp_id; //sms_logs storing smpp_name        
      }

      var sms_logs_query = [];
      sms_logs_query.push(
        {
          $match: conditions
        },
        {
          $project: {
            "year": {
                $year: "$created_at"
            },
            "month": {
                $month: "$created_at"
            },
            "day": {
                $dayOfMonth: "$created_at"
            },
            mobile_no:"$mobile_no",
            sms_content:"$sms_content",
            sender_id:"$sender_id",
            campaign_content_label_id:"$campaign_content_label_id",
            campaign_id:"$campaign_id",
            message_status:"$message_status",
            message_id:"$message_id",
            message_submit_date:"$message_submit_date",
            message_err:"$message_err",
            message_delivered_code:"$message_delivered_code",
            updated_at:"$updated_at",
            //created_at:"$created_at",
          }
        },
        
        {
            $sort: {
               '_id': -1
            }
        },
        {
          $limit: 2000000
        }

      );

      // if (!isInValid(limit_msgs_val)) {
      //   sms_logs_query.push({
      //     $limit: limit_msgs_val
      //   });
      // }

      const sms_logs  = await db.collection('sms_logs').aggregate(sms_logs_query).toArray();

      var report_data = [];
              
      if (sms_logs.length > 0) {
        sms_logs.reverse();
        for (var i = 0; i < sms_logs.length; i++) {

          var sender_id                 = sms_logs[i].sender_id;
          var mobile_no                 = sms_logs[i].mobile_no;
          var message_submit_date       = getIndianDateFormat(sms_logs[i].message_submit_date,'date');
          var message_submit_time       = getIndianDateFormat(sms_logs[i].message_submit_date,'time');

          if (sms_logs[i].updated_at) {
            var message_delivery_date   = getIndianDateFormat(sms_logs[i].updated_at,'date');
            var message_delivery_time   = getIndianDateFormat(sms_logs[i].updated_at,'time');

          } else {
            var message_delivery_date   = '';
            var message_delivery_time   = '';
          }

          var sms_content               = sms_logs[i].sms_content; 
          var message_status            = sms_logs[i].message_status;
          var message_reason            = sms_logs[i].message_status;
          var message_id                = sms_logs[i].message_id;

          var campaign_id               = sms_logs[i].campaign_id;
          var campaign_content_label_id = sms_logs[i].campaign_content_label_id;
          var campaign_name             = await db.collection('campaign').findOne({'_id':campaign_id});
          var campaign_label_name       = await db.collection('campaign_content_labels').findOne({'_id':campaign_content_label_id});
         
          //var message                   = sms_content.replace(/(\r\n|\n|\r)/gm, "");
          //var export_message            = "\"" + message + "\"";

          var message_err_code          = sms_logs[i].message_err;
          var get_err_desc              = constants.SMPP_ERR_CODE[message_err_code];

          var msg_err_code_txt          = 'Message is not delivered yet.'; 
          if (message_err_code && message_err_code != '' && get_err_desc) {
            msg_err_code_txt            = get_err_desc.description;
          } 

          if (message_err_code && !get_err_desc) {
            msg_err_code_txt            = 'Cannot say the error';
          }
          //sms_content = JSON.stringify(sms_content);
          report_data.push({
            'Campaign Name': campaign_name.name,
            'Campaign Label Name': campaign_label_name.label_name,
            'Sender ID': sender_id,
            'Mobile number': mobile_no,
            'Submit Date': message_submit_date,
            'Submit Time': message_submit_time,
            'Delivery Date': message_delivery_date,
            'Delivery Time': message_delivery_time,
            'Message': sms_content,
            'Status':message_status,
            'Reason':msg_err_code_txt,
            'MID':message_id
          });
        }
      }

      // //export data
      // var fs = require('fs');
      // var date = helper.getCurrentDateTime(); 
      // var file_name = 'sms_logs_report' ;

      // //export category and campaign data
      // var export_category_campaign_label_data = {};
      // export_category_campaign_label_data[file_name.toString()] = report_data;
      // csv_export.export(export_category_campaign_label_data, function(buffer) {

      //   //this module returns a buffer for the csv files already compressed into a single zip.
      //   //save the zip or force file download via express or other server
      //   fs.writeFileSync('./public/export/sms_logs_report.zip', buffer);
      //   //res.download('./contacts_list_csv.zip'); // Set disposition and send it.   

      // });

      /* exporting to excel file */
      var Excel = require('exceljs');
      const workbook = new Excel.Workbook({
          useStyles: true
      })

      const headers = [
          { header: 'Campaign Name', key: 'Campaign Name' },
          { header: 'Campaign Label Name', key: 'Campaign Label Name'},
          { header: 'Sender ID', key: 'Sender ID' },
          { header: 'Mobile number', key: 'Mobile number' },
          { header: 'Submit Date', key: 'Submit Date'},
          { header: 'Submit Time', key: 'Submit Time' },
          { header: 'Delivery Date', key: 'Delivery Date' },
          { header: 'Delivery Time', key: 'Delivery Time' },
          { header: 'Message', key: 'Message', width: 40 },
          { header: 'Status', key: 'Status' },
          { header: 'Reason', key: 'Reason' },
          { header: 'MID', key: 'MID' }
      ];

      var file_name        = "./public/test_log_bulk/sms_log_report_" + helper.getCurrentTime() + ".xlsx";
      workbook.xlsx.writeFile(file_name);

        if (report_data.length > 0) {

        var length = report_data.length - 1; var each_arr_size = 1048575;
        iteration_batch_min_and_max_values = [];

        if (length > each_arr_size) {
          for (var size = 0; size < length;) {

            if (iteration_batch_min_and_max_values.length > 0) {
              var min_val       = length - size - each_arr_size; 
              var min_range_val = min_val;
              if (min_val < 0) {
                min_range_val   = 0;
              }
              iteration_batch_min_and_max_values.push([length - size - 1, min_range_val]);
            } else {
              iteration_batch_min_and_max_values.push([length, length - each_arr_size]);
            }      
            size                = size + each_arr_size;
          }
        } else {
          iteration_batch_min_and_max_values.push([length, 0])
        } 

        for (var i = 0; i < iteration_batch_min_and_max_values.length; i++) {
          var sheet_name     =  i.toString(); 
          const worksheet    = await workbook.addWorksheet('Sheet' + sheet_name);
          worksheet.state    = 'visible';
          worksheet.columns  = headers;

          var min_value      = iteration_batch_min_and_max_values[i][0];
          var max_value      = iteration_batch_min_and_max_values[i][1]; 
          for (var j = min_value; j >= max_value; j--) {
            await worksheet.addRow(report_data[j]).commit();
          }
          //await worksheet.commit();
          await workbook.xlsx.writeFile(`${file_name}`);      
        }
        status_code = 200;
      }
      /* end exporting to excel file */

      res.send({
        'status': status_code,
        'msg': 'successfully exported as csv',
        'file_name': file_name
      });
      client.close();
    })(); //end async 
  }); //end mongoclient
});

router.post('/smpp_report_export_as_csv', ensureAuthenticated, (req, res, next) => {

    let category_id = req.body.category_id;
    let campaign_id = parseInt(req.body.campaign_id);
    let content_label_id = parseInt(req.body.content_label_id);

    var fromdate = req.body.fromdate;
    var todate = req.body.todate;

    var start_date = new Date(fromdate);
    start_date.setHours(0, 0, 0, 0);


    var end_date = new Date(todate);
    var add_one_day_to_date = new Date(end_date.getTime() + 1 * 24 * 60 * 60000);


    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);

            (async () => {

                let count;


                if(campaign_id>0&&campaign_content_id>0){

                    var conditions = {
                        'created_at': {
                            $gt: start_date,
                            $lte: add_one_day_to_date
                        },
                        'campaign_id':parseInt(campaign_id),
                        'campaign_content_id':parseInt(content_label_id)
                    };

                }else if(campaign_id>0){

                    var conditions = {
                        'created_at': {
                            $gt: start_date,
                            $lte: add_one_day_to_date
                        },
                        'campaign_id':parseInt(req.body.campaign_id)
                    };

                }else{

                    var conditions = {
                        'created_at': {
                            $gt: start_date,
                            $lte: add_one_day_to_date
                        }
                    };

                }
                

                const sms_logs = await db.collection('sms_logs').aggregate([

                    {
                        $match: conditions
                    },
                    {
                        $project: {
                            "year": {
                                $year: "$created_at"
                            },
                            "month": {
                                $month: "$created_at"
                            },
                            "day": {
                                $dayOfMonth: "$created_at"
                            },
                            destination_addr:"$destination_addr",
                            smpp_id:"$smpp_id",
                            message:"$message",
                            sequence_number:"$sequence_number",
                            source_addr:"$source_addr",
                            campaign_content_label_id:"$campaign_content_label_id",
                            campaign_id:"$campaign_id",
                            message_status:"$message_status",
                            message_id:"$message_id",
                            message_sent_date:"$message_sent_date",
                            created_at:"$created_at",
                        }
                    },

                  
                    {
                        $sort: {
                            "_id.day": 1,
                            "_id.month": 1,
                            "_id.year": 1
                        }
                    },

                ]).toArray();

                var report_data = [];

                if (sms_logs.length > 0) {

                    for (var i = 0; i < sms_logs.length; i++) {

                        var smpp = sms_logs[i].smpp_id;
                        var sender_id = sms_logs[i].destination_addr;
                        var campaign_id = sms_logs[i].campaign_id;
                        var campaign_content_label_id = sms_logs[i].campaign_content_label_id;
                        var source_addr=sms_logs[i].source_addr;
                        var message_status=sms_logs[i].message_status;
                        var sequence_number=sms_logs[i].sequence_number;
                        var message_id=sms_logs[i].message_id;
                        var message_sent_date=sms_logs[i].message_sent_date;
                        var message=sms_logs[i].message;

                        var campaign_name = await db.collection('campaign').findOne({'_id':campaign_id});
                        var campaign_label_name = await db.collection('campaign_content_labels').findOne({'_id':campaign_content_label_id});

                        report_data.push({
                            'smpp': smpp,
                            'sender_id': sender_id,
                            'campaign_name': campaign_name.name,
                            'content_label': campaign_label_name.label_name,
                            'mobile_no': source_addr,
                            'sms_content': message,
                            'message_id': message_id,
                            'status': message_status,
                            'sent_date': message_sent_date
                        });

                    }


                }

                //export data
                var fs = require('fs');
                var date = helper.getCurrentDateTime(); 
                var file_name = 'sms_logs_report' ;

                //export category and campaign data
                var export_category_campaign_label_data = {};
                export_category_campaign_label_data[file_name.toString()] = report_data;
                csv_export.export(export_category_campaign_label_data, function(buffer) {

                    //this module returns a buffer for the csv files already compressed into a single zip.
                    //save the zip or force file download via express or other server
                    fs.writeFileSync('./public/export/sms_logs_report.zip', buffer);
                    //res.download('./contacts_list_csv.zip'); // Set disposition and send it.   

                });

                res.send({
                    'status': 200,
                    'msg': 'successfully exported as csv',
                    'file_name': file_name
                });
                client.close();
            })();

        });


});

router.post('/sms_campaign_summary_report_export_as_csv', ensureAuthenticated, (req, res, next) => {

    var fromdate         = req.body.fromdate;
    var todate           = req.body.todate;

    var start_date       = new Date(fromdate);
    start_date.setHours(0, 0, 0, 0);


    var end_date         = new Date(todate);
    var add_one_day_to_date = new Date(end_date.getTime() + 1 * 24 * 60 * 60000);

    var smpp_id          = req.body.smpp_id;
    var sender_id        = req.body.sender_id;
    var campaign_id      = req.body.campaign_id;
    var content_label_id = req.body.content_label_id;
    var status_code      = 400; 

    MongoClient.connect(db, db_options).then(client => {
      const db           = client.db(database_name);

      (async () => {

        let count;
        var conditions   = {
          'date_added'   : {
            $gt          : start_date,
            $lte         : add_one_day_to_date
          },
          //'mode'         : { $in: ['1'] },
        };                

        if (campaign_id > 0) {
          conditions['campaign_id']        = parseInt(campaign_id);
        }

        if (content_label_id > 0) {
          conditions['content_label_id']   = parseInt(content_label_id);
        }
        if (smpp_id) {
          conditions['segments.smpp_id']   = smpp_id;
        }

        if (sender_id) {
          conditions['segments.sender_id'] = sender_id;
        }
        const campaign_today = await db.collection('campaign_content_labels').aggregate([
            {
                $match : conditions
            },

            {
              $project         : {
                "campaign_id"  : "$campaign_id",
                "total_sender" : "$total_sender",
                "year"         : {
                  $year        : "$date_added"
                },
                "month"        : {
                  $month       : "$date_added"
                },
                "day"          : {
                  $dayOfMonth  : "$date_added"
                }
              }
            },

            {
              "$group"         : {       
                _id            : {
                  campaign_id  : "$campaign_id",                                    
                  count        : {$sum:1},
                  year         : "$year",
                  month        : "$month",
                  day          : "$day"
                },
                total_sender   : {$sum:"$total_sender"}
              }
            }
        ]).toArray(); 

        if (campaign_today.length > 0) {
          status_code           = 200;
          /* Loop 1 - Start */
          
          var result            = [];
          var result_date       = [];
          var total_deliverd    = 0;
          var total_undeliverd  = 0;
          var total_dlr_pending = 0;
          var total_failed      = 0;
          var total_expired     = 0;
          var total_dnd_failed  = 0;
          var total_blocked     = 0;
          var others            = 0;

          for (var campaign = 0; campaign < campaign_today.length; campaign++) {
              
            var campaign_id     = campaign_today[campaign]._id.campaign_id;
            var date            = campaign_today[campaign]._id.day + '-' + campaign_today[campaign]._id.month + '-' + campaign_today[campaign]._id.year;
            var total_sent      = campaign_today[campaign].total_sender;
            result_date[campaign] = date;
            
            var campaign_start_date = new Date(campaign_today[campaign]._id.year + '-' + campaign_today[campaign]._id.month + '-' + campaign_today[campaign]._id.day);
            campaign_start_date.setHours(0, 0, 0, 0);

            var campaign_end_date = new Date(campaign_today[campaign]._id.year + '-' + campaign_today[campaign]._id.month + '-' + campaign_today[campaign]._id.day);
            campaign_end_date.setHours(23, 59, 59, 59);

            var delivery_params = {
              "created_at"  : {
                $gt         : campaign_start_date,
                $lte        : campaign_end_date
              },
              'campaign_id' : parseInt(campaign_id),
              "message_err" : { $in: [/^001/,/^000/] }
            };

            var undelivered_params = {
              "created_at" : {
                $gt        : campaign_start_date,
                $lte       : campaign_end_date
              },
              'campaign_id' : parseInt(campaign_id),
              "message_err" : { $nin: [/^001/,/^000/] },
              $nor : [{
                'message_status': { $in: [/^PENDING/,/^SCHEDULE/,/^SCHEDULE CANCELLED/,/^SUBMIT/] }
              }]
            };

            var dlr_pending_params = {
              "created_at":{
                $gt: campaign_start_date,
                $lte: campaign_end_date
              },
              'campaign_id':parseInt(campaign_id),
              "message_status": { $in: [/^PENDING/] }
            };

            var failed_params = {
              "created_at": {
                $gt  : campaign_start_date,
                $lte : campaign_end_date
              },
              'campaign_id' : parseInt(campaign_id),
              "message_err" : { $in: [/^019/,/^023/,/^219/,/^035/,/^999/,/^002/] }
            };

            var expired_params = {
              "created_at" : {
                  $gt  : campaign_start_date,
                  $lte : campaign_end_date
              },
              'campaign_id' : parseInt(campaign_id),
              "message_err" : { $in: [/^009/,/^031/,/^032/,/^033/,/^034/,/^035/,/^036/,/^037/] }
            };

            var dnd_failed_params = {
              "created_at":{
                $gt: campaign_start_date,
                $lte: campaign_end_date
              },
              'campaign_id':parseInt(campaign_id),
              "message_err": { $in: [/^004/] }
            };

            var blocked_params = {
              "created_at":{
                $gt: campaign_start_date,
                $lte: campaign_end_date
              },
              'campaign_id':parseInt(campaign_id),
              "message_err": { $in: [/^507/,/^519/,/^005/] }
            };

            var others_params = {
              "created_at":{
                $gt: campaign_start_date,
                $lte: campaign_end_date
              },
              'campaign_id':parseInt(campaign_id),
              "message_status": { $nin: [/^SUBMIT/] },
               $nor:[{
                  'message_err': { $in: [/^-01/,/^000/,/^001/,/^002/,/^004/,/^005/,/^006/,/^007/,/^008/,/^009/,/^010/,/^011/,/^012/,/^013/,/^014/,/^015/,/^016/,/^017/,/^019/,/^020/,/^021/,/^031/,/^032/,/^033/,/^034/,/^035/,/^036/,/^037/,/^051/,/^052/,/^053/,/^064/,/^066/,/^067/,/^068/,/^069/,/^072/,/^073/,/^080/,/^081/,/^083/,/^084/,/^085/,/^088/,/^089/,/^097/,/^098/,/^099/,/^100/,/^101/,/^102/,/^103/,/^104/,/^110/,/^120/,/^130/,/^140/,/^150/,/^160/,/^190/,/^192/,/^193/,/^194/,/^195/,/^196/,/^200/,/^210/,/^245/,/^255/,/^507/,/^508/,/^517/,/^519/,/^522/,/^537/,/^999/] }
              }]
            };

            var params_data = [
              {'type':'total_deliverd','params':delivery_params},
              {'type':'total_undeliverd','params':undelivered_params},
              {'type':'total_dlr_pending','params':dlr_pending_params},
              {'type':'total_failed','params':failed_params},
              {'type':'total_expired','params':expired_params},
              {'type':'total_dnd_failed','params':dnd_failed_params},
              {'type':'total_blocked','params':blocked_params},
              {'type':'others','params':others_params},
            ];

            var campaign_details = await db.collection('campaign').findOne({
              '_id': parseInt(campaign_id)
            });  

            result[campaign+'_'+date] = {
              'date':date,
              'campaign_name':campaign_details.name,
              'total_deliverd':total_deliverd,
              'total_undeliverd':total_undeliverd,
              'total_dlr_pending':total_dlr_pending,
              'total_failed':total_failed,
              'total_expired':total_expired,
              'total_dnd_failed':total_dnd_failed,
              'total_blocked':total_blocked,
              'others':others,
              'total':total_sent,
            };


            if (params_data.length > 0) {
                

              for (var param_i = 0; param_i < params_data.length; param_i++) {
                    
                var sms_logs = await db.collection('sms_logs').aggregate([
                  {
                      $match:params_data[param_i].params    
                  },

                  {
                    $project: {
                      "year": {
                        $year: "$created_at"
                      },
                      "month": {
                        $month: "$created_at"
                      },
                      "day": {
                        $dayOfMonth: "$created_at"
                      }
                    }
                  },                  
                  // Group by year, month and day and get the count
                  {
                    $group: {
                      _id: {
                        year: "$year",
                        month: "$month",
                        day: "$day"
                      },
                      "count": {
                        $sum: 1
                      }
                  }
              }
                ]).toArray();

                if (sms_logs.length > 0) {
                  var total_status_count = sms_logs[0].count;

                  if (params_data[param_i].type == 'total_deliverd') {
                      result[campaign + '_' + date].total_deliverd = total_status_count;
                  } else if (params_data[param_i].type == 'total_undeliverd') {
                      result[campaign + '_' + date].total_undeliverd = total_status_count;
                  } else if (params_data[param_i].type == 'total_dlr_pending') {
                      result[campaign + '_' + date].total_dlr_pending = total_status_count;
                  } else if (params_data[param_i].type == 'total_failed'){
                      result[campaign + '_' + date].total_failed = total_status_count;
                  } else if (params_data[param_i].type == 'total_expired'){
                      result[campaign + '_' + date].total_expired = total_status_count;
                  } else if (params_data[param_i].type == 'total_dnd_failed'){
                      result[campaign + '_' + date].total_dnd_failed = total_status_count;
                  } else if (params_data[param_i].type == 'total_blocked'){
                      result[campaign + '_' + date].total_blocked = total_status_count;
                  } else if (params_data[param_i].type == 'others'){
                      result[campaign + '_' + date].others = total_status_count;
                  }

                }
                
            }

          }
          /* Loop 1 -stop */
          var export_array      = [];

        if (Object.keys(result).length > 0) {

          for (var export_i = 0; export_i < Object.keys(result).length; export_i++) {

            var date_export        = result_date[export_i];
            export_array[export_i] = {
              'Date'           : result[export_i + '_' + date_export].date,
              'Country'        : 'India',
              'Total Sent'     : result[export_i + '_' + date_export].total,
              //'total_sent'   : result[export_i+'_'+date_export].total,
              'Campaign Name'  : result[export_i + '_' + date_export].campaign_name,
              'Delivered'      : result[export_i + '_' + date_export].total_deliverd,
              'Un Delivered'   : result[export_i + '_' + date_export].total_undeliverd,
              'DLR Pending'    : result[export_i + '_' + date_export].total_dlr_pending,
              //'dlr_pending'  : result[export_i+'_'+date_export].total_dlr_pending,
              'Failed'         : result[export_i + '_' + date_export].total_failed,
              'Expired'        : result[export_i + '_' + date_export].total_expired,
              'DND Failed'     : result[export_i + '_' + date_export].total_dnd_failed,
              //'dnd_failed'   : result[export_i+'_'+date_export].total_dnd_failed,
              'Blocked'        : result[export_i + '_' + date_export].total_blocked,
              'Others'         : result[export_i + '_' + date_export].others,
            };
          }
        }

        }


        //export data
        var fs = require('fs');
        var date = helper.getCurrentDateTime(); 
        var file_name = 'sms_campaign_summary_report' ;

        //export category and campaign data
        var export_category_campaign_label_data = {};
        export_category_campaign_label_data[file_name.toString()] = export_array;
        csv_export.export(export_category_campaign_label_data, function(buffer) {

        //this module returns a buffer for the csv files already compressed into a single zip.
        //save the zip or force file download via express or other server
        fs.writeFileSync('./public/export/sms_campaign_summary_report.zip', buffer);
        //res.download('./contacts_list_csv.zip'); // Set disposition and send it.   

        });
        }

        res.send({
          'status': status_code,
          'msg': 'successfully exported as csv',
          //'file_name': file_name
        });
        client.close();
      })();
    });
});


function getIndianDateFormat(date,type)
{

    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hours = d.getHours(),
        minutes = d.getMinutes(),
        seconds = d.getSeconds();
    
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var hours = d.getHours();
    var minutes = d.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;


    if(type=='date_and_time')
    {
        var date_format=day+'-'+month+'-'+year+' '+strTime;
    }else if(type=='date'){
        var date_format=day+'-'+month+'-'+year;
    }else if(type=='time'){
        var date_format=strTime;
    }else{
        var date_format=day+'-'+month+'-'+year+' '+strTime;
    }  

    return date_format;
    
}



/* description: Testing csv export whether bulk data downloading in excel file  correctly or not */
router.get('/test_export_as_csv', ensureAuthenticated, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);

    (async () => {

      // var Excel = require('exceljs');
      //   var workbook = new Excel.Workbook();
      //   var worksheet = workbook.addWorksheet('My Sheet');
      //   worksheet.columns = [
      //       { header: 'Id', key: 'id', width: 10 },
      //       { header: 'Name', key: 'name', width: 32 },
      //       { header: 'D.O.B.', key: 'DOB', width: 10 }
      //   ];
      //   worksheet.addRow({id: 1, name: 'John Doe', dob: new Date(1970,1,1)});
      //   worksheet.addRow({id: 2, name: 'Jane Doe', dob: new Date(1965,1,7)});
      //   await workbook.xlsx.writeFile('./public/test_log_bulk/contact_dummy_report_excel.xlsx').then(function() {
      //       // done
      //       console.log('file is written');
      //   });

      //   res.send({
      //   'status': 200,
      //   'msg': 'successfully exported as csv',
       
      // });
      // const contact_dummy  = await db.collection('contacts_dummy').find({}).toArray();

      // var report_data = [];
              
      // if (contact_dummy.length > 0) {
        
      //   for (var i = 0; i < contact_dummy.length; i++) {

      //     report_data.push({
      //       'is_active': contact_dummy[i].is_active,
      //       'unique id': contact_dummy[i].unique_id,
      //       'created_at': contact_dummy[i].created_at,
            
      //     });
      //   }
      // }

      // //export data
      // var fs = require('fs');
      // var date = helper.getCurrentDateTime(); 
      // var file_name = 'contact_dummy_report' ;

      // //export category and campaign data
      // var export_category_campaign_label_data = {};
      // export_category_campaign_label_data[file_name.toString()] = report_data;
      // csv_export.export(export_category_campaign_label_data, function(buffer) {

      //   //this module returns a buffer for the csv files already compressed into a single zip.
      //   //save the zip or force file download via express or other server
      //   fs.writeFileSync('./public/test_log_bulk/contact_dummy_report.zip', buffer);
      //   //res.download('./contacts_list_csv.zip'); // Set disposition and send it.   

      // });

      // res.send({
      //   'status': 200,
      //   'msg': 'successfully exported as csv',
      //   'file_name': file_name
      // });
      client.close();
    })(); //end async 
  }); //end mongoclient
}); //test_export_as_csv


module.exports = router;
