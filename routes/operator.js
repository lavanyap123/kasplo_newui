const express             = require('express');
var mongoose              = require('mongoose');
var MongoClient           = require('mongodb').MongoClient;
var autoIncrement         = require("mongodb-autoincrement");

const router              = express.Router();
const {ensureAuthenticated, roleAuthenticated} = require('../config/auth');
var activityController    = require('./../controllers/activity_ctrl');
// DB Config
const db                  = require('../config/keys').mongoURI;
const db_options          = require('../config/keys').db_options;
const database_name       = require('../config/keys').database_name;

// operator - List
router.get('/all', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db             = client.db(database_name);
    const collection     = db.collection('operator');

    collection.find().sort({'date_added': -1}).toArray(function(err, operator) {
      res.render('operator/manage', {
        operator,
        user          : req.user,
        activity_logs : req.activity_logs,
        activity_logs_count : req.activity_logs_count,
      });
    });
    client.close();
  });

});


router.post('/popup_content', ensureAuthenticated, (req, res) => {
    var type = req.body.type;
    var id   = req.body.id;


    if (type == 'add') {

      var operator_html_content = '<div class="modal-header"> <h5 class="modal-title" id="createoperatorModal">Create operator</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><div id="notification"></div> <form id="add_operator" type="post"> <div class="row"> <div class="col-12"> <div class="form-group"> <label for="exampleInputEmail1">Operator Name</label> <input type="text" class="form-control" name="name" id="operator_name" placeholder="Enter operator"> </div><div class="form-group"> <label for="exampleInputEmail1">Operator Description</label> <textarea class="form-control" name="description" id="operator_description" rows="2" placeholder="Enter operator Description"></textarea> </div><div class="form-group"> <label class="col-form-label">Status</label> <div class=""> <select class="form-control" name="status" id="status"> <option value="">---Status ---</option> <option value="1">Active</option> <option value="0">De-active</option> </select> </div></div></div><div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> <button type="submit" class="btn btn-primary" id="save_operator">Save</button><img src="/public/images/loader_form.gif" class="img-responsive" id="loader" style="width: 25px;display:none;"/> </form> </div></div></div>';
      res.send({
          'popup_content': operator_html_content
      });

    } else {


      MongoClient.connect(db, db_options).then(client => {
      const db         = client.db(database_name);
      const collection = db.collection('operator');

      collection.findOne({
          '_id': parseInt(id)
      }).then(function(doc) {


          if (!doc) {
              throw new Error('No record found.');
          } else {
              var status = doc.status;
              if (status == '1') {
                  var active_selected = 'selected';
                  var de_active_selected = '';

              } else {
                  var active_selected = '';
                  var de_active_selected = 'selected';
              }

              var operator_html_content = '<div class="modal-header"> <h5 class="modal-title" id="createoperatorModal">Update Operator - #' + id + ' </h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><div id="notification"></div> <form id="add_operator" type="post"><input type="hidden" id="update_operator_id" value="' + id + '"> <div class="row"> <div class="col-12"> <div class="form-group"> <label for="exampleInputEmail1">Operator Name</label> <input type="text" class="form-control" name="name" id="operator_name" placeholder="Enter operator" value="' + doc.name + '"> </div><div class="form-group"> <label for="exampleInputEmail1">Operator Description</label> <textarea class="form-control" name="description" id="operator_description" rows="2">' + doc.description + '</textarea> </div><div class="form-group"> <label class="col-form-label">Status</label> <div class=""> <select class="form-control" name="status" id="status"> <option value="">---Status ---</option> <option value="1" ' + active_selected + '>Active</option> <option value="0"  ' + de_active_selected + '>De-active</option> </select> </div></div></div><div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> <button type="submit" class="btn btn-primary" id="btn_update_operator">Update</button><img src="/public/images/loader_form.gif" id="loader" class="img-responsive" style="width: 25px;display:none;"/> </form> </div></div></div>';

          }

          res.send({
              'popup_content': operator_html_content
          });

      });

      client.close();

      });
    }
});

router.post('/add', ensureAuthenticated, (req, res) => {

  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);
    const collection = db.collection('operator');

    autoIncrement.getNextSequence(db, 'operator', function(err, autoIndex) {

    var data = {
      '_id'         : autoIndex,
      'name'        : req.body.cat_name,
      'description' : req.body.description,
      'status'      : req.body.status,
      'created_at'  : new Date().toLocaleString('en-US', {
                      timeZone      : 'Asia/Calcutta'
                    }),
      'date_added'  : new Date()
    };

    collection.insert(data)
    .catch((error) => {
      console.log('error.message');
    });
    client.close();

    });

    res.send({
      'msg'    : 'success',
      'status' : 200
    });
  });

});

router.post('/update', ensureAuthenticated, (req, res) => {

MongoClient.connect(db, db_options).then(client => {
  const db         = client.db(database_name);
  const collection = db.collection('operator');

  var data = {
    'name'        : req.body.cat_name,
    'description' : req.body.description,
    'status'      : req.body.status,
    'updated_at'  : new Date().toLocaleString('en-US', {
        timeZone : 'Asia/Calcutta'
    })
  };

  var id         = req.body.operator_id;

  collection.updateOne({_id: parseInt(id)}, // Filter
      {
          $set: data
      }, // Update
      {
          upsert: false
      }
    )
    .then((obj) => {
        res.send({
            'msg': 'success',
            'status': 200
        });
    })
    .catch((err) => {
        res.send({
            'msg': err,
            'status': 400
        });
    });

  client.close();
  });

});

/**
 function: checkOperatorExists
 method  : POST
 description: checking if operator name given already exists or not in db
 params: {string} name $name
         {int} id $id
 return: true or false
*/
router.post('/checkOperatorExists', ensureAuthenticated, (req, res) => {

  MongoClient.connect(db, db_options).then(client => {
    const db         = client.db(database_name);
    var name         = req.body.name; //console.log(name)
    const collection = db.collection('operator');
    (async () => {
      var check_type = req.body.check_type;
      if (check_type == 'add') {
        var name_status = await collection.findOne({'name':new RegExp('^' + name + '$', 'i')});
      } else {
        var category_id = parseInt(req.body.category_id); 
        var name_status = await collection.findOne({'name':new RegExp('^' + name + '$', 'i'), '_id': {$ne: category_id}}); 
      }
      if (name_status) {               
        res.send('false');     
        return false;
      } else {
        res.send('true');     
        return true;
      }
      client.close();
    })(); //async
  }); 

}); //checkOperatorExists end

module.exports = router;