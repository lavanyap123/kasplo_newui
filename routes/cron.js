const express                = require('express');
const router                 = express.Router();
const {ensureAuthenticated}  = require('../config/auth');
var json2csv                 = require('json2csv'); //not working
var csv_export               = require('csv-export');
const stringify              = require('csv-stringify');
var autoIncrement            = require("mongodb-autoincrement");
var nodemailer               = require('nodemailer');

var csv                      = require('fast-csv');
const csv_parser             = require('csv-parser');

const fs                     = require('fs');
const path                   = require('path');

const request                = require('request');

var mongoose                 = require('mongoose');
var MongoClient              = require('mongodb').MongoClient;

// Dashboard
var await                    = require("await");
var async                    = require("async");
var Hashids                  = require('hashids');
// DB Config
const db                     = require('../config/keys').mongoURI;
const db_options             = require('../config/keys').db_options;
const database_name          = require('../config/keys').database_name;
const mail_from              = require('../config/keys').mail_from;
const to_mail_ids            = require('../config/keys').to_mail_ids;
const smpp_credentials       = require('../config/keys').smpp_credentials;
var cron_upload_path         = require('../config/keys').cron_path;
var mode                     = require('../config/keys').mode;

var XLSX                     = require('xlsx')

var ObjectID                 = require('mongodb').ObjectID; 

const {convertArrayToCSV}    = require('convert-array-to-csv');
const converter              = require('convert-array-to-csv');
var ISODate                  = require("isodate");

var shortlink                = require('shortlink');

router.get('/', (req, res) => res.render('login'));

router.get('/delete_all_files', (req, res, next) => {

  const directory = 'public/uploads';

  fs.readdir(directory, (err, files) => {
    if (err) throw err;

    for (const file of files) {
      fs.unlink(path.join(directory, file), err => {
        if (err) throw err;
      });
    }
  });

  res.send({
    'status': 200,
    'message': 'upload temp files are deleted'
  });

});

router.get('/delete_all_zip', (req, res, next) => {

  const directory = 'public/export';

  fs.readdir(directory, (err, files) => {
    if (err) throw err;

    for (const file of files) {
      fs.unlink(path.join(directory, file), err => {
        if (err) throw err;
      });
    }
  });

  res.send({
    'status': 200,
    'message': 'export zip files are deleted'
  });

});

router.get('/kx_clickers', (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db           = client.db(database_name);

    var url_parameters = req.query;

    if (Object.entries(url_parameters).length === 0) {

      var result       = {'status' : 204, 'message' : 'required parameters are missing'};

    } else {

      const api_clickers        = db.collection('karix_clickers');
      url_parameters.created_at = new Date();

      api_clickers.insert(url_parameters);
      var result                = {'status' : 200, 'message' : 'successfully data saved'}
    }

    res.send(result);
  });
});

router.get('/update_clickers_ip_location', (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db            = client.db(database_name);
    const clickers      = db.collection('clicks');
    const activity_logs = db.collection('activity_logs');

    var start_date      = new Date();
    start_date.setHours(0, 0, 0, 0);

    var end_date        = new Date();
    end_date.setHours(23, 59, 59, 59);
    
    clickers.find({
      'location'  :'',
      'created_at': {
        $gt       : start_date,
        $lte      : end_date
      }
    }).sort({
      'created_at': -1
    }).toArray(function (err, click_records) {

      if (click_records.length > 0) {

        click_records.forEach( (click) => {

          var p_ip = click.public_ip;  
          if (p_ip) {
            request('https://api.ipdata.co/' + p_ip + '?api-key=752ce37d4df46975b9613f45282aafed6703501a89ba8cf975d16ae6', function(error, response, geo_locations) {

              /* IP to GeoLocation - (start) */
              var glocation = JSON.parse(geo_locations);
              /* IP to GeoLocation - (stop) */
              var click_id = click._id;

              if (Number.isInteger(click_id)) {
                //console.log('if');
                db.collection('clicks').updateOne(
                  {'_id':click_id},                             
                  {$set: {"location":glocation}},
                  {upsert: true}
                );
              } else {             
                db.collection('clicks').updateOne(
                  {'_id':ObjectID(click_id)},                             
                  {$set: {"location":glocation}},
                  {upsert: true}
                );
              }
            });
          }
        });

        var activity_data = {
          'type': 'IP DATA API - CRON',
          'Location': 'CRON',
          'description': 'All Clickers IP Location updated successfully',
          'total_contacts': '',
          'duplicate_contacts': '',
          'filter_params': '',
          'user_name': 'System',
          'created_at': new Date().toLocaleString('en-US', {
              timeZone: 'Asia/Calcutta'
          })
        };

        activity_logs.insert(activity_data);

        res.send({
          'status': 'successfully updated ip locations'
        });
      }
    });
  });
});

router.get('/remove_duplicate_clicks', (req, res, next) => {

  MongoClient.connect(db, db_options).then( client => {
    const db = client.db(database_name);
    db.collection('clicks').aggregate([
      // discard selection criteria, You can remove "$match" section if you want
      {
        $match: {
          contact_no: {
            "$ne": ''
          }
        }
      },
      {
        $group: {
          _id: {
            contact_no: "$contact_no",
            public_ip: "$public_ip",
            content_label_id: "$content_label_id",

          }, // can be grouped on multiple properties
          dups: {
            "$addToSet": "$_id"
          },
          count: {
            "$sum": 1
          }
        }
      },
      {
        $match: {
          count: {
            "$gt": 1
          } // Duplicates considered as count greater than one
        }
      }
    ],{allowDiskUse: true} ) // You can display result until this and check duplicates
    .forEach(function (doc) {
      doc.dups.shift(); // First element skipped for deleting
      db.collection('clicks').remove({
        _id: {
          $in: doc.dups
        }
      }); // Delete remaining duplicates
    });

    res.send({
      'status': 'successfully deleted duplicated records'
    });
  });
});


/**
* method: upload_psftft_file
* description: Inserting/updating data to db from given path files
* return: success message
*/
router.get('/upload_psftft_file', (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
  const db          = client.db(database_name);
  var upload_type   = req.query;
  (async () => {

    if (upload_type == 'knowastro') {
      var directory = cron_upload_path['knowastro'];
    } else {
      var directory = cron_upload_path['other'];
      //var directory = path.join(__dirname, '/../public/upload-cron-files/psftft_data/');
      if (mode == 'localhost') {
        directory   = path.join(__dirname, directory);
      }
    }

    const collection          = db.collection('contacts');
    const activity_logs       = db.collection('activity_logs');
    const contacts_collection = db.collection('contacts');

    var cat_id; //'21'; //paisafatafat Category ID
    var user_name = 'system';

    collection.createIndex({
      'mobile_no': 1
    }, {
      unique: true
    });

    fs.readdir(directory, (err, files) => {
      var status_code  = 400;
      var message      = 'Failed to insert records';
      if (!err) {
        var max_length = files.length;
        if (max_length > 0) {
          status_code  = 200;
          message      = 'Successfully inserted records';
          for (var i = 0; i < max_length; i++) {
            (function(i) {
              setTimeout(function() {
                (async () => {
                  /* Total Contacts - START */
                  var existing_contacts         = await contacts_collection.count();
                  var filename                  = files[i];
                  var extension                 = filename.substr(filename.lastIndexOf(".") + 1);
                  var file_directory_path       = directory + '/' + filename;

                  /* Import -  Excel - Start */
                  var workbook                  = XLSX.readFileSync(file_directory_path);
                  var sheet_name_list           = workbook.SheetNames;
                  var excel_data                = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

                  var hashids                   = new Hashids();
                  var source_id                 = '';

                  var results                   = [];
                  var mobile_number_list        = [];

                  var bulk                      = collection.initializeUnorderedBulkOp();

                  for (i = 0; i < excel_data.length; i++) {
                    //var mobile_no = (excel_data[i].mobile_no.replace(/\D/g, '').slice(-10)); console.log(mobile_no)
                     if (typeof excel_data[i] !== "undefined" && validateMobileNum(excel_data[i].mobile_no)) {
                       var mobile_no = (excel_data[i].mobile_no.replace(/\D/g, '').slice(-10));
                    var data                    = {};
                    var new_id                  = new mongoose.Types.ObjectId();
                    var id                      = new_id.toString(),
                      ctr                       = 0;
                    var timestamp               = parseInt(id.slice(ctr, (ctr += 8)), 16);
                    var machineID               = parseInt(id.slice(ctr, (ctr += 6)), 16);
                    var processID               = parseInt(id.slice(ctr, (ctr += 4)), 16);
                    var counter                 = parseInt(id.slice(ctr, (ctr += 6)), 16);
                    mobile_number_list.push(mobile_no);
                    data['_id']                 = new mongoose.Types.ObjectId();
                    cat_id                      = excel_data[i].category_ids;
                    data.category_ids           = [cat_id];
                    data['mobile_no']           = mobile_no; //mobile no
                    data['campaign_ids']        = []; // Campaign ID
                    data['campaign_content_id'] = []; // Content Label ID
                    data['sender_category_ids'] = []; // sender_category_ids ID
                    data['sender_status']       = [];
                    data['cat_id']              = [];
                    data['sub_cat_id']          = [cat_id];
                    data['is_active']           = 1;
                    data['unique_id']           = shortlink.generate(8);
                    data['created_at']          = new Date().toLocaleString('en-US', {
                      timeZone: 'Asia/Calcutta'
                    });
                    var add_to_set_data = {
                      category_ids: cat_id
                    };

                    if (isValid(excel_data[i].source)) {
                      source_id                 = excel_data[i].source;
                      data['source']            = excel_data[i].source;
                      add_to_set_data['source'] = source_id;
                    }
                    if (isValid(excel_data[i].lead_date)) {
                      var lead_date             = new Date(excel_data[i].lead_date);
                      lead_date.setHours(11, 0, 0, 0);
                      data['lead_date']          = ISODate(lead_date);
                      add_to_set_data['lead_date'] = lead_date;
                    }

                    var final_data = {
                      '_id'                 : data._id,
                      'mobile_no'           : data.mobile_no,
                      'raw_category_id'     : data.sub_cat_id,
                      'campaign_ids'        : data.campaign_ids,
                      'campaign_content_id' : data.campaign_content_id,
                      'sender_category_ids' : data.sender_category_ids,
                      'sender_status'       : data.sender_status,
                      'cat_id'              : data.cat_id,
                      'sub_cat_id'          : data.sub_cat_id,
                      'is_active'           : data.is_active,
                      'unique_id'           : data.unique_id,
                      'created_at'          : data.created_at
                    }; 

                    // upsert method inserts new document if mobile_no exists in db
                    // $setOnInsert inserts new document if no match is there
                    // $addToSet updates passed json data when match is there
                    await bulk.find({
                      mobile_no: data['mobile_no']
                    }).upsert().update({
                      $setOnInsert : final_data,
                      $addToSet    : add_to_set_data,
                      $set : {
                        updated_at : data.created_at
                      }
                    }); //end bulk    
                    } 
                  } //end for

                  await bulk.execute(function(err, result) {
                    if (result) {
                      (async () => {

                        /* Total Contacts - START */
                        var total_imported = parseInt(result.nUpserted);
                        var duplicates     = parseInt(excel_data.length) - parseInt(result.nUpserted);

                        var activity_data  = {
                          'type'               : 'Import',
                          'Location'           : 'CRON',
                          'description'        : 'Total ' + total_imported + ' records have been successfully inserted.',
                          'total_contacts'     : excel_data.length,
                          'user_name'          : 'System',
                          'source'             : 'Paisafatafat',
                          'file_name'          : filename,
                          'duplicate_contacts' : Math.abs(duplicates),
                          'is_read'            : 0,
                          'created_at'         : new Date().toLocaleString('en-US', {
                            timeZone           : 'Asia/Calcutta'
                          })
                        };

                        await activity_logs.insert(activity_data);
                        //client.close();
                      })(); //async end
                    } // if result end
                  }); //bulk execute end

                  /* File Upload - Success - Sending Emails & File Unlink - Start */
                  await sendEmail('', filename, directory, filename);
                  /* File Upload - Success - Sending Emails & File Unlink - STOP */

                })(); //async 

              }, 10000 * i);

            })(i);

          } // File-Foreach Functions
        } //end max length
      }
      res.send({
        'status': status_code,
        'message': message
      });
    }); // File-Directory Reading Functions end
  })(); //async end

}); //mongoclient
}); // upload_psftft_file


function sendEmail(err, file_name,directory, file) {
  var transporter = nodemailer.createTransport(smpp_credentials);

  var mailOptions = {
    from: mail_from, 
    to: to_mail_ids,      
    subject: "Kasplo - Bulk SMS - Paisafatafat - File uploading successfully", // Subject line
    text: 'Hi Team,<br> <b> Cron Executed successfully. Below files are uploaded successfully and deleted successfully', // plain text body
    html: "<p><b style='color:green;'>File name: " + file_name + "</b></p>" // html body
  };

  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log(error);
    } else {

      fs.unlink(path.join(directory, file), err => {
        if (err) throw err;
      });

    }
  });
}

module.exports = router;