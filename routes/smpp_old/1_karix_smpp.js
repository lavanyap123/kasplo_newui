const express = require('express');
const router = express.Router();
const {
    ensureAuthenticated
} = require('../../config/auth');
const bcrypt = require('bcryptjs');
const passport = require('passport');
var json2csv = require('json2csv'); //not working
var csv_export = require('csv-export');
const stringify = require('csv-stringify');
var autoIncrement = require("mongodb-autoincrement");
var smpp = require('smpp');
var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
// Dashboard
var await = require("await");
var async = require("async");
var Hashids = require('hashids');
var nodemailer = require('nodemailer');
var ObjectId = require('mongodb').ObjectId;

const crypto = require('crypto');
var algorithm = 'aes256';
var key = 'password';
var activityController = require('./../../controllers/activity_ctrl');
// Load User model
const User = require('../../models/user');
// DB Config
const db = require('../../config/keys').mongoURI;
const db_options = require('../../config/keys').db_options;
const database_name= require('../../config/keys').database_name;
var cron = require('node-cron');
var ISODate = require("isodate");

var constants = require('./../../config/constants');
var keys = require('./../../config/keys');

cron.schedule('0-59/2 * * * * *', () => {

	/*
			Karix - TPS is unlimited but suggested each bind fire to 1000 TPS. There is no limited TPS
			Karix - Maximum session is 10 TR and 10 RX

	*/

	/* Every second Fetching the record 1000 from datatabase & SMS Sending 1000 TPS - (Start) */

    MongoClient.connect(db, db_options).then(client => {

	        const db = client.db(database_name);

	        (async () => {

		        const sms_logs = db.collection('sms_logs');
		        const smpp_collection = db.collection('smpp_details');

			    var smpp_details = await smpp_collection.findOne({
			        'smpp_id': "1" //Karix
			    });

			    var karix_tps = smpp_details.tps;
			    var tps_limit = karix_tps-2;
/*
			    console.log('Karix - TPS');
			    console.log(karix_tps);

			    console.log('Customize - TPS');
			    console.log(tps_limit);*/

                var karix_smpp_sms_list = await sms_logs.find({
                    'smpp_id': "1",
                    "message_status" : "SUBMIT"
                }).limit(parseInt(tps_limit)).toArray();

                if(karix_smpp_sms_list.length>0)
	            {

	                (async () => {
		               

					    var session = new smpp.Session({host: smpp_details.ip_address, port: smpp_details.host[0]});


					    delete smpp.encodings.ASCII;

					    var didConnect = false; 

					    session.on('connect', function(){

					        didConnect = true;

					        session.bind_transceiver({
					              system_id: smpp_details.system_id, //SMPP - Settings Table
					              password: smpp_details.password,  //SMPP - Settings Table
					        }, function(pdu) {

					           if(pdu.command_status == 0) { 

					                var mobile_number_list = [];
					                var save_sms_response = [];

					                karix_smpp_sms_list.forEach(function(record) {

					                    //Successfully bound
					                    session.submit_sm({
					                        source_addr: record.sender_id, //SENDER ID
					                        destination_addr:record.mobile_no, // Mobile Number
					                        short_message:record.sms_content, // Text SMS
					                        registered_delivery:1, //Mandatory Send Delivery Response
					                    }, function(pdu) {

					                        var message_list = [];

					                        if (pdu.command_status == 0) {
					                            // Message successfully sent
					                            var response = {};
					                            response['command_id'] = pdu.command_id;
					                            response['command_status'] = pdu.command_status;
					                            response['sequence_number'] = pdu.sequence_number;
					                            response['message_id'] = pdu.message_id;
					                            response['submit_sm_resp'] = pdu;
					                            response['message_status'] = 'PENDING'; //default message status
					                            response['message_type'] = 'Text SMS'; //default message status
					                            response['updated_at'] = new Date(); 

					                        /* Update - Message Status & Delivery Response - Start */
					                            updatingMessageCollection({
					                                    'message_status':'SUBMIT',
					                                    'smpp_id':"1",
					                                    'campaign_id':parseInt(record.campaign_id),
					                                    'campaign_content_label_id':parseInt(record.campaign_content_label_id),
					                                    'mobile_no':record.mobile_no
					                                },
					                                response,sms_logs
					                            );
					                        /* Update - Message Status & Delivery Response - Stop */

					                        }

					                    }); //submit_sm session - closed
					                    
					                 
					                }); //foreach - closed

					            }//PDU-Command Status Condtions - Closing

					         }); //session-bind - closed


					        // After sending Delivery Report
					        session.on('pdu', function(pdu) {

					            if(pdu.command=='query_sm')
					            {
					                var query_response = {};

					                var fromNumber = pdu.source_addr.toString();
					                var toNumber = pdu.destination_addr.toString();

					                var text = '';
					                var receipted_message_id = '';
					                var user_message_reference ='';

					                if (pdu.short_message && pdu.short_message.message) {
					                    text = pdu.short_message.message;
					                    receipted_message_id = pdu.short_message.message_payload;
					                    user_message_reference=pdu.short_message.user_message_reference;
					                }


					                var parse_deliver_text_str = text.toString(); 
					                var receipted_id = receipted_message_id; 

					                const querystring = require('querystring');    
					                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {decodeURIComponent: s =>  s.trim()});

					                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {decodeURIComponent: s =>  s.trim()});

					                  var msg_id = parse_deliver_text_json.id;
					                  query_response['service_type'] = pdu.service_type;
					                  query_response['protocol_id'] = pdu.protocol_id;
					                  query_response['priority_flag'] = pdu.priority_flag;
					                  query_response['message_status'] = parse_deliver_text_json.stat;
					                  query_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
					                  query_response['sub'] = parse_deliver_text_json.sub;
					                  query_response['message_err'] = parse_deliver_text_json.err;
					                  query_response['deliver_sm_data'] = pdu;
					                  query_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
					                  query_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
					                  query_response['updated_at'] = new Date().toLocaleString('en-US', {
					                    timeZone: 'Asia/Calcutta'
					                  });
					              
					                 session.deliver_sm_resp({ sequence_number: pdu.sequence_number });

					                /* Update - Message Status & Delivery Response - Start */
					                    updatingMessageCollection({'message_id':msg_id},delivery_response,sms_logs);
					                /* Update - Message Status & Delivery Response - Stop */

					            }
					            if(pdu.command == 'deliver_sm') {

					/*
					                console.log('Delivery pdu');
					                console.log(pdu);

					                console.log('Delivery - pdu response');
					                console.log(pdu.response());*/



					                var delivery_response = {};

					                var fromNumber = pdu.source_addr.toString();
					                var toNumber = pdu.destination_addr.toString();

					                var text = '';
					                var receipted_message_id = '';
					                var user_message_reference ='';

					                if (pdu.short_message && pdu.short_message.message) {
					                    text = pdu.short_message.message;
					                    receipted_message_id = pdu.short_message.message_payload;
					                    user_message_reference=pdu.short_message.user_message_reference;
					                }


					                var parse_deliver_text_str = text.toString(); 
					                var receipted_id = receipted_message_id; 

					                const querystring = require('querystring');    
					                var parse_deliver_text_json = querystring.parse(parse_deliver_text_str, ' ', ':', {decodeURIComponent: s =>  s.trim()});

					                var parse_test_json = querystring.parse(receipted_id, ' ', ':', {decodeURIComponent: s =>  s.trim()});

					                  var msg_id = parse_deliver_text_json.id;
					                  delivery_response['service_type'] = pdu.service_type;
					                  delivery_response['protocol_id'] = pdu.protocol_id;
					                  delivery_response['priority_flag'] = pdu.priority_flag;
					                  delivery_response['message_status'] = parse_deliver_text_json.stat;
					                  delivery_response['message_delivered_code'] = parse_deliver_text_json.dlvrd;
					                  delivery_response['sub'] = parse_deliver_text_json.sub;
					                  delivery_response['message_err'] = parse_deliver_text_json.err;
					                  delivery_response['deliver_sm_data'] = pdu;
					                  delivery_response['smpp_submit_date'] = parse_deliver_text_json.date[0];
					                  delivery_response['smpp_delivered_date'] = parse_deliver_text_json.date[1];
					                  delivery_response['updated_at'] = new Date().toLocaleString('en-US', {
					                    timeZone: 'Asia/Calcutta'
					                  });
					              
					                session.deliver_sm_resp({ sequence_number: pdu.sequence_number });

					                /* Update - Message Status & Delivery Response - Start */
					                    updatingMessageCollection({'message_id':msg_id},delivery_response,sms_logs);
					                /* Update - Message Status & Delivery Response - Stop */

					            }
					        });

					        /* Second Session - End */



					        session.on('close', function(){
					            console.log('SMPP CLOSE');
					           /* if(didConnect) {
					              connectSMPP(session);
					            }*/
					        });

					        session.on('enquire_link', function(pdu) {
					            console.log('enquire_link')
					            session.send(pdu.response());
					        });

					        session.on('unbind', function(pdu) {
					            console.log('unbind')
					            session.send(pdu.response());
					            session.close();
					        });

					        session.on('error', function(error){
					            console.log('smpp error', error)
					            didConnect = false;
					        }); 


					     });
		        	
					})();

	            }

	            await client.close();
			})();           	
       
	});

	/* Every second Fetching the record 200 from datatabase & SMS Sending 200 TPS - (Stop) */


});


async function updatingMessageCollection(find_query,update_data,sms_logs){

    sms_logs.findOneAndUpdate(
      find_query,
      { 
        $set:update_data
      });

}


module.exports = router;