const express                     = require('express');
var mongoose                      = require('mongoose');
var MongoClient                   = require('mongodb').MongoClient;
var autoIncrement                 = require("mongodb-autoincrement");
var nanoid                        = require('nanoid');
var constants                     = require('./../config/constants');
var pagination_helper             = require('./../helpers/create_pagination');
var functions_handler             = require('./../helpers/functions_handler'); 

const router                      = express.Router();
const { ensureAuthenticated, roleAuthenticated } = require('../config/auth');
var XLSX                          = require('xlsx');
// Dashboard
var await                         = require("await");
var async                         = require("async");
// DB Config
const db                          = require('../config/keys').mongoURI;
const db_options                  = require('../config/keys').db_options;
const database_name               = require('../config/keys').database_name;
var global_func                   = require('./../helpers/global_func');
var activityController            = require('./../controllers/activity_ctrl');
var ObjectId                      = require('mongodb').ObjectId;

/**
* route: /delete/:id
* method: GET
* description: Deleting draft campaign based on campaign id
* params: campaign_id
* return: success message
*/
router.get('/delete/:id', ensureAuthenticated, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db    = client.db(database_name);
    var camp_id = req.params.id;
    db.collection('campaign', function(err, collection) {
      collection.deleteOne({
        _id: camp_id.toString()
      });
    });
    res.json({'status' : 'success'});
    client.close();
  });

});//delete/:id

/**
* route: /list
* method: GET
* description: Loading campaign index page
* params: 
* return: listing data
*/
router.get('/list', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  res.render('campaign/index', {
    user                 : req.user,
    activity_logs        : req.activity_logs,
    activity_logs_count  : req.activity_logs_count
  });

});//list


/**
 * @request     : {AJAX} {POST} /get_campaign_list
 * @description : Listing all the campaign data
 * @return      : {table} campaign list
*/

router.post('/get_campaign_list', ensureAuthenticated, (req, res, next) => {
  MongoClient.connect(db, db_options).then(client => {
    (async () => {
      const db           = client.db(database_name);
      const collection   = db.collection('campaign');
      var camp_list      = [];
      var status         = 400;
      var message        = 'No data available';
      var perPage        = parseInt(req.body.per_page) || 10;
      var current_pageno = req.body.page_no;
      var skip_off_val   = (current_pageno - 1) * perPage; 
      var search_keyword = req.body.search_keyword;
      var find_json      = {};
      if (search_keyword) {
         find_json['$or'] = [ { 'name': new RegExp(search_keyword) }, { 'created_by': new RegExp(search_keyword) } ];
        
         var camp_list_count = await collection.find(find_json).count();
      } else {
        var camp_list_count = await collection.count();
      }
      console.log(find_json);
      camp_list        = await collection.find(find_json).sort({'date_added': -1}).skip(skip_off_val).limit(perPage).toArray();
      
      var camp_len     = camp_list.length;
      if (camp_len > 0) {
        status         = 200;
        message        = 'Data fetched successfully';
        var tbl_data_html = '';

        for(var i = 0; i < camp_len; i++) {
          var camp_status = '<td><i class="fa fa-check-square completeColor font18" aria-hidden="true"></i></td>';
          var camp_action = '<td> <a href="/campaign/view/'+ camp_list[i]._id  +'"><i class="fa fa-eye primaryColor font18 handPointer" aria-hidden="true"></i> </a></td>';

          if (camp_list[i].draft_status == 0) {
            camp_status = '<td><i class="fa fa-window-close incompleteColor font18" aria-hidden="true"></i></td>';
            camp_action = '<td><i class="fa fa-edit primaryColor font18 handPointer" aria-hidden="true"><a href="/draft/view/"'+ camp_list[i]._id  +'></i> <i class="fa fa-delete primaryColor font18 handPointer" aria-hidden="true"><a href="/draft/delete/"'+ camp_list[i]._id  +'></i></td>'
          }
          tbl_data_html += '<tr>';
          tbl_data_html += '<td>' + camp_list[i]._id + '</td>';
          tbl_data_html += '<td>' + camp_list[i].name + '</td>';
          tbl_data_html += camp_status;
          tbl_data_html += '<td>' + camp_list[i].created_by + '</td>';
          tbl_data_html += '<td>' + getDateUserFormat(camp_list[i].created_at, 'DD-MM-YYYY hh:mm A', 'M/D/YYYY h:mm:ss A') + '</td>';
          tbl_data_html += camp_action;
          tbl_data_html += '</tr>';
        } //end for
      } //end if
      
      var total_pages     = Math.ceil(camp_list_count / perPage);
      var pagination_data = pagination_helper.createPagination(total_pages, current_pageno, perPage, camp_list_count, skip_off_val);
      res.send({
        'status'       : status,
        'message'      : message,
        'table_data'   : tbl_data_html,
        'pagination_content': pagination_data
      });

    })();
  });
}); //get_campaign_list


/**
* route: /add-new-campaign
* method: GET
* description: Getting default dropdown values for campaign, category, profession, operator, source, country, mobile-numbers,  nationalities
* params: 
* return: listing data
*/
router.get('/add_campaign', ensureAuthenticated, roleAuthenticated,  activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);
    (async () => {
      let category_details;
      var campaign_label_details = [];

      const campaign_details     = await db.collection('campaign').find({'draft_status':1}).sort({_id:-1}).toArray();
      const category_list        = await db.collection('category').find().toArray();

      var countries_list         = [];
      var city                   = [];
      var district               = [];
      var state                  = [];
      var nationality            = [];
      var profession             = await db.collection('profession').find().toArray();
      var operator_name          = await db.collection('operator').find().toArray();
      var source                 = await db.collection('source').find().toArray();
      //get countries data
      const countries_data       = await db.collection('country').find().sort({'country_name':1}).forEach(function (country_record) {
        //console.log(record);
        countries_list.push(country_record);
      });

      const category             = await db.collection('category').find({}).forEach(function (cat_record) {
        category_details         = cat_record;
      });

      var add_numbers_list       = [];
      const numbers_data         = await db.collection('add_mobile_numbers').find().forEach(function (num_record) {
        //console.log(record);
        add_numbers_list.push(num_record);
      });

      const counters             = await db.collection('counters').findOne({
        "_id": "campaign"
      });
      if (counters) {
          var camp_counter_id    = counters.seq + 1;
      } else {
          var camp_counter_id    = 1;
      }
      var d                      = new Date();

      var campaign_id            = camp_counter_id + nanoid(3);
                
      var campaignIndex          = campaign_id; // this will return the id of object inserted

      const nationality_arr      = await db.collection('nationalities').find().forEach(function(nationality_record) {
        nationality.push(nationality_record);
      });

      return res.render('campaign/add-new-campaign', {
        campaign_details,
        category_details,
        campaign_label_details,
        countries_list,
        category_list,
        city,
        district,
        state,
        nationality,
        profession,
        source,
        operator_name,
        user: req.user,
        activity_logs       : req.activity_logs,
        activity_logs_count : req.activity_logs_count,
        add_numbers_list
      });
      client.close();
    })();
  }); 
});//add-new-campaign

/**
* route: /draft/:id
* method: GET
* description: Getting saved data and default dropdown values for campaign, category, profession, operator, source, country, mobile-numbers,  nationalities
* params: campaign_id
* return: saved data
*/
router.get('/draft/:id', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db                     = client.db(database_name);
    (async () => {
      var campaignIndex          = req.params.id;
      var campaign_draft_details = await db.collection('campaign').findOne({
          '_id': campaignIndex
      });

      if (!campaign_draft_details) {
          res.render('404');
      } else {
        let category_details;
        var campaign_label_details = [];
        var countries_list         = [];
        const campaign_details     = await db.collection('campaign').find({'draft_status':1}).sort({_id:-1}).toArray();
        const category_list        = await db.collection('category').find().toArray();
        var city                   = [];
        var district               = [];
        var state                  = [];
        var nationality            = [];
        var profession             = await db.collection('profession').find().toArray();
        var operator_name          = await db.collection('operator').find().toArray();
        var source                 = await db.collection('source').find().toArray();

        //get countries data
        const countries_data       = await db.collection('country').find().sort({'country_name':1}).forEach(function(country_record) {
            countries_list.push(country_record);
        });

        var add_numbers_list       = [];
        const numbers_data         = await db.collection('add_mobile_numbers').find().forEach(function(num_record) {
          //console.log(record);
          add_numbers_list.push(num_record);
        });

        var nationality            = [];
        //get nationality data
        const nationality_arr      = await db.collection('nationalities').find().forEach(function(nationality_record) {
          nationality.push(nationality_record);
        });

        var smpp_list              = [];
        //get countries data
        const smpp_data            = await db.collection('smpp_details').find().forEach(function(smpp_record) {
            smpp_list.push(smpp_record);
        });

        if (campaign_draft_details.draft_segments.filters) {
          if ('filter_campaign_ids' in campaign_draft_details.draft_segments.filters && campaign_draft_details.draft_segments.filters.filter_campaign_ids !== null) {
            var filter_campaign_ids  = campaign_draft_details.draft_segments.filters.filter_campaign_ids;
            var filter_content_label_ids  = [];
            for (i = 1; i <= Object.keys(filter_campaign_ids).length; i++) {
             
              var filter_camp_id = filter_campaign_ids['filter_campaign_id_' + i];
              var filter_content_lbl_name = 'filter_content_label_id_' + i;

              if (Array.isArray(filter_camp_id)) {
                var filter_camps_id     = filter_camp_id.map(function(i_campaign_id) {
                  return parseInt(i_campaign_id, 10);
                });


                var get_content_dropdown_details = await db.collection('campaign_content_labels').find({
                  'campaign_id': {
                    $in: filter_camps_id
                  }
                }).sort({_id:-1}).toArray();
                
                filter_content_label_ids.push({'filter_content_label_id' : get_content_dropdown_details});
              } else {
                var get_content_dropdown_details = await db.collection('campaign_content_labels').find({
                  'campaign_id': parseInt(filter_camp_id)
                }).sort({_id:-1}).toArray();
                filter_content_label_ids.push({'filter_content_label_id' : get_content_dropdown_details});
              }
              
            } //end for
            
          } else {
            var filter_campaign_id               = [];
            var get_content_dropdown_details     = [];
            var filter_content_label_id          = [];
          }

              var city                              = [];
              var district                          = [];
              var state                             = [];
             
              /*state */

              var filter_state_id                   = [];
              var state_country_dt                  = [];
              if ('country' in campaign_draft_details.draft_segments.filter_search) {
                var filter_country_id               = campaign_draft_details.draft_segments.filter_search.country; 
                //if ('state' in campaign_draft_details.draft_segments.filter_search) {

                var state_condition = {
                  'country_id': {
                    $in: filter_country_id
                  }
                }; 
                var filter_state_id                 = campaign_draft_details.draft_segments.filter_search.state;

                const get_state_country_data        = await db.collection('state').aggregate([
                  {
                    $match: state_condition
                  },
                  {                     
                    $lookup  :  {
                       from         : "country",
                       localField   : "country_id",
                       foreignField : "country_id",
                       as           : "state_country_data"
                     }
                  },
                 {
                    $project : { 
                      state_id      : "$state_id",
                      state_name    : "$state_name",
                      country_id    : "$country_id",
                      state_data    : { $arrayElemAt : [ "$state_country_data", 0 ] }
                    }
                  }
                ]).forEach(function(state_record) {
                  state_country_dt.push(state_record); 
                });
              }

              /* Tree heirarchy structure */
              const tree_list = state_country_dt.reduce((r,{country_id, state_name, state_id, state_data}) =>{
                r[country_id] = r[country_id] || {country_id, state_data, state_data_arr:[]};
                r[country_id]["state_data_arr"].push({'state_name': state_name, 'state_id': state_id});
                return r;
              }, {});
              var  get_state_dropdown_details = Object.values(tree_list);
              /* tree hierarchy structure end */
  
            /*end state */
            /*city */
            var city_state_dt = [];
            var get_city_dropdown_details = [];
            if ('state' in campaign_draft_details.draft_segments.filter_search) {
              
              var filter_state_id = campaign_draft_details.draft_segments.filter_search.state; 
                //console.log(filter_country_id_data)
              //if ('city' in campaign_draft_details.draft_segments.filter_search) {
                var city_condition = {
                  'state_id': {
                    $in: filter_state_id
                  }
                }; 

                var filter_city_id = campaign_draft_details.draft_segments.filter_search.city;
                
                var city_state_dt = [];
                var get_city_data = await db.collection('city').aggregate([
                {
                  $match: city_condition
                },
               {
                   
                $lookup:
                   {
                     from: "state",
                     localField: "state_id",
                     foreignField: "state_id",
                     as: "city_state_data"
                   }
                },
                {
                  $project: {
                      
                    city_id: "$city_id",
                    city_name: "$city_name",
                    state_id: "$state_id",
                    city_data: { $arrayElemAt: [ "$city_state_data", 0 ] }
                     
                  }
                }
              ]).forEach(function(city_record) {
                city_state_dt.push(city_record); 
              });

            }
                  
                  /* Tree heirarchy structure */
                    const tree_city_list = city_state_dt.reduce((r,{state_id, city_name, city_id, city_data}) =>{
                      r[state_id] = r[state_id] || {state_id, city_data, city_data_arr:[]};
                      r[state_id]["city_data_arr"].push({'city_name': city_name, 'city_id': city_id});
                      return r;
                    }, {});

                    get_city_dropdown_details = Object.values(tree_city_list); 
                    /* tree hierarchy structure end */

                  //console.log(get_state_dropdown_details)
                /*end city */
                var smpp_id_val = campaign_draft_details.draft_segments;
                var get_sender_details = [];
                if ('smpp_id' in campaign_draft_details.draft_segments != 'undefined') {
                  var smpp_id = campaign_draft_details.draft_segments.smpp_id; 
                  if ('smpp_sender_name' in campaign_draft_details.draft_segments != 'undefined') {
                    var smpp_sender_name = campaign_draft_details.draft_segments.smpp_sender_name;
                    get_sender_details = await db.collection('smpp_details').findOne({
                        'smpp_id': smpp_id
                    });
                    
                  }  else {
                    get_sender_details = await db.collection('smpp_details').findOne({
                        'smpp_id': smpp_id
                    });
                  }
                }
                }
                if (get_sender_details) {
                  var smpp_sender_list = get_sender_details.sender_address;
                }


                if (campaign_draft_details) {

                    return res.render('campaign/draft', {
                        campaign_details,
                        category_details,
                        campaign_label_details,
                        countries_list,
                        category_list,
                        campaign_draft_details,
                        city,
                        district,
                        state,
                        nationality,
                        profession,
                        source,
                        operator_name,
                        user: req.user,
                        campaignIndex,
                        //get_content_dropdown_details,
                        get_state_dropdown_details,
                        get_city_dropdown_details,
                        activity_logs: req.activity_logs,
                        activity_logs_count: req.activity_logs_count,
                        smpp_list,
                        smpp_sender_list,
                        add_numbers_list,
                        filter_content_label_ids
                    });

                } 
              }
                client.close();

            })();
          });
});//draft/:id

/**
* route: /add-new-content/:id
* method: GET
* description: Getting default dropdown values for campaign, category, profession, operator, source, country, mobile-numbers,  nationalities
* params: campaign_id
* return: Fetched data
*/
router.get('/add-new-content/:id', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);

            (async () => {

                //let campaign_details;
                //let get_category_details;
                let category_details;
                var campaign_label_details = [];
                var category_list = [];
                var campaign_id = req.params.id;

                var get_campaign_details = await db.collection('campaign').findOne({
                    '_id': parseInt(campaign_id)
                }); 

                if (get_campaign_details) {
                /*console.log('get_campaign_details');
                console.log(get_campaign_details);

                */

                var campaign_label_details = await db.collection('campaign_content_labels').find({
                    'campaign_id': parseInt(campaign_id)
                }).sort({_id:-1}).toArray(); 

                var camp_lbl_ids_arr = campaign_label_details.map(function(data) { return data["_id"]; });

                const campaign_details     = await db.collection('campaign').find({'draft_status':1}).sort({_id:-1}).toArray();

                const cat_arr = await db.collection('category').find().forEach(function(record) {
                    category_list.push(record);
                });

                var get_category_details = await db.collection('category').findOne({
                    '_id': parseInt(get_campaign_details.category_id)
                });
                /*forEach(function(record) {
                                    category_list.push(record);
                                });*/

                /*console.log('campaign_label_details');
                console.log(campaign_label_details);


                console.log('category_details');
                console.log(category_details);
                return;*/

                var add_numbers_list = [];
                const numbers_data = await db.collection('add_mobile_numbers').find().forEach(function(num_record) {
                    add_numbers_list.push(num_record);
                });

                var countries_list = [];
                var city = [];
                var district = [];
                var state = [];
                var nationality = [];
                var profession = await db.collection('profession').find().toArray();
                var operator_name = await db.collection('operator').find().toArray();
                var source = await db.collection('source').find().toArray();
                /*if (!campaign_details) {
                    return res.render('404');
                } else {*/
                //get countries data
                const countries_data = await db.collection('country').find().sort({'country_name':1}).forEach(function(country_record) {
                    //console.log(record);
                    countries_list.push(country_record);
                });

                //get nationality data
                const nationality_arr = await db.collection('nationalities').find().forEach(function(nationality_record) {
                    nationality.push(nationality_record);
                });

                const get_latest_label_det = await db.collection('campaign_content_labels').find({
                                            campaign_id: parseInt(campaign_id)
                                            })
                                            .sort({ _id: -1 })
                                            .limit(1).toArray();
                      
                return res.render('campaign/add-new-content', {
                    campaign_details,
                    category_details,
                    get_category_details,
                    get_campaign_details,
                    campaign_label_details,
                    category_list,
                    countries_list,
                    city,
                    district,
                    state,
                    nationality,
                    profession,
                    source,
                    operator_name,
                    user: req.user,
                    campaign_id,
                    camp_lbl_ids_arr,
                    get_latest_label_det,
                    activity_logs: req.activity_logs,
                    activity_logs_count: req.activity_logs_count,
                    add_numbers_list

                });
              } else {
                return res.render('404');
              }
                /*   }
                 */
                client.close();
            })();


        });
});//add-new-content/:id


/**
* route: /copy-content/:id
* method: GET
* description: Getting default dropdown values for campaign, category, profession, operator, source, country, mobile-numbers,  nationalities
* params: campaign_id
* return: Fetched data
*/
router.get('/copy-content/:id', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);
    (async () => {

        var params         = req.params.id;  

        var campaign_id    = params.substr(0, params.indexOf('_')); 
        var countries_list = [];
        var campaign_index = params.lastIndexOf("_"); 
        var campaign_content_id = params.substr(campaign_index + 1);

        var get_campaign_details = await db.collection('campaign').findOne({
            '_id': parseInt(campaign_id)
        }); 
        
        var campaign_label_details = await db.collection('campaign_content_labels').find({
            '_id': parseInt(campaign_content_id)
        }).sort({_id:-1}).toArray(); 
        if (get_campaign_details && campaign_label_details.length > 0) {
        var camp_lbl_ids_arr = campaign_label_details.map(function(data) { return data["_id"]; });
       

        //console.log('campaign_id: '+campaign_id);
        //console.log('campaign_content_id: '+campaign_content_id);
        //get countries data
        const countries_data = await db.collection('country').find().sort({'country_name':1}).forEach(function(country_record) {
            //console.log(record);
            countries_list.push(country_record);
        });
        var add_numbers_list = [];
        const numbers_data   = await db.collection('add_mobile_numbers').find().forEach(function(num_record) {
            //console.log(record);
            add_numbers_list.push(num_record);
        });

        var get_campaign_label_details = await db.collection('campaign_content_labels').findOne({
            '_id': parseInt(campaign_content_id)
        });

        var campaign_details     = await db.collection('campaign').find({'draft_status':1}).sort({_id:-1}).toArray();

        var category_list        = await db.collection('category').find().toArray();

        var campaign_all_list    = await db.collection('campaign').find({'draft_status': 1}).sort({_id:-1}).toArray();

        var get_category_details = await db.collection('category').findOne({
            '_id': parseInt(get_campaign_details.category_id)
        });


        var filter_category_id   = get_campaign_label_details.segments.filter_category_id;

        if (Array.isArray(filter_category_id)) {
           
            var get_campaign_dropdown_details = await db.collection('campaign').find({
                // 'category_id': {
                //     $in: filter_category_id
                // },
               'draft_status':1
            }).sort({_id:-1}).toArray();

        } else {
         
            var get_campaign_dropdown_details = await db.collection('campaign').find({
                //'category_id': filter_category_id,//get_campaign_details.filter_category_id
                'draft_status':1
            }).sort({_id:-1}).toArray();

        }
         
        if ('filter_campaign_ids' in get_campaign_label_details.segments && get_campaign_label_details.segments.filter_campaign_ids !== null) {

            var filter_campaign_ids       = get_campaign_label_details.segments.filter_campaign_ids;
            var filter_content_label_ids  = [];

            for (i = 1; i <= Object.keys(filter_campaign_ids).length; i++) {
              var filter_camp_id = filter_campaign_ids['filter_campaign_id_' + i];
              var filter_content_lbl_name = 'filter_content_label_id_' + i;

              if (Array.isArray(filter_camp_id)) {
                var filter_camps_id     = filter_camp_id.map(function(i_campaign_id) {
                  return parseInt(i_campaign_id, 10);
                });

                var get_content_dropdown_details = await db.collection('campaign_content_labels').find({
                  'campaign_id': {
                    $in: filter_camps_id
                  }
                }).sort({_id:-1}).toArray();
                
                filter_content_label_ids.push({'filter_content_label_id' : get_content_dropdown_details});
              } else {
                var get_content_dropdown_details = await db.collection('campaign_content_labels').find({
                  'campaign_id': parseInt(filter_camp_id)
                }).sort({_id:-1}).toArray();
                filter_content_label_ids.push({'filter_content_label_id' : get_content_dropdown_details});
              }
             
            } //end for

        } else {
            var filter_campaign_id           = [];
            var get_content_dropdown_details = [];
            var filter_content_label_id      = [];
        }

        var city        = [];
        var district    = [];
        var state       = [];
        var nationality = [];

        /*state */

         var state_country_dt = [];
          var get_state_dropdown_details = [];

          if ('filter_search' in get_campaign_label_details.segments) {

          if ('country' in get_campaign_label_details.segments.filter_search) {

            var filter_country_id = get_campaign_label_details.segments.filter_search.country_id;  
              //console.log(filter_country_id_data)
            //if ('state' in get_campaign_label_details.segments.filter_search) {

              //var filter_state_id = get_campaign_label_details.segments.filter_search.state_id; 
              var state_condition = {
                'country_id': {
                  $in: filter_country_id
                }
              }; 
              var filter_state_id = get_campaign_label_details.segments.filter_search.state;

              const get_state_country_data = await db.collection('state').aggregate([
              {
                $match: state_condition
              },
             {
                 
               $lookup:
                 {
                   from: "country",
                   localField: "country_id",
                   foreignField: "country_id",
                   as: "state_country_data"
                 }
              },
               {
                  $project: {
                      
                    state_id: "$state_id",
                    state_name: "$state_name",
                    country_id: "$country_id",
                    state_data: { $arrayElemAt: [ "$state_country_data", 0 ] }
                     
                  }
                }
            ]).forEach(function(state_record) {
              state_country_dt.push(state_record); 
            });
           
            }

             /* Tree heirarchy structure */
            const tree_list = state_country_dt.reduce((r,{country_id, state_name, state_id, state_data}) =>{
              r[country_id] = r[country_id] || {country_id, state_data, state_data_arr:[]};
              r[country_id]["state_data_arr"].push({'state_name': state_name, 'state_id': state_id});
            
              return r;
            }, {});
           get_state_dropdown_details = Object.values(tree_list);
            /* tree hierarchy structure end */
            
              

          //}
        }
        /*end state */

        /*city */

         var city_state_dt = [];
          var get_city_dropdown_details = [];

          if ('filter_search' in get_campaign_label_details.segments) {

          if ('state' in get_campaign_label_details.segments.filter_search) {

            var filter_state_id = get_campaign_label_details.segments.filter_search.state_id; 
              //console.log(filter_country_id_data)
            var city_condition = {
                'state_id': {
                  $in: filter_state_id
                }
              }; 

              var filter_city_id = get_campaign_label_details.segments.filter_search.city;
              
              var city_state_dt = [];
              var get_city_data = await db.collection('city').aggregate([
              {
                $match: city_condition
              },
             {
                 
              $lookup:
                 {
                   from: "state",
                   localField: "state_id",
                   foreignField: "state_id",
                   as: "city_state_data"
                 }
              },
              {
                $project: {
                    
                  city_id: "$city_id",
                  city_name: "$city_name",
                  state_id: "$state_id",
                  city_data: { $arrayElemAt: [ "$city_state_data", 0 ] }
                   
                }
              }
            ]).forEach(function(city_record) {
              city_state_dt.push(city_record); 
            });

          }
          
          /* Tree heirarchy structure */
            const tree_city_list = city_state_dt.reduce((r,{state_id, city_name, city_id, city_data}) =>{
              r[state_id] = r[state_id] || {state_id, city_data, city_data_arr:[]};
              r[state_id]["city_data_arr"].push({'city_name': city_name, 'city_id': city_id});
              return r;
            }, {});

            get_city_dropdown_details = Object.values(tree_city_list); 
            /* tree hierarchy structure end */
        }
          //console.log(get_state_dropdown_details)
        /*end city */
        var profession = await db.collection('profession').find().toArray();
        var operator_name = await db.collection('operator').find().toArray();
        var source = await db.collection('source').find().toArray();

        //get nationality data
        const nationality_arr = await db.collection('nationalities').find().forEach(function(nationality_record) {
            nationality.push(nationality_record);
        });

        const category_details = await db.collection('category').find({}).toArray();
        var smpp_list = [];
         //get countries data
        const smpp_data = await db.collection('smpp_details').find().forEach(function(smpp_record) {
            smpp_list.push(smpp_record);
        });
        
        var segments = get_campaign_label_details.segments;

         var smpp_id_val = get_campaign_label_details.segments;
        var get_sender_details = [];
        if ('smpp_id' in get_campaign_label_details.segments != 'undefined') {
          var smpp_id = get_campaign_label_details.segments.smpp_id; 
          if ('smpp_sender_name' in get_campaign_label_details.segments != 'undefined') {
            var smpp_sender_name = get_campaign_label_details.segments.smpp_sender_name;
            get_sender_details = await db.collection('smpp_details').findOne({
                'smpp_id': smpp_id
            });
            
          }  else {

            get_sender_details = await db.collection('smpp_details').findOne({
                'smpp_id': smpp_id
            });
              
          }

          
        }
        if (get_sender_details) {
          var smpp_sender_list= get_sender_details.sender_address;
        }

                 
        return res.render('campaign/copy-content', {
            filter_campaign_id,
            get_content_dropdown_details,
            filter_content_label_id,
            campaign_details,
            category_details,
            get_campaign_details,
            category_list,
            campaign_all_list,
            get_category_details,
            countries_list,
            city,
            district,
            state,
            nationality,
            profession,
            source,
            operator_name,
            user: req.user,
            campaign_id,
            campaign_content_id,
            get_campaign_label_details,
            get_campaign_dropdown_details,
            get_state_dropdown_details,
            get_city_dropdown_details,
            segments,
            camp_lbl_ids_arr,
            activity_logs: req.activity_logs,
            activity_logs_count: req.activity_logs_count,
            smpp_list,
            smpp_sender_list,
            add_numbers_list,
            filter_content_label_ids
        });
      } else {
        return res.render('404');
      }

       client.close();
    })();
  });
});// copy-content/:id

/**
* route: /view/:id
* method: GET
* description: Getting campaign and content label details based on campaign id
* params: campaign_id
* return: camapign and campaign content label details
*/
router.get('/view/:id', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db        = client.db(database_name);
    var campaign_id = req.params.id;
    (async () => {

      var campaign_details = await db.collection('campaign').findOne({'_id': parseInt(campaign_id)});

      if (!campaign_details) {
        res.render('404');
      } 
      res.render('campaign/view', {
        user: req.user,
        campaign_id: campaign_id,
        activity_logs: req.activity_logs,
        activity_logs_count: req.activity_logs_count,
      });
      client.close();
    })();
  });
});// view/:id

/**
* route: /get_campaign_label_details
* method: POST
* description: Getting campaign label details based on campaign id and content label id
* params: campaign_id, campaign_label_id
* return: campaign label details
*/
router.post('/get_campaign_label_details', function(req, res, next) {

  MongoClient.connect(db, db_options).then(client => {
          const db = client.db(database_name);
          const campaign_content_labels = db.collection('campaign_content_labels');

          var campaign_id = req.body.campaign_id;
          var id = req.body.campaign_label_id;

          (async () => {

              var campaign_labels_details = [];

              const campaign_labels = await campaign_content_labels.find({
                  '_id': parseInt(id),
                  'campaign_id': parseInt(campaign_id)
              }).forEach(function(cam_label_record) {
                  campaign_labels_details = cam_label_record;
              });

              res.send(campaign_labels_details);
              client.close();
          })();

  });
});//get_campaign_label_details

/**
 * @request     : {AJAX} {POST} /get_label_count
 * @description : Listing all the campaign data
 * @return      : {table} campaign list
*/

router.post('/get_label_count', ensureAuthenticated, (req, res, next) => {
  MongoClient.connect(db, db_options).then(client => {
    const db               = client.db(database_name);
    const label_coll       = db.collection('campaign_content_labels');
    const click_coll       = db.collection('clicks');
    const cat_coll         = db.collection('category');
    const camp_coll        = db.collection('campaign');

    (async () => {
      var campaign_id        = parseInt(req.body.campaign_id);
      var total_sender_sum   = 0;
      var total_clicker_sum  = 0;
      var campaign_details   = await camp_coll.findOne({'_id': campaign_id});
      var cat_details        = await cat_coll.findOne({'_id': parseInt(campaign_details.category_id)});
      var total_contents     = await label_coll.find({'campaign_id' : campaign_id}).count();
      
      var total_sent_count   = await label_coll.aggregate([{ $match: {'campaign_id': campaign_id, 'draft_status' : 1}},{ $group: { _id : '$campaign_id', tot_sender : { $sum: "$total_sender" } }}]).toArray();
      
      var total_clic_count  = await click_coll.aggregate([{ $match: {'campaign_id': campaign_id}},
          { $group: { _id : '$campaign_id', tot_clicker : { $sum: "$count" } }
      }]).toArray(); 
      if (total_sent_count.length > 0) {
        total_sender_sum = total_sent_count[0].tot_sender;
      }

      if (total_clic_count.length > 0) {
        total_clicker_sum = total_clic_count[0].tot_clicker;
      }
      
      res.send({
        'status'              : 200,
        'total_sender_sum'    : total_sender_sum,
        'total_clicker_sum'   : total_clicker_sum,
        'total_contents'      : total_contents,
        'cat_name'            : cat_details.name,
        'camp_name'           : campaign_details.name
      });

      client.close();
    })();
  });
}); //get_campaign_list

/**
* route: /popup_content
* method: POST
* description: Getting model popup content 
* params: campaign_id, campaign_label_id
* return: content html
*/
router.post('/popup_content', function(req, res, next) {

    var type = req.body.type;
    var id   = req.body.id;

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const collection = db.collection('category');
            const campaign = db.collection('campaign');
            const campaign_content_labels = db.collection('campaign_content_labels');

            (async () => {

                var category_content = '';
                var update_category_content = '';
                var category_list = [];
                let campaign_data;
                let campaign_content_data;

                const user = await collection.find().forEach(function(record) {
                    category_list.push(record);
                });

                for (var i = 0; i < category_list.length; i++) {
                    category_content += '<option value="' + category_list[i]._id + '">' + category_list[i].name + '</option>';
                }

                if (type == 'add') {

                    var category_html_content = '<div class="modal-header"> <h5 class="modal-title" id="createCategoryModal">Create Campaign</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"> <form id="add_campaign" type="post"> <div id="notification"></div><div class="row"> <div class="col-12"> <div class="form-group"> <label class="col-sm-3 col-form-label">Category</label> <div class="col-sm-9"> <select class="form-control" name="category" id="category"> <option value="">---Select Category ---</option>' + category_content + '</select> </div></div><div class="form-group"> <label for="exampleInputEmail1">Campaign name</label> <input type="text" class="form-control" name="name" id="campaign_name" placeholder="Enter Campaign name"> </div><div class="form-group"> <label for="exampleInputEmail1">SMS Content</label> <textarea class="form-control" name="description" id="sms_content" rows="6" placeholder="Enter SMS Content"></textarea> </div><div class="form-group"> <label for="exampleInputEmail1">Destination URL</label> <input class="form-control" type="url" placeholder="Enter Destination URL" id="destination_url" name="destination_url"> </div><div class="form-group"> <label class="col-sm-3 col-form-label">Status</label> <div class="col-sm-9"> <select class="form-control" name="status" id="status"> <option value="">---Status ---</option> <option value="1">Active</option> <option value="0">De-active</option> </select> </div></div></div><div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> <button type="submit" class="btn btn-primary" id="save_campaign">Save</button> <img src="/public/images/loader_form.gif" class="img-responsive" id="loader" style="width: 25px;display:none;"/> </form> </div></div></div>';
                    res.send({
                        'popup_content': category_html_content
                    });

                } else {

                    const campaign_label = await campaign_content_labels.find({
                        '_id': parseInt(id)
                    }).forEach(function(campaign_content_record) {
                        campaign_content_data = campaign_content_record;
                    });
                   
                    var content_label_name = campaign_content_data.label_name;
                    var dest_url           = campaign_content_data.destination_url;
                    var sms_content        = campaign_content_data.segments.original_sms_content;
                    var status             = campaign_content_data.status;
                    res.send({
                      'status_code'    :200,
                      'label_name': content_label_name,
                      'content_label_id' : id,
                      'dest_url'  : dest_url,
                      'sms_content' : sms_content,
                      'status'      : status
                    });

                }

                client.close();
            })();

    });
});//popup_content

/**
* route: /update
* method: POST
* description: Updating data for camapign based on campaign id 
* params: campaign_id
* return: status code, message
*/
router.post('/update', ensureAuthenticated, (req, res) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const collection = db.collection('campaign_content_labels');
            var text_message = req.body.sms_content;
            var message = text_message.replace(/(\r\n|\n|\r)/gm, "");
            var remove_quotes_message = functions_handler.stripEndQuotes(message);
             //var add_space_message = functions_handler.addSpaceBeforeBrace(message);

            var data = {
                'label_name': req.body.label_name,
                'sms_content': remove_quotes_message,
                'destination_url': req.body.destination_url,
                'status': req.body.status,
                'updated_at': new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                })
            };

            var id = req.body.content_label_id;

            collection.updateOne({
                        _id: parseInt(id)
                    }, // Filter
                    {
                        $set: data
                    }, // Update
                    {
                        upsert: false
                    }
                )
                .then((obj) => {
                    res.send({
                        'msg': 'success',
                        'status': 200
                    });
                })
                .catch((err) => {
                    res.send({
                        'msg': err,
                        'status': 400
                    });
                });

            client.close();

        });
});//update


router.post('/popup_label_content', function(req, res, next) {

    var type = req.body.type;
    var campaign_id = req.body.campaign_id;


    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const collection = db.collection('campaign_content_labels');

            (async () => {

                var label_content = '';

                if (type == 'add') {

                    var label_content = '<div class="modal-header"> <h5 class="modal-title" id="createCategoryModal">Create Content Label</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"> <form id="add_campaign" type="post"> <div id="notification"></div> <input type="hidden" value="' + campaign_id + '" id="campaign_id"> <div class="row"> <div class="col-12"> <div class="form-group"> <label for="exampleInputEmail1">Label name</label> <input type="text" class="form-control" name="name" id="campaign_label_name" placeholder="Enter Campaign Label name"> </div><div class="form-group"> <label for="exampleInputEmail1">SMS Content</label> <textarea class="form-control" name="description" id="sms_content" rows="6" placeholder="Enter SMS Content"></textarea> </div><div class="form-group"> <label for="exampleInputEmail1">Destination URL</label> <input class="form-control" type="url" placeholder="Enter Destination URL" id="destination_url" name="destination_url"> </div><div class="form-group"> <label class="col-sm-3 col-form-label">Status</label> <div class="col-sm-9"> <select class="form-control" name="status" id="status"> <option value="">---Status ---</option> <option value="1">Active</option> <option value="0">De-active</option> </select> </div></div></div><div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> <button type="submit" class="btn btn-primary" id="save_campaign">Save</button> <img src="/public/images/loader_form.gif" class="img-responsive" id="loader" style="width: 25px;display:none;"/> </form> </div></div></div>';
                    res.send({
                        'popup_label_content': label_content
                    });

                } else {


                }

                client.close();
            })();

        });
});//popup_label_content

router.post('/add', ensureAuthenticated, (req, res) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const campaign_collection = db.collection('campaign');
            const campaign_content_labels = db.collection('campaign_content_labels');


            autoIncrement.getNextSequence(db, 'campaign', function(err, campaignIndex) {

                var data = {
                    '_id': campaignIndex,
                    'category_id': req.body.category_id,
                    'name': req.body.campaign_name,
                    'sms_content': req.body.sms_content,
                    'destination_url': req.body.destination_url,
                    'status': req.body.status,
                    'created_by':req.user.name,
                    'created_at': new Date(),
                    'local_time': new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Calcutta'
                    }),
                };

                campaign_collection.insert(data);

                autoIncrement.getNextSequence(db, 'campaign_content_labels', function(err, labelIndex) {

                    var url_shortcode = nanoid(3);
                    var label_data = {
                        '_id': labelIndex,
                        'url_shortcode': url_shortcode,
                        'campaign_id': campaignIndex,
                        'label_name': 'Initial Content',
                        'sms_content': req.body.sms_content,
                        'destination_url': req.body.destination_url,
                        'status': req.body.status,
                        'created_by':req.user.name,
                        'created_at': new Date().toLocaleString('en-US', {
                            timeZone: 'Asia/Calcutta'
                        })
                    };

                    campaign_content_labels.insert(label_data);

                });

            });

            res.send({
                'msg': 'success',
                'status': 200
            });

            client.close();
        });
});//add

router.post('/content_label_add', ensureAuthenticated, (req, res) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const campaign_content_labels = db.collection('campaign_content_labels');

            autoIncrement.getNextSequence(db, 'campaign_content_labels', function(err, campaignIndex) {

                var url_shortcode = nanoid(2);

                var data = {
                    '_id': campaignIndex,
                    'campaign_id': parseInt(req.body.campaign_id),
                    'label_name': req.body.campaign_label_name,
                    'sms_content': req.body.sms_content,
                    'destination_url': req.body.destination_url,
                    'status': req.body.status,
                    'url_shortcode': url_shortcode,
                    'created_by':req.user.name,                
                    'created_at': new Date().toLocaleString('en-US', {
                        timeZone: 'Asia/Calcutta'
                    })

                };

                campaign_content_labels.insert(data)
                    .catch((error) => {
                        //console.log('error.message');
                    });

            });


            res.send({
                'msg': 'success',
                'status': 200
            });

            client.close();
        });

});//content_label_add

router.post('/update', ensureAuthenticated, (req, res) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const collection = db.collection('campaign');

            var data = {
                'name': req.body.cat_name,
                'description': req.body.description,
                'status': req.body.status,
                'updated_at': new Date(Date.now()).toISOString()
            };

            var id = req.body.category_id;

            collection.updateOne({
                        _id: parseInt(id)
                    }, // Filter
                    {
                        $set: data
                    }, // Update
                    {
                        upsert: false
                    }
                )
                .then((obj) => {
                    res.send({
                        'msg': 'success',
                        'status': 200
                    });
                })
                .catch((err) => {
                    res.send({
                        'msg': err,
                        'status': 400
                    });
                });


            client.close();

        });

});//update


/**
* route: /campaign_label_list
* method: POST
* description: Fetching campaignwise list based on campaign id and dynamic pagination 
* params: campaign_id
* return: pagination, content html 
*/
router.post('/campaign_label_list', (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db                 = client.db(database_name);
    (async () => { 
      var ISODate            = require("isodate");
      var campaign_id        = parseInt(req.body.campaign_id);
      var perPage            = constants.PER_PAGE_LIST_COUNT || 1;
      var current_pageno     = parseInt(req.body.page_no);
      var skip_off_val       = parseInt((current_pageno - 1) * perPage);
      var status             = 400;
      var message            = 'No data available';
      var campaign_details   = await db.collection('campaign').findOne({'_id': parseInt(campaign_id)});

      var campaign_find_json = {'campaign_id' : campaign_id}
      /***************campaign with from, to date ********/
      var lbl_from_date      = new Date(req.body.from_date);
      var lbl_to_date        = new Date(req.body.to_date);

      if (isValidDate(lbl_from_date) && isValidDate(lbl_to_date)) {  
        lbl_from_date.setHours(0, 0, 0, 0);
        var add_one_day_lbl_to_date      = new Date(lbl_to_date.getTime() + 1 * 24 * 60 * 60000);
        campaign_find_json['date_added'] =  {
          $gt   :  ISODate(lbl_from_date),
          $lte  : ISODate(add_one_day_lbl_to_date)
        };
      } //end if

      /**************end campaign with from, to date ******/
      var total_sender_sum  = 0;
      var total_clicker_sum = 0;

      if (!campaign_details) {
        res.render('404');
      } else {

        var current     = req.body.page_no;
        var campaign_label_details_count = await db.collection('campaign_content_labels').find(campaign_find_json).count();

        var campaign_label_details      = await db.collection('campaign_content_labels').find(campaign_find_json).skip(skip_off_val).limit(perPage).sort({'_id': -1}).toArray();
        var lbl_det_len           = campaign_label_details.length;
        content_html    = '';

        if (lbl_det_len > 0) {
          status             = 200;
          message            = 'Data fetched successfully';
          for (var i = 0; i < lbl_det_len; i++) {

              var content_label_clicks = await db.collection('clicks').find({
                  'content_label_id': parseInt(campaign_label_details[i]._id)
              }).count();

              var gross_content_clicks = 0;
              var gross_clicks = await db.collection('clicks').find({
                  'content_label_id': parseInt(campaign_label_details[i]._id)
              }).forEach(function(gross_click_no) {
                  gross_content_clicks += gross_click_no.gross_clicks;
              });

             
              var s_no = i+1;
              var label_name = campaign_label_details[i].label_name;
              var camp_content_label_id = campaign_id + '_' + campaign_label_details[i]._id;
              var camp_cont_link  = '/campaign/copy-content/' + camp_content_label_id;
              var draft_text_html = '';
              if (campaign_label_details[i].draft_status === 0) {
                var segment = campaign_label_details[i].draft_segments;
                camp_content_label_id = campaign_id + '/' + campaign_label_details[i]._id;
                camp_cont_link  = '/campaign/draft/' + camp_content_label_id;
                var category_id  = segment.filters.filter_category_id;
                var copy_link_html = '<i class="fa fa-eye primaryColor font18 handPointer  handPointer " aria-hidden="true"><a href="' + camp_cont_link + '" class="copy_campaign"  id="' + campaign_label_details[i]._id + '" target="_blank" title="Content Draft"></a></i>&nbsp;<i class="fa fa-eye primaryColor font18 handPointer  handPointer " aria-hidden="true"><a href=JavaScript:deleteDraftByID("'+  campaign_id + '","' + campaign_label_details[i]._id + '"); class="delete_draft" id="<%= campaign[i]._id %>" title="Delete"></a></i>';
                draft_text_html = '<br><span style="color:red;font-weight:bold;font-size: 10px;">(Draft)</span>';
              } else {
                var segment     = campaign_label_details[i].segments;
                var category_id = segment.filter_category_id;
                var copy_link_html = '<i class="fa fa-copy primaryColor font18 handPointer  handPointer " aria-hidden="true"><a href="' + camp_cont_link + '" class="copy_campaign"  id="' + campaign_label_details[i]._id + '" target="_blank" title="Copy"></a></i>&nbsp;<i class="fa fa-edit primaryColor font18 handPointer  update_campaign" data-toggle="modal" data-target="#updateLabelModal" aria-hidden="true" id="' + campaign_label_details[i]._id + '" title="Edit"></i>';
              }
              var sms_content = segment.original_sms_content;
              var destination_url = campaign_label_details[i].destination_url;
              var url_shortcode = campaign_label_details[i].url_shortcode;
              var total_clicker = parseInt(content_label_clicks);
              var total_sender = parseInt(campaign_label_details[i].total_sender);
              var created_by = campaign_label_details[i].created_by;
              var schedule_status =  campaign_label_details[i].schedule_status;

              total_sender_sum += total_sender;
              total_clicker_sum += total_clicker;

              //var sent_date = date;
              if (total_clicker == 0 && total_sender == 0)
                  var ctr_percentage = 0;
              else
                  var ctr_percentage = (total_clicker / total_sender) * 100;

              var get_category_name = [];

              

              var category_id = category_id.toString();

              var cat_arr_chk = category_id.split(",").map(Number);

              //console.log(cat_arr_chk);
              
              if(cat_arr_chk.includes(0)){
                  get_category_name.push('Others');
              }

              const content_tbl = await db.collection('category').find({
                  '_id': {
                      '$in': category_id.split(",").map(Number)
                  }
              }).forEach(function(category_details) {
                  get_category_name.push(category_details.name);
              });

              var content_category_name = get_category_name.toString();
              if (content_category_name) {
                  var cat_name = content_category_name;
              } else {
                  var cat_name = 'Others';
              }

              var total_contacts_val = parseInt(segment.total_contacts);
              var smpp_id = segment.smpp_id;
              var sender_id = segment.sender_id;

              var min_prev_record = parseInt(segment.minRecord);

              if (!isInValid(min_prev_record)) {
                
                var min_rec_val = min_prev_record;
                if (min_rec_val == 0) {
                  var min_rec_val = 1;
                }
              } else {

                var min_rec_val = 1;
              }
              
              var max_prev_record = parseInt(segment.maxRecord); 

              if (!isInValid(max_prev_record)) {
                var max_rec_val = max_prev_record;
                if (max_rec_val == 0) {
                  var max_rec_val = 1;
                }
              } else {
                var max_rec_val = total_sender;
              }

              if (total_sender == 0) {
                min_rec_val    = 0;
                max_rec_val    = 0;
              }
              if(schedule_status=='0'){
                  var schdle_bg_color='#f3e97ad9';
                  var schedule_date_and_time = segment.schedule_date_and_time;
                  var schedule_icon='<i class="mdi mdi-calendar-clock re_schedule" data-name="mdi mdi-calendar-clock" style="font-size: 22px;color: #0088cc;cursor: pointer;" title="Re-Schedule" onClick="reSchedule('+campaign_id+','+campaign_label_details[i]._id+')"></i>&nbsp;&nbsp;<i class="mdi mdi-delete schedule_delete" data-name="mdi-delete" title="Delete Schedule" style="font-size:22px;color: var(--red);cursor: pointer;" onClick="cancelSchedule('+campaign_id+','+campaign_label_details[i]._id+')"></i>';
                  var schedule_date_time = '<br><blink><small style="font-size: 12px;font-weight: bold;cursor: pointer;"><span style="color: #08c;font-weight: bold;">Schedule Date: </span>'+schedule_date_and_time+'</small></blink><br>';
              }else if(schedule_status=='2'){

                  var schedule_canceled_date = campaign_label_details[i].schedule_canceled_date;
                  var schdle_bg_color='#ffa7b9';
                  var schedule_icon='<i class="mdi mdi-calendar-clock" data-name="mdi mdi-calendar-clock" style="font-size: 22px;color: #0088cc;">';
                  var schedule_date_time = '<br><blink><small style="font-size: 12px;font-weight: bold;cursor: pointer;"><span style="color: #08c;font-weight: bold;">Schedule Cancelled Date: </span>'+schedule_canceled_date+'</small></blink><br>';
              }else{
                  var schdle_bg_color='#fff';
                  var schedule_icon='';
                  var schedule_date_time = '';
              }

              content_html += '<tr style="background:'+schdle_bg_color+'">';
              content_html += '<td>' + s_no + draft_text_html + '</td>';
              content_html += '<td id="campaign_label_info"><b style="text-decoration: underline;cursor: pointer;" data-toggle="modal"  data-target="#showContentLabelModal">'+ label_name + '</b><br/><small style="font-size: 85%">(<span style="color: #E91E63;font-weight: bold;">Filter Category: </span>' + cat_name + ')</small>'+schedule_date_time+schedule_icon+'</td>';
              content_html += '<td style="display:none;">' + sms_content + '</td>';
              content_html += '<td style="display:none;">' + destination_url + '</td>';

              content_html += '<td><a rel="_editable" data-value="' + sms_content + '">Content</a></td>';
              content_html += '<td><a rel="_editable" data-value="' + destination_url + '">URL</a></td>';
              content_html += '<td>' + url_shortcode + '</td>';

              if(typeof constants.SMPP_NAME[smpp_id]!=='undefined')
              {
                  var smpp_label = getSMPPLabelName(constants.SMPP_NAME[smpp_id]);
              }else{
                  var smpp_label = '<span class="badge badge-danger">Export</span>';
              }

              if(typeof sender_id=='undefined'){
                  var sender_id = '-';
              }

              var filter_topper_selc   = segment.filter_topper; 
              
              content_html += '<td>' + smpp_label + '</td>';
              content_html += '<td>' + sender_id + '</td>';
              content_html += '<td>' + total_sender + '</td>';
              content_html += '<td>' + total_clicker + '</td>';
              content_html += '<td>' + gross_content_clicks + '</td>';
              content_html += '<td>' + ctr_percentage.toFixed(2) + '%</td>';
              content_html += '<td>' + min_rec_val + '</td>';
              content_html += '<td>' + max_rec_val + '</td>';
              content_html += '<td><a href="/report/sms_log_report?campaign_id='+campaign_id+'&content_label_id='+ campaign_label_details[i]._id +'" style="text-decoration: underline;" target="_blank">Click here</a></td>';
               content_html += '<td style="display:none;">/report/sms_log_report?campaign_id='+campaign_id+'&content_label_id='+campaign_label_details[i]._id+'</td>';
               var sent_date = getDateUserFormat(campaign_label_details[i].created_at, 'DD-MM-YYYY hh:mm A', 'M/D/YYYY h:mm:ss A');
               content_html += '<td class="hide_content">' + sent_date + '</td>'; 
               content_html += '<td class="hide_content">' + campaign_label_details[i].created_by + '</td>'; 
              if (segment.hasOwnProperty('is_past_camp_data')) { 
                if (filter_topper_selc == 'sender') { 
                  content_html += '<td>';
                content_html += '<a href="' + camp_cont_link + '" class="copy_campaign"  id="' + campaign_label_details[i]._id + '" target="_blank" title="Copy"><i class="mdi mdi-content-copy"></i></a>&nbsp;<a href="#" class="update_campaign" data-toggle="modal" data-target="#updateLabelModal" id="' + campaign_label_details[i]._id + '" title="Edit"><i class="mdi mdi-pencil" data-name="mdi-pencil"></i></a>';
                content_html += '</td>'; 
              } } else {

                content_html += '<td>' + copy_link_html + '</td>'; 
              }
             
              content_html += '</tr>';
          } //endfor
        } //endif
       
        var total_pages = Math.ceil(campaign_label_details_count / perPage);

        var pagination_data = pagination_helper.createPagination(total_pages, current_pageno, perPage, campaign_label_details_count, skip_off_val);
        res.send({
          'status'       : status,
          'message'      : message,
          'pagination_content' : pagination_data,
          'table_data'   : content_html
        });

      client.close();
      } //else end
    })();
  });
});//campaignwise_list

/**
* route : /get_state_dropdown
* method: POST
* description: Fetching state data based on country_id
* params: (int) country_id
*/
router.post('/get_state_dropdown', ensureAuthenticated, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const country = db.collection('country');
            const state = db.collection('state');

            (async () => {

                var type = req.body.dropdown_type;
                var state_list = [];
                var state_list_html = '';
                var country_id = req.body.country_id;

                if (typeof country_id !== "undefined") {
                    var post_country_id = country_id;
                } else {
                    var post_country_id = [];
                }

                const state_tbl = await state.find({
                    'country_id': {
                        '$in': Object.values(post_country_id)
                    }
                }).sort({
                    'country_id': 1
                }).forEach(function(state_record) {
                    state_list.push(state_record);
                });
                var state_country_dt = [];
                const check_foreign = await state.aggregate([
                  {
                    $match: {
                      'country_id': {
                        '$in': Object.values(post_country_id)
                      }
                    }
                  },
                 {
                     
                   $lookup:
                     {
                       from: "country",
                       localField: "country_id",
                       foreignField: "country_id",
                       as: "state_country_data"
                     }
                  },
                   {
                      $project: {
                          
                        state_id: "$state_id",
                        state_name: "$state_name",
                        country_id: "$country_id",
                        state_data: { $arrayElemAt: [ "$state_country_data", 0 ] }
                         
                      }
                    }
                ]).forEach(function(state_record) {
                  state_country_dt.push(state_record); 
                }); console.log(state_country_dt);

                /* Tree heirarchy structure */
                const tree_list = state_country_dt.reduce((r,{state_name, country_id, state_id, state_data}) =>{

                  r[country_id] = r[country_id] || {state_data, children:[]};
                  r[country_id]["children"].push({"state_id": state_id, 'state_name': state_name})
                  return r;
                }, {})

                const final_tree_list = Object.values(tree_list);
                /* tree hierarchy structure end */

                if (final_tree_list) {
                    for (var i = 0; i < final_tree_list.length; i++) {
                      state_list_html +=  '<optgroup label="' +final_tree_list[i].state_data.country_name +'">';
                      for (var j = 0; j < final_tree_list[i].children.length; j++) {
                        state_list_html += '<option value="' + final_tree_list[i].children[j].state_id + '">' + final_tree_list[i].children[j].state_name + '</option>';
                      }
                      state_list_html += '</optgroup>';
                    } //for end
                } //if end

               
                res.send({
                    'msg': 'success',
                    'html': state_list_html
                });

                client.close();
            })(); //async end
        }); //mongoconnection end
}); // get state dropdown by country id

/**
* method: /get_city_dropdown
* description: Getting city dropdown based on state id
* params: state_id
* return: city data
*/
router.post('/get_city_dropdown', ensureAuthenticated, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const country = db.collection('country');
            const state = db.collection('state');
            const city = db.collection('city');

            (async () => {

                var type = req.body.dropdown_type;
                var city_list = [];
                var city_list_html = '';
                var state_id = req.body.state_id;

                if (typeof state_id !== "undefined") {
                    var post_state_id = state_id;
                } else {
                    var post_state_id = [];
                }
                // const city_tbl = await city.find({
                //     'state_id': {
                //         '$in': post_state_id
                //     }
                // }).sort({
                //     city: 1
                // }).forEach(function(city_record) {
                //     city_list.push(city_record);
                // });
                var city_state_dt = [];

                const check_foreign = await city.aggregate([
                  {
                    $match: {
                      'state_id': {
                        '$in': post_state_id
                      }
                    }
                  },
                 {
                     
                   $lookup:
                     {
                       from: "state",
                       localField: "state_id",
                       foreignField: "state_id",
                       as: "city_state_data"
                     }
                  },
                   {
                      $project: {
                          
                        city_id: "$city_id",
                        city_name: "$city_name",
                        state_id: "$state_id",
                        city_data: { $arrayElemAt: [ "$city_state_data", 0 ] }
                         
                      }
                    },

                ]).forEach(function(state_record) {
                  city_state_dt.push(state_record); 
                }); 

                /* Tree heirarchy structure */
                const tree_city_list = city_state_dt.reduce((r,{state_id, city_name, city_id, city_data}) =>{
                  r[state_id] = r[state_id] || {state_id, city_data, city_data_arr:[]};
                  r[state_id]["city_data_arr"].push({'city_name': city_name, 'city_id': city_id});
                  return r;
                }, {});

                city_list = Object.values(tree_city_list); 
                /* tree hierarchy structure end */

                if (city_list) {
                    for (var i = 0; i < city_list.length; i++) {
                      city_list_html +=  '<optgroup label="' +city_list[i].city_data.state_name +'">';
                      for (var j = 0; j < city_list[i].city_data_arr.length; j++) {
                        city_list_html += '<option value="' + city_list[i].city_data_arr[j].city_id + '">' + city_list[i].city_data_arr[j].city_name + '</option>';
                      }
                      city_list_html += '</optgroup>';
                    } //for end
                }
                res.send({
                    'msg': 'success',
                    'html': city_list_html
                });
                client.close();
            })(); // async end
        }); // mongo connection end
}); // get state dropdown by country id


/**
* method: /checkNumberExists
* description: Checking mobile number already exists or not in db
* params: mobile_number, type, id
* return: true/false
*/
router.post('/checkNumberExists', ensureAuthenticated, (req, res) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            
            const collection = db.collection('add_mobile_numbers');
            var type = req.body.check_type;

            if (type == 'update') {
              
              var mobile_number = req.body.mbl_num;
              var id = req.body.id;  
              var o_id = new ObjectId(id);
              var query = {'mobile_number': mobile_number, '_id': {$ne: o_id}};
            } else {
              
              var mobile_number = req.body.user_mbl_num;
              var query = {'mobile_number': mobile_number}
            }
             
            (async () => {
              
            var name_status = await collection.findOne(query); 
             
              if (name_status) {               
                res.send('false');     
                return false;
              } else {
                res.send('true');     
                return true;
              }
              await client.close();
            })();
    });
}); //checkNumberExists

/**
* method: /add_numbers
* description: Adding name and mobile numeber in db
* params: mobile_number, user_name
* return: status_code
*/
router.post('/add_numbers', ensureAuthenticated, (req, res) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            var user_name = req.body.user_name;
            var mobile_number = req.body.mobile_number;
            const collection = db.collection('add_mobile_numbers');
            var response = {};
            response['status'] = 200;
            response['message'] = 'success';
            response['name'] = user_name;
            response['mobile_number'] = mobile_number;
            (async () => {
              var data = {
                "name": user_name,
                "mobile_number": mobile_number
              }
              try {
                 var insert_number_data = await collection.insert(data);
                 
              } catch (e) {
               
                response['data'] = 'err';
                if (e.code === 11000) {
                  response['status'] = 400;
                  response['message'] = 'fail';
                  
                }
              } 
             
             client.close();
              res.send(response);
            })();

    });
});//add_numbers

/**
* method: /get_prev_rec
* description: Getting previous record data based on filters
* params: campaign_id, filter_category_id, filter_topper_name
* return: status_code
*/
router.post('/get_prev_rec', function(req, res, next) {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const campaign_content_labels = db.collection('campaign_content_labels');

            var campaign_id = req.body.campaign_id;
            var filter_category_id = req.body.filter_category_id;
            var filter_topper_name = req.body.filter_topper_name;

            (async () => {

                var campaign_labels_details = [];

                const prev_rec_data = await campaign_content_labels.find({'campaign_id': parseInt(campaign_id), 'segments.filter_category_id': filter_category_id, 'segments.filter_topper': filter_topper_name
                }).sort({ _id: -1 }).limit(1).toArray();

                res.send({
                  'status': 200,
                  'previous_record_data': prev_rec_data
                });
                client.close();
            })();

        });

});//get_prev_rec

/**
  route : /add_adv_post_url
  method: POST
  description: Fetching state data based on country_id
  params: (int) country_id
*/
router.post('/add_adv_post_url', ensureAuthenticated, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
      const db = client.db(database_name);
      var adv_url = req.body.adv_url;
      var campaign_id = req.body.campaign_id;
      var conv_cost = req.body.conv_cost;
      const campaign_collection = db.collection('campaign');

      (async () => {
       await campaign_collection.updateOne({'_id': parseInt(campaign_id)}, {$set:{'adv_postback_url': adv_url, 'conversion_cost':conv_cost, updated_at:new Date()}});
       client.close();
        res.send({
          'msg': 'success',
          'status': 200,
      });
      })();
    });
}); // add_adv_post_url


/**
* route: /get_adv_postback_url
* method: GET
* description: Getting advertisement postback url data
* params: campaign_id
* return: status_code, previous record data
*/
router.get('/get_adv_postback_url', function(req, res, next) {

    MongoClient.connect(db, db_options).then(client => {
      const db = client.db(database_name);
      const campaign = db.collection('campaign');

      (async () => {

          var campaign_id = req.query.campaign_id; 

          const prev_rec_data = await campaign.findOne({'_id': parseInt(campaign_id)});
          res.send({
            'status': 200,
            'previous_record_data': prev_rec_data
          });
          client.close();
      })();
  });

});//get_adv_postback_url


/**
* route: /export_content_labels
* method: POST
* description: Exporting content label details to xlsx format 
* params: campaign_id, from_date, to_date
* return: status_code, campaign content label list
*/
router.post('/export_content_labels', function(req, res, next) {

  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);
    const campaign_col = db.collection('campaign');
    const campaign_cont_col = db.collection('campaign_content_labels');
    const sms_logs_col = db.collection('sms_logs');

    (async () => {

      var campaign_id = parseInt(req.body.campaign_id); 
      var from_date   = req.body.from_date;
      var to_date     = req.body.to_date;
      var status_code = 400;
      var download_file_name = '';

      var campaign_details = await db.collection('campaign').findOne({
        '_id': campaign_id
      });

      var campaign_find_json = {
        'campaign_id': campaign_id
      }

      var sms_log_conditions = { "campaign_id": campaign_id, 'mode': { $in: ['1'] },
        $nor:[{
          'message_status': { $in: [/^SCHEDULE CANCELLED/] },
        }]};

      var deliverd_conditions = {
        "campaign_id": campaign_id,
        "message_err": { $in: [/^001/,/^000/] },
        'mode': { $in: ['1'] },
      };

      var un_deliverd_conditions = {
        "campaign_id": campaign_id,
        "message_err": { $in: [/^001/,/^000/] },
        'mode': { $in: ['1'] },
      };

      var dlr_pending_conditions = {
        "campaign_id": campaign_id,
         "message_status": { $in: [/^PENDING/] },
        'mode': { $in: ['1'] },
        "message_id" : {"$exists" : true }
      };
      
      var queue_conditions = {
        "campaign_id": campaign_id,
        "message_status": { $in: [/^SUBMIT/] },
        'mode': { $in: ['1'] }
      };

      var rejection_conditions = {
         "campaign_id": campaign_id,             
        "message_status": { $in: [/^PENDING/] },
        'mode': { $in: ['1'] },
        "message_id" : {"$exists" : false }
      };

      /***************campaign with from, to date ********/
        var ISODate = require("isodate");
        var lbl_from_date   = new Date(req.body.from_date); 
        
        var lbl_to_date     = new Date(req.body.to_date);
     
        if (isValidDate(lbl_from_date) && isValidDate(lbl_to_date)) {  
          lbl_from_date.setHours(0, 0, 0, 0);
          var add_one_day_lbl_to_date = new Date(lbl_to_date.getTime() + 1 * 24 * 60 * 60000);
          campaign_find_json['date_added'] = {
            $gt:  ISODate(lbl_from_date),
            $lte: ISODate(add_one_day_lbl_to_date)
          };

          sms_log_conditions['created_at'] = {
            $gt:  ISODate(lbl_from_date),
            $lte: ISODate(add_one_day_lbl_to_date)
          };

          deliverd_conditions['created_at'] = {
            $gt:  ISODate(lbl_from_date),
            $lte: ISODate(add_one_day_lbl_to_date)
          };

          un_deliverd_conditions['created_at'] = {
            $gt:  ISODate(lbl_from_date),
            $lte: ISODate(add_one_day_lbl_to_date)
          };

          dlr_pending_conditions['created_at'] = {
            $gt:  ISODate(lbl_from_date),
            $lte: ISODate(add_one_day_lbl_to_date)
          };

          queue_conditions['created_at'] = {
            $gt:  ISODate(lbl_from_date),
            $lte: ISODate(add_one_day_lbl_to_date)
          };

          rejection_conditions['created_at'] = {
            $gt:  ISODate(lbl_from_date),
            $lte: ISODate(add_one_day_lbl_to_date)
          };
        } //end if

        //console.log(campaign_find_json);
      /**************end campaign with from, to date ******/
      
      if (!campaign_details) {
        res.render('404');
      } else {
        var campaign_name = campaign_details.name;
        var export_camp_label_details = await db.collection('campaign_content_labels').find(campaign_find_json).sort({_id: -1}).toArray(); 

        // console.log('export_camp_label_details');
        // console.log(export_camp_label_details);
        
        if (export_camp_label_details.length > 0) {
          var camp_content_label_ids = [];
          var lbl_json_data = [];
          for (i = 0; i < export_camp_label_details.length; i++) {
           //console.log(export_camp_label_details);
           camp_content_label_ids.push(export_camp_label_details[i]._id);

           lbl_json_data.push( {'content_label_name': export_camp_label_details[i].label_name, 'total_sent': export_camp_label_details[i].total_sender , 'total_sms_log_sent_count':0, 'delivered_count': 0, 'queue_count': 0, 'un_delivered_count': 0, 'rejection_count': 0, 'sent_date': export_camp_label_details[i].created_at,});
          }//end for
          sms_log_conditions['campaign_content_label_id'] = {
            $in: camp_content_label_ids
          };
          
          deliverd_conditions['campaign_content_label_id'] = {
            $in: camp_content_label_ids
          };

          un_deliverd_conditions['campaign_content_label_id'] = {
            $in: camp_content_label_ids
          };

          dlr_pending_conditions['campaign_content_label_id'] = {
            $in: camp_content_label_ids
          };

          queue_conditions['campaign_content_label_id'] = {
            $in: camp_content_label_ids
          };

          rejection_conditions['campaign_content_label_id'] = {
            $in: camp_content_label_ids
          };
          //console.log(deliverd_conditions);console.log(un_deliverd_conditions);

          var total_sms_sent_logs_conds = await getSmsLogsContlblCount(sms_log_conditions, sms_logs_col); 
          /* total sms logs sent count */
          
          if (total_sms_sent_logs_conds.length > 0) {
            for (j = 0;j < total_sms_sent_logs_conds.length;j++) {

              lbl_json_data[j].total_sms_log_sent_count = total_sms_sent_logs_conds[j].count;
            } 
          }
          /* end total sms logs sent count */

          var cont_lbl_deliv_conds = await getSmsLogsContlblCount(deliverd_conditions, sms_logs_col); 

          /* delivered sms count */
          
          if (cont_lbl_deliv_conds.length > 0) {
            for (j = 0;j < cont_lbl_deliv_conds.length;j++) {
              lbl_json_data[j].delivered_count = cont_lbl_deliv_conds[j].count;
            } 
          }
          /* end delivered sms count */


          var cont_lbl_undeliv_conds = await getSmsLogsContlblCount(un_deliverd_conditions, sms_logs_col); 

          /* delivered sms count */
          
          if (cont_lbl_undeliv_conds.length > 0) {
            for (j = 0;j < cont_lbl_undeliv_conds.length;j++) {
              lbl_json_data[j].un_delivered_count = cont_lbl_undeliv_conds[j].count;
            } 
          }
          /* end delivered sms count */

          var cont_lbl_dlr_pending_conds = await getSmsLogsContlblCount(dlr_pending_conditions, sms_logs_col); 
          
          /* un delivered sms count */
          
          if (cont_lbl_dlr_pending_conds.length > 0) {
            for (j = 0;j < cont_lbl_dlr_pending_conds.length;j++) {
              lbl_json_data[j].un_delivered_count = cont_lbl_dlr_pending_conds[j].count;
            } 
          }
           /* end un delivered sms count */

          var cont_lbl_queue_conds = await getSmsLogsContlblCount(queue_conditions, sms_logs_col); 
          /* queue sms count */
         
          if (cont_lbl_queue_conds.length > 0) {
            for (j = 0;j < cont_lbl_queue_conds.length;j++) {
              lbl_json_data[j].queue_count = cont_lbl_queue_conds[j].count;
            } 
          }
          /* end queue sms count */


          var cont_lbl_rejection_conds = await getSmsLogsContlblCount(rejection_conditions, sms_logs_col); 

          /* queue sms count */
         
          if (cont_lbl_rejection_conds.length > 0) {
            for (j = 0;j < cont_lbl_rejection_conds.length;j++) {
              lbl_json_data[j].rejection_count = cont_lbl_rejection_conds[j].count;
            } 

          }
          /* end queue sms count */
          /* Excel downloading starts */
         
         
          


          var Excel = require('exceljs');
          const workbook = new Excel.Workbook({
            useStyles: true
          })

          const headers = [
            { key: 'content_label_name' },
            { key: 'total_sent' },
            { key: 'total_sms_log_sent_count' },
            { key: 'delivered_count' },
            { key: 'queue_count' },
            { key: 'un_delivered_count' },
            { key: 'rejection_count' },
            { key: 'sent_date' },
          ];

          var file_name = campaign_name + '_' + functions_handler.getCurrentDateTime() + '.xlsx';
          var file_path = './public/content_label_list/' + file_name;
          await workbook.xlsx.writeFile(file_path);

          const worksheet    = await workbook.addWorksheet('label_list');

          worksheet.mergeCells('A1', 'H2');
          worksheet.getCell('A1').value = campaign_name;
          worksheet.getCell('A1').font  = {
            name: 'Comic Sans MS',
            family: 4,
            size: 11,
            underline: true,
            bold: true
          };
          worksheet.getCell('A1').alignment = { vertical: 'middle', horizontal: 'center' };
          worksheet.getRow(2).values        = ['content_label_name', 'total_sent', 'total_sms_log_sent_count', 'delivered_count', 'queue_count', 'un_delivered_count', 'rejection_count', 'sent_date'];

          worksheet.state    = 'visible';
          worksheet.columns  = headers;
          worksheet.addRows(lbl_json_data);
          await workbook.xlsx.writeFile(`${file_path}`);      
          /* make the worksheet */
          // var ws = XLSX.utils.json_to_sheet(final_export_arr);
         
          // /* add to workbook */
          // var wb = XLSX.utils.book_new();

          // XLSX.utils.book_append_sheet(wb, ws, "label_list");

          // 
          // /* write workbook */
          // XLSX.writeFile(wb, file_path);

          /* Excel downloading ends */

          status_code = 200;
          download_file_name = file_path;
        }
        
      }  

      res.send({
        'status': status_code,
        'download_file_name': download_file_name
      });
      client.close();

    })();
  });

});//export_content_labels

async function getSmsLogsContlblCount(conditions, sms_log_collection) {

  var total_conditions_count = await sms_log_collection.aggregate([

  {
      $match:conditions   
  },

                
  // Group by year, month and day and get the count
  {
      $group: {
          _id: {
            campaign_content_label_id: "$campaign_content_label_id"
              
          },
          "count": {
              $sum: 1
          }
      }
  },
  {
    $sort: {
      campaign_content_label_id: -1
    }
  }
  ]).toArray();

  return total_conditions_count;
}


router.get('/delete_label_all_files', (req, res, next) => {

  const directory = 'public/content_label_list';
  var cron = require('node-cron');
 
cron.schedule('20 17 * * *', () => {
  fs.readdir(directory, (err, files) => {
    if (err) throw err;

    for (const file of files) {
      fs.unlink(path.join(directory, file), err => {
        if (err) throw err;
      });
    }
  });

  res.send({
    'status': 200,
    'message': 'upload temp files are deleted'
  });
});

  

});
module.exports = router;
