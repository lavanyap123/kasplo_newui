const express = require('express');
const router = express.Router();
const {
    ensureAuthenticated, roleAuthenticated
} = require('../config/auth');
var json2csv = require('json2csv'); //not working
var csv_export = require('csv-export');
const stringify = require('csv-stringify');
/*var autoIncrement = require("mongodb-autoincrement");*/

var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
// Dashboard
var await = require("await");
var async = require("async");
var Hashids = require('hashids');
// DB Config
const db = require('../config/keys').mongoURI;
const db_options = require('../config/keys').db_options;
const database_name = require('../config/keys').database_name;

var functions_handler = require('./../helpers/functions_handler');

const UAParser = require('ua-parser-js');
const request = require('request');
const useragent = require('express-useragent');

router.get('/', (req, res) => res.redirect('http://kasplo.com'));


router.get('/:short_id', (req, res, next) => {

    var short_id = req.params.short_id;

    var contact_unique_id = short_id.substr(0, short_id.indexOf('_'));

    var p_ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

    if (p_ip.substr(0, 7) == "::ffff:") {
        p_ip = p_ip.substr(7)
    }

    if (contact_unique_id == 'kasplo') {

        var campaign_short_code = short_id.substr(short_id.indexOf("_") + 1);

        //console.log('campaign_short_code='+campaign_short_code);

        MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            (async () => {

                const test_sms_logs_collection = db.collection('sms_logs');
                const server_logs_collection = db.collection('server_logs');
                var check_shortcode_details = await test_sms_logs_collection.findOne({
                    'url_shortcode': campaign_short_code.toString()
                });

                //console.log('check_shortcode_details');
                //console.log(check_shortcode_details);

                if (check_shortcode_details) {

                    db.collection('sms_logs').updateOne({
                        'url_shortcode': campaign_short_code.toString()
                    }, {
                        $inc: {
                            gross_clicks: 1
                        }
                    });

                    if (check_shortcode_details.destination_url) {
                        var destination_url = check_shortcode_details.destination_url;
                    } else {
                        var destination_url = 'http://kasplo.com/';
                    }
                    client.close();
                    res.redirect(destination_url);


                } else {

                    var server_logs_data = {
                        'short_id': short_id,
                        'contact_unique_id': contact_unique_id,
                        'campaign_short_code': campaign_short_code,
                        'count': 1,
                        'gross_clicks': 1,
                        'Action': 'SMS campaign',
                        'public_ip': p_ip,
                        'Code': 'Invalid Shortcode passing in URL (short_id: ' + short_id + ')',
                        'Message': 'Short Code is not found',
                        'created_at': new Date().toLocaleString('en-US', {
                            timeZone: 'Asia/Calcutta'
                        })
                    };

                    server_logs_collection.insert(server_logs_data)
                        .catch((error) => {
                            console.log('error.message');
                        });

                    client.close();
                    res.redirect('http://kasplo.com');

                }


            })(); // async end        
        });


    } else if (contact_unique_id) {
//console.log('2-contact_unique_id');
        var campaign_short_code = short_id.substr(short_id.indexOf("_") + 1);

        MongoClient.connect(db, db_options).then(client => {

            const db = client.db(database_name);

            (async () => {

                const clicks_collection = db.collection('clicks');
                const server_logs_collection = db.collection('server_logs');

                var contacts_details = await db.collection('contacts').findOne({
                    'unique_id': contact_unique_id
                }, {
                    projection: {
                        mobile_no: 1,
                        unique_id: 1,
                        _id: 0
                    }
                });

                //if (contacts_details) {

                    var campaign_label_details = await db.collection('campaign_content_labels').findOne({
                        'url_shortcode': campaign_short_code.toString()
                    }, {
                        projection: {
                            label_name: 1,
                            campaign_id: 1,
                            destination_url: 1,
                            'segments.smpp_id': 1
                        }
                    });
                    //console.log(campaign_label_details)
                    if (campaign_label_details) {
//console.log('1-campaign_label_details');
                        var campaign_details = await db.collection('campaign').findOne({
                            '_id': parseInt(campaign_label_details.campaign_id)
                        }, {
                            projection: {
                                name: 1,
                                category_id: 1
                            }
                        });

                        if (campaign_details) {
//console.log('2-campaign_details');
                            

                            var camp_sender_category_id = parseInt(campaign_details.category_id);

                            if (contacts_details) {
                                var mobile_no = contacts_details.mobile_no;

                                const contacts_movement_collection = db.collection('contacts');

                                var bulk = contacts_movement_collection.initializeUnorderedBulkOp();

                                if (camp_sender_category_id) {

                                    bulk.find({
                                        mobile_no: {
                                            $in: [mobile_no.toString()]
                                        },

                                        $nor: [{
                                            category_ids: {
                                                $elemMatch: {
                                                    $in: [camp_sender_category_id.toString()]
                                                }
                                            }
                                        }]

                                    }).update({
                                        $addToSet: {
                                            category_ids: camp_sender_category_id.toString()
                                        }
                                    }, {
                                        upsert: true
                                    });

                                }

                                bulk.execute(function(err, result) {

                                });
                            }

                            var category_details = await db.collection('category').findOne({
                                '_id': parseInt(campaign_details.category_id)
                            }, {
                                projection: {
                                    name: 1,
                                    _id: 0
                                }
                            });

                            /* Checking Unique Clicks or Gross Clicks */

                            if (contacts_details) {
                            var mobile_no = contacts_details.mobile_no;
                            var user_clicks = await db.collection('clicks').findOne({
                                'contact_no': mobile_no.toString(),
                                'content_label_id': parseInt(campaign_label_details._id),
                                'public_ip': p_ip.toString(),
                            });

                            if (user_clicks) {
//console.log('3-user_clicks');
                                /*  const user_clicks = await db.collection('clicks').findOne({'contact_no': mobile_no.toString(),'content_label_id':parseInt(campaign_label_details._id)});

                                  if(user_clicks)
                                  {*/

                                db.collection('clicks').updateOne({
                                    'contact_no': mobile_no.toString(),
                                    'content_label_id': parseInt(campaign_label_details._id)
                                }, {
                                    $inc: {
                                        gross_clicks: 1
                                    }
                                });

                                if (campaign_label_details.destination_url) {
                                    var destination_url = campaign_label_details.destination_url;
                                } else {
                                    var destination_url = 'http://kasplo.com/';
                                }

                                console.log('destination_url');
                                console.log(destination_url);

                                client.close();
                                res.redirect(destination_url);

                            } else {
//console.log('3-user_clicks - Else');
                                //autoIncrement.getNextSequence(db, 'clicks', function(err, clickAutoIndex) {
                                /* Checking browser details -  (start) */
                                //const source = req.headers['user-agent']; 
                                const source = [];
                                var browser_details = {};
                                if (functions_handler.checkArrayEmpty(source)) {
                                    const user_agent = useragent.parse(source);
                                    var hasFalseKeys = Object.keys(user_agent).forEach(function(key) {
                                        if (user_agent[key] != false && Object.keys(key).length != 0)
                                            browser_details[key] = user_agent[key];
                                    });
                                }

                                var campaign_label_name = {
                                    'label_name': campaign_label_details.label_name
                                };
                                var campaign_name = {
                                    'name': campaign_details.name
                                };
                                /* Checking browser details -  (stop) */

                                var click_data = {
                                    //'_id': clickAutoIndex,
                                    'category_id': parseInt(campaign_details.category_id),
                                    'category_details': category_details,
                                    'campaign_id': parseInt(campaign_label_details.campaign_id),
                                    'campaign_details': campaign_name,
                                    'content_label_id': parseInt(campaign_label_details._id),
                                    'contact_no': contacts_details.mobile_no,
                                    'count': 1,
                                    'gross_clicks': 1,
                                    'contacts_details': contacts_details,
                                    'campaign_label_details': campaign_label_name,
                                    'smpp_id': campaign_label_details.segments.smpp_id,
                                    'public_ip': p_ip,
                                    'location': '',
                                    'browser_information': browser_details,
                                    'created_at': new Date(),
                                    'click_date': new Date().toLocaleString('en-US', {
                                        timeZone: 'Asia/Calcutta'
                                    })
                                };

                                clicks_collection.insert(click_data)
                                    .catch((error) => {
                                        console.log('error.message');
                                    });

                                if (campaign_label_details.destination_url) {
                                    var destination_url = campaign_label_details.destination_url;
                                } else {
                                    var destination_url = 'http://kasplo.com/';
                                }
                                //console.log('destination_utl' + destination_url)
                                client.close();
                                res.redirect(destination_url);
                                // }); //CURL END

                            } // user clicks end
                         }else{
//console.log('contact wrong redirect');
                                if (campaign_label_details.destination_url) {
                                    var destination_url = campaign_label_details.destination_url;
                                } else {
                                    var destination_url = 'http://kasplo.com/';
                                }                            
//console.log(destination_url);
                                client.close();
                                res.redirect(destination_url);
                         }
                            //console.log('If- gross click updated');
                        } else {
//console.log('3-user_clicks - second else');
                            //console.log('3');
                            var server_logs_data = {
                                'short_id': short_id,
                                'contact_unique_id': contact_unique_id,
                                'campaign_short_code': campaign_short_code,
                                'count': 1,
                                'gross_clicks': 1,
                                'Action': 'SMS campaign',
                                'public_ip': p_ip,
                                'Code': 'Invalid Shortcode passing in URL (short_id: ' + short_id + ')',
                                'Message': 'Short Code is not found',
                                'created_at': new Date().toLocaleString('en-US', {
                                    timeZone: 'Asia/Calcutta'
                                })
                            };

                            server_logs_collection.insert(server_logs_data)
                                .catch((error) => {
                                    console.log('error.message');
                                });

                            client.close();
                            res.redirect('http://kasplo.com');
                        } //campaign details

                    } else {
console.log('3-user_clicks - third else');

                        var server_logs_data = {
                            'short_id': short_id,
                            'contact_unique_id': contact_unique_id,
                            'campaign_short_code': campaign_short_code,
                            'count': 1,
                            'Action': 'SMS campaign',
                            'public_ip': p_ip,
                            'Code': 'Invalid Shortcode passing in URL (short_id: ' + short_id + ')',
                            'Message': 'Short Code is not found',
                            'created_at': new Date().toLocaleString('en-US', {
                                timeZone: 'Asia/Calcutta'
                            })
                        };

                        server_logs_collection.insert(server_logs_data)
                            .catch((error) => {
                                console.log('error.message');
                            });
                        client.close();
                        res.redirect('http://kasplo.com');

                    } //campaign_label_details

               
            })(); // async end
        });

    } else {
console.log('3-user_clicks - finall else');
console.log('contact_unique_id');
        MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const server_logs_collection = db.collection('server_logs');

            var server_logs_data = {
                'short_id': short_id,
                'contact_unique_id': contact_unique_id,
                'campaign_short_code': '',
                'count': 1,
                'Action': 'SMS campaign',
                'Code': 'Invalid Shortcode passing in URL (short_id: ' + short_id + ')',
                'Message': 'Short Code is not found',
                'public_ip': p_ip,
                'created_at': new Date().toLocaleString('en-US', {
                    timeZone: 'Asia/Calcutta'
                })
            };

            server_logs_collection.insert(server_logs_data)
                .catch((error) => {
                    console.log('error.message');
                });
            client.close();
            res.redirect('http://kasplo.com');
        });
    } //contact unique id
});

module.exports = router;