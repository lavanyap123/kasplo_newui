const express            = require('express');
const router             = express.Router();
const {ensureAuthenticated, roleAuthenticated} = require('../config/auth');
var json2csv             = require('json2csv'); //not working
var csv_export           = require('csv-export');
const stringify          = require('csv-stringify');
var autoIncrement        = require("mongodb-autoincrement");

var mongoose             = require('mongoose');
var MongoClient          = require('mongodb').MongoClient;
// Dashboard
var await                = require("await");
var async                = require("async");

var constants            = require('./../config/constants');
var pagination_helper    = require('./../helpers/create_pagination');
var helper               = require('./../helpers/functions_handler');
var activityController   = require('./../controllers/activity_ctrl');
var ObjectId             = require('mongodb').ObjectId;

var global_func          = require('./../helpers/global_func');

var ISODate              = require("isodate");

// DB Config
const db                 = require('../config/keys').mongoURI;
const db_options         = require('../config/keys').db_options;
const database_name      = require('../config/keys').database_name;

router.get('/test_sms_log_rpt', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  return res.render('report/test_sms_log_report',{ 
    user                : req.user,
    activity_logs       : req.activity_logs,
    activity_logs_count : req.activity_logs_count,
  });
});
router.post('/test_sms_report_data', activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  let perPage     = 20;
  let page        = req.body.page_no || 1;
  var current     = req.body.page_no;

  var fromdate    = req.body.fromdate;
  var todate      = req.body.todate;

  var start_date  = new Date(fromdate);
  start_date.setHours(0, 0, 0, 0);

  var end_date    = new Date(todate);
  end_date.setHours(23, 59, 59, 59);

  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);

    (async () => {

      var conditions = {
        'created_at': {
          $gt: start_date,
          $lte: end_date
        },
        'mode': { $in:['0'] }
      };

      const total_count_logs = await db.collection('sms_logs').aggregate([
        {
          $match: conditions
        },
        {
          $project : {
            "year" : {
              $year                   : "$created_at"
            },
            "month" : {
              $month                  : "$created_at"
            },
            "day" : {
              $dayOfMonth             : "$created_at"
            },
            mobile_no                 :"$mobile_no",
            sms_content               :"$sms_content",
            sender_id                 : "$sender_id",
            campaign_content_label_id :"$campaign_content_label_id",
            campaign_id               : "$campaign_id",
            gross_clicks              : "$gross_clicks",
            message_status            : "$message_status",
            message_id                : "$message_id",
            message_submit_date       : "$message_submit_date",
            message_delivered_code    : "$message_delivered_code",
            updated_at                : "$updated_at",
            id                        : "$_id",
            //created_at:"$created_at",
          }
        },
      ]).toArray();

      const total_sms_logs = await db.collection('sms_logs').aggregate([
        {
          $match   : conditions
        },
        {
          $project : {
            "year" : {
              $year                     : "$created_at"
            },
            "month" : {
              $month                    : "$created_at"
            },
            "day" : {
              $dayOfMonth               : "$created_at"
            },
            mobile_no                   : "$mobile_no",
            sms_content                 : "$sms_content",
            sender_id                   : "$sender_id",
            campaign_content_label_id   : "$campaign_content_label_id",
            campaign_id                 : "$campaign_id",
            gross_clicks                : "$gross_clicks",
            message_status              : "$message_status",
            message_id                  : "$message_id",
            message_submit_date         : "$message_submit_date",
            message_delivered_code      : "$message_delivered_code",
            updated_at                  : "$updated_at",
            id                          : "$_id",
            //created_at:"$created_at",
          }
        },

        {
          $skip : (perPage * page) - perPage
        },
        {
          $limit : perPage
        },

    ]).toArray();

    var table_content = '<table class="table table-bordered" style="text-align: center;">';
    table_content     += '<thead><tr style="background: #424964c7;color: #fff;"> <th> No </th><th>Sender</th><th>Destination</th> <th>Submit Date</th> <th>Submit Time</th><th> Delivery Date </th> <th>Delivery Time</th><th> Message</th> <th> Status </th> <th> Reason </th><th> Click Count </th> <th> MID </th></tr>';
    table_content     += '</thead>';
    table_content     += '<tbody>';

    if (total_sms_logs.length > 0) {
      total_sms_logs.reverse();

      for (var i = 0; i < total_sms_logs.length; i++) {

        var sender_id             = total_sms_logs[i].sender_id;
        var mobile_no             = total_sms_logs[i].mobile_no;
        var message_submit_date   = getIndianDateFormat(total_sms_logs[i].message_submit_date,'date');
        var message_submit_time   = getIndianDateFormat(total_sms_logs[i].message_submit_date,'time');

        var message_err_code      = total_sms_logs[i].message_delivered_code;
        var get_err_desc          = constants.SMPP_ERR_CODE[message_err_code];
        
        var msg_err_code_txt      = 'Message is not delivered yet.'; 
        if (message_err_code && message_err_code != '' && get_err_desc) {
          msg_err_code_txt        = get_err_desc.description;
        } 

        if (message_err_code && !get_err_desc) {
          msg_err_code_txt        = 'Cannot say the error';
        }

        if (total_sms_logs[i].updated_at) {
          var message_delivery_date = getIndianDateFormat(total_sms_logs[i].updated_at,'date');
          var message_delivery_time = getIndianDateFormat(total_sms_logs[i].updated_at,'time');

        } else {
          var message_delivery_date = '';
          var message_delivery_time = '';
        }

        var sms_content     = total_sms_logs[i].sms_content;
        var message_id      = total_sms_logs[i].message_id;
        var message_reason  = total_sms_logs[i].message_status;
        var message_status  = total_sms_logs[i].message_status;

        var click_count     = total_sms_logs[i].gross_clicks;

        if (!click_count) {
          var click_count = 0;
        }
        
        if (message_status == 'PENDING') {

          var status_span    = '<span class="badge badge-warning">' + message_status + '</span>';

        } else if(message_status == 'DELIVRD') {
          var status_span    = '<span class="badge badge-success">Delivered</span>';
          var message_reason = 'Delivered';

        } else {
          var status_span    = '<span class="badge badge-danger">' + message_status + '</span>';
          var message_reason = total_sms_logs[i].message_status;

        }

        table_content += '<tr>';
        table_content += '<td>'+ (i + 1) +'</td>';
        table_content += '<td>'+ sender_id +'</td>';
        table_content += '<td>'+ mobile_no + '</td>';
        table_content += '<td>'+ message_submit_date +'</td>';
        table_content += '<td>'+ message_submit_time +'</td>';
        table_content += '<td>'+ message_delivery_date +'</td>';
        table_content += '<td>'+ message_delivery_time +'</td>';
        table_content += '<td><a rel="_editable" data-value="' + sms_content + '">Content</td>';
        table_content += '<td>' + status_span + '</td>';
        table_content += '<td><a rel="_editable" data-value="' + msg_err_code_txt + '">' + message_reason + '</a></td>';
        table_content += '<td>' + click_count + '</td>';
        table_content += '<td>' + message_id + '</td>';
        table_content += '</tr>';

      }

    } else {

      table_content     += '<tr><td colspan="19"><img src="/public/images/no_result.gif"  style="height: 250px;width: 250px;display: block;margin: auto;"></td></tr>';                    
    }

    table_content         += '</tbody>';
    table_content         += '</table>';

    const table_count     = total_count_logs.length;

    var pages             = Math.ceil(table_count / perPage);

    var pagination_content = '';
    pagination_content    += '<div class="row" style="float: right;margin-top: 10px;">';

    if (pages > 0) {

      pagination_content += '<nav class="mx-auto">';
      pagination_content += '<ul class="pagination">';

      if (current == 1) {
        pagination_content += '<li class="page-item disabled"><a class="page-link" href="#">First</a></li>';
      } else {
        pagination_content += '<li class="page-item"><a class="page-link" href="#" data-value="1">First</a></li>';
      }

      var i = (Number(current) > 5 ? Number(current) - 4 : 1);
      if (i !== 1) {
        pagination_content += '<li class="page-item disabled"><a class="page-link" href="#">...</a></li>';
      }

      for (; i <= (Number(current) + 4) && i <= pages; i++) {

        if (i == current) {
          pagination_content += '<li class="page-item active"><a class="page-link" href="javascript:void(0)" data-value="' + i + '">' + i + '</a></li>';
        } else {
          pagination_content += '<li class="page-item"><a class="page-link" href="javascript:void(0)" data-value="' + i + '">' + i + '</a></li>';
        }
        if (i == Number(current) + 4 && i < pages) {
          pagination_content += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">...</a></li>';
        }
      }
      if (current == pages) {
        pagination_content += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">Last</a></li>';
      } else {
        pagination_content += '<li class="page-item"><a class="page-link" href="javascript:void(0)" data-value="' + i + '">Last</a></li>';
      }
      pagination_content += '</ul></nav>';
    }

    pagination_content   += '</div>';

    if (table_count > 0) {
      pagination_content += '<p>Showing ' + current + '- <b>' + current * perPage + '</b> of <b class="bold">' + table_count + ' items';
    }

    res.send({
      pagination_content  : pagination_content,
      table_content       : table_content,
      activity_logs       : req.activity_logs,
      activity_logs_count : req.activity_logs_count,
    });
    client.close();
    })();
  });
});

router.get('/contactwise_report',ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {


  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);

    (async () => {

      var campaign_label_details = [];


      const campaign_details  = await db.collection('campaign').find({'draft_status':1}).toArray();
      const category_list     = await db.collection('category').find().toArray();

      const city              = [];
      const district          = [];
      const state             = [];
      const nationality       = [];
      const category_contacts = [];

      var profession          = await db.collection('profession').find().toArray();
      var operator_name       = await db.collection('operator').find().toArray();
      var source              = await db.collection('source').find().toArray();
      var category_details    = await db.collection('category').find({}).toArray();
      var countries_list      = await db.collection('country').find({}).toArray();
      var total_category_contacts = 0;
      /* getting others category count statically */
      var category_count_others = await db.collection('contacts').find({'category_ids':{ $in: ['0'] }}).count(); //,$nor: [{'is_active': {$in: [0]}}]
      if (category_count_others > 0)
       total_category_contacts  = category_count_others;
      /* end of others count*/
      
      var total_suppress_count  = await db.collection('contacts').find({'is_active': {$in: [0]}}).count(); //,$nor: [{'is_active': {$in: [0]}}]
      return res.render('report/contactwise_report', {
        campaign_details,
        category_details,
        campaign_label_details,
        category_list,
        countries_list,
        city,
        district,
        state,
        nationality,
        profession,
        source,
        operator_name,
        total_category_contacts,
        total_suppress_count,
        user: req.user,
        activity_logs: req.activity_logs,
        activity_logs_count: req.activity_logs_count,
      });
      client.close();
    })();
  });
});

router.post('/contact_total_count_report', ensureAuthenticated, (req, res, next) => {


  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);
    (async () => {
      var perPage = constants.PER_PAGE_LIST_COUNT;
      var page = parseInt(req.body.page_no) || 1;
      var current = req.body.page_no;
      var offset = parseInt((perPage * page) - perPage);

      const category_contacts = [];
      
      const category_list = await db.collection('category').find().skip((perPage * page) - perPage)
              .limit(perPage).sort({
                  '_id': -1
              }).toArray();
      var category_count = category_list.length; //console.log(category_count)
      var category_list_count = await db.collection('category').find().count();
      
      var content_html =  '<table class="table table-bordered ">';
          content_html += '<thead><tr><th>Category ID</th><th>Name</th><th> Total Contacts</th><th>Total suppress Contacts</th></tr>';
          content_html += '</thead>';
          content_html += '<tbody>';
          
      for (var index = 0; index < category_list.length; index++) {
        var category_id = category_list[index]._id;
        var name = category_list[index].name;

        var category_count = await db.collection('contacts').find({'category_ids':{ $in: [category_id.toString()] },$nor: [{'is_active': {$in: [0]}}]}).count(); //
        var suppress_count = await db.collection('contacts').find({'category_ids':{ $in: [category_id.toString()] },'is_active': {$in: [0]}}).count(); //,$nor: [{'is_active': {$in: [0]}}]

        //var s_no = offset + index + 1;
        content_html += '<tr>';
        content_html += '<td>' + category_id + '</td>';
        content_html += '<td>' + name + '</td>';
        content_html += '<td>' + category_count + '</td>';
        content_html += '<td>' + suppress_count + '</td>';
        content_html += '</tr>';
      }
      
      content_html   += '</tbody>';
      content_html   += '</table>';

      var pages     = Math.ceil(category_list_count / perPage); 

      var pagination_data = pagination_helper.createPagination(pages, current, perPage, category_list_count, offset);
      res.send({
        pagination_content : pagination_data,
        content_html       : content_html
      });

      client.close();
    })();

  });

});

router.post('/export_as_csv', ensureAuthenticated, (req, res, next) => {

  var fromdate    = req.body.fromdate;
  var todate      = req.body.todate;

  var start_date  = new Date(fromdate + ' 00:00:00');
  var end_date    = new Date(todate);
  var add_one_day_to_date = new Date(end_date.getTime() + 1 * 24 * 60 * 60000);

  var category_id = req.body.category_id;

  MongoClient.connect(db, db_options).then(client => {
    const db      = client.db(database_name);
    (async () => {

      let count;

      if (category_id > 0) {
          var conditions = {
              'category_id': category_id,
              'created_at': {
                  $gte: ISODate(start_date),
                  $lte: ISODate(add_one_day_to_date)
              }
          };
      } else {

          var conditions = {
              'created_at': {
                  $gte: ISODate(start_date),
                  $lte: ISODate(add_one_day_to_date)
              }
          };
      }

      /*    
       console.log('conditions');
       console.log(conditions);*/

      const category = await db.collection('clicks').aggregate([

          {
              $match: conditions
          },
          // / "created_at" : { '$gte' : todate},
          // Get the year, month and day from the createdTimeStamp
          {
              $project: {
                  "year": {
                      $year: "$created_at"
                  },
                  "month": {
                      $month: "$created_at"
                  },
                  "day": {
                      $dayOfMonth: "$created_at"
                  },
                  "category_id": "$category_id",
                  //"created_at":"$created_at",
                  category_details: "$category_details"

              }
          },


          {
              $group: {
                  "_id": {
                      category_id: "$category_id",
                      year: "$year",
                      month: "$month",
                      day: "$day",
                      category_details: "$category_details",
                      //created_at:"$created_at",

                  },
                  count: {
                      $sum: 1
                  }
              },
          },

          {
              $sort: {
                  "_id.day": -1,
                  "_id.month": -1,
                  "_id.year": -1
              }
          },

      ]).toArray();


      var report_data = [];

      if (category.length > 0) {

          for (var i = 0; i < category.length; i++) {

              var date = category[i]._id.day + '-' + category[i]._id.month + '-' + category[i]._id.year;
              var clicks = category[i].count;
              var category_name = category[i]._id.category_details.name;

              report_data.push({
                  'Date': date,
                  'Clicks': clicks,
                  'Category Name': category_name
              });
          }


      }

      //export data
      var fs = require('fs');

      csv_export.export({
          'category_report': report_data
      }, function(buffer) {

          //this module returns a buffer for the csv files already compressed into a single zip.
          //save the zip or force file download via express or other server
          fs.writeFileSync('./public/export/category_report.zip', buffer);
          //res.download('./contacts_list_csv.zip'); // Set disposition and send it.   

      });

      res.send({
          'status': 200,
          'msg': 'successfully exported is csv'
      });


      client.close();
    })();

  });

});


router.get('/categorywise_report',ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db         = client.db(database_name);

    (async () => {

      var category_list = [];

      const category = await db.collection('category').find().forEach(function(cat_record) {
          category_list.push(cat_record);
      });

      res.render('report/categorywise_report', {
          category_list,
          user          : req.user,
          activity_logs : req.activity_logs,
          activity_logs_count : req.activity_logs_count,
      });

      client.close();
    })();

  });

});

router.get('/campaignwise_report', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);

    (async () => {

      var category_list = [];

      const category   = await db.collection('category').find().forEach(function(cat_record) {
      category_list.push(cat_record);
      });

      res.render('report/campaignwise_report', {
        category_list,
        user          : req.user,
        activity_logs : req.activity_logs,
        activity_logs_count : req.activity_logs_count,
      });

      client.close();
    })();

  });
});


router.post('/categorywise_report_filter', (req, res, next) => {


    var perPage = constants.PER_PAGE_LIST_COUNT;
    var page = req.body.page_no || 1;
    var category_id = req.body.category_id;
    var current = req.body.page_no;

    var fromdate = req.body.fromdate;
    var todate = req.body.todate;

    var start_date = new Date(fromdate);
    start_date.setHours(0, 0, 0, 0);

    var end_date = new Date(todate);
    var add_one_day_to_date = new Date(end_date.getTime() + 1 * 24 * 60 * 60000);

    MongoClient.connect(db, db_options).then(client => {
        const db = client.db(database_name);

            (async () => {

                let count;

                if (category_id > 0) {
                    var conditions = {
                        'category_id': parseInt(category_id),
                        'created_at': {
                            $gt: ISODate(start_date),
                            $lte: ISODate(add_one_day_to_date)
                        }
                    };
                } else {

                    var conditions = {
                        'created_at': {
                            $gte: ISODate(start_date),
                            $lte: ISODate(add_one_day_to_date)
                        }
                    };
                }


                //console.log('conditions');
                //console.log(conditions);

                const category = await db.collection('clicks').aggregate([

                    {
                        $match: conditions
                    },
                    // / "created_at" : { '$gte' : todate},
                    // Get the year, month and day from the createdTimeStamp
                    {
                        $project: {
                            "year": {
                                $year: "$created_at"
                            },
                            "month": {
                                $month: "$created_at"
                            },
                            "day": {
                                $dayOfMonth: "$created_at"
                            },
                            "category_id": "$category_id",
                            //"created_at":"$created_at",
                            category_details: "$category_details"

                        }
                    },


                    {
                        $group: {
                            "_id": {
                                category_id: "$category_id",
                                year: "$year",
                                month: "$month",
                                day: "$day",
                                category_details: "$category_details"
                            },
                            count: {
                                $sum: 1
                            }
                        },
                    },
                    {
                        $skip: (perPage * page) - perPage
                    },
                    {
                        $limit: perPage
                    },
                    {
                        $sort: {
                            "_id.day": -1,
                            "_id.month": -1,
                            "_id.year": -1
                        }
                    },

                ]).toArray();

                const total_category = await db.collection('clicks').aggregate([

                    {
                        $match: conditions
                    },

                    {
                        $group: {
                            "_id": {
                                category_id: "$category_id",
                                year: "$year",
                                month: "$month",
                                day: "$day",

                            },
                            count: {
                                $sum: 1
                            }
                        },
                    },

                    {
                        $sort: {
                            "_id.day": -1,
                            "_id.month": -1,
                            "_id.year": -1
                        }
                    },

                ]).toArray();

                var total_sum_of_click = 0;
                if (total_category.length > 0) {

                    for (var i = 0; i < total_category.length; i++) {
                        total_sum_of_click += total_category[i].count;
                    }
                }


                var table_content = '<p id="total_clicks" style="color: #002d56;float: right;"><b>Total Clicks: ' + total_sum_of_click + '</b></p>';
                table_content += '<table class="table table-bordered">';
                table_content += '<thead><tr style="background: #424964c7;color: #fff;"> <th> No </th> <th> Date </th> <th> Category </th> <th> Clicks </th> </tr>';
                table_content += '</thead>';
                table_content += '<tbody>';



                if (category.length > 0) {

                    for (var i = 0; i < category.length; i++) {

                        var date = category[i]._id.day + '-' + category[i]._id.month + '-' + category[i]._id.year;
                        var clicks = category[i].count;
                        var category_name = category[i]._id.category_details.name;
                        table_content += '<tr>';
                        table_content += '<td>' + (i + 1) + '</td>';
                        table_content += '<td>' + date + '</td>';
                        table_content += '<td>' + category_name + '</td>';
                        table_content += '<td>' + clicks + '</td>';
                        table_content += '</tr>';
                    }

                }

                table_content += '</tbody>';
                table_content += '</table>';


                const table_count = category.length;

                var pages = Math.ceil(table_count / perPage);
                var offset = parseInt((perPage * page) - perPage);

                var pagination_data = pagination_helper.createPagination(pages, current, perPage, table_count, offset);
                res.send({
                    pagination_content: pagination_data,
                    table_content: table_content
                });

                client.close();
            })();

        });


});

router.post('/campaign_report_filter', ensureAuthenticated, (req, res, next) => {

    let perPage = constants.PER_PAGE_LIST_COUNT;
    let page = req.body.page_no || 1;
    let category_id = req.body.category_id;
    let campaign_id = parseInt(req.body.campaign_id);
    var current = req.body.page_no;

    var fromdate = req.body.fromdate;
    var todate = req.body.todate;

    var start_date = new Date(fromdate);
    start_date.setHours(0, 0, 0, 0);

    var end_date = new Date(todate);
    var add_one_day_to_date = new Date(end_date.getTime() + 1 * 24 * 60 * 60000);

    MongoClient.connect(db, db_options).then(client => {
        const db = client.db(database_name);

            (async () => {

                let count;

                if (category_id > 0 && campaign_id > 0) {

                    var conditios = {
                        'category_id': parseInt(category_id),
                        'campaign_id': parseInt(req.body.campaign_id),
                        'created_at': {
                            $gt: ISODate(start_date),
                            $lte: ISODate(add_one_day_to_date)
                        }
                    };

                } else if (category_id > 0) {
                    var conditios = {
                        'category_id': parseInt(category_id),
                        'created_at': {
                            $gte: ISODate(start_date),
                            $lte: ISODate(add_one_day_to_date)
                        }
                    };

                } else {

                    var conditios = {
                        'created_at': {
                            $gte: ISODate(start_date),
                            $lte: ISODate(add_one_day_to_date)
                        }
                    };
                }


                const campaign = await db.collection('clicks').aggregate([

                    {
                        $match: conditios
                    },
                    // / "created_at" : { '$gte' : todate},
                    // Get the year, month and day from the createdTimeStamp
                    {
                        $project: {
                            "year": {
                                $year: "$created_at"
                            },
                            "month": {
                                $month: "$created_at"
                            },
                            "day": {
                                $dayOfMonth: "$created_at"
                            },
                            campaign_id: "$campaign_id",
                            campaign_details: "$campaign_details",
                            campaign_label_details: "$campaign_label_details",
                            category_details: "$category_details",
                            //created_at:"$created_at",

                        }
                    },

                    {
                        $group: {
                            "_id": {
                                campaign_id: "$campaign_id",
                                year: "$year",
                                month: "$month",
                                day: "$day",
                                campaign_details: "$campaign_details",
                                campaign_label_details: "$campaign_label_details",
                                category_details: "$category_details"
                                //created_at:"$created_at",
                            },
                            count: {
                                $sum: 1
                            }
                        },
                    },
                    {
                        $skip: (perPage * page) - perPage
                    },
                    {
                        $limit: perPage
                    },
                    {
                        $sort: {
                            "_id.day": -1,
                            "_id.month": -1,
                            "_id.year": -1
                        }
                    },

                ]).toArray();


                const total_campaign = await db.collection('clicks').aggregate([

                    {
                        $match: conditios
                    },
                    // / "created_at" : { '$gte' : todate},
                    // Get the year, month and day from the createdTimeStamp


                    {
                        $group: {
                            "_id": {
                                campaign_id: "$campaign_id",
                                year: "$year",
                                month: "$month",
                                day: "$day",
                            },
                            count: {
                                $sum: 1
                            }
                        },
                    },

                    {
                        $sort: {
                            "_id.day": -1,
                            "_id.month": -1,
                            "_id.year": -1
                        }
                    },

                ]).toArray();


                var total_sum_of_click = 0;
                if (total_campaign.length > 0) {

                    for (var i = 0; i < total_campaign.length; i++) {
                        total_sum_of_click += total_campaign[i].count;
                    }
                }

                var table_content = '<p id="total_clicks" style="color: #002d56;float: right;"><b>Total Clicks: ' + total_sum_of_click + '</b></p>';
                table_content += '<table class="table table-bordered">';
                table_content += '<thead><tr style="background: #424964c7;color: #fff;"> <th> No </th> <th> Date </th><th> Category </th><th> Campaign </th><th> Campaign Content</th> <th> Clicks </th> </tr>';
                table_content += '</thead>';
                table_content += '<tbody>';



                if (campaign.length > 0) {

                    for (var i = 0; i < campaign.length; i++) {

                        var date = campaign[i]._id.day + '-' + campaign[i]._id.month + '-' + campaign[i]._id.year;
                        var clicks = campaign[i].count;
                        if (campaign[i]._id.campaign_details)
                            var campaign_name = campaign[i]._id.campaign_details.name;
                        else
                            var campaign_name = '#';

                        var campaign_label_name = campaign[i]._id.campaign_label_details.label_name;
                        var category_name = campaign[i]._id.category_details.name;
                        table_content += '<tr>';
                        table_content += '<td>' + (i + 1) + '</td>';
                        table_content += '<td>' + date + '</td>';
                        table_content += '<td>' + category_name + '</td>';
                        table_content += '<td>' + campaign_name + '</td>';
                        table_content += '<td>' + campaign_label_name + '</td>';
                        table_content += '<td>' + clicks + '</td>';
                        table_content += '</tr>';
                    }


                }

                table_content += '</tbody>';
                table_content += '</table>';


                const table_count = campaign.length;

                var pages = Math.ceil(table_count / perPage);
                var offset = parseInt((perPage * page) - perPage);

                var pagination_data = pagination_helper.createPagination(pages, current, perPage, table_count, offset);
                res.send({
                    pagination_content: pagination_data,
                    table_content: table_content
                });


                client.close();
            })();


        });


});


// Server Logs - Index
router.get('/server-logs',ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
        const db = client.db(database_name);

            (async () => {

                var server_logs_details = [];

                const server_logs = await db.collection('server_logs').find().forEach(function(server_log_record) {
                    server_logs_details.push(server_log_record);
                });


                res.render('server-logs/index', {
                    user: req.user,
                    server_logs_details,
                    activity_logs: req.activity_logs,
                    activity_logs_count: req.activity_logs_count,
                });

                client.close();

            })();

        });
});

// Activity Logs - Index
router.get('/activity-logs', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            res.render('activity-logs/index', {
                user: req.user,
                activity_logs: req.activity_logs,
                activity_logs_count: req.activity_logs_count,
               
            });

            client.close();
        });
});

router.post('/test_filter', ensureAuthenticated, (req, res, next) => {

    let perPage = 20;
    let page = req.body.page_no || 1;
    let category_id = req.body.category_id;
    let campaign_id = parseInt(req.body.campaign_id);
    let content_label_id = parseInt(req.body.content_label_id);
    var current = req.body.page_no;

    var fromdate = req.body.fromdate;
    var todate = req.body.todate;

    var start_date = new Date(fromdate);
    start_date.setHours(0, 0, 0, 0);


    var end_date = new Date(todate);
    var add_one_day_to_date = new Date(end_date.getTime() + 1 * 24 * 60 * 60000);

    MongoClient.connect(db, db_options).then(client => {
        const db = client.db(database_name);

            (async () => {

                let count;

                if (category_id > 0 && campaign_id > 0 && content_label_id > 0) {
                    var conditios = {
                        'category_id': parseInt(category_id),
                        'campaign_id': parseInt(req.body.campaign_id),
                        'content_label_id': parseInt(content_label_id),
                        'created_at': {
                            $gt: start_date,
                            $lte: add_one_day_to_date
                        }
                    };

                } else if (category_id > 0 && campaign_id > 0) {
                    var conditios = {
                        'category_id': parseInt(category_id),
                        'campaign_id': parseInt(req.body.campaign_id),
                        'created_at': {
                            $gt: start_date,
                            $lte: add_one_day_to_date
                        }
                    };

                } else if (category_id > 0) {
                    var conditios = {
                        'category_id': parseInt(category_id),
                        'created_at': {
                            $gt: start_date,
                            $lte: add_one_day_to_date
                        }
                    };

                } else {

                    var conditios = {
                        'created_at': {
                            $gt: start_date,
                            $lte: add_one_day_to_date
                        }
                    };
                }


                const campaign = await db.collection('test_clicks').aggregate([

                    {
                        $match: conditios
                    },
                    {
                        $project: {
                            "year": {
                                $year: "$created_at"
                            },
                            "month": {
                                $month: "$created_at"
                            },
                            "day": {
                                $dayOfMonth: "$created_at"
                            },
                            campaign_id: "$campaign_id",
                            campaign_details: "$campaign_details",
                            campaign_label_details: "$campaign_label_details",
                            category_details: "$category_details",
                            location: "$location",
                            browser_information: "$browser_information",
                            public_ip: "$public_ip",
                            gross_clicks: "$gross_clicks"

                        }
                    },

                    {
                        $skip: (perPage * page) - perPage
                    },
                    {
                        $limit: perPage
                    },
                    {
                        $sort: {
                            "_id.day": 1,
                            "_id.month": 1,
                            "_id.year": 1
                        }
                    },

                ]).toArray();


                const total_campaign = await db.collection('test_clicks').aggregate([

                    {
                        $match: conditios
                    },

                    {
                        $group: {
                            _id: {
                                year: "$year",
                                month: "$month",
                                day: "$day"
                            },
                            count: {
                                $sum: 1
                            }
                        },
                    },

                ]).toArray();


                var total_sum_of_click = 0;
                if (total_campaign.length > 0) {

                    for (var i = 0; i < total_campaign.length; i++) {
                        total_sum_of_click += total_campaign[i].count;
                    }
                }

                var table_content = '<p id="total_clicks" style="color: #002d56;float: right;"><b>Total Clicks: ' + total_sum_of_click + '</b></p>';

                table_content += '<table class="table table-bordered">';
                table_content += '<thead><tr style="background: #424964c7;color: #fff;"> <th> No </th> <th> Date </th><th> Category </th><th> Campaign </th><th> Campaign Content</th> <th> Clicks </th><th>Gross Clicks </th> <th> IP </th><th> Browser </th></tr>';
                table_content += '</thead>';
                table_content += '<tbody>';


                if (campaign.length > 0) {

                    for (var i = 0; i < campaign.length; i++) {

                        var date = campaign[i].day + '-' + campaign[i].month + '-' + campaign[i].year;
                        var clicks = 1; //campaign[i].count;
                        var category_name = campaign[i].category_details.name;
                        var campaign_name = campaign[i].campaign_details.name;
                        var campaign_label_name = campaign[i].campaign_label_details.label_name;
                        var browser_information = campaign[i].browser_information;
                        var ip = campaign[i].public_ip;
                        var gross_clicks = campaign[i].gross_clicks;

                        table_content += '<tr>';
                        table_content += '<td>' + (i + 1) + '</td>';
                        table_content += '<td>' + date + '</td>';
                        table_content += '<td>' + category_name + '</td>';
                        table_content += '<td>' + campaign_name + '</td>';
                        table_content += '<td>' + campaign_label_name + '</td>';
                        table_content += '<td>' + clicks + '</td>';
                        table_content += '<td>' + gross_clicks + '</td>';
                        table_content += '<td>' + ip + '</td>';
                        table_content += '<td>' + browser_information + '</td>';

                        table_content += '</tr>';
                    }


                }

                table_content += '</tbody>';
                table_content += '</table>';


                const table_count = total_sum_of_click;

                var pages = Math.ceil(table_count / perPage);

                var pagination_content = '';
                pagination_content += '<div class="row" style="float: right;margin-top: 10px;">';

                if (pages > 0) {

                    pagination_content += '<nav class="mx-auto">';
                    pagination_content += '<ul class="pagination">';

                    if (current == 1) {
                        pagination_content += '<li class="page-item disabled"><a class="page-link" href="#">First</a></li>';
                    } else {
                        pagination_content += '<li class="page-item"><a class="page-link" href="#" data-value="1">First</a></li>';
                    }

                    var i = (Number(current) > 5 ? Number(current) - 4 : 1)
                    if (i !== 1) {

                        pagination_content += '<li class="page-item disabled"><a class="page-link" href="#">...</a></li>';
                    }

                    for (; i <= (Number(current) + 4) && i <= pages; i++) {

                        if (i == current) {
                            pagination_content += '<li class="page-item active"><a class="page-link" href="javascript:void(0)" data-value="' + i + '">' + i + '</a></li>';
                        } else {

                            pagination_content += '<li class="page-item"><a class="page-link" href="javascript:void(0)" data-value="' + i + '">' + i + '</a></li>';
                        }

                        if (i == Number(current) + 4 && i < pages) {
                            pagination_content += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">...</a></li>';

                        }
                    }
                    if (current == pages) {
                        pagination_content += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">Last</a></li>';
                    } else {
                        pagination_content += '<li class="page-item"><a class="page-link" href="javascript:void(0)" data-value="' + i + '">Last</a></li>';
                    }
                    pagination_content += '</ul></nav>';
                }

                pagination_content += '</div>'

                if (table_count > 0) {
                    pagination_content += '<p>Showing ' + current + '- <b>' + current * perPage + '</b> of <b class="bold">' + table_count + ' items';

                } else {
                    pagination_content += '<img src="/public/images/no_result.gif"  style="height: 300px;display: block;margin: auto;">';
                }


                res.send({
                    pagination_content: pagination_content,
                    table_content: table_content
                });

                client.close();

            })();


        });
});

router.post('/general_report_filter', ensureAuthenticated, (req, res, next) => {

  let perPage           = constants.PER_PAGE_LIST_COUNT;
  let page              = req.body.page_no || 1;
  let category_id       = req.body.category_id;
  let campaign_id       = parseInt(req.body.campaign_id);
  let content_label_id  = parseInt(req.body.content_label_id);
  var current           = req.body.page_no;
  var fromdate          = req.body.fromdate;
  var todate            = req.body.todate;
  var start_date        = new Date(fromdate);
  start_date.setHours(0, 0, 0, 0);
  var end_date          = new Date(todate);
  var add_one_day_to_date = new Date(end_date.getTime() + 1 * 24 * 60 * 60000);
  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);

    (async () => {

      let count;

      if (category_id > 0 && campaign_id > 0 && content_label_id > 0) {
        var conditios        = {
          'category_id'      : parseInt(category_id),
          'campaign_id'      : parseInt(req.body.campaign_id),
          'content_label_id' : parseInt(content_label_id),
          'created_at'       : {
            $gt              : start_date,
            $lte             : add_one_day_to_date
          }
        };

      } else if (category_id > 0 && campaign_id > 0) {
        var conditios       = {
          'category_id'     : parseInt(category_id),
          'campaign_id'     : parseInt(req.body.campaign_id),
          'created_at'      : {
              $gt           : start_date,
              $lte          : add_one_day_to_date
          }
        };

      } else if (category_id > 0) {
        var conditios       = {
          'category_id'   : parseInt(category_id),
          'created_at'    : {
              $gt         : start_date,
              $lte        : add_one_day_to_date
          }
        };

      } else {
        var conditios     = {
          'created_at'    : {
            $gt           : start_date,
            $lte          : add_one_day_to_date
          }
        };
      }

      const campaign = await db.collection('clicks').aggregate([
      {
        $match       : conditios
      },
      // Get the year, month and day from the createdTimeStamp
      {
        $project     : {
          "year"     : {
            $year    : "$created_at"
          },
          "month"    : {
            $month   : "$created_at"
          },
          "day"      : {
            $dayOfMonth : "$created_at"
          },
          campaign_id   : "$campaign_id",
          campaign_details       : "$campaign_details",
          campaign_label_details : "$campaign_label_details",
          category_details       : "$category_details",
          location               : "$location",
          browser_information    : "$browser_information",
          public_ip              : "$public_ip",
        }
      },
      {
          $skip     : (perPage * page) - perPage
      },
      {
          $limit    : perPage
      },
      {
        $sort: {
          "_id.day"    : 1,
          "_id.month"  : 1,
          "_id.year"   : 1
        }
      },

      ]).toArray();

    const total_campaign = await db.collection('clicks').aggregate([
    {
      $match     : conditios
    },

    {
      $group     : {
        _id      : {
          year   : "$year",
          month  : "$month",
          day    : "$day"
        },
        count    : {
          $sum   : 1
        }
      },
    },
    ]).toArray();

    var total_sum_of_click   = 0;
    if (total_campaign.length > 0) {

      for (var i = 0; i < total_campaign.length; i++) {
        total_sum_of_click   += total_campaign[i].count;
      }
    }

    var table_content        = '<p id="total_clicks" style="color: #002d56;float: right;"><b>Total Clicks: ' + total_sum_of_click + '</b></p>';

    table_content            += '<table class="table table-bordered">';
    table_content            += '<thead><tr style="background: #424964c7;color: #fff;"> <th> No </th> <th> Date </th><th> Category </th><th> Campaign </th><th> Campaign Content</th> <th> Clicks </th> <th> IP </th><th> City </th><th> State </th><th> Country </th><th> Latitude </th><th> Longitude </th><th> Pin Code </th><th> Network </th> <th> Browser </th><th> OS </th><th> Platform </th><th> UserAgent </th> </tr>';
    table_content            += '</thead>';
    table_content            += '<tbody>';

    if (campaign.length > 0) {
      for (var i = 0; i < campaign.length; i++) {

        var date            = campaign[i].day + '-' + campaign[i].month + '-' + campaign[i].year;
        var clicks          = 1; //campaign[i].count;
        var category_name   = campaign[i].category_details.name;
        var campaign_name   = campaign[i].campaign_details.name;
        var campaign_label_name = campaign[i].campaign_label_details.label_name;
        var location        = campaign[i].location;
        var browser_information = campaign[i].browser_information;
        var ip              = campaign[i].public_ip;
        var city            = '';
        var state           = '';
        var country         = '';
        var latitude        = '';
        var longitude       = '';
        var postal          = '';
        var network         = '';

        if (ip != '::1') {
          if (location) {
            city            = location.city;
            state           = location.region;
            country         = location.country_name;
            latitude        = location.latitude;
            longitude       = location.longitude;
            postal          = location.postal;
            network         = location.organisation;
          } 
        } 

          table_content     += '<tr>';
          table_content     += '<td>' + (i + 1) + '</td>';
          table_content     += '<td>' + date + '</td>';
          table_content     += '<td>' + category_name + '</td>';
          table_content     += '<td>' + campaign_name + '</td>';
          table_content     += '<td>' + campaign_label_name + '</td>';
          table_content     += '<td>' + clicks + '</td>';

          table_content     += '<td>' + ip + '</td>';

          table_content     += '<td>' + city + '</td>';
          table_content     += '<td>' + state + '</td>';
          table_content     += '<td>' + country + '</td>';
          table_content     += '<td>' + latitude + '</td>';
          table_content     += '<td>' + longitude + '</td>';
          table_content     += '<td>' + postal + '</td>';
          table_content     += '<td>' + network + '</td>';

          table_content     += '<td>' + helper.checkEmptyVal(browser_information.browser) + '</td>';
          table_content     += '<td>' + helper.checkEmptyVal(browser_information.os) + '</td>';
          table_content     += '<td>' + helper.checkEmptyVal(browser_information.platform) + '</td>';
          table_content     += '<td>' + helper.checkEmptyVal(browser_information.source) + '</td>';

          table_content     += '</tr>';
      }

    }

    table_content           += '</tbody>';
    table_content           += '</table>';


    const table_count       = total_sum_of_click;

    var pages               = Math.ceil(table_count / perPage);
    var offset              = parseInt((perPage * page) - perPage);

    var pagination_data     = pagination_helper.createPagination(pages, current, perPage, table_count, offset);
    res.send({
    pagination_content: pagination_data,
    table_content: table_content
    });

    client.close();

    })();


      });
});


router.post('/search_contacts', ensureAuthenticated, (req, res, next) => {


    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const contacts_collection = db.collection('contacts');
            const clicks_collection = db.collection('clicks');
            const campaign_collection = db.collection('campaign');
            const campaign_content_labels = db.collection('campaign_content_labels');

            var filter_params = req.body;

            var filter_category_id = filter_params.filter_category_id;

            var minAge = filter_params.minAge; // Min Age
            var maxAge = filter_params.maxAge; // Max Age

            var filter_search = filter_params.filter_search;

            var find_paramas_query = [];

            /* Step-1 - Sender & Non-sender - Stop */

            var filter_sender_category_ids = [];
            var cat_id_filter = [];

            if (filter_category_id) {
                for (var i = 0, len = filter_category_id.length; i < len; i++) {
                    cat_id_filter.push(filter_category_id[i]);
                }

                /* Checking selected Categorys - Mandatroy */
                find_paramas_query.push({
                    'category_ids': {
                        $in: cat_id_filter
                    }
                });
            }


            /* Step-2 - Basic Filter - Start */
            if (typeof filter_search !== "undefined") {
                if ('city' in filter_search) {
                    var city = filter_search.city;
                    find_paramas_query.push({
                        'city': city.toString()
                    });
                }

                if ('district' in filter_search) {
                    var district = filter_search.district;
                    find_paramas_query.push({
                        'district': district.toString()
                    });
                }

                if ('state' in filter_search) {
                    var state = filter_search.state;
                    find_paramas_query.push({
                        'state': state.toString()
                    });
                }

                if ('nationality' in filter_search) {
                    var nationality = filter_search.nationality;
                    find_paramas_query.push({
                        'nationality': nationality.toString()
                    });
                }

                if ('pincode' in filter_search) {
                    var pincode = filter_search.pincode;
                    find_paramas_query.push({
                        'pincode': pincode.toString()
                    });
                }

                if ('gender' in filter_search) {
                    var gender = filter_search.gender;
                    find_paramas_query.push({
                        'gender': gender.toString()
                    });
                }

                if ('locality' in filter_search) {
                    var locality = filter_search.locality;
                    find_paramas_query.push({
                        'locality': locality.toString()
                    });
                }

                if ('profession' in filter_search) {
                    var profession = filter_search.profession;
                    find_paramas_query.push({
                        'profession': profession.toString()
                    });
                }

                if ('salary' in filter_search) {
                    var salary = filter_search.salary;
                    find_paramas_query.push({
                        'salary': salary.toString()
                    });
                }

                if ('source' in filter_search) {
                    var source = filter_search.source;
                    find_paramas_query.push({
                        'source': source.toString()
                    });
                }

                if ('operator_name' in filter_search) {
                    var operator_name = filter_search.operator_name;
                    find_paramas_query.push({
                        'operator_name': operator_name.toString()
                    });
                }

            }

            if (minAge > 1 && maxAge == '100' || minAge > 1 && maxAge < 100 || minAge == '1' && maxAge < 100) {
                if (maxAge == '100') {
                    var maxAge = 99;
                }
                //minAge and maxAge
                find_paramas_query.push({
                    'age': {
                        $gte: minAge.toString(),
                        $lte: maxAge.toString()
                    }
                });
            }
            //find_paramas_query.push({$nor: [{'is_active': {$in: [0]}}]});
            /* Step-2 - Basic Filter - Stop */

            /* Array Convert to objects */
            var filter_query = find_paramas_query.reduce((key, value) => Object.assign(key, value), {});

            //console.log('filter_query');
            //console.log(filter_query);

            (async () => {

                var total_contacts = await contacts_collection.count(filter_query);

                res.send({
                    'status': 200,
                    'total_contacts': total_contacts
                });

                client.close();
            })();

        });
});

router.get('/general_report', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db       = client.db(database_name);
    const contacts = db.collection('contacts');
    const category = db.collection('category');
    const campaign = db.collection('campaign');
    const campaign_content_labels = db.collection('campaign_content_labels');

    (async () => {

      var contacts_list         = [];
      var category_list         = [];
      var campaign_list         = [];
      var campaign_content_list = [];

      const category_tbl        = await category.find().forEach(function(category_record) {
        category_list.push(category_record);
      });

      const campaign_tbl        = await campaign.find().forEach(function(campaign_record) {
        campaign_list.push(campaign_record);
      });

      const campaign_content_tbl = await campaign_content_labels.find().forEach(function(campaign_content_labels_rec) {
          campaign_content_list.push(campaign_content_labels_rec);
      });

      //const contact_total = await contacts.count({$nor: [{'is_active': {$in: [0]}}]});

      res.render('report/general_report', {
        user                  : req.user,
        category_list         : category_list,
        campaign_list         : campaign_list,
        campaign_content_list : campaign_content_list,
        activity_logs         : req.activity_logs,
        activity_logs_count   : req.activity_logs_count
      });
    client.close();
    })();
  });
});

router.get('/test_report', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);
    const contacts = db.collection('contacts');
    const category = db.collection('category');
    const campaign = db.collection('campaign');
    const campaign_content_labels = db.collection('campaign_content_labels');

    (async () => {

      var contacts_list         = [];
      var category_list         = [];
      var campaign_list         = [];
      var campaign_content_list = [];

      const category_tbl        = await category.find().forEach(function(category_record) {
          category_list.push(category_record);
      });

      const campaign_tbl        = await campaign.find().forEach(function(campaign_record) {
          campaign_list.push(campaign_record);
      });

      const campaign_content_tbl = await campaign_content_labels.find().forEach(function(campaign_content_labels_rec) {
          campaign_content_list.push(campaign_content_labels_rec);
      });

      res.render('report/test_report', {
        user                  : req.user,
        category_list         : category_list,
        campaign_list         : campaign_list,
        campaign_content_list : campaign_content_list,
        activity_logs         : req.activity_logs,
        activity_logs_count   : req.activity_logs_count,
      });
      client.close();
    })();


  });
});


router.post('/test_filter', ensureAuthenticated, (req, res, next) => {

    let perPage = 20;
    let page = req.body.page_no || 1;
    let category_id = req.body.category_id;
    let campaign_id = parseInt(req.body.campaign_id);
    let content_label_id = parseInt(req.body.content_label_id);
    var current = req.body.page_no;

    var fromdate = req.body.fromdate;
    var todate = req.body.todate;


    var start_date = new Date(fromdate);
    start_date.setHours(0, 0, 0, 0);


    var end_date = new Date(todate);
    var add_one_day_to_date = new Date(end_date.getTime() + 1 * 24 * 60 * 60000);



    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);

            (async () => {

                let count;

                if (category_id > 0 && campaign_id > 0 && content_label_id > 0) {
                    var conditios = {
                        'category_id': parseInt(category_id),
                        'campaign_id': parseInt(req.body.campaign_id),
                        'content_label_id': parseInt(content_label_id),
                        'created_at': {
                            $gt: start_date,
                            $lte: add_one_day_to_date
                        }
                    };

                } else if (category_id > 0 && campaign_id > 0) {
                    var conditios = {
                        'category_id': parseInt(category_id),
                        'campaign_id': parseInt(req.body.campaign_id),
                        'created_at': {
                            $gt: start_date,
                            $lte: add_one_day_to_date
                        }
                    };

                } else if (category_id > 0) {
                    var conditios = {
                        'category_id': parseInt(category_id),
                        'created_at': {
                            $gt: start_date,
                            $lte: add_one_day_to_date
                        }
                    };

                } else {

                    var conditios = {
                        'created_at': {
                            $gt: start_date,
                            $lte: add_one_day_to_date
                        }
                    };
                }


                const campaign = await db.collection('test_clicks').aggregate([

                    {
                        $match: conditios
                    },
                    {
                        $project: {
                            "year": {
                                $year: "$created_at"
                            },
                            "month": {
                                $month: "$created_at"
                            },
                            "day": {
                                $dayOfMonth: "$created_at"
                            },
                            campaign_id: "$campaign_id",
                            campaign_details: "$campaign_details",
                            campaign_label_details: "$campaign_label_details",
                            category_details: "$category_details",
                            location: "$location",
                            browser_information: "$browser_information",
                            public_ip: "$public_ip",

                        }
                    },

                    {
                        $skip: (perPage * page) - perPage
                    },
                    {
                        $limit: perPage
                    },
                    {
                        $sort: {
                            "_id.day": 1,
                            "_id.month": 1,
                            "_id.year": 1
                        }
                    },

                ]).toArray();


                const total_campaign = await db.collection('test_clicks').aggregate([

                    {
                        $match: conditios
                    },

                    {
                        $group: {
                            _id: {
                                year: "$year",
                                month: "$month",
                                day: "$day"
                            },
                            count: {
                                $sum: 1
                            }
                        },
                    },

                ]).toArray();

                /*console.log('conditios');
                console.log(conditios);

                console.log('total_campaign');
                console.log(total_campaign);*/
                //console.log('campaign');
                //console.log(campaign);

                var total_sum_of_click = 0;
                if (total_campaign.length > 0) {

                    for (var i = 0; i < total_campaign.length; i++) {
                        total_sum_of_click += total_campaign[i].count;
                    }
                }

                var table_content = '<p id="total_clicks" style="color: #002d56;float: right;"><b>Total Clicks: ' + total_sum_of_click + '</b></p>';

                table_content += '<table class="table table-bordered">';
                table_content += '<thead><tr style="background: #424964c7;color: #fff;"> <th> No </th> <th> Date </th><th> Category </th><th> Campaign </th><th> Campaign Content</th> <th> Clicks </th> <th> IP </th><th> Browser </th></tr>';
                table_content += '</thead>';
                table_content += '<tbody>';


                if (campaign.length > 0) {

                    for (var i = 0; i < campaign.length; i++) {


                        var date = campaign[i].day + '-' + campaign[i].month + '-' + campaign[i].year;
                        var clicks = 1; //campaign[i].count;
                        var category_name = campaign[i].category_details.name;
                        var campaign_name = campaign[i].campaign_details.name;
                        var campaign_label_name = campaign[i].campaign_label_details.label_name;
                        var browser_information = campaign[i].browser_information;
                        var ip = campaign[i].public_ip;

                        table_content += '<tr>';
                        table_content += '<td>' + (i + 1) + '</td>';
                        table_content += '<td>' + date + '</td>';
                        table_content += '<td>' + category_name + '</td>';
                        table_content += '<td>' + campaign_name + '</td>';
                        table_content += '<td>' + campaign_label_name + '</td>';
                        table_content += '<td>' + clicks + '</td>';
                        table_content += '<td>' + ip + '</td>';
                        table_content += '<td>' + browser_information + '</td>';

                        table_content += '</tr>';
                    }


                }

                table_content += '</tbody>';
                table_content += '</table>';


                const table_count = total_sum_of_click;

                var pages = Math.ceil(table_count / perPage);

                var pagination_content = '';
                pagination_content += '<div class="row" style="float: right;margin-top: 10px;">';

                if (pages > 0) {

                    pagination_content += '<nav class="mx-auto">';
                    pagination_content += '<ul class="pagination">';

                    if (current == 1) {
                        pagination_content += '<li class="page-item disabled"><a class="page-link" href="#">First</a></li>';
                    } else {
                        pagination_content += '<li class="page-item"><a class="page-link" href="#" data-value="1">First</a></li>';
                    }

                    var i = (Number(current) > 5 ? Number(current) - 4 : 1)
                    if (i !== 1) {

                        pagination_content += '<li class="page-item disabled"><a class="page-link" href="#">...</a></li>';
                    }

                    for (; i <= (Number(current) + 4) && i <= pages; i++) {

                        if (i == current) {
                            pagination_content += '<li class="page-item active"><a class="page-link" href="javascript:void(0)" data-value="' + i + '">' + i + '</a></li>';
                        } else {

                            pagination_content += '<li class="page-item"><a class="page-link" href="javascript:void(0)" data-value="' + i + '">' + i + '</a></li>';
                        }

                        if (i == Number(current) + 4 && i < pages) {
                            pagination_content += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">...</a></li>';

                        }
                    }
                    if (current == pages) {
                        pagination_content += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">Last</a></li>';
                    } else {
                        pagination_content += '<li class="page-item"><a class="page-link" href="javascript:void(0)" data-value="' + i + '">Last</a></li>';
                    }
                    pagination_content += '</ul></nav>';
                }

                pagination_content += '</div>'

                if (table_count > 0) {
                    pagination_content += '<p>Showing ' + current + '- <b>' + current * perPage + '</b> of <b class="bold">' + table_count + ' items';

                } else {
                    pagination_content += '<img src="/public/images/no_result.gif"  style="height: 300px;display: block;margin: auto;">';
                }


                res.send({
                    pagination_content: pagination_content,
                    table_content: table_content
                });

                client.close();

            })();


        });
});

router.get('/sms_log_report', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);

            (async () => {


                const campaign_list = await db.collection('campaign').find({'draft_status':1}).sort({_id:-1}).toArray();

                return res.render('report/sms_log_report',{ 
                    user: req.user,
                    campaign_list: campaign_list,
                    activity_logs: req.activity_logs,
                    activity_logs_count: req.activity_logs_count,
                });

                client.close();

        })();

    });            
});

router.get('/smpp_report', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);

            (async () => {


                const campaign_list = await db.collection('campaign').find({'draft_status':1}).sort({_id:-1}).toArray();

                return res.render('report/smpp_report',{ 
                    user: req.user,
                    campaign_list: campaign_list,
                    activity_logs: req.activity_logs,
                    activity_logs_count: req.activity_logs_count
                });

                client.close();

        })();

    });            
});


router.post('/sms_report_data', activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

    let perPage = 20;
    let page = req.body.page_no || 1;
    var current = req.body.page_no;

    var fromdate = req.body.fromdate;
    var todate = req.body.todate;

    var start_date = new Date(fromdate);
    start_date.setHours(0, 0, 0, 0);


    var end_date = new Date(todate);
    end_date.setHours(23, 59, 59, 59);

    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            const sms_log_collection = db.collection('sms_logs');
            (async () => {

                let count;

                var campaign_id = parseInt(req.body.campaign_id);
                var content_label_id = parseInt(req.body.content_label_id);
                var filter_type = req.body.filter_type;
                var smpp_id = req.body.smpp_id;
                var sender_id = req.body.sender_id;
                var smpp_name = req.body.smpp_name;
                var sender_name = req.body.sender_name;

                var conditions = {
                  'created_at': {
                    $gt: start_date,
                    $lte: end_date
                  },
                  'mode': { $in: ['1'] },
                  $nor:[{
                    'message_status': { $in: [/^SCHEDULE CANCELLED/] },
                  }]
                };

                var chart_conditions = {
                        'date_added': {
                            $gt: start_date,
                            $lte: end_date
                        },
                        $nor:[{
                            'schedule_status': { $in: [/^2/] }
                        }]

                    };

                 var click_conditions = {
                        "created_at": {
                            $gt: start_date,
                            $lte: end_date
                        }
                    };      

                    var sms_logs_in_conditions = {
                        "created_at": {
                           $gt: start_date,
                            $lte: end_date
                          },
                        "message_err": { $in: [/^001/,/^000/] },
                        'mode': { $in: ['1'] },
                    };

                    var sms_logs_nin_conditions = {
                        "created_at": {
                            $gt: start_date,
                            $lte: end_date
                        },
                        "message_err": { $nin: [/^001/,/^000/] },
                        $nor:[{
                            'message_status': { $in: [/^PENDING/,/^SCHEDULE/,/^SCHEDULE CANCELLED/,/^SUBMIT/] }
                        }],
                        'mode': { $in: ['1'] },
                        
                    };

                    var sms_logs_nin_dlr_pending_conditions = {
                        "created_at": {
                            $gt: start_date,
                            $lte: end_date
                        },
                        "message_status": { $in: [/^PENDING/] },
                        'mode': { $in: ['1'] },
                        "message_id" : {"$exists" : true }
                    };

                    var sms_logs_nin_rejection_conditions = {
                        "created_at": {
                            $gt: start_date,
                            $lte: end_date
                        },
                        "message_status": { $in: [/^PENDING/] },
                        'mode': { $in: ['1'] },
                        "message_id" : {"$exists" : false }
                    };

                    var sms_logs_schedule_conditions = {
                        "created_at": {
                            $gt: start_date,
                            $lte: end_date
                        },
                        "message_status": { $in: ['SCHEDULE'] },
                        'mode': { $in: ['1'] }
                    };

                    var sms_logs_failed_conditions = {
                        "created_at": {
                            $gt: start_date,
                            $lte: end_date
                        },
                        "message_err": { $in: [/^019/,/^023/,/^219/,/^035/,/^999/,/^002/] },
                        'mode': { $in: ['1'] }
                    };
                    
                    var sms_logs_expired_conditions = {
                        "created_at": {
                            $gt: start_date,
                            $lte: end_date
                        },
                        "message_err": { $in: [/^009/,/^031/,/^032/,/^033/,/^034/,/^035/,/^036/,/^037/] },
                        'mode': { $in: ['1'] }
                    };

                    var sms_logs_dnd_failed_conditions = {
                        "created_at": {
                            $gt: start_date,
                            $lte: end_date
                        },
                        "message_err": { $in: [/^004/] },
                        'mode': { $in: ['1'] }
                    };

                    var sms_logs_blocked_conditions = {
                        "created_at": {
                            $gt: start_date,
                            $lte: end_date
                        },
                        "message_err": { $in: [/^507/,/^519/,/^005/] },
                        'mode': { $in: ['1'] }
                    };

                    var others_conditions = {
                      "created_at": {
                            $gt: start_date,
                            $lte: end_date
                        },
                        "message_status": { $nin: [/^SUBMIT/, /^PENDING/] },
                         $nor:[{
                            'message_err': { $in: [/^-01/,/^000/,/^001/,/^002/,/^004/,/^005/,/^006/,/^007/,/^008/,/^009/,/^010/,/^011/,/^012/,/^013/,/^014/,/^015/,/^016/,/^017/,/^019/,/^020/,/^021/,/^031/,/^032/,/^033/,/^034/,/^035/,/^036/,/^037/,/^051/,/^052/,/^053/,/^064/,/^066/,/^067/,/^068/,/^069/,/^072/,/^073/,/^080/,/^081/,/^083/,/^084/,/^085/,/^088/,/^089/,/^097/,/^098/,/^099/,/^100/,/^101/,/^102/,/^103/,/^104/,/^110/,/^120/,/^130/,/^140/,/^150/,/^160/,/^190/,/^192/,/^193/,/^194/,/^195/,/^196/,/^200/,/^210/,/^245/,/^255/,/^507/,/^508/,/^517/,/^519/,/^522/,/^537/,/^999/] }
                        }],
                        'mode': { $in: ['1'] }
                    };

                    var sms_logs_submit_conditions = {
                        "created_at": {
                            $gt: start_date,
                            $lte: end_date
                        },
                        "message_status": { $in: [/^SUBMIT/] },
                        'mode': { $in: ['1'] }
                    };

                    var test_sms_conditions = {
                      'created_at': {
                          $gt: start_date,
                          $lte: end_date
                      },
                      'mode':{ $in:['0'] }
                    };

                    var get_today_campaign_lbl = click_conditions;

                if (campaign_id>0) {
                  conditions['campaign_id']=parseInt(campaign_id);
                  chart_conditions['campaign_id'] = parseInt(campaign_id);
                  click_conditions['campaign_id'] = parseInt(campaign_id);     
                  sms_logs_in_conditions['campaign_id'] = parseInt(campaign_id);
                  sms_logs_nin_conditions['campaign_id'] = parseInt(campaign_id);
                  sms_logs_nin_dlr_pending_conditions['campaign_id'] = parseInt(campaign_id);
                  sms_logs_schedule_conditions['campaign_id'] = parseInt(campaign_id);
                  sms_logs_failed_conditions['campaign_id'] = parseInt(campaign_id);
                  sms_logs_expired_conditions['campaign_id'] = parseInt(campaign_id);
                  sms_logs_dnd_failed_conditions['campaign_id'] = parseInt(campaign_id);
                  sms_logs_blocked_conditions['campaign_id'] = parseInt(campaign_id);
                  others_conditions['campaign_id'] = parseInt(campaign_id);
                  sms_logs_submit_conditions['campaign_id'] = parseInt(campaign_id);
                  test_sms_conditions['campaign_id']=parseInt(campaign_id);
                  sms_logs_nin_rejection_conditions['campaign_id'] = parseInt(campaign_id);
                }
                
                if (content_label_id>0) {

                  conditions['campaign_content_label_id'] = parseInt(content_label_id);
                  chart_conditions['_id'] = parseInt(content_label_id);
                  click_conditions['content_label_id'] = parseInt(content_label_id);     
                  sms_logs_in_conditions['campaign_content_label_id'] = parseInt(content_label_id);
                  sms_logs_nin_conditions['campaign_content_label_id'] = parseInt(content_label_id);
                  sms_logs_nin_dlr_pending_conditions['campaign_content_label_id'] = parseInt(content_label_id);
                  sms_logs_schedule_conditions['campaign_content_label_id'] = parseInt(content_label_id);
                  sms_logs_failed_conditions['campaign_content_label_id'] = parseInt(content_label_id);
                  sms_logs_expired_conditions['campaign_content_label_id'] = parseInt(content_label_id);
                  sms_logs_dnd_failed_conditions['campaign_content_label_id'] = parseInt(content_label_id);
                  sms_logs_blocked_conditions['campaign_content_label_id'] = parseInt(content_label_id);  
                  others_conditions['campaign_content_label_id'] = parseInt(content_label_id); 
                  sms_logs_submit_conditions['campaign_content_label_id'] = parseInt(content_label_id); 
                  test_sms_conditions['campaign_content_label_id'] = parseInt(content_label_id);
                  sms_logs_nin_rejection_conditions['campaign_content_label_id'] = parseInt(content_label_id);
                }

                if (smpp_id) {
                   conditions['smpp_id'] = smpp_id; //sms_logs storing smpp_name
                   click_conditions['smpp_id'] = smpp_id;
                   chart_conditions['segments.smpp_id'] = smpp_id; //contentlabels storing smpp_id
                   sms_logs_in_conditions['smpp_id'] = smpp_id;
                   sms_logs_nin_conditions['smpp_id'] = smpp_id;
                    sms_logs_nin_dlr_pending_conditions['smpp_id'] = smpp_id;
                   sms_logs_failed_conditions['smpp_id'] = smpp_id;
                  sms_logs_expired_conditions['smpp_id'] = smpp_id;
                  sms_logs_dnd_failed_conditions['smpp_id'] = smpp_id;
                  sms_logs_blocked_conditions['smpp_id'] = smpp_id;
                  others_conditions['smpp_id'] = smpp_id;
                  sms_logs_submit_conditions['smpp_id'] = smpp_id;
                  test_sms_conditions['smpp_id'] = smpp_id;
                  sms_logs_nin_rejection_conditions['smpp_id'] = smpp_id;
                }

                if (sender_id) {
                  conditions['sender_id'] = sender_id; //sms_logs storing smpp_name
                  click_conditions['sender_id'] = sender_id;
                  chart_conditions['segments.sender_id'] = sender_id; //contentlabels storing smpp_id
                  sms_logs_in_conditions['sender_id'] = sender_id;
                  sms_logs_nin_conditions['sender_id'] = sender_id;
                  sms_logs_failed_conditions['sender_id'] = sender_id;
                  sms_logs_expired_conditions['sender_id'] = sender_id;
                  sms_logs_dnd_failed_conditions['sender_id'] = sender_id;
                  sms_logs_blocked_conditions['sender_id'] = sender_id;
                  others_conditions['sender_id'] = sender_id;
                  sms_logs_submit_conditions['sender_id'] = sender_id;
                  test_sms_conditions['sender_id'] = sender_id;
                  sms_logs_nin_rejection_conditions['sender_id'] = sender_id;
                }

                const campaign_today = await db.collection('campaign_content_labels').aggregate([
                    {
                       $match: chart_conditions
                    },

                    {
                        $project: {
                            "campaign_id": "$campaign_id",
                            "total_sender": "$total_sender",
                             "date_added": "$date_added" ,

                        }
                    },

                    {
                        "$group" : {
                                    
                          _id: {
                              campaign_id: "$campaign_id",                                    
                              count:{$sum:1}
                          },
                          total_sender: {$sum:"$total_sender"},
                          date_added: { "$push": "$date_added" },
                          single_sent_record: {  "$push": "$total_sender" }
                        }
                    }
                ]).toArray();

                const sms_logs = await db.collection('sms_logs').aggregate([
                    {
                        $match: conditions
                    },
                    {
                        $project: {
                            "year": {
                                $year: "$created_at"
                            },
                            "month": {
                                $month: "$created_at"
                            },
                            "day": {
                                $dayOfMonth: "$created_at"
                            },
                            mobile_no:"$mobile_no",
                            sms_content:"$sms_content",
                            smpp_id:"$smpp_id",
                            sender_id:"$sender_id",
                            campaign_content_label_id:"$campaign_content_label_id",
                            campaign_id:"$campaign_id",
                            message_status:"$message_status",
                            message_delivered_code:"$message_delivered_code",
                            message_err:'$message_err',
                            message_id:"$message_id",
                            message_submit_date:"$message_submit_date",
                            message_err:"$message_err",
                            updated_at:"$updated_at",
                            created_at:"$created_at",
                            id:"$_id",
                            //created_at:"$created_at",
                        }
                    },

                    {
                        $sort: {
                            "created_at": -1
                        }
                    },

                    {
                        $skip: (perPage * page) - perPage
                    },
                    {
                        $limit: perPage
                    },

                ]).toArray();


                const total_sms_logs = await db.collection('sms_logs').aggregate([

                    {
                        $match: conditions
                    },
                    {
                        $project: {
                            "year": {
                                $year: "$created_at"
                            },
                            "month": {
                                $month: "$created_at"
                            },
                            "day": {
                                $dayOfMonth: "$created_at"
                            },
                            mobile_no:"$mobile_no",
                            sms_content:"$sms_content",
                            sender_id:"$sender_id",
                            campaign_content_label_id:"$campaign_content_label_id",
                            campaign_id:"$campaign_id",
                            message_status:"$message_status",
                            message_id:"$message_id",
                            message_submit_date:"$message_submit_date",
                            message_err:"$message_err",
                            updated_at:"$updated_at",
                            id:"$_id",
                        }
                    },

                ]).toArray();

                
                const test_sms_count = await getSmsLogsConditionsCount(test_sms_conditions, sms_log_collection); 

                const total_campaign_content_today = await db.collection('campaign_content_labels').count(chart_conditions);

                const sms_logs_delivered_tbl = await getSmsLogsConditionsCount(sms_logs_in_conditions, sms_log_collection);

                const sms_logs_un_delivered_tbl = await getSmsLogsConditionsCount(sms_logs_nin_conditions, sms_log_collection);

                const sms_count = await db.collection('sms_logs').count();

                var total_campaign_today = 0;
                var total_sender_today = 0;
                var total_dates_obj_list = helper.getDateArray(start_date, end_date);
                var total_dates_list = Object.keys(total_dates_obj_list);
                
                var total_dates_sent_obj_list = helper.getDateArray(start_date, end_date);

                const sms_logs_un_dlr_pending_tbl = await getSmsLogsConditionsCount(sms_logs_nin_dlr_pending_conditions, sms_log_collection); 

                const sms_logs_schedul_tbl = await getSmsLogsConditionsCount(sms_logs_schedule_conditions, sms_log_collection);

                const sms_logs_failed_tbl = await getSmsLogsConditionsCount(sms_logs_failed_conditions, sms_log_collection);

                const sms_logs_expired_tbl = await getSmsLogsConditionsCount(sms_logs_expired_conditions, sms_log_collection);

                const sms_logs_dnd_failed_tbl = await getSmsLogsConditionsCount(sms_logs_dnd_failed_conditions, sms_log_collection);

                 const sms_logs_blckd_tbl = await getSmsLogsConditionsCount(sms_logs_blocked_conditions, sms_log_collection);

                 const sms_logs_others_tbl = await getSmsLogsConditionsCount(others_conditions, sms_log_collection);

                 const sms_logs_submit_tbl = await getSmsLogsConditionsCount(sms_logs_submit_conditions, sms_log_collection);

                 const sms_logs_rejection_tbl = await getSmsLogsConditionsCount(sms_logs_nin_rejection_conditions, sms_log_collection);

                var total_dates_delvrd_list = helper.getDateArray(start_date, end_date); 

                var total_clicks_list = helper.getDateArray(start_date, end_date);

                var total_sent_camp_ids = [];

                if(campaign_today.length>0){
                  
                    for (var i in campaign_today) {

                        total_campaign_today += campaign_today[i]._id.count;
                        total_sender_today += campaign_today[i].total_sender;
                        total_sent_camp_ids.push(campaign_today[i]._id.campaign_id);

                        for (var j in campaign_today[i].date_added) {
                          var date_from_db = helper.getCurrentDay(campaign_today[i].date_added[j]) + '-' + helper.getCurrentMonth(campaign_today[i].date_added[j]) + '-' + helper.getCurrentYear(campaign_today[i].date_added[j]);
                          total_dates_sent_obj_list[date_from_db] = total_dates_sent_obj_list[date_from_db]+campaign_today[i].single_sent_record[j];
                        }  
                    }     
                }

                /*** test sms count */
                var total_today_test_sms = 0;
                if (test_sms_count.length > 0) {
                  for (var l in test_sms_count) {
                        total_sender_today += test_sms_count[l].count;
                        total_today_test_sms += test_sms_count[l].count;
                    } 
                }
                /*** end test sms count */

                var total_dates_val_list = Object.values(total_dates_sent_obj_list); 

                if ((!campaign_id > 0 && !content_label_id > 0) || (isNaN(campaign_id) && isNaN(content_label_id))) {
                  if (total_sent_camp_ids.length > 0) {
                     click_conditions['campaign_id'] = {$in: total_sent_camp_ids}
                  }
                 
                }



                if (total_sent_camp_ids.length > 0) {              

                    var click_tbl = await db.collection('clicks').aggregate([

                        {
                            $match: click_conditions
                        },

                        {
                            $project: {
                                "year": {
                                    $year: "$created_at"
                                },
                                "month": {
                                    $month: "$created_at"
                                },
                                "day": {
                                    $dayOfMonth: "$created_at"
                                }
                            }
                        },                  
                        // Group by year, month and day and get the count
                        {
                            $group: {
                                _id: {
                                    year: "$year",
                                    month: "$month",
                                    day: "$day"
                                },
                                "count": {
                                    $sum: 1
                                }
                            }
                        }
                    ]).toArray();
                
                }else{
                   var click_tbl = [];
                }

                var total_today_clicks = 0;

                if(click_tbl.length>0){

                    for (var j in click_tbl) {

                        total_today_clicks += click_tbl[j].count;
                        var clicked_year = click_tbl[j]._id.year;
                        var clicked_day = click_tbl[j]._id.day;
                        var clicked_month = click_tbl[j]._id.month;
                        var clicked_date_db = clicked_month + '-' + clicked_day + '-' + clicked_year;
                        var get_click_date = helper.getCurrentDay(clicked_date_db) + '-' + helper.getCurrentMonth(clicked_date_db) + '-' + helper.getCurrentYear(clicked_date_db);
                        total_clicks_list[get_click_date] = total_clicks_list[get_click_date]+click_tbl[j].count;
                    }                    
                }


                var total_today_delivered = 0;

                if(sms_logs_delivered_tbl.length>0){

                    for (var k in sms_logs_delivered_tbl) {
                        
                        total_today_delivered += sms_logs_delivered_tbl[k].count;
                        var year = sms_logs_delivered_tbl[k]._id.year;
                        var day = sms_logs_delivered_tbl[k]._id.day;
                        var month = sms_logs_delivered_tbl[k]._id.month;
                        var delivered_date_db = month + '-' + day + '-' + year;
                        var get_date = helper.getCurrentDay(delivered_date_db) + '-' + helper.getCurrentMonth(delivered_date_db) + '-' + helper.getCurrentYear(delivered_date_db); 
                        total_dates_delvrd_list[get_date] = total_dates_delvrd_list[get_date]+sms_logs_delivered_tbl[k].count;
                    }                    
                }

                var total_today_non_delivered = 0;

                if(sms_logs_un_delivered_tbl.length>0){

                    for (var l in sms_logs_un_delivered_tbl) {

                        total_today_non_delivered += sms_logs_un_delivered_tbl[l].count;
                    }                    
                }


                var total_today_dlr_pending = 0;

                if(sms_logs_un_dlr_pending_tbl.length>0){

                    for (var l in sms_logs_un_dlr_pending_tbl) {

                        total_today_dlr_pending += sms_logs_un_dlr_pending_tbl[l].count;
                    }                    
                }

                /* sms rejection count */
                var total_today_rejected = 0;

                if(sms_logs_rejection_tbl.length>0){

                    for (var l in sms_logs_rejection_tbl) {

                        total_today_rejected += sms_logs_rejection_tbl[l].count;
                    }                    
                }
                /* sms rejection count end */
                var total_schedule_count = 0;

                if(sms_logs_schedul_tbl.length>0){

                    for (var l in sms_logs_schedul_tbl) {

                        total_schedule_count += sms_logs_schedul_tbl[l].count;
                    }                    
                }

                var total_today_failed = 0;

                if(sms_logs_failed_tbl.length>0){

                    for (var l in sms_logs_failed_tbl) {

                        total_today_failed += sms_logs_failed_tbl[l].count;
                    }                    
                }

                 var total_today_expired = 0;

                if(sms_logs_expired_tbl.length>0){

                    for (var l in sms_logs_expired_tbl) {

                        total_today_expired += sms_logs_expired_tbl[l].count;
                    }                    
                }

                var total_today_dnd_failed = 0;

                if(sms_logs_dnd_failed_tbl.length>0){

                    for (var l in sms_logs_dnd_failed_tbl) {

                        total_today_dnd_failed += sms_logs_dnd_failed_tbl[l].count;
                    }                    
                }

                 var total_today_blocked = 0;

                if(sms_logs_blckd_tbl.length>0){

                    for (var l in sms_logs_blckd_tbl) {

                        total_today_blocked += sms_logs_blckd_tbl[l].count;
                    }                    
                }

                var total_today_others = 0;

                if(sms_logs_others_tbl.length>0){

                    for (var l in sms_logs_others_tbl) {

                        total_today_others += sms_logs_others_tbl[l].count;
                    }                    
                }

                var total_today_submit = 0;

                if(sms_logs_submit_tbl.length>0){

                    for (var l in sms_logs_submit_tbl) {

                        total_today_submit += sms_logs_submit_tbl[l].count;
                    }                    
                }

                
                res.send({
                    
                    activity_logs: req.activity_logs,
                    activity_logs_count: req.activity_logs_count,
                    total_campaign_today:total_campaign_today,
                    total_campaign_content_today:total_campaign_content_today,
                    total_sender_today:total_sender_today,
                    total_today_clicks:total_today_clicks,
                    total_today_delivered:total_today_delivered,
                    total_today_non_delivered:total_today_non_delivered,
                    total_dates_list: total_dates_list,
                    total_dates_val_list: total_dates_val_list,
                    total_delivered_val_list:Object.values(total_dates_delvrd_list),
                    total_clicks_list: Object.values(total_clicks_list),
                    total_today_dlr_pending:total_today_dlr_pending,
                    total_schedule_count:total_schedule_count,
                    sms_count: sms_count,
                    total_today_failed: total_today_failed,
                    total_today_expired: total_today_expired,
                    total_today_dnd_failed: total_today_dnd_failed,
                    total_today_blocked: total_today_blocked,
                    total_today_others: total_today_others,
                    total_today_submit: total_today_submit,
                    total_today_test_sms: total_today_test_sms,
                    total_today_rejected: total_today_rejected
                });

                client.close();

            })();

    }).catch(err => {
      
      if (err) {
        res.send({ success: false, message: 'query error', error: 'please check your internet connection' });
                return;
      }
    });
});

router.post('/smpp_report_data', activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {
    let perPage = 20;
    let page = req.body.page_no || 1;

    var current = req.body.page_no;
    var fromdate = req.body.fromdate;
    var todate = req.body.todate;

    var start_date = new Date(fromdate);
    start_date.setHours(0, 0, 0, 0);


    var end_date = new Date(todate);
    var add_one_day_to_date = new Date(end_date.getTime() + 1 * 24 * 60 * 60000);


    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);

            (async () => {

                let count;

                var smpp_id = req.body.smpp_id;
                var sender_id = req.body.sender_id;
                var smpp_name = req.body.smpp_name;
                var sender_name = req.body.sender_name;


                if(smpp_id>0&&sender_id!=''){

                    var conditions = {
                        'created_at': {
                            $gt: start_date,
                            $lte: add_one_day_to_date
                        },
                        'smpp_id':smpp_name,
                        'sender_id':sender_name
                    };

                }else if(smpp_id>0){

                    var conditions = {
                        'created_at': {
                            $gt: start_date,
                            $lte: add_one_day_to_date
                        },
                        'smpp_id':smpp_name
                    };

                }else{

                    var conditions = {
                        'created_at': {
                            $gt: start_date,
                            $lte: add_one_day_to_date
                        }
                    };

                }

                const sms_logs = await db.collection('sms_logs').aggregate([

                    {
                        $match: conditions
                    },
                    {
                        $project: {
                            "year": {
                                $year: "$created_at"
                            },
                            "month": {
                                $month: "$created_at"
                            },
                            "day": {
                                $dayOfMonth: "$created_at"
                            },
                            smpp_id:"$smpp_id",
                            message_status:"$message_status",
                            message_sent_date:"$message_sent_date",
                            sender_id: '$sender_id',
                        }
                    },
                    {
                      $group: {

                          _id: {
                              smpp_id: '$smpp_id',
                              sender_id: '$sender_id',
                              message_status: '$message_status',                              
                              year: "$year",
                              month: "$month",
                              day: "$day"
                          },
                          count:{$sum:1}
                      }
                    }
                ]).toArray(); 

                const sms_count = await db.collection('sms_logs').count();

                var table_content = '<table class="table table-bordered">';
                table_content += '<thead><tr style="background: #424964c7;color: #fff;"> <th> No </th><th>SMPP</th> <th>Sender ID</th> <th>Total Sent</th> <th>Status</th><th> Message Sent Date </th></tr>';
                table_content += '</thead>';
                table_content += '<tbody>';


                if (sms_logs.length > 0) {

                    for (var i = 0; i < sms_logs.length; i++) {

                        var smpp = sms_logs[i]._id.smpp_id;
                        var sender_id = sms_logs[i]._id.sender_id;
                        var message_status = sms_logs[i]._id.message_status;
                        var total_count = sms_logs[i].count;
                        var message_sent_date = sms_logs[i]._id.day+'-'+sms_logs[i]._id.month+'-'+sms_logs[i]._id.year;
                       
                        if(message_status=='PENDING'){

                            var status_span = '<span class="badge badge-warning">'+message_status+'</span>';

                        }else if(message_status=='DELIVRD'){
                            var status_span = '<span class="badge badge-success">Delivered</span>';
                            var message_reason='Delivered';

                        }else{
                            var status_span = '<span class="badge badge-danger">'+message_status+'</span>';
                           
                        }

                        table_content += '<tr>';
                        table_content += '<td>'+ (i + 1) +'</td>';
                        table_content += '<td>'+smpp+'</td>';
                        table_content += '<td>'+sender_id+ '</td>';
                        table_content += '<td>'+total_count+ '</td>';
                        table_content += '<td>'+status_span+'</td>';
                        table_content += '<td>'+message_sent_date+'</td>';
                        table_content += '</tr>';

                    }

                }else{

                    table_content += '<tr><td colspan="19"><img src="/public/images/no_result.gif"  style="height: 250px;width: 250px;display: block;margin: auto;"></td></tr>';                    
                }

                table_content += '</tbody>';
                table_content += '</table>';


                const table_count = sms_logs.length;

                var pages = Math.ceil(table_count / perPage);

                var pagination_content = '';
                pagination_content += '<div class="row" style="float: right;margin-top: 10px;">';

                if (pages > 0) {

                    pagination_content += '<nav class="mx-auto">';
                    pagination_content += '<ul class="pagination">';

                    if (current == 1) {
                        pagination_content += '<li class="page-item disabled"><a class="page-link" href="#">First</a></li>';
                    } else {
                        pagination_content += '<li class="page-item"><a class="page-link" href="#" data-value="1">First</a></li>';
                    }

                    var i = (Number(current) > 5 ? Number(current) - 4 : 1)
                    if (i !== 1) {

                        pagination_content += '<li class="page-item disabled"><a class="page-link" href="#">...</a></li>';
                    }

                    for (; i <= (Number(current) + 4) && i <= pages; i++) {

                        if (i == current) {
                            pagination_content += '<li class="page-item active"><a class="page-link" href="javascript:void(0)" data-value="' + i + '">' + i + '</a></li>';
                        } else {

                            pagination_content += '<li class="page-item"><a class="page-link" href="javascript:void(0)" data-value="' + i + '">' + i + '</a></li>';
                        }

                        if (i == Number(current) + 4 && i < pages) {
                            pagination_content += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">...</a></li>';

                        }
                    }
                    if (current == pages) {
                        pagination_content += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">Last</a></li>';
                    } else {
                        pagination_content += '<li class="page-item"><a class="page-link" href="javascript:void(0)" data-value="' + i + '">Last</a></li>';
                    }
                    pagination_content += '</ul></nav>';
                }

                pagination_content += '</div>'

                if (table_count > 0) {
                    pagination_content += '<p>Showing ' + current + '- <b>' + current * perPage + '</b> of <b class="bold">' + table_count + ' items';

                }


                res.send({
                    pagination_content: pagination_content,
                    table_content: table_content,
                    activity_logs: req.activity_logs,
                    activity_logs_count: req.activity_logs_count
                });

                client.close();

            })();


        });
});

router.get('/karix_click_report',ensureAuthenticated,  roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {
    
    return res.render('report/karix_click_report',{ 
        user: req.user,
        activity_logs: req.activity_logs,
        activity_logs_count: req.activity_logs_count
    });


});

router.post('/karix_click_api_report', activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

    let perPage = 20;
    let page = req.body.page_no || 1;
    let category_id = req.body.category_id;
    let campaign_id = parseInt(req.body.campaign_id);
    let content_label_id = parseInt(req.body.content_label_id);
    var current = req.body.page_no;

    var fromdate = req.body.fromdate;
    var todate = req.body.todate;
    var search_campaign=req.body.search_campaign;

    var start_date = new Date(fromdate);
    start_date.setHours(0, 0, 0, 0);


    var end_date = new Date(todate);
    var add_one_day_to_date = new Date(end_date.getTime() + 1 * 24 * 60 * 60000);


    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);

            (async () => {

                let count;

                if(search_campaign!=''){

                    var conditions = {
                        'created_at': {
                            $gt: start_date,
                            $lte: add_one_day_to_date
                        },
                        'Campaign':{$regex:'^'+search_campaign+'$','$options' : 'i'}
                    };

                }else{

                    var conditions = {
                        'created_at': {
                            $gt: start_date,
                            $lte: add_one_day_to_date
                        }
                    };

                }
                

                const clicks = await db.collection('karix_clickers').aggregate([

                    {
                        $match: conditions
                    },
                    {
                        $project: {
                            "year": {
                                $year: "$created_at"
                            },
                            "month": {
                                $month: "$created_at"
                            },
                            "day": {
                                $dayOfMonth: "$created_at"
                            },
                            mobile:"$mobile",
                            city:"$city",
                            IP:"$IP",
                            Campaign:"$Campaign",
                            Device:"$Device",
                            Operating:"$Operating",
                            Platform:"$Platform",
                            Browser:"$Browser",
                            BrowVersion:"$BrowVersion",
                            Engine:"$Engine",
                            Ename:"$Ename",
                            Country:"$Country",
                            Region:"$Region",
                            Latitude:"$Latitude",
                            Longitude:"$Longitude",
                            DeviceMake:"$DeviceMake",
                            clickeddate:"$clickeddate",
                            clickedtime:"$clickedtime",
                            MID:"$MID"

                        }
                    },

                    {
                        $skip: (perPage * page) - perPage
                    },
                    {
                        $limit: perPage
                    },
                    {
                        $sort: {
                            "_id.day": 1,
                            "_id.month": 1,
                            "_id.year": 1
                        }
                    },

                ]).toArray();


                const total_clicks = await db.collection('karix_clickers').aggregate([

                    {
                        $match: conditions
                    },

                    {
                        $group: {
                            _id: {
                                year: "$year",
                                month: "$month",
                                day: "$day"
                            },
                            count: {
                                $sum: 1
                            }
                        },
                    },

                ]).toArray();


                var total_sum_of_click = 0;
                if (total_clicks.length > 0) {

                    for (var i = 0; i < total_clicks.length; i++) {
                        total_sum_of_click += total_clicks[i].count;
                    }
                }

                var table_content = '<p id="total_clicks" style="color: #002d56;float: right;"><b>Total Clicks: ' + total_sum_of_click + '</b></p>';

                table_content += '<table class="table table-bordered">';
                table_content += '<thead><tr style="background: #424964c7;color: #fff;"> <th> No </th><th>Clicked Date</th> <th>Clicked Time</th> <th>Campaign Name</th> <th>Campaign Type MID</th><th> Mobile Number </th><th> Mobile Without 91 </th><th> IP Device type</th> <th> Operating System </th> <th> Platform version </th><th> Browser Name  </th> <th>Version Engine</th>  <th>Version Engine Name</th>  <th>Country</th>  <th>Region</th>  <th>City</th> <th>Latitude</th> <th>Longitude</th> <th>Device Make</th></tr>';
                table_content += '</thead>';
                table_content += '<tbody>';


                if (clicks.length > 0) {

                    for (var i = 0; i < clicks.length; i++) {

                        var date = clicks[i].day + '-' + clicks[i].month + '-' + clicks[i].year;
                        var click_count = 1; 

                        var mobile = clicks[i].mobile;
                        var city = clicks[i].city;
                        var IP = clicks[i].IP;
                        var Campaign = clicks[i].Campaign;
                        var Device = clicks[i].Device;
                        var Operating = clicks[i].Operating;
                        var Platform = clicks[i].Platform;
                        var Browser = clicks[i].Browser;
                        var BrowVersion = clicks[i].BrowVersion;
                        var Engine = clicks[i].Engine;
                        var Ename = clicks[i].Ename;
                        var Country = clicks[i].Country;
                        var Region = clicks[i].Region;
                        var Latitude = clicks[i].Latitude;
                        var Longitude = clicks[i].Longitude;
                        var DeviceMake = clicks[i].DeviceMake;
                        var clickeddate = clicks[i].clickeddate;
                        var clickedtime = clicks[i].clickedtime;
                        var MID = clicks[i].MID;

                        table_content += '<tr>';
                        table_content += '<td>' + (i + 1) + '</td>';
                        table_content += '<td>' +clickeddate+ '</td>';
                        table_content += '<td>' +clickedtime+ '</td>';
                        table_content += '<td>' +Campaign+ '</td>';
                        table_content += '<td>' +MID+ '</td>';
                        table_content += '<td>' +mobile+ '</td>';
                        table_content += '<td></td>';
                        table_content += '<td>' + IP + '</td>';
                        table_content += '<td>' +Device+ '</td>';
                        table_content += '<td>' +Operating+ '</td>';
                        table_content += '<td>' +Platform+ '</td>';
                        table_content += '<td>' + Browser + '</td>';
                        table_content += '<td>' + BrowVersion + '</td>';
                        table_content += '<td>' +Country  + '</td>';
                        table_content += '<td>' +Region  + '</td>';
                        table_content += '<td>' + city + '</td>';
                        table_content += '<td>' + Latitude + '</td>';
                        table_content += '<td>' + Longitude + '</td>';
                        table_content += '<td>' + DeviceMake + '</td>';
                        table_content += '</tr>';

                    }

                }else{

                    table_content += '<tr><td colspan="19"><img src="/public/images/no_result.gif"  style="height: 250px;width: 250px;display: block;margin: auto;"></td></tr>';                    
                }

                table_content += '</tbody>';
                table_content += '</table>';


                const table_count = total_sum_of_click;

                var pages = Math.ceil(table_count / perPage);

                var pagination_content = '';
                pagination_content += '<div class="row" style="float: right;margin-top: 10px;">';

                if (pages > 0) {

                    pagination_content += '<nav class="mx-auto">';
                    pagination_content += '<ul class="pagination">';

                    if (current == 1) {
                        pagination_content += '<li class="page-item disabled"><a class="page-link" href="#">First</a></li>';
                    } else {
                        pagination_content += '<li class="page-item"><a class="page-link" href="#" data-value="1">First</a></li>';
                    }

                    var i = (Number(current) > 5 ? Number(current) - 4 : 1)
                    if (i !== 1) {

                        pagination_content += '<li class="page-item disabled"><a class="page-link" href="#">...</a></li>';
                    }

                    for (; i <= (Number(current) + 4) && i <= pages; i++) {

                        if (i == current) {
                            pagination_content += '<li class="page-item active"><a class="page-link" href="javascript:void(0)" data-value="' + i + '">' + i + '</a></li>';
                        } else {

                            pagination_content += '<li class="page-item"><a class="page-link" href="javascript:void(0)" data-value="' + i + '">' + i + '</a></li>';
                        }

                        if (i == Number(current) + 4 && i < pages) {
                            pagination_content += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">...</a></li>';

                        }
                    }
                    if (current == pages) {
                        pagination_content += '<li class="page-item disabled"><a class="page-link" href="javascript:void(0)">Last</a></li>';
                    } else {
                        pagination_content += '<li class="page-item"><a class="page-link" href="javascript:void(0)" data-value="' + i + '">Last</a></li>';
                    }
                    pagination_content += '</ul></nav>';
                }

                pagination_content += '</div>'

                if (table_count > 0) {
                    pagination_content += '<p>Showing ' + current + '- <b>' + current * perPage + '</b> of <b class="bold">' + table_count + ' items';

                }


                res.send({
                    pagination_content: pagination_content,
                    table_content: table_content,
                    activity_logs: req.activity_logs,
                    activity_logs_count: req.activity_logs_count
                });

                client.close();

            })();


        });
});


/*
  name: activity_report_pagination
  method: post
  description: Fetching activity report data and pagination data 
*/

router.post('/activity_report_pagination', (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    const db                  = client.db(database_name);
    (async () => {

      var perPage             = constants.PER_PAGE_LIST_COUNT;
      var page                = parseInt(req.body.page_no) || 1;
      var current             = req.body.page_no;
      var activity_logs_count = await db.collection('activity_logs').find({}).count(); 

      var offset              = parseInt((perPage * page) - perPage); 
      var pages               = Math.ceil(activity_logs_count / perPage);

      const activity_logs_details = await db.collection('activity_logs').find().skip((perPage * page) - perPage).limit(perPage).sort({'_id':-1}).toArray();

      var content_html        = '<table class="table table-striped table-bordered">';
      content_html            += '<thead><tr><th><input type="checkbox" id="checkAll" onchange="checkAll(this)"></th><th>ID</th><th>User Name</th><th>Action</th><th>Message</th><th>Total Upload Contacts</th><th>Status</th><th>Created at</th></tr>';
      content_html            += '</thead>';
      content_html            += '<tbody>';

      if (activity_logs_details.length > 0) {
        for (var i = 0; i < activity_logs_details.length; i++) {
          var tr_class        = '<tr>';
          if (activity_logs_details[i].is_read == 0) {
            tr_class          = '<tr class="addfont">';
          } 
          var s_no            = offset + i + 1 ; 
          var date_format     = helper.getCurrentDateTimeampm(activity_logs_details[i].created_at); 
          content_html        += tr_class;
          content_html        += '<td><input type="checkbox" id="checkItem" name="check"></td>';
          content_html        += '<td style="display:none;" class="lo_id"><input type="hidden" name="log_id" class="log_id" value='+ activity_logs_details[i]._id +'></td>';
          content_html        += '<td>'+ s_no + '</td>';
          content_html        += '<td>'+ activity_logs_details[i].user_name + '</td>';
          if (activity_logs_details[i].type == 'Import' || activity_logs_details[i].type == 'Login') { 
            content_html      += '<td><label class="badge badge-success">'+ activity_logs_details[i].type + '</label></td>';
          } else {
            content_html      += '<td><label class="badge badge-danger">'+ activity_logs_details[i].type+'</label></td>';
          } 
          
          content_html        += '<td>'+ activity_logs_details[i].description;

          if (activity_logs_details[i].type == 'Save') { 
            content_html      += '<p><b>(Total '+ isValueNull(activity_logs_details[i].total_contacts) + ' records have been exported successfully)</b></p>';
          } 

          if (activity_logs_details[i].type == 'Import') { 
            content_html      += ' <br><span style="font-size: 11px;">(<b style="color:#ff6d8c;">Filename:</b><b>' + activity_logs_details[i].file_name +' </b>)</span>'  
          } else if (activity_logs_details[i].type == 'Export') {
            content_html      += ' <br><span style="font-size: 11px;">(<b style="color:#ff6d8c;">Total Duplicate contacts is :</b><b>' + activity_logs_details[i].duplicate_contacts +' </b>)</span>'  
          }

          content_html        += '</td>';

          if(activity_logs_details[i].type == 'Import' || activity_logs_details[i].type == 'Suppress') {
            content_html      += '<td>'+ isValueNull(activity_logs_details[i].total_contacts) + '</td>';
          } else {
            content_html      += '<td>'+ 0 + '</td>';
          }
          
          if (activity_logs_details[i].is_read == 1) { 
            content_html      += '<td class="is_read"><label class="badge badge-success">Read</label></td>';
          } else {
            content_html      += '<td class="is_read"><label class="badge badge-danger">Unread</label></td>';
          } 

          content_html        += '<td>'+ date_format + '</td>';
          content_html        += '</tr>';
        } //end for
      } //endif
      content_html            += '</tbody>';
      content_html            += '</table>';
      var pagination_data     = pagination_helper.createPagination(pages, current, perPage, activity_logs_count, offset); 
      res.send({
        pagination_content: pagination_data,
        content_html: content_html
      });
      client.close();
    })(); // async end
  }); //mongoclient
});

/*
  name: server_report_pagination
  method: post
  description: Fetching server report data and pagination data 
*/

router.post('/server_report_pagination', (req, res, next) => {
    
    MongoClient.connect(db, db_options).then(client => {
            const db = client.db(database_name);
            (async () => {
              //var server_logs_details = [];

              var perPage = constants.PER_PAGE_LIST_COUNT;
              var page = parseInt(req.body.page_no) || 1;

               var current = req.body.page_no;
               var server_logs_count = await db.collection('server_logs').find({}).count(); //console.log(server_logs_count)

               var offset = parseInt((perPage * page) - perPage); 
                var pages = Math.ceil(server_logs_count / perPage);
                //var content_html = '';
               const server_logs_details = await db.collection('server_logs').find()
                                         .skip((perPage * page) - perPage)
                                         .limit(perPage)
                                         .sort({'_id':-1})
                                         .toArray(); 

              var content_html = '<table class="table table-striped table-bordered">';
                  content_html += '<thead><tr><th>ID</th><th>Action</th><th>Code</th><th>Message</th><th>Quantity</th><th>Created at</th></tr>';
                  content_html += '</thead>';
                  content_html += '<tbody>';
               if (server_logs_details.length > 0) {
                 for (var i = 0; i < server_logs_details.length; i++) {
                   var s_no = offset + i + 1 ; 
                   var date_format= helper.getCurrentDateTimeForReport(server_logs_details[i].created_at, '', '-',':'); 
                   content_html += '<tr>';
                   content_html += '<td>'+ s_no + '</td>';
                   content_html += '<td>'+ server_logs_details[i].Action + '</td>';
                   content_html += '<td>'+ server_logs_details[i].Code + '</td>';
                   content_html += '<td>'+ server_logs_details[i].Message + '</td>';
                   content_html += '<td>'+ server_logs_details[i].count + '</td>';
                   content_html += '<td>'+ date_format + '</td>';
                   content_html += '</tr>';
                 } //end for
               } //endif
                var pagination_data = pagination_helper.createPagination(pages, current, perPage, server_logs_count, offset); 
                res.send({
                  pagination_content: pagination_data,
                  content_html: content_html
                });

                client.close();
            })();

  });
});



router.post('/mark_status_by_ids', (req, res, next) => {
    
    MongoClient.connect(db, db_options).then(client => {
        const db = client.db(database_name);
        (async () => {
            var update_ids = req.body.checked_ids; 
            var object_ids = [];
            for (i=0; i < update_ids.length;i++) {
              var o_id = new ObjectId(update_ids[i]);
              object_ids.push(o_id);
            }

            const activity_log_count = await db.collection('activity_logs').update({'_id':{$in:object_ids}, 'is_read':0},  {$set:{'is_read':1}}, { multi: true, upsert: false}).then((obj) => {  
                var total_updated = obj.result.nModified;  
                res.send({
                    'msg': 'success',
                    'status': 200,
                    total_updated: total_updated
                });
              })
              .catch((err) => {

                  res.send({
                      'msg': err,
                      'status': 400
                  });
              });


            client.close();
        })();

  });
});



async function getSmsLogsConditionsCount(conditions, sms_log_collection) {

  var total_conditions_count = await sms_log_collection.aggregate([

                    {
                        $match:conditions   
                    },

                    {
                        $project: {
                            "year": {
                                $year: "$created_at"
                            },
                            "month": {
                                $month: "$created_at"
                            },
                            "day": {
                                $dayOfMonth: "$created_at"
                            }
                        }
                    },                  
                    // Group by year, month and day and get the count
                    {
                        $group: {
                            _id: {
                                year: "$year",
                                month: "$month",
                                day: "$day"
                            },
                            "count": {
                                $sum: 1
                            }
                        }
                    }
                ]).toArray();

  return total_conditions_count;
}

function downloadCsv(posts, req, res) {

    // adding appropriate headers, so browsers can start downloading
    // file as soon as this request starts to get served
    res.setHeader('Content-Type', 'text/csv');
    res.setHeader('Content-Disposition', 'attachment; filename=\"' + 'download-' + Date.now() + '.csv\"');
    res.setHeader('Cache-Control', 'no-cache');
    res.setHeader('Pragma', 'no-cache');

    // ta-da! this is cool, right?
    // stringify return a readable stream, that can be directly piped
    // to a writeable stream which is "res" (the response object from express.js)
    // since res is an abstraction over node http's response object which supports "streams"
    stringify(posts, {
            header: true
        })
        .pipe(res);
};

function getIndianDateFormat(date,type) {

    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hours = d.getHours(),
        minutes = d.getMinutes(),
        seconds = d.getSeconds();
    
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var hours = d.getHours();
    var minutes = d.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;


    if(type=='date_and_time')
    {
        var date_format=day+'-'+month+'-'+year+' '+strTime;
    }else if(type=='date'){
        var date_format=day+'-'+month+'-'+year;
    }else if(type=='time'){
        var date_format=strTime;
    }else{
        var date_format=day+'-'+month+'-'+year+' '+strTime;
    }  

    return date_format;
    
}


router.post('/smpp_report_table', ensureAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {
  let perPage     = constants.PER_PAGE_LIST_COUNT;
  let page        = req.body.page_no || 1;
  var current     = req.body.page_no;

  var fromdate    = req.body.fromdate;
  var todate      = req.body.todate;

  var start_date  = new Date(fromdate);
  start_date.setHours(0, 0, 0, 0);

  var end_date    = new Date(todate);
  end_date.setHours(23, 59, 59, 59);

  var limit_value = parseInt(req.body.limit_val); 
  var offset      = parseInt((perPage * page) - perPage);

  MongoClient.connect(db, db_options).then(client => {
    const db      = client.db(database_name);
    const sms_log_collection = db.collection('sms_logs');
    (async () => {

      let count;

      var campaign_id      = parseInt(req.body.campaign_id);
      var content_label_id = parseInt(req.body.content_label_id);
      var filter_type      = req.body.filter_type;
      var smpp_id          = req.body.smpp_id;
      var sender_id        = req.body.sender_id;
      var smpp_name        = req.body.smpp_name;
      var sender_name      = req.body.sender_name;
      var status_code      = 400;
      var sorder_order     = parseInt(req.body.sort_order);
      var sort_clmn_name   = req.body.sort_clmn_name;
      var sort             = {
         "created_at": -1
      }

      if (sort_clmn_name != '' && sorder_order != '') {
        sort = {
          [sort_clmn_name] : sorder_order
        }
      }

      var conditions = {
        'created_at': {
          $gt: start_date,
          $lte: end_date
        },
        'mode': { $in: ['1'] },
        $nor:[{
          'message_status': { $in: [/^SCHEDULE CANCELLED/] },
        }]
      };

      if (campaign_id > 0) {
        conditions['campaign_id'] = parseInt(campaign_id);
      }
                
      if (content_label_id > 0) {
        conditions['campaign_content_label_id'] = parseInt(content_label_id);
      }

      if (smpp_id) {
        conditions['smpp_id'] = smpp_id; //sms_logs storing smpp_name
      }

      if (sender_id) {
        conditions['sender_id'] = sender_id; //sms_logs storing smpp_name
      }
 
        var searched = req.body.search;
        var is_searched = req.body.is_search; 
        if (is_searched == 'search' && searched != '') {
          conditions['mobile_no'] = new RegExp(searched);
        }

        var sms_logs_query = [];
        sms_logs_query.push(
          {
            $match: conditions
          },
          {
            $project: {
              "year": {
                $year: "$created_at"
              },
              "month": {
                $month: "$created_at"
              },
              "day": {
                $dayOfMonth: "$created_at"
              },
              mobile_no:"$mobile_no",
              sms_content:"$sms_content",
              smpp_id:"$smpp_id",
              sender_id:"$sender_id",
              campaign_content_label_id:"$campaign_content_label_id",
              campaign_id:"$campaign_id",
              message_status:"$message_status",
              message_delivered_code:"$message_delivered_code",
              message_err:'$message_err',
              message_id:"$message_id",
              message_submit_date:"$message_submit_date",
              message_err:"$message_err",
              updated_at:"$updated_at",
              created_at:"$created_at",
              id:"$_id",
              //created_at:"$created_at",
            }
          },

          {
            $sort: sort
          }
        );

                if (!isInValid(limit_value)) {
                  sms_logs_query.push({
                    $limit: limit_value
                  });
                }

                const sms_logs = await db.collection('sms_logs').aggregate(sms_logs_query).toArray(); 


                var table_content = '<table class="table table-bordered" style="text-align: center;" id="sort_table" class="sort_table">';
                table_content += '<thead><tr style="background: #424964c7;color: #fff;"> <th> No </th><th class="campaign_name">Campaign Name<span class="sorting_asc_icon mdi mdi-sort-ascending" id="campaign_name"></span></th><th class="camp_cont_lbl">Campaign Content Label<span class="sorting_asc_icon mdi mdi-sort-ascending" id="camp_cont_lbl"></span></th><th>SMPP</th><th>Sender</th><th>Destination</th> <th class="message_submit_date">Submit Date <span class="sorting_asc_icon mdi mdi-sort-ascending" id="message_submit_date"></span></th> <th class="message_submit_time">Submit Time<span class="sorting_asc_icon mdi mdi-sort-ascending" id="message_submit_time"></span></th><th> Delivery Date </th> <th>Delivery Time</th><th> Message</th> <th> Status </th> <th> Reason </th> <th> MID </th></tr>';
                table_content += '</thead>';
                table_content += '<tbody>';

                if (sms_logs.length > 0) {
                    status_code = 200;
                    //sms_logs.reverse();

                    for (var i = 0; i < sms_logs.length; i++) {
                        var smpp_id                   = sms_logs[i].smpp_id;
                        var sender_id                 = sms_logs[i].sender_id;
                        var mobile_no                 = sms_logs[i].mobile_no;
                        var message_submit_date       = getIndianDateFormat(sms_logs[i].message_submit_date,'date');
                        var message_submit_time       = getIndianDateFormat(sms_logs[i].message_submit_date,'time');

                        if(sms_logs[i].updated_at)
                        {
                            var message_delivery_date = getIndianDateFormat(sms_logs[i].updated_at,'date');
                            var message_delivery_time = getIndianDateFormat(sms_logs[i].updated_at,'time');

                        }else{
                            var message_delivery_date = '';
                            var message_delivery_time = '';
                        }

                        var sms_content               = sms_logs[i].sms_content;
                        var message_id                = sms_logs[i].message_id;

                        var campaign_id               = sms_logs[i].campaign_id;
                        var campaign_content_label_id = sms_logs[i].campaign_content_label_id;

                        var campaign_name             = await db.collection('campaign').findOne({'_id':parseInt(campaign_id)});
                        var campaign_label_name       = await db.collection('campaign_content_labels').findOne({'_id':parseInt(campaign_content_label_id)});
                        var message_reason            = sms_logs[i].message_status;
                        var message_status            = sms_logs[i].message_status;

                        var message_err_code          = sms_logs[i].message_err;
                        var get_err_desc              = constants.SMPP_ERR_CODE[message_err_code];
                        
                        var msg_err_code_txt          = 'Message is not delivered yet.'; 
                        if (message_err_code && message_err_code != '' && get_err_desc) {
                          msg_err_code_txt            = get_err_desc.description;
                        } 

                        if (message_err_code && !get_err_desc) {
                          msg_err_code_txt            = 'Cannot say the error';
                        }
                        

                        if(message_status=='PENDING'){

                            var status_span          = '<span class="badge badge-warning">'+message_status+'</span>';

                        }else if(message_status=='DELIVRD'){
                            var status_span          = '<span class="badge badge-success">Delivered</span>';
                            var message_reason       = 'Delivered';

                        }else{
                            var status_span          = '<span class="badge badge-danger">'+message_status+'</span>';
                            var message_reason       = sms_logs[i].message_status;
                        }

                        var smpp_label               = getSMPPLabelName(smpp_id);

                        table_content += '<tr>';
                        table_content += '<td>'+ (i + 1) +'</td>';
                        table_content += '<td class="campaign_name">'+campaign_name.name+'</td>';
                        table_content += '<td class="camp_cont_lbl">'+campaign_label_name.label_name+ '</td>';
                        table_content += '<td >'+smpp_label+ '</td>';
                        table_content += '<td>'+sender_id+'</td>';
                        table_content += '<td>'+mobile_no+ '</td>';
                        table_content += '<td class="message_submit_date">'+message_submit_date+'</td>';
                        table_content += '<td class="message_submit_time">'+message_submit_time+'</td>';
                        table_content += '<td>'+message_delivery_date+'</td>';
                        table_content += '<td>'+message_delivery_time+'</td>';
                        table_content += '<td><a rel="_editable" data-value="' + sms_content + '">Content</td>';
                        table_content += '<td>' +status_span+ '</td>';
                        table_content += '<td><a rel="_editable" data-value="' + msg_err_code_txt + '">' +message_reason+ '</a></td>';
                        table_content += '<td>' +message_id+ '</td>';
                        table_content += '</tr>';

                    }

                } else {

                    table_content += '<tr><td colspan="19"><img src="/public/images/no_result.gif"  style="height: 250px;width: 250px;display: block;margin: auto;"></td></tr>';                    
                }

                table_content += '</tbody>';
                table_content += '</table>';

                const table_count = sms_logs.length;

                var pages = Math.ceil(table_count / perPage);

                var pagination_content = pagination_helper.createPagination(pages, current, perPage, table_count, offset, false);

                res.send({
                  'status'           : status_code,
                  pagination_content : pagination_content,
                  table_content      : table_content,
                  activity_logs      : req.activity_logs,
                  activity_logs_count: req.activity_logs_count,
                    
                });

                client.close();

            })();

    }).catch(err => {
      console.log('connection timeout error');
      console.log('plase check your internet connection');
      console.log(err);
      if (err) {
        res.send({ success: false, message: 'query error', error: 'please check your internet connection' });
                return;
      }
    });
 });

module.exports = router;


