const express = require('express');
const router = express.Router();
const {
    ensureAuthenticated
} = require('../config/auth');
const bcrypt = require('bcryptjs');
const passport = require('passport');
var json2csv = require('json2csv'); //not working
var csv_export = require('csv-export');
const stringify = require('csv-stringify');
var autoIncrement = require("mongodb-autoincrement");

var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
// Dashboard
var await = require("await");
var async = require("async");
var Hashids = require('hashids');

var ObjectId = require('mongodb').ObjectId;

const crypto = require('crypto');
var algorithm = 'aes256';
var key = 'password';
var activityController = require('./../controllers/activity_ctrl');
// Load User model
const User = require('../models/user');
// DB Config
const db = require('../config/keys').mongoURI;
const db_options = require('../config/keys').db_options;
const database_name= require('../config/keys').database_name;
var keys = require('../config/keys');

/* RabbitMQ Settings & Library - (Start) */
var connection1 = require('amqplib').connect(keys.rabbitmq_uri);

console.log('Rabbitmq URL');
console.log(keys.rabbitmq_uri);

var SMS_LOG_QUEUE_NAME = 'sms_logs';
/* RabbitMQ Settings & Library - (Stop) */

router.get('/resend_sms', (req, res, next) => {

	MongoClient.connect(db, db_options).then(client => {
	    const db = client.db(database_name);
		const sms_logs = db.collection('sms_logs');

	    (async () => {

		    var start_date = new Date();
		    start_date.setHours(0, 0, 0, 0);


		    var end_date = new Date();
		    end_date.setHours(23, 59, 59, 59);

		    var pending_queue = await sms_logs.find({
                'created_at': {
                    $gt: start_date,
                    $lte: end_date
                },		    	
                "message_id" : {"$exists" : false}
		    }).toArray();

		    console.log('pending_queue');
		    console.log(pending_queue.length);

			connection1.then(function(conn) {
			  return conn.createChannel();
			}).then(function(ch) {

			   ch.assertQueue(SMS_LOG_QUEUE_NAME).then(function(ok) {

				    pending_queue.forEach(function(state_record) {
				    	delete state_record['_id'];
					    console.log("Message was sent!");
					    //console.log(state_record);
					    
					    ch.sendToQueue(SMS_LOG_QUEUE_NAME, Buffer.from(JSON.stringify(state_record)));
					});

			    });


			}).catch(function(err) {
			    console.log(err);
			    return console.log("Message was rejected...  Boo!");
			});

			res.send({'status':200,'message':'Successfully send all pending sms'});
        })();

	});
});

router.get('/update_iso_date', (req, res, next) => {

	MongoClient.connect(db, db_options).then(client => {
	    const db = client.db(database_name);
		const delivery_logs = db.collection('delivery_logs');
		const sms_logs = db.collection('sms_logs');

	    (async () => {


				var cursor = await sms_logs.find({"created_at": {"$exists": true},'campaign_content_label_id': {$in:[543]} }).forEach(function(response) {

				  console.log('response');
				  console.log(response._id);

				   sms_logs.findOneAndUpdate(
				    {"_id": response._id}, 
				    {"$set": {"created_at": new Date()}}
				   ) 
				});

		    	res.send({'status':200,'message':'Successfully updated all delivery logs'});
		  
        })();

});


});

router.get('/update_delivery_logs', (req, res, next) => {


	MongoClient.connect(db, db_options).then(client => {
	    const db = client.db(database_name);
		const delivery_logs = db.collection('delivery_logs');
		const sms_logs = db.collection('sms_logs');

	    (async () => {

			    var start_date = new Date();
			    start_date.setHours(0, 0, 0, 0);


			    var end_date = new Date();
			    end_date.setHours(23, 59, 59, 59);

		    	var delivery_logs_update = await delivery_logs.find({
	                'created_at': {
	                    $gt: start_date,
	                    $lte: end_date
	                }			    		
		    	}).forEach(function(response) {
			    	
			    	delete response['_id'];
			    	console.log('Response Message ID');
			    	console.log(response.message_id);

					sms_logs.findOneAndUpdate(
						{'message_id':response.message_id },
						{ $set: response }, (err, doc) => {
					    if (err) {
					        console.log("Delivery Logs -Something wrong when updating data!");
					    }

				    	console.log('Delivery Queue - Outside Loop');
				    	

					});
				});

		    	res.send({'status':200,'message':'Successfully updated all delivery logs'});
		   /* }*/


        })();

});

});

module.exports = router;