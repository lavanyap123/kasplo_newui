var express             = require('express');
var mongoose            = require('mongoose');
var MongoClient         = require('mongodb').MongoClient;
const router            = express.Router();
var csv                 = require('fast-csv');
const csv_parser        = require('csv-parser');
const fs                = require('fs');
var multer              = require('multer');
var upload              = multer({dest: 'public/uploads/'});
var XLSX                = require('xlsx')
const { convertArrayToCSV } = require('convert-array-to-csv');
const converter         = require('convert-array-to-csv');
var ISODate             = require("isodate");
var ObjectId            = require('mongodb').ObjectId;
var cron                = require('node-cron');

// DB Config
const db                = require('../config/keys').mongoURI;
const baseURL           = require('../config/keys').base_URL;
const db_options        = require('../config/keys').db_options;
const database_name     = require('../config/keys').database_name;
var functions_handler   = require('./../helpers/functions_handler');
var uploadController    = require('./../controllers/upload_ctrl');
const format            = require('nanoid/format');
const randomBytes       = require('random-bytes');
var autoIncrement       = require("mongodb-autoincrement");
var constants           = require('./../config/constants');

var upload_db_options   = {
  useNewUrlParser   : true,
  poolSize          : 30,
  keepAlive         : true,
  autoReconnect     : true,
  reconnectTries    : Number.MAX_VALUE,
  reconnectInterval : 1000,
  socketTimeoutMS   : 600000,
};

/******* unique id testing ***/
var shortlink           = require('shortlink');
/******* end unique id testing ***/

// upload
router.get('/test', (req, res, next) => {
  res.json({
    "status" : 4000
  })
});

/**
  method: /save
  description: Uploads the Past Campaign data from file to contacts collection upto 5 lakhs data maximum at a time
*/
router.post('/campaign_data_save', upload.single('file'), (req, res, next) => {
  res.append('Connection', 'keep-alive');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
  res.setHeader('Access-Control-Allow-Credentials', true); // If needed

  var category_id = req.body.category_id;
  var file = req.file;
  var user_name = req.body.user_name;
  var user_id = req.body.user_id;

  if (category_id && file) {

    MongoClient.connect(db, upload_db_options).then(client => {
      const db = client.db(database_name);

        const collection = db.collection('contacts');
        const activity_logs = db.collection('activity_logs');
        const contacts_collection = db.collection('contacts');
        const campaign_content_labels = db.collection('campaign_content_labels');
        
        db.collection('counters').findOneAndUpdate(
            { _id: 'campaign_content_labels' },
            { $inc: { seq: 1} },
            function (err, documents) {

           (async () => {
            var campaign_content_id = documents.value.seq+1;

    
            var cat_id = req.body.category_id;
            var campaign_id = req.body.campaign_id;
            var source_id = req.body.source_id;
            var topper = req.body.topper;
            var campaign_sent_date = req.body.campaign_sent_date;

            console.log('Post Parameters:');
            console.log('cat_id=' + cat_id);
            console.log('campaign_id=' + campaign_id);
            console.log('source_id=' + source_id);
            console.log('topper=' + topper);
            console.log('campaign_sent_date=' + campaign_sent_date);

            var user_name = req.body.user_name;
            var filter_category_id = cat_id.replace(/\//ig, "");

            /* Total Contacts - START */
            var smsDataCsvFile = req.file;
            var file_extension = req.file.originalname;
            var extension = file_extension.substr(file_extension.lastIndexOf(".") + 1);


            /* Import -  Excel - Start */

            var workbook = XLSX.readFileSync(smsDataCsvFile.path);
            var sheet_name_list = workbook.SheetNames;
            var excel_data = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);


            if (excel_data.length > 0) {


              /* batch size dynamic count */
              var length = excel_data.length - 1;
              var each_arr_size = 100000;
              var iteration_batch_min_and_max_values = [];

              if (length > 100000) {
                for (var size = 0; size < length;) {

                  if (iteration_batch_min_and_max_values.length > 0) {
                    var min_val = length - size - each_arr_size;
                    var min_range_val = min_val;
                    if (min_val < 0) {
                      min_range_val = 0;
                    }
                    iteration_batch_min_and_max_values.push([length - size - 1, min_range_val]);

                  } else {
                    iteration_batch_min_and_max_values.push([length, length - each_arr_size]);
                  }

                  size = size + each_arr_size;

                }
              } else {
                iteration_batch_min_and_max_values.push([length, 0])
              }

              /* end batch size dynamcic count */

              /* BATCH Size - MIN AND MAX - Declaring Array of Value - (Start) */
              

                  for (var i = 0; i < iteration_batch_min_and_max_values.length; i++) {

                    var min_value = iteration_batch_min_and_max_values[i][0];
                    var max_value = iteration_batch_min_and_max_values[i][1];

                    /* BATCH Wise LOOP - ARRAY PREPARATION - (Start) */

                    var mobile_number_list = [];
                    var batch_array_data_prepartion = [];
                    var total_valid_contacts_uploaded = 0;

                    for (var j = min_value; j >= max_value; j--) {

                      if (typeof excel_data[j] !== "undefined" && validateMobileNum(excel_data[j].mobile_no)) {
                        total_valid_contacts_uploaded = total_valid_contacts_uploaded + 1;

                        var new_id = new mongoose.Types.ObjectId();

                        var id = new_id.toString(),
                          ctr = 0;
                        var timestamp = parseInt(id.slice(ctr, (ctr += 8)), 16);
                        var machineID = parseInt(id.slice(ctr, (ctr += 6)), 16);
                        var processID = parseInt(id.slice(ctr, (ctr += 4)), 16);
                        var counter = parseInt(id.slice(ctr, (ctr += 6)), 16);


                        var mobile_no = excel_data[j].mobile_no;

                        var filter_reg_exp_mobile_no = mobile_no;
                        mobile_number_list.push(new RegExp('^' + filter_reg_exp_mobile_no));
                        var objectidd = new mongoose.Types.ObjectId();
                        excel_data[j]._id = objectidd;
                        excel_data[j].raw_category_id = [cat_id.toString()]; //category ID
                        excel_data[j].category_ids = [cat_id.toString()]; //category ID


                        if (topper == 'non-sender') {
                          //Non-Sender - Un-used number                          
                          excel_data[j].campaign_ids = []; // Campaign ID
                          excel_data[j].campaign_content_id = []; // Content Label ID
                          excel_data[j].sender_category_ids = [];
                          excel_data[j].sender_status = [];
                          var delete_sender_status = cat_id + '-Yes';
                          var delete_sender_cat_ids = cat_id.toString();
                        } else {
                          //Sender - used number
                          excel_data[j].campaign_ids = [campaign_id.toString()]; // Campaign ID
                          excel_data[j].campaign_content_id = [campaign_content_id.toString()]; // Content Label ID
                          excel_data[j].sender_category_ids = [cat_id.toString()]; // sender_category_ids ID
                          excel_data[j].sender_status = [cat_id + '-Yes'];
                        }

                        excel_data[j].clicker_category_id = [];
                        excel_data[j].cat_id = [];
                        excel_data[j].sub_cat_id = [cat_id.toString()];
                        excel_data[j].is_active = 1;
                        if (source_id) {
                          //console.log('##########1############')
                          excel_data[j].source = [source_id.toString()];
                        } else {
                          //console.log('#########22222222######')

                        }

                        if (excel_data[j].lead_date) {
                          var lead_date = new Date(excel_data[j].lead_date);
                          lead_date.setHours(11, 0, 0, 0);
                          excel_data[j].lead_date = ISODate(lead_date);
                        }


                        excel_data[j].unique_id = shortlink.generate(8);
                        excel_data[j].created_at = new Date().toLocaleString('en-US', {
                          timeZone: 'Asia/Calcutta'
                        });

                        batch_array_data_prepartion.push(excel_data[j]);


                      }
                    } // end for

                if (batch_array_data_prepartion) {

                  //console.log('Before Array length='+mobile_number_list.length);

                  await collection.insertMany(batch_array_data_prepartion, {
                    ordered: false
                  }).then(result => {

                    console.log('==== insert many ===');
                    console.log(result)
                    var bulk = collection.initializeUnorderedBulkOp();

                    if (source_id) {
                      bulk.find({
                        mobile_no: {
                          $in: mobile_number_list
                        },
                        source: {
                          $nin: [source_id.toString()]
                        }
                      }).update({
                        $addToSet: {
                          source: source_id
                        }
                      }, {
                        upsert: true
                      });
                    } 

                      bulk.find({
                        mobile_no: {
                          $in: mobile_number_list
                        },
                        category_ids: {
                          $nin: [new RegExp('^' + filter_category_id)]
                        }
                      }).update({
                        $addToSet: {
                          category_ids: cat_id
                        }
                      }, {
                        upsert: true
                      });

                      bulk.find({
                        mobile_no: {
                          $in: mobile_number_list
                        },
                        raw_category_id: {
                          $nin: [new RegExp('^' + filter_category_id)]
                        }
                      }).update({
                        $addToSet: {
                          raw_category_id: cat_id
                        }
                      }, {
                        upsert: true
                      });

                   
                 
                    if (topper == 'sender') {
                       bulk.find({
                        mobile_no: {
                          $in: mobile_number_list
                        }
                      }).update({
                        $addToSet: {
                          campaign_ids: campaign_id.toString(),
                          campaign_content_id: campaign_content_id.toString(),
                          sender_category_ids : cat_id.toString(),
                          sender_status : cat_id + '-Yes'
                        }
                      }, {
                        upsert: true
                      });
                    }
                    // Execute the operations
                    bulk.execute(function(err, result) {
                      //console.log('Bulk Result:');
                      //console.log(result); 

                    });
                  }).catch((error) => {
                    console.log('==== catch error ===');
                    var bulk = collection.initializeUnorderedBulkOp();
                    if (topper == 'non-sender') {

                        if (error) {
                        
                        var error_json_obj   = JSON.stringify(error);
                        var error_json_parse = JSON.parse(error_json_obj);
                        var write_error      = error_json_parse.result.writeErrors;
                        var write_err_len    = write_error.length;
                        var dup_mbl_numbers  = [];
                        for (i = 0;i < write_err_len; i++) {
                          dup_mbl_numbers.push(write_error[i].op.mobile_no);
                        }
                        if (dup_mbl_numbers.length > 0) {
                          bulk.find({
                            mobile_no: {
                              $in: dup_mbl_numbers
                            }
                          }).update({
                            $pull: {
                              campaign_ids: campaign_id.toString(),
                              campaign_content_id: campaign_content_id.toString(),
                              sender_category_ids : cat_id.toString(),
                              sender_status : cat_id + '-Yes'
                            }
                          }, {
                            upsert: true
                          });
                        }
                        
                      }
                      
                    }

                    if (source_id) {
                     
                      bulk.find({
                        mobile_no: {
                          $in: mobile_number_list
                        },
                        source: {
                          $nin: [source_id.toString()]
                        }
                      }).update({
                        $addToSet: {
                          source: source_id
                        }
                      }, {
                        upsert: true
                      });

                    } 

                    bulk.find({
                      mobile_no: {
                        $in: mobile_number_list
                      },
                      category_ids: {
                        $nin: [new RegExp('^' + filter_category_id)]
                      }
                    }).update({
                      $addToSet: {
                        category_ids: cat_id
                      }
                    }, {
                      upsert: true
                    });

                    bulk.find({
                      mobile_no: {
                        $in: mobile_number_list
                      },
                      raw_category_id: {
                        $nin: [new RegExp('^' + filter_category_id)]
                      }
                    }).update({
                      $addToSet: {
                        raw_category_id: cat_id
                      }
                    }, {
                      upsert: true
                    });

                   

                    if (topper == 'sender') {
                       bulk.find({
                        mobile_no: {
                          $in: mobile_number_list
                        }
                      }).update({
                        $addToSet: {
                          campaign_ids: campaign_id.toString(),
                          campaign_content_id: campaign_content_id.toString(),
                          sender_category_ids : cat_id.toString(),
                          sender_status : cat_id + '-Yes'
                        }
                      }, {
                        upsert: true
                      });
                    }

                    if (topper == 'non-sender') {
                       
                    }


                    // Execute the operations
                    bulk.execute(function(err, data) {

                    });

                  });
                } // end batch array if

              }
              /* BATCH Wise LOOP - ARRAY PREPARATION - (Stop) */

            }

            /* BATCH Size - MIN AND MAX - Declaring Array of Value - (Stop) */
            

            /* Checking Campaign already exist or not - (Start) */

            var get_campaign_details = await db.collection('campaign').findOne({
              '_id': parseInt(campaign_id)
            });

            var campaign_label_details = await db.collection('campaign_content_labels').findOne({
              'campaign_id': parseInt(campaign_id),
            });

            var url_shortcode = await functions_handler.check_shortid_exists(campaign_content_labels, 3);
            
            var label_data = {
              '_id': campaign_content_id,
              'url_shortcode': url_shortcode,
              'campaign_id': parseInt(campaign_id),
              'label_name': topper + '-' + campaign_sent_date,
              'sms_content': "",
              'destination_url': "",
              'status': "1",
              "draft_status" : 1,
              "total_sender":total_valid_contacts_uploaded,
              "segments":{
                "filter_category_id":cat_id,
                "category_id":cat_id,
                "campaign_id":campaign_id,
                "filter_campaign_id":campaign_id,
                "filter_campaign_id" : [],
                "filter_all_camp_sel_ids" : "",
                "filter_content_label_id" : [],
                "check_non_selected_con_ids" : "",
                "minRecord" : "0",
                "maxRecord" : total_valid_contacts_uploaded,
                "total_contacts" : total_valid_contacts_uploaded,
                "filter_mode" : "exclude",
                "filter_topper":topper,
                "nationality" : "",
                "pincode" : "",
                "gender" : "",
                "locality" : "",
                "salary" : "",
                "lead_from_date" : "",
                "lead_to_date" : "",
                "minAge" : "0",
                "maxAge" : "100",
                "action" : "send_sms_now",
                "page_name" : "view",
                "filter_search":{
                    "source" : [ 
                        source_id
                    ]
                }
               },
              'user_id' : user_id,
              'created_by': user_name,
              'created_at': new Date(campaign_sent_date).toLocaleString('en-US', {
                timeZone: 'Asia/Calcutta'
              }),
              'date_added': new Date(campaign_sent_date)
            };

            console.log('label_data');
            //console.log(label_data);

            campaign_content_labels.insert(label_data);

            /* Checking Campaign already exist or not - (End) */

            var activity_data = {
              'type': 'Import',
              'Location': 'Admin Panel',
              'description': 'Hi ' + user_name + ', data successfully inserted into database', //'Total ' + total_imported + ' records have been successfully inserted.',
              'total_contacts': total_valid_contacts_uploaded,
              'category_id': cat_id,
              'user_name': user_name,
              'file_name': file_extension,
              'duplicate_contacts': 0,
              'created_by': user_id,
              'is_read': 0,
              'created_at': new Date().toLocaleString('en-US', {
                timeZone: 'Asia/Calcutta'
              })
            };

            await activity_logs.insert(activity_data).then((data) => {


            });
            await client.close();
        
        
          res.send({
            'status': 200
          });


          })();
        });
      
       });
  } else {
    res.status(401).send("User does not have permission to complete the operation.")

  }

});

/**
 * @request     : {AJAX} {POST} /save
 * @description : Uploads the data from file to contacts collection upto 5 lakhs data maximum at a time
 * @return      : {success} 
*/

router.post('/save', upload.single('file'), (req, res, next) => {
  res.append('Connection', 'keep-alive');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
  res.setHeader('Access-Control-Allow-Credentials', true); // If needed

  var category_id     = req.body.category_id;
  var file            = req.file;
  var user_name       = req.body.user_name;
  var user_id         = req.body.user_id;

  if (category_id && file) {

    MongoClient.connect(db, upload_db_options).then(client => {
      const db        = client.db(database_name);

      (async () => {
        const collection      = db.collection('contacts');
        const activity_logs   = db.collection('activity_logs');
        const contacts_collection = db.collection('contacts');

        var cat_id            = req.body.category_id;
        var user_name         = req.body.user_name;
        var source_id         = req.body.source_id;
        var filter_category_id = cat_id.replace(/\//ig, "");

        /* Total Contacts - START */
        var smsDataCsvFile    = req.file;
        var file_extension    = req.file.originalname;
        var extension         = file_extension.substr(file_extension.lastIndexOf(".") + 1);

        /* Import -  Excel - Start */

        var workbook          = XLSX.readFileSync(smsDataCsvFile.path);
        var sheet_name_list   = workbook.SheetNames;
        var excel_data        = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);

        var excel_length      = excel_data.length;

        if (excel_length > 0) {

          /* batch size dynamic count */
          var length          = excel_length - 1;
          var each_arr_size   = 100000;
          var iteration_batch_min_and_max_values = [];

          if (length > 100000) {
            for (var size = 0; size < length;) {

              if (iteration_batch_min_and_max_values.length > 0) {
                var min_val     = length - size - each_arr_size;
                var min_range_val = min_val;
                if (min_val < 0) {
                  min_range_val   = 0;
                }
                iteration_batch_min_and_max_values.push([length - size - 1, min_range_val]);

              } else {
                iteration_batch_min_and_max_values.push([length, length - each_arr_size]);
              }

              size = size + each_arr_size;

            }
          } else {
            iteration_batch_min_and_max_values.push([length, 0])
          }

          /* end batch size dynamcic count */

          var lead_date_bulk = contacts_collection.initializeUnorderedBulkOp();

          /* BATCH Size - MIN AND MAX - Declaring Array of Value - (Start) */

          for (var i = 0; i < iteration_batch_min_and_max_values.length; i++) {

            var min_value     = iteration_batch_min_and_max_values[i][0];
            var max_value     = iteration_batch_min_and_max_values[i][1];

            /* BATCH Wise LOOP - ARRAY PREPARATION - (Start) */

            var mobile_number_list          = [];
            var batch_array_data_prepartion = [];
            var total_valid_contacts_uploaded = 0;

            for (var j = min_value; j >= max_value; j--) {

              if (typeof excel_data[j] !== "undefined" && validateMobileNum(excel_data[j].mobile_no)) {
                total_valid_contacts_uploaded = total_valid_contacts_uploaded + 1;

                var new_id = new mongoose.Types.ObjectId();

                var id  = new_id.toString(),
                  ctr   = 0;
                var timestamp = parseInt(id.slice(ctr, (ctr += 8)), 16);
                var machineID = parseInt(id.slice(ctr, (ctr += 6)), 16);
                var processID = parseInt(id.slice(ctr, (ctr += 4)), 16);
                var counter   = parseInt(id.slice(ctr, (ctr += 6)), 16);


                var mobile_no = excel_data[j].mobile_no;


                var filter_reg_exp_mobile_no = mobile_no;
                mobile_number_list.push(new RegExp('^' + filter_reg_exp_mobile_no));
                var objectidd                     = new mongoose.Types.ObjectId();
                excel_data[j]._id                 = objectidd;
                excel_data[j].raw_category_id     = [cat_id]; //category ID
                excel_data[j].category_ids        = [cat_id]; //category ID
                excel_data[j].campaign_ids        = []; // Campaign ID
                excel_data[j].campaign_content_id = []; // Content Label ID
                excel_data[j].sender_category_ids = []; // sender_category_ids ID
                excel_data[j].clicker_category_id = [];
                excel_data[j].sender_status       = [];
                excel_data[j].cat_id              = [];
                excel_data[j].sub_cat_id          = [cat_id];
                excel_data[j].is_active           = 1;
                if (source_id) {
                  //console.log('##########1############')
                  excel_data[j].source            = [source_id];
                } 

                if (excel_data[j].lead_date) {
                   var lead_date           = new Date(excel_data[j].lead_date);
                   lead_date.setHours(11, 0, 0, 0);
                   excel_data[j].lead_date = ISODate(lead_date);
                    await lead_date_bulk.find({'mobile_no': mobile_no.toString()}).upsert().update({
                      $set : {
                        'lead_date' :ISODate(lead_date),
                        'updated_at' : new Date()
                      }
                    }); //end bulk 
                }

                excel_data[j].unique_id     = shortlink.generate(8);
                excel_data[j].created_at    = new Date().toLocaleString('en-US', { 'timeZone': 'Asia/Calcutta'});

                batch_array_data_prepartion.push(excel_data[j]);

              }
            } // end for


            if (batch_array_data_prepartion) {

              await collection.insertMany(batch_array_data_prepartion, {
                ordered: false
              }).then(result => {
                var bulk    = collection.initializeUnorderedBulkOp();

                if (source_id) {
                  bulk.find({
                    mobile_no: {
                      $in: mobile_number_list
                    },
                    source: {

                      $nin: [source_id.toString()]
                    }
                  }).update({
                    $addToSet: {
                      category_ids: cat_id,
                      source: source_id
                    }
                  }, {
                    upsert: true
                  });

                } else {

                  bulk.find({
                    mobile_no: {
                      $in: mobile_number_list
                    },
                    category_ids: {
                      $nin: [new RegExp('^' + filter_category_id)]
                    }
                  }).update({
                    $addToSet: {
                      category_ids: cat_id
                    }
                  }, {
                    upsert: true
                  });

                  bulk.find({
                    mobile_no: {
                      $in: mobile_number_list
                    },
                    raw_category_id: {
                      $nin: [new RegExp('^' + filter_category_id)]
                    }
                  }).update({
                    $addToSet: {
                      raw_category_id: cat_id
                    }
                  }, {
                    upsert: true
                  });

                }
                // Execute the operations
                bulk.execute(function(err, result) {

                   lead_date_bulk.execute(function(err, result) {
                      console.log('Place - 1 - lead_date_bulk - Errors:');
                      console.log(err);

                      console.log('Place - 2 - lead_date_bulk - Results');
                      console.log(result);

                  });
                });
              }).catch((error) => {

                var bulk = collection.initializeUnorderedBulkOp();

                if (source_id) {
                  bulk.find({
                    mobile_no: {
                      $in: mobile_number_list
                    },
                   source: {
                      $nin: [source_id.toString()]
                    }
                  }).update({
                    $addToSet: {
                      category_ids: cat_id,
                      source: source_id
                    }
                  }, {
                    upsert: true
                  });

                } else {

                  bulk.find({
                    mobile_no: {
                      $in: mobile_number_list
                    },
                    category_ids: {
                      $nin: [new RegExp('^' + filter_category_id)]
                    }
                  }).update({
                    $addToSet: {
                      category_ids: cat_id
                    }
                  }, {
                    upsert: true
                  });

                  bulk.find({
                    mobile_no: {
                      $in: mobile_number_list
                    },
                    raw_category_id: {
                      $nin: [new RegExp('^' + filter_category_id)]
                    }
                  }).update({
                    $addToSet: {
                      raw_category_id: cat_id
                    }
                  }, {
                    upsert: true
                  });

                }
                // Execute the operations
                bulk.execute(function(err, data) {

                     lead_date_bulk.execute(function(err, result) {
                        console.log('Place - 2 - lead_date_bulk - Errors:');
                        console.log(err);

                        console.log('Place - 2 - lead_date_bulk - Results');
                        console.log(result);

                    });
                });

              });

            } // end batch array if
          } //end for
          /* BATCH Wise LOOP - ARRAY PREPARATION - (Stop) */

        } //end if

        /* BATCH Size - MIN AND MAX - Declaring Array of Value - (Stop) */

        var activity_data = {
          'type'        : 'Import',
          'Location'    : 'Admin Panel',
          'description' : 'Hi ' + user_name + ', data successfully inserted into database', //'Total ' + total_imported + ' records have been successfully inserted.',
          'total_contacts': total_valid_contacts_uploaded,
          'category_id' : cat_id,
          'user_name'   : user_name,
          'file_name'   : file_extension,
          'duplicate_contacts': 0,
          'created_by'  : user_id,
          'is_read'     : 0,
          'created_at'  : new Date().toLocaleString('en-US', {
            timeZone    : 'Asia/Calcutta'
          })
        };

        await activity_logs.insert(activity_data);
        await client.close();
        res.send({'status': 200});

      })(); //async
    }); //mongoconnect
  } else {
    res.status(401).send("User does not have permission to complete the operation.")
  } //end if
}); //save


/**
  method: /suppress
  description: Blocking the contacts in contacts collection by updating is_active field to 0 in database. If mobile numbers not exists in database while uploading block contacts file then not existed mobile number will add new record to contacts collection and set is_active field default to 0
  return: status_code, file(invalid numbers)
*/
router.post('/suppress', upload.single('file'), uploadController.getMobileNumbers, uploadController.getInvalidNumbers, (req, res, next) => {

  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
  res.setHeader('Access-Control-Allow-Credentials', true); // If needed

  MongoClient.connect(db, upload_db_options).then(client => {
    const db = client.db(database_name);

    (async () => {
      const activity_logs = db.collection('activity_logs');
      const contacts_collection = db.collection('contacts');
      var user_name = req.body.user_name;
      var user_id = req.body.user_id;

      /* Total Contacts - START */
      var existing_contacts = await contacts_collection.count();
      var smsDataCsvFile = req.file;
      var file_extension = req.file.originalname;
      var extension = file_extension.substr(file_extension.lastIndexOf(".") + 1);
      if (req.valid_mbl_num_list.length > 0) {
        let mbl_nms = req.valid_mbl_num_list.map(a => a.mobile_no);
        var bulk = contacts_collection.initializeUnorderedBulkOp();

        await bulk.find({
          mobile_no: {
            $in: mbl_nms
          }
        }).update({
          $set: {
            is_active: 0,
            updated_by: user_id,
            updated_at: functions_handler.getCurrentIndianDateTime()
          }
        }, {
          upsert: false
        });

        // Execute the operations
        await bulk.execute(function(err, data) {
          console.log('Bulk Result:');
          console.log(err);
          console.log(data);
          (async () => {

            var duplicates = parseInt(req.valid_mbl_num_list.length) - parseInt(data.result.nModified);
            var activity_data = {
              'type': 'Block Contacts',
              'Location': 'Admin Panel',
              'description': 'Total ' + data.result.nModified + ' valid records have been blocked successfully.',
              'total_contacts': req.valid_mbl_num_list.length,
              //'category_id': cat_id,
              'user_name': user_name,
              'file_name': file_extension,
              'duplicate_contacts': Math.abs(duplicates),
              'created_by': user_id,
              'is_read': 0,
              'created_at': new Date().toLocaleString('en-US', {
                timeZone: 'Asia/Calcutta'
              })
            };

            await activity_logs.insert(activity_data)
              .catch((error) => {
                //console.log(error)
                //console.log('Acitivity Log is not inserted');
              });


          })(); // async end

        });
      }
      await client.close();
      res.send({
        'status': 200,
        'download_file_name': req.file_path
      });

    })();

  });


});


/*
description: inserting dummy contacts data using iteration for testing unique id genreated contains any duplicates
*/
router.get('/insert_dummy', (req, res, next) => {
  res.append('Connection', 'keep-alive');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
  res.setHeader('Access-Control-Allow-Credentials', true); // If needed
  MongoClient.connect(db, upload_db_options).then(client => {
    const db = client.db(database_name);
    (async () => {
      const collection = db.collection('contacts');
      const contacts_collection = db.collection('contacts');

      /* BATCH Size - MIN AND MAX - Declaring Array of Value - (Start) */
      var batch_array_data_prepartion = [];


      for (var i = 0; i < 500000; i++) {

        /* BATCH Wise LOOP - ARRAY PREPARATION - (Start) */
        var excel_data = {};
        var mobile_number_list = [];
        var mobile_no = i;

        var objectidd = new mongoose.Types.ObjectId();
        excel_data['_id'] = objectidd;

        excel_data['is_active'] = 1;

        excel_data['unique_id'] = shortlink.generate(8);
        excel_data['created_at'] = new Date().toLocaleString('en-US', {
          timeZone: 'Asia/Calcutta'
        });

        batch_array_data_prepartion.push(excel_data);

      } // end for

      if (batch_array_data_prepartion) {
        await collection.insertMany(batch_array_data_prepartion, {
          ordered: false
        }).then(result => {}).catch((error) => {});
      } // end batch array if
      /* BATCH Size - MIN AND MAX - Declaring Array of Value - (Stop) */
      //
      await client.close();
      res.send({
        'status': 200,
        'success': 'data successfully inserted'
      });

    })();

  });

});


/* description: updating is_active field to 1 for old data existing in database

*/
router.get('/update_active', (req, res, next) => {
  res.append('Connection', 'keep-alive');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
  res.setHeader('Access-Control-Allow-Credentials', true); // If needed

  MongoClient.connect(db, upload_db_options).then(client => {
    const db = client.db(database_name);
    (async () => {
      const contacts_collection = db.collection('contacts');
      const get_count = await contacts_collection.find({
        is_active: {
          "$exists": false
        }
      }).limit(500000).toArray();
      if (get_count.length > 0) {
        /* batch size dynamic count */
        var length = get_count.length - 1;
        var each_arr_size = 100000;
        var iteration_batch_min_and_max_values = [];
        if (length > 100000) {
          for (var size = 0; size < length;) {

            if (iteration_batch_min_and_max_values.length > 0) {
              var min_val = length - size - each_arr_size;
              var min_range_val = min_val;
              if (min_val < 0) {
                min_range_val = 0;
              }
              iteration_batch_min_and_max_values.push([length - size - 1, min_range_val]);
            } else {
              iteration_batch_min_and_max_values.push([length, length - each_arr_size]);
            } //end if
            size = size + each_arr_size;
          } //end for
        } else {
          iteration_batch_min_and_max_values.push([length, 0])
        } //end if 

        for (var i = 0; i < iteration_batch_min_and_max_values.length; i++) {

          var min_value = iteration_batch_min_and_max_values[i][0];
          var max_value = iteration_batch_min_and_max_values[i][1];

          /* BATCH Size - MIN AND MAX - Declaring Array of Value - (Start) */
          var batch_array_data_prepartion = [];
          for (var j = min_value; j >= max_value; j--) {
            var o_id = new ObjectId(get_count[j]._id);
            /* BATCH Wise LOOP - ARRAY PREPARATION - (Start) */
            batch_array_data_prepartion.push(o_id);

          } // end for

          if (batch_array_data_prepartion) {
            var bulk = contacts_collection.initializeUnorderedBulkOp();
            bulk.find({
              _id: {
                $in: batch_array_data_prepartion
              }
            }).update({
              $set: {
                is_active: 1
              }
            }, {
              upsert: true,
              multi: true
            });

            // Execute the operations
            bulk.execute(function(err, data) {

            });
          } // end batch array if
        } // end for


      } //end if
      await client.close();
      res.send({
        'status': 200,
        'success': 'data updated successfully'
      });
    })();
  });
});



/*
description: updating unique id with new short id generation for testing purpose
*/
router.get('/update_unique_id', (req, res, next) => {
  res.append('Connection', 'keep-alive');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
  res.setHeader('Access-Control-Allow-Credentials', true); // If needed*/
  //cron.schedule('*/5 * * * *', () => {

  MongoClient.connect(db, upload_db_options).then(client => {
    const db = client.db(database_name);

    (async () => {

      const collection = db.collection('contacts');
      const contacts_collection = db.collection('contacts');
      const get_count = await contacts_collection.find({
        'is_unique_id_updated': {
          "$exists": false
        }
      }).limit(500000).toArray();
      if (get_count.length > 0) {
        /* batch size dynamic count */
        var length = get_count.length - 1;
        var each_arr_size = 100000;
        var iteration_batch_min_and_max_values = [];
        if (length > 100000) {
          for (var size = 0; size < length;) {

            if (iteration_batch_min_and_max_values.length > 0) {
              var min_val = length - size - each_arr_size;
              var min_range_val = min_val;
              if (min_val < 0) {
                min_range_val = 0;
              }
              iteration_batch_min_and_max_values.push([length - size - 1, min_range_val]);
            } else {
              iteration_batch_min_and_max_values.push([length, length - each_arr_size]);
            } //end if
            size = size + each_arr_size;
          } //end for
        } else {
          iteration_batch_min_and_max_values.push([length, 0])
        } //end if 



        for (var i = 0; i < iteration_batch_min_and_max_values.length; i++) {

          var min_value = iteration_batch_min_and_max_values[i][0];
          var max_value = iteration_batch_min_and_max_values[i][1];

          /* BATCH Size - MIN AND MAX - Declaring Array of Value - (Start) */
          var bulk = contacts_collection.initializeUnorderedBulkOp();

          var bulkOps = [];

          for (var j = min_value; j >= max_value; j--) {

            var o_id = new ObjectId(get_count[j]._id);

            var data_unique_id = shortlink.generate(8);


            bulkOps.push({
              "updateOne": {
                "filter": {
                  "_id": o_id
                },
                "update": {
                  "$set": {
                    unique_id: data_unique_id,
                    'is_unique_id_updated': 1
                  }
                }
              }
            });

          } // end for

          //console.log(bulk)


          await contacts_collection.bulkWrite(bulkOps)

          /* BATCH Size - MIN AND MAX - Declaring Array of Value - (Stop) */
        }
        await client.close();
        res.send({
          'status': 200,
          'success': 'data successfully updated'
        });
      } else {
        res.send({
          'status': 200,
          'error': 'count is 0'
        });
      } //end if 


    })();


  });
  //}); 
});

router.get('/update_lead_date', (req, res, next) => {
  res.append('Connection', 'keep-alive');
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
  res.setHeader('Access-Control-Allow-Credentials', true); // If needed

  MongoClient.connect(db, upload_db_options).then(client => {
    const db = client.db(database_name);
    (async () => {
      const contacts_collection = db.collection('contacts');
      var bulk                      = contacts_collection.initializeUnorderedBulkOp();
      const get_count = await contacts_collection.find(
        {'lead_date.0' : {$exists: true}
      }).limit(500000).toArray(); console.log(get_count.length);
      var message = 'No data available to update';
      if (get_count.length > 0) {
        message = 'Data successfully updated';

        /* batch size dynamic count */
        var length = get_count.length - 1;
        var each_arr_size = 100000;
        var iteration_batch_min_and_max_values = [];
        if (length > 100000) {
          for (var size = 0; size < length;) {

            if (iteration_batch_min_and_max_values.length > 0) {
              var min_val = length - size - each_arr_size;
              var min_range_val = min_val;
              if (min_val < 0) {
                min_range_val = 0;
              }
              iteration_batch_min_and_max_values.push([length - size - 1, min_range_val]);
            } else {
              iteration_batch_min_and_max_values.push([length, length - each_arr_size]);
            } //end if
            size = size + each_arr_size;
          } //end for
        } else {
          iteration_batch_min_and_max_values.push([length, 0])
        } //end if 

        for (var i = 0; i < iteration_batch_min_and_max_values.length; i++) {

          var min_value = iteration_batch_min_and_max_values[i][0];
          var max_value = iteration_batch_min_and_max_values[i][1];

          /* BATCH Size - MIN AND MAX - Declaring Array of Value - (Start) */
          var batch_array_data_prepartion = [];
          for (var j = min_value; j >= max_value; j--) {
            var o_id = new ObjectId(get_count[j]._id);
            var lead_date = get_count[j].lead_date[0];
            var current_time = new Date().toLocaleString('en-US', {
                      timeZone: 'Asia/Calcutta'
                    });
            await bulk.find({
                      _id: o_id
                    }).upsert().update({
                      
                      $set : {
                        lead_date: lead_date,
                        updated_at : current_time
                      }
                    }); //end bulk 

          } // end for

          await bulk.execute(function(err, result) {
            console.log(result);
            console.log('===errr===');
            console.log(err);
          });
        } // end for


      } //end if
      await client.close();
      res.send({
        'status': 200,
        'success': message,

      });
    })();
  });
});




function random(size) {
  const result = []
  for (let i = 0; i < size; i++) {
    result.push(randomBytes())
  }
  return result
}




module.exports = router;