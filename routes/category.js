const express               = require('express');
var mongoose                = require('mongoose');
var MongoClient             = require('mongodb').MongoClient;
var autoIncrement           = require("mongodb-autoincrement");

const router                = express.Router();
const { ensureAuthenticated, roleAuthenticated } = require('../config/auth');

// DB Config
const db                    = require('../config/keys').mongoURI;
const db_options            = require('../config/keys').db_options;
const database_name         = require('../config/keys').database_name;

var helper                  = require('./../helpers/functions_handler');
var activityController      = require('./../controllers/activity_ctrl');

/**
* route: /group
* method: GET
* description: 
* params: 
* return: success message, error message
*/
router.get('/group', ensureAuthenticated, roleAuthenticated, activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {

    const db         = client.db(database_name);
    const collection = db.collection('category');

    (async () => {
      const category_data = await collection.find().sort({'date_added': -1}).toArray(); 
      var category        = category_data.map((v,i,a) => {
        v.date_added      = helper.getCurrentDateTimeForReport(v.date_added, true, '-'); 
        return v;
      }); 
      return res.render('category/clist', {
        category,
        user          : req.user,
        activity_logs : req.activity_logs,
        activity_logs_count : req.activity_logs_count,
        permissions         : req.permissions
      });

      client.close();
    })();
  });
           
}); //group

/**
 * @request     : {GET} /get_all_category
 * @description : Fetching data from category table for dropdown listing 
 * @param       : {Int} _limit
 * @param       : {Int} _page               
 * @return      : category_data, category_count, success
*/
router.get('/get_all_category', ensureAuthenticated,  activityController.getActivityTotalCount, activityController.getRecentAcitityLogs, (req, res, next) => {

  MongoClient.connect(db, db_options).then(client => {
    
    const db              = client.db(database_name);
    const collection      = db.collection('category');

    
    (async () => {
      var limit           = parseInt(req.query._limit);
      var current_page    = parseInt(req.query._page);
      var offset          = parseInt((current_page - 1) * limit); 
      
      var cat_count       = await collection.count();
      var category_data   = [];
      if (current_page == 1) {
        category_data.push({'_id' : '0', 'name' : 'Others'});
        cat_count        += 1;
      }
      const cat_list      = await collection.find({'status' : '1'}).sort({'_id' : 1}).skip(offset).limit(limit).forEach(function (cat_record) {;
        category_data.push(cat_record);
      });

      res.send({
        'status'        : 200,
        'total_count'   : cat_count,
        'category_data' : category_data
      });

      client.close();
    })(); //async
  }); //mongoclient
           
}); //get_all_category


/**
* route: /popup_content
* method: POST
* description: add and edit content for category based html
* params: form fields
* return: content html
*/
router.post('/popup_content', ensureAuthenticated, (req, res) => {

  var type = req.body.type;
  var id = req.body.id;

  if (type == 'add') {

    var category_html_content = '<div class="modal-header"> <h5 class="modal-title" id="createCategoryModal">Create Category</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><div id="notification"></div> <form id="add_category" type="post"> <div class="row"> <div class="col-12"> <div class="form-group"> <label for="exampleInputEmail1">Category Name</label> <input type="text" class="form-control" name="name" id="category_name" placeholder="Enter Category"> </div><div class="form-group"> <label for="exampleInputEmail1">Category Description</label> <textarea class="form-control" name="description" id="category_description" rows="2" placeholder="Enter Category Description"></textarea> </div><div class="form-group"> <label class=" col-form-label">Status</label> <div class=""> <select class="form-control" name="status" id="status"> <option value="">---Status ---</option> <option value="1">Active</option> <option value="0">De-active</option> </select> </div></div></div><div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> <button type="submit" class="btn btn-primary" id="save_category">Save</button><img src="/public/images/loader_form.gif" class="img-responsive" id="loader" style="width: 25px;display:none;"/> </form> </div></div></div>';
    res.send({
        'popup_content': category_html_content
    });

  } else {


    MongoClient.connect(db, db_options).then(client => {
      const db = client.db(database_name);
      const collection = db.collection('category');

      collection.findOne({
          '_id': parseInt(id)
      }).then(function(doc) {


          if (!doc) {
              throw new Error('No record found.');
          } else {
              var status = doc.status;
              if (status == '1') {
                  var active_selected = 'selected';
                  var de_active_selected = '';

              } else {
                  var active_selected = '';
                  var de_active_selected = 'selected';
              }

              var category_html_content = '<div class="modal-header"> <h5 class="modal-title" id="createCategoryModal">Update Category - #' + id + ' </h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><div id="notification"></div> <form id="add_category" type="post"><input type="hidden" id="update_cat_id" value="' + id + '"> <div class="row"> <div class="col-12"> <div class="form-group"> <label for="exampleInputEmail1">Category Name</label> <input type="text" class="form-control" name="name" id="category_name" placeholder="Enter Category" value="' + doc.name + '"> </div><div class="form-group"> <label for="exampleInputEmail1">Category Description</label> <textarea class="form-control" name="description" id="category_description" rows="2">' + doc.description + '</textarea> </div><div class="form-group"> <label class=" col-form-label">Status</label> <div class=""> <select class="form-control" name="status" id="status"> <option value="">---Status ---</option> <option value="1" ' + active_selected + '>Active</option> <option value="0"  ' + de_active_selected + '>De-active</option> </select> </div></div></div><div class="modal-footer"> <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> <button type="submit" class="btn btn-primary" id="btn_update_category">Update</button><img src="/public/images/loader_form.gif" id="loader" class="img-responsive" style="width: 25px;display:none;"/> </form> </div></div></div>';

          }

          res.send({
              'popup_content': category_html_content
          });

      });

      client.close();

    });
  }
}); //popup_content


/**
* route: /add
* method: POST
* description: add category data
* params: form fields
* return: status, message
*/
router.post('/add', ensureAuthenticated, (req, res) => {

  MongoClient.connect(db, db_options).then(client => {
    const db         = client.db(database_name);
    const collection = db.collection('category');

    autoIncrement.getNextSequence(db, 'category', function(err, autoIndex) {

    var data    = {
    '_id'         : autoIndex,
    'name'        : req.body.cat_name,
    'description' : req.body.description,
    'status'      : req.body.status,
    'created_at'  : new Date().toLocaleString('en-US', {
          timeZone      : 'Asia/Calcutta'
    }),
    'date_added'  : new Date()
    };

    collection.insert(data)
    .catch((error) => {
    console.log('error.message');
    });
    client.close();

    });

    res.send({
    'msg': 'success',
    'status': 200
    });

  });
}); //add

/**
* route: /update
* method: POST
* description: update category data
* params: form fields
* return: status, message
*/
router.post('/update', ensureAuthenticated, (req, res) => {

  MongoClient.connect(db, db_options).then(client => {
    const db         = client.db(database_name);
    const collection = db.collection('category');

    var data = {
      'name'         : req.body.cat_name,
      'description'  : req.body.description,
      'status'       : req.body.status,
      'updated_at'   : new Date(Date.now()).toISOString()
    };

    var id       = req.body.category_id;

    collection.updateOne({
      _id: parseInt(id)
    }, // Filter
    {
      $set: data
    }, // Update
    {
      upsert: false
    }
    )
    .then((obj) => {
      res.send({
        'msg': 'success',
        'status': 200
      });
    })
    .catch((err) => {
      res.send({
          'msg': err,
          'status': 400
      });
    });

    client.close();
  });

}); //update

/**
 function: checkCategoryExists
 method  : POST
 description: checking if category name given already exists or not in db
 params: {string} name $name
         {int} id $id
 return: true or false
*/
router.post('/checkCategoryExists', ensureAuthenticated, (req, res) => {

  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);
    const collection = db.collection('category');
    (async () => {
      var name = req.body.name; //console.log(name)
      var check_type = req.body.check_type;
      if (check_type == 'add') {
        var name_status = await collection.findOne({'name':new RegExp('^' + name + '$', 'i')});
      } else {
      var category_id = parseInt(req.body.category_id); 
      var name_status = await collection.findOne({'name':new RegExp('^' + name + '$', 'i'), '_id': {$ne: category_id}}); 
      }
      if (name_status) {               
      res.send('false');     
      return false;
      } else {
      res.send('true');     
      return true;
      }
      client.close();

  })(); //async
  }); 
}); //checkCategoryExists end

module.exports = router;
