const express    = require('express');
const fs         = require("fs");
const path       = require("path");
var cron         = require('node-cron');
const app        = express();
const mongoose   = require('mongoose');
var nodemailer = require('nodemailer');

// DB Config
const db         = require('./config/keys').mongoURI;
const db_options = require('./config/keys').db_options;
const database_name= require('./config/keys').database_name;
const mode = require('./config/keys').mode;
const base_URL= require('./config/keys').base_URL;
const mail_from = require('./config/keys').mail_from;
const to_mail_ids= require('./config/keys').to_mail_ids;
const smtp_credentials= require('./config/keys').smtp_credentials;
var MongoClient = require('mongodb').MongoClient;

// Connect to MongoDB
mongoose.connect(db, db_options)
.then(() => console.log('APP.js = MongoDB Connected from='+db))
.catch(err => console.log(err));

mongoose.connection.on('error', (err) => {
  //console.log(err);
  sendEmail(err,'MongoDB error');
});

process.on('uncaughtException', (err) => {
  sendEmail(err,'Uncaught Exception thrown');
});

process.on('unhandledRejection', (err) => {
  console.log(err);
  sendEmail(err,'Unhandled Rejection at Promise');
});

function sendEmail(err,subject)
{
    var transporter = nodemailer.createTransport(smpp_credentials);

    if(mode=='localhost')
    { 
      var subject="This error is "+mode+"("+base_URL+")";
    }else{
      var subject=mode+"- Kasplo - Bulk SMS[app.js] - Critical alert -"+subject;
    }

    var mailOptions = {
        from: mail_from, 
        to: 'vinodhtamilvanan@adcanopus.com',        
        subject:subject,// Subject line
        text: err.stack.toString(), // plain text body
        html: "<p><b style='color:red;'>"+err.stack.toString()+"</b></p>" // html body
    };

    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
          console.log("Message sent: %s", info.messageId);
          console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
      }
    });    
}

const directory = 'public/content_label_list';
  
cron.schedule('55 17 * * *', () => {
  console.log('cron scheduled');
  fs.readdir(directory, (err, files) => {
    if (err) throw err;
    var total_del = 0;
    for (const file of files) {
      fs.unlink(path.join(directory, file), err => {
        if (err) throw err;
        console.log(" file succesfully deleted");
        total_del += 1;
      });
    }
  MongoClient.connect(db, db_options).then(client => {
    const db = client.db(database_name);
    const activity_logs = db.collection('activity_logs');
    var data = {
      'type': 'Cron content label list',
      'Location': 'Admin Panel',
      'user_name': 'Cron',
      'total_contacts': total_del,
      'description': 'Total ' + total_del + '  content label list files deleted from ' + directory + '  path',
      'is_read': 0,
      'created_at': new Date().toLocaleString('en-US', {
        timeZone: 'Asia/Calcutta'
      }),
    };

    activity_logs.insert(data).catch((error) => {
      console.log(error)
    });
    client.close();
  });
});
});


const PORT = process.env.PORT || 5008;

app.listen(PORT, console.log(`Server started on port ${PORT}`));