const express = require('express');
const app6 = express();
var cookieParser = require('cookie-parser');
const flash = require('connect-flash');
const mongoose = require('mongoose');
var nodemailer = require('nodemailer');

// DB Config
const db = require('./config/keys').mongoURI;
const db_options = require('./config/keys').db_options;
const database_name = require('./config/keys').database_name;
const mode = require('./config/keys').mode;
const base_URL = require('./config/keys').base_URL;
const mail_from = require('./config/keys').mail_from;
const to_mail_ids = require('./config/keys').to_mail_ids;
const smtp_credentials = require('./config/keys').smtp_credentials;

mongoose.connect(db, db_options)
    .then(() => console.log('APP.js = MongoDB Connected from=' + db))
    .catch(err => console.log(err));


mongoose.connection.on('error', (err) => {
    //console.log(err);
    sendEmail(err, 'MongoDB error');
});

process.on('uncaughtException', (err) => {
    console.log(err);

    sendEmail(err, 'Uncaught Exception thrown');
});

process.on('unhandledRejection', (err) => {
    console.log(err);

    sendEmail(err, 'Unhandled Rejection at Promise');
});


function sendEmail(err, subject) {

    console.log(err);
    /* var transporter = nodemailer.createTransport(smpp_credentials);

     if(mode=='localhost')
     { 
       var subject="This error is "+mode+"("+base_URL+")";
     }else{
       var subject=mode+"- Kasplo - Bulk SMS[karix.js] - Critical alert -"+subject;
     }

     var mailOptions = {
         from: mail_from, 
         to: 'vinodhtamilvanan@adcanopus.com',        
         subject:subject,
         text: err.stack.toString(),
         html: "<p><b style='color:red;'>"+err.stack.toString()+"</b></p>"
     };

     transporter.sendMail(mailOptions, function(error, info){
       if (error) {
         console.log(error);
       } else {
           console.log("Message sent: %s", info.messageId);
           console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
       }
     });   */
}

app6.set('view engine', 'ejs');
app6.use("/public", express.static(__dirname + '/public'));

// Express body parser
app6.use(express.urlencoded({
    extended: true
}));


app6.use(cookieParser());


app6.use('/karix_smpp', require('./routes/smpp/karix/1_karix_receiver_queue.js'));


app6.use(function(req, res) {
    res.render('404');
});

process.on('SIGINT', function() {
    mongoose.connection.close(function() {
        console.log('Mongoose disconnected on app termination');
        process.exit(0);
    });
});

app6.listen(5006, console.log(`Server started on port 5006`));