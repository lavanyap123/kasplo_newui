let express    = require('express');
let app        = express();
var MongoClient = require('mongodb').MongoClient;
var cron = require('node-cron');
const db = require('./config/keys').mongoURI;
const db_options = require('./config/keys').db_options;
const database_name = require('./config/keys').database_name;

app.use('/update_delivery_logs', require('./routes/queue/update_delivery_logs.js'));

app.listen(5007);