$(document).ready(function() {
	$(function(){
	load_label_listtable(1);
	});
	var campaign_id = $('#campaign_id').val(); 
	/* show dashbaord count loader here */
	$.ajax({
		type     : 'POST',
		url      : '/campaign/get_label_count',
		data     : {'campaign_id' : campaign_id},
		dataType : 'json',
		success  : function (resp) {
		  console.log(resp);
		  if (resp.status == 200) {
		    /* hide dashbaord loader here */
		    $('#total_sender').text(resp.total_sender_sum);
		    $('#total_clicker').text(resp.total_clicker_sum);
		    $('#total_cont_lbls').text(resp.total_contents);
		    $('#cat_name').text(resp.cat_name);
		    $('#camp_name').text(resp.camp_name);
		  }          
		},
	}); //get_dashboard_count

	$(document).on('click', '.page-link', function() {
      var page_no = $(this).data('value');
      load_label_listtable(page_no);
    }); 

	$("#campaign_label_form").validate({
    rules: { 
      from_date : {
        required : true
      },
      to_date   : {
        required : true
      }
    },

    messages : {
    
      from_date : {
        required : "Please select from date"
      },
      to_date   : {
        required : "Please select to date"
      }
    },
    submitHandler : function (form) {
    	console.log(form)
    }
  })

	$('#campaign_label_btn').click(function () {
    $("#campaign_label_form").validate();
    if ($("#campaign_label_form").valid()) {
      load_label_listtable(1);
    }
  });

  $('#campaign_label_export_btn').click(function () {
    $("#campaign_label_form").validate();
    if ($("#campaign_label_form").valid()) {
     
      var from_date   = $("#from_content_lbl_date").val();  
      var to_date     = $("#to_content_lbl_date").val(); 
      $("#download_export").show();
      $.ajax({
        type       : 'POST',
        url        : '/campaign/export_content_labels',
        data       : {
          'campaign_id' : campaign_id, 
          'from_date'   : from_date, 
          'to_date'     : to_date
        },
        dataType   : 'json',
        success    : function (response) {
          console.log(response);
          $("#download_export").hide();
          if (response.status == 200) {
            document.location.href = '../../' + response.download_file_name;
          } else {
            swal("Error", "No data available to export", "error");
          }
        }
      });   
    }
  }); 

  $('#showContentLabelModal').on('show.bs.modal', function(){
      var cells = $(event.target).closest("tr").children("td");
      var cell2 = cells.eq(1).html();
      var cell3 = cells.eq(2).text();
      var cell4 = cells.eq(3).text();
      var cell5 = cells.eq(6).text(); 
      var cell6 = cells.eq(7).text();
      var cell7 = cells.eq(8).text();
      var cell8 = cells.eq(9).text();
      var cell9 = cells.eq(10).text();
      var cell10 = cells.eq(11).text();
      var cell11 = cells.eq(12).text(); 
      var cell12 = cells.eq(13).text();
      var cell13 = cells.eq(14).text();
      var cell14 = cells.eq(16).text();
      var cell15 = cells.eq(17).text();
      var cell16 = cells.eq(18).text();
 
      var smpp_label = '<span class="badge badge-danger">'+cell6+'</span>';
      
      if (cell6 == 'Karix') {
        smpp_label = '<span class="badge badge-success">'+cell6+'</span>';
      } 

      $('#campaing-lbl-name').html(cell2);
      $('#sms-content').text(cell3);
      $('#dest-url').text(cell4);
      $('#short-code').text(cell5);
      $('#get-smpp-name').html(smpp_label);
      $('#get-sender-id').text(cell7);
      $('#total-sent').text(cell8);
      $('#total-unique-clicker').text(cell9);
      $('#total-clicks').text(cell10);
      $('#ctr').text(cell11);
      $('#min-record').text(cell12);
      $('#max-record').text(cell13);
      $('#det-report').html('<a href='+cell14+' target="_blank">Detailed Report</a>');
      $('#sent-date').text(cell15);
      $('#created-by').text(cell16);
    });

	function load_label_listtable (page_no) {
		$("#load_label_data").html('');
	  $("#load_label_data").html('<td colspan="8" class="text-center"><img src="/public/images/loader_form.gif" class="img-responsive" alt="Loading..."></td>');
	  var from_date  = $("#from_content_lbl_date").val();  
  	var to_date    = $("#to_content_lbl_date").val(); 

		$.ajax({
			type     : 'POST',
			url      : '/campaign/campaign_label_list',
			data     : {'campaign_id' : campaign_id, 'page_no' : page_no, 'from_date'   : from_date,'to_date'     : to_date},
			dataType : 'json',
			success  : function (response) {

			  $("#load_label_data").html('');
			 
	      if (response.status == 200) {
	        $('#load_label_data').append(response.table_data);
	      } else {
	        $("#load_label_data").html('<td colspan="5" class="text-center">' + response.message + '</td>');
	      }
	      $("#load_label_data").show(); 
	      $("#pagination").html(response.pagination_content);  
	      jQuery(document).ready(function () {

       jQuery('a[rel="_editable"]').editable({
         "title": "Enter Note",
         "rows": 4,
         "tpl": "<textarea cols='60' class='form-control'></textarea>",
         "emptytext": "Not set",
         "inputclass": "code_view",
         "type": "textarea",
         "mode": "inline",
         "name": "name",
         "showbuttons": false
       });
     });      
			},
		}); //get_dashboard_count
	}

  $(document).on('click', '.update_campaign', function () {
   
     var campaign_label_id = this.id;
 
     $.ajax({
       url: '/campaign/popup_content',
       type: 'POST',
       data: { 'type': 'edit', 'id': campaign_label_id },
       success: function (data) {
        $("#status").select2();
         if (data.status_code == 200) {
          $('#label_name').val(data.label_name);
          $('#sms_content').val(data.sms_content);
          $('#destination_url').val(data.dest_url);
          $('#status').val(data.status).trigger('change');
          $('#content_label_id').val(data.content_label_id);
         }
         
       }
     });
  });

  $(document).on('click', '#update_label', function () {
       var campaignForm = $("#add_campaign");
   
       var validator = campaignForm.validate({
   
         rules: {
           label_name: {
             required: true
           },
           sms_content: {
             required: true
           },
           
           status: {
             required: true
           },
         },
   
         messages: {
           label_name: {
             required: "Label name is required"
           },
           sms_content: {
             required: "SMS content is required"
           },
           
           status: {
             required: "Please select status"
           },
   
         },
   
         submitHandler: function (form) {
   
           $("#update_campaign").prop('disabled', true);
           $("#loader").show();
   
           var content_label_id = $("#content_label_id").val();
           var name             = $("#label_name").val();
           //var sms_content = $(".hide_text").val();
           var destination_url  = $("#destination_url").val();
           var status           = $("#status").val();
            var message         = $("#sms_content").val(); 
            if (message.includes('{url}')) {
              var separators = ['{url}'];
              for (var i = 0; i < separators.length; i++) { 
                var rg = new RegExp("\\" + separators[i], "g"); 
                message = message.replace(rg, " " + separators[i] + " "); 
              }
            }
            message = $.trim(message.replace(/[\t\n]+/g, ' '));
            message = message.replace(/ +(?= )/g,''); 
           
           $.ajax({
             url: '/campaign/update',
             type: 'POST',
             data: { 'content_label_id': content_label_id, 'label_name': name, 'sms_content': message, 'destination_url': destination_url, 'status': status },
             success: function (data) {
   
               $("#loader").hide();
   
               if (data.status == '200') {
   
                 $('#notification').html('<div class="alert alert-success"><strong>Success!</strong> Campaign Updated successfully.</div>');
   
               } else {
                 $("#update_campaign").prop('disabled', false);
                 $('#notification').html('<div class="alert alert-danger "><strong>Error!</strong> A problem has been occurred while submitting your data.</div>');
   
               }
   
               setTimeout(function () {
                 $('#notification').hide();
                 location.reload();
               }, 3000);
             }
           });
         }
       });
     });

});//document

