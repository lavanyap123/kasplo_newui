$(document).ready(function () {

  $( "#menu_type" ).change(function() {
    
    $('#menu_data').empty();
    var menu_type_id = $('#menu_type').val();
    //$('.loader_gif').show();
    getMenuTypeById(menu_type_id);
  });
  
});

function getMenuTypeById(menu_type_id, type = '', type_data = '') {
  $('.role_popup_name').prop('disabled', true);
  $('.loader_gif').show();
  $.ajax({
      url      : '/roles/gole_role_by_menu_type_id',
      type     : 'POST',
      data     : {
        'menu_type_id'   : menu_type_id,
      },
      success  : function (data) {
        $("#loader").hide();
        $('.loader_gif').hide();
        $('.role_popup_name').prop('disabled', false);
        if (data.status == 200) {
          var menu_html     = '';
          var menu_data     = data.sub_menu_data;
          for (i = 0; i < menu_data.length; i++) {
            menu_html += '<div class="panel-group col-lg-6 col-md-6 col-sm-6"><div class="panel panel-default" ><div class="card"><h4 class="card-header"><a data-toggle="collapse" href="#menu_'+ i +'" data-menuid="'+ menu_data[i].menu_id +'">'+ menu_data[i].menu_name +'</a></h4></div><div id="menu_'+ i +'" class="panel-collapse collapse"><div class="full-block"><table class="table table-bordered dt-responsive rolesPermissionTable"><thead>';
            var sub_menu_data  = menu_data[i].sub_menus;
            if (sub_menu_data) {
              menu_html += '<tr class="showRolesPermission tbl_background float-center"><th scope="col" id="'+ menu_data[i].menu_id +'">Sub Menus</th><th scope="col">Create</th><th scope="col">Read</th><th scope="col">Update</th><th scope="col">Delete</th></tr></thead><tbody>';
            for (j = 0; j < sub_menu_data.length; j++) {
              menu_html += '<tr class="float-center" ><th scope="row" class="thfont" id="'+ sub_menu_data[j].sub_menu_id +'">'+ sub_menu_data[j].sub_menu_name +'</th>';
              menu_html += '<td><div class="icheckbox_flat-green float-center" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="chk_update flat-red" name="data['+  menu_data[i].menu_id +'][' + sub_menu_data[j].sub_menu_id +'][create]" value="1" style="position: absolute;" data-permission="create"></div></td><td><div class="icheckbox_flat-green float-center" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="chk_update flat-red" name="data['+  menu_data[i].menu_id +'][' + sub_menu_data[j].sub_menu_id +'][read]" value="1" style="position: absolute;" data-permission="read"></div></td><td><div class="icheckbox_flat-green float-center" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="chk_update flat-red" name="data['+  menu_data[i].menu_id +'][' + sub_menu_data[j].sub_menu_id +'][update]" value="1" style="position: absolute;" data-permission="update"></div></td><td><div class="icheckbox_flat-green float-center" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="chk_update flat-red" name="data['+  menu_data[i].menu_id +'][' + sub_menu_data[j].sub_menu_id +'][delete]" value="1" style="position: absolute;" data-permission="delete"></div></td></tr>';
            }
          } else {
            menu_html += '<tr class="showRolesPermission tbl_background float-center"><th scope="col" id="'+ menu_data[i].menu_id +'">Menus</th><th scope="col">Create</th><th scope="col">Read</th><th scope="col">Update</th><th scope="col">Delete</th></tr></thead><tbody>';
            menu_html += '<tr class="float-center"><th scope="row" class="thfont">'+ menu_data[i].menu_name +'</th>';
            menu_html += '<td><div class="icheckbox_flat-green float-center" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="chk_update flat-red" name="data['+  menu_data[i].menu_id +'][create]" value="1" style="position: absolute;" data-permission="create"></div></td><td><div class="icheckbox_flat-green float-center" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="chk_update flat-red" name="data['+  menu_data[i].menu_id +'][read]" value="1" style="position: absolute;" data-permission="read"></div></td><td><div class="icheckbox_flat-green float-center" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="chk_update flat-red" name="data['+  menu_data[i].menu_id +'][update]" value="1" style="position: absolute;" data-permission="update"></div></td><td><div class="icheckbox_flat-green float-center" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" class="chk_update flat-red" name="data['+  menu_data[i].menu_id +'][delete]" value="1" style="position: absolute;" data-permission="delete"></div></td></tr>';

          }
            menu_html += '</tbody></table></div></div></div></div>';
          
          }
          $('#menu_data').append(menu_html);

          if (type == 'update' && type_data != '') {
            var sub_menus      = type_data.roles_data[0].permissions; 
            var sub_menus_len  = Object.keys(type_data.roles_data[0].permissions).length;
            for (i = 0;i < sub_menus_len; i++) {
             
              var sub_menu_vals     = Object.values(sub_menus)[i]; console.log(sub_menu_vals)
              var sub_menu_val_keys = Object.keys(sub_menu_vals);
              
              for (j = 0; j < sub_menu_val_keys.length; j++) {
                //console.log(sub_menu_val_vals);
                var sub_menu_val_vals = Object.values(sub_menu_vals)[j];  console.log(sub_menu_val_vals)
                for (k = 0; k < sub_menu_val_vals.length; k++) {
                  console.log(sub_menu_val_vals[k]); console.log("====sub_menu_val_vals===")
                  var sub_menu_per_keys  = Object.keys(sub_menu_val_vals[k]); console.log(sub_menu_per_keys);
                  if (sub_menu_per_keys.includes("0"))
                    var check_val_name  = "input[name='data[" + Object.keys(sub_menus)[i] + "][" + sub_menu_val_keys[j] + "]']";
                  else
                  var check_val_name  = "input[name='data[" + Object.keys(sub_menus)[i] + "][" + sub_menu_val_keys[j] + "][" + sub_menu_per_keys + "]']";
                  console.log(check_val_name)
                   $(check_val_name).prop('checked', true);
                }
                //var sub_menu_per_keys  = Object.keys(sub_menu_vals)
                
              }  
            }
            }
        }

      }
    });
}