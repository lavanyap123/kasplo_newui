$(function() {

    $("#campaign-save").validate({

        rules: {

            "campaign_name": {
                required: true,
            },
            "category_id": {
                required: true,
            },

            "content_label_name": {
                required: true,
            },
            "message": {
                required: true,
            },
            /*"destination_url": {
                required : true,
            },*/
            "filter_category_id": {
                required: true,
            },
            
        },
        messages: {

            "campaign_name": {
                required: "Please provide an campaign name"
            },
            "category_id": {
                required: "Please select a category"
            },

            "content_label_name": {
                required: "Please provide an content label name"
            },
            "message": {
                required: "Please provide an content",
                remote: "Please add spaces for url"
            },
            /*"destination_url": {
                required : "Please provide an destination url"
            },*/
            "filter_category_id": {
                required: "Please select a category"
            },

        },
        
        errorPlacement: function(error, element) {

            if (element.attr("name") == "message") {
                $("#message_error").html(error);
            }
            else {
                element.after(error); // default error placement
            }

        }
    });

    $(".contentSection").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "fade",
        // enableAllSteps: true,
        autoFocus: true,
        transitionEffectSpeed: 500,
        titleTemplate: '<div class="title">#title#</div>',
        labels: {
            previous: '<div class="navigationSec marginTop3em"><div class="addCampaignBtn marginRight2em" id="contactBack"><img src="../../public/images/addCampaign/previous.png" alt=""></div></div>',
            next: '<div class="navigationSec marginTop3em"><div class="addCampaignBtn" id="campaignNext"><img src="../../public/images/addCampaign/next.png" alt=""></div></i></div>',
            finish: '<div class="navigationSec marginTop3em"><div class="addCampaignBtn" id="complete"><img src="../../public/images/addCampaign/done.png" alt=""></div></div>',
            current: ''
        },
        onStepChanging: function(event, currentIndex, newIndex) {
            console.log(event); console.log(currentIndex); console.log(newIndex)
          $("#campaign-save").validate().settings.ignore = ":disabled,:hidden";
          return $("#campaign-save").valid();
        },
        onStepChanged: function(event, currentIndex, newIndex) {
            alert('steps');
            var check_page = $("#campaign_id").val();
             $("#record_loader").show();
             $('#preview').prop('disabled', true);
             $('#scheduled_sms').prop('disabled', true);

             $('#from_rec_val').prop("disabled", true);
             $('#to_rec_val').prop("disabled", true);
             
            $('#zero_notification').hide();

            if (check_page > 0) {
                var category_id = $("#get_category_name").val();
                var campaign_name = $("#get_campaign_name").val();

                if ($("#newContent").prop("checked") == true) {
                    var content_label_name = $("#content_label_name").val();
                } else {
                    var content_label_name = $("#label_id option:selected").text();
                }

            } else {

                var category_id = $("#category_id option:selected").text();
                var campaign_name = $("#campaign-name").val();
                var content_label_name = $("#content_label_name").val();

            }

            var smpp_name=$("#smpp_name option:selected").text();
            var sender_id=$("#sender_addr option:selected").text();
            $("#preview_smpp_name").text(smpp_name);
            $("#preview_smpp_sender_id").text(sender_id);

            var detination_url = $("#destination_url").val();
            var message = $("#sent_messages").val(); 
            if (message.includes('{url}')) {
              var separators = ['{url}'];
              for (var i = 0; i < separators.length; i++) { 
                var rg = new RegExp("\\" + separators[i], "g"); 
                message = message.replace(rg, " " + separators[i] + " "); 
              }
            }
            
            //message = $.trim(message.replace(/[\t\n]+/g, ' '));
            message = message.replace(/ +(?= )/g,'');
           
            $(".hide_text").text(message);
            /* Filters - START */
            var country = $("#country").val();
            var city = $("#city").val();
            var state = $("#state").val();
            var nationality = $("#nationality").val();
            var pincode = $("#pincode").val();
            var gender = $("#gender").val();
            var locality = $("#locality").val();
            var profession = $("#profession").val();
            var salary = $("#salary").val();
            var source = $("#source").val();
            var operator_name = $("#operator_name").val();
            var minAge = $("#minAge").val();
            var maxAge = $("#maxAge").val();
            var lead_from_date = $(".from_date").val();
            var lead_to_date = $(".to_date").val();

            var filter_selected = new Array();
            var filter_obj = {};
            var filter_by_html = '';

            if (country.length > 0) {
                filter_selected.push('country');
                
                var country_name = $("#country option:selected").map(function() {
                    return $(this).text();
                }).get();
                filter_obj['country'] = country_name;
                filter_by_html += '<p><b>country:</b> ' + country_name.join(', ') + '</p>';
            }
            
            if (state.length > 0) {
                filter_selected.push('state');
                
                var state_name = $("#state option:selected").map(function() {
                    return $(this).text();
                }).get();
                filter_obj['state'] = state_name;
                filter_by_html += '<p><b>State:</b> ' + state_name.join(', ') + '</p>';
            }

            if (city.length > 0) {
                filter_selected.push('city');
                
                var city_name = $("#city option:selected").map(function() {
                    return $(this).text();
                }).get();
                filter_obj['city'] = city_name;
                filter_by_html += '<p><b>City:</b> ' + city_name.join(', ') + '</p>';
            }

            if (nationality != '') {
                filter_selected.push('nationality');
                var nationality_name = $("#nationality option:selected").map(function() {
                    return $(this).text();
                }).get();
                filter_obj['nationality'] = nationality_name; 
                filter_by_html += '<p><b>Nationality:</b> ' + nationality_name.join(', ') + '</p>';
            }

            if (pincode != '') {
                filter_selected.push('pincode');
                filter_obj['pincode'] = pincode;
                filter_by_html += '<p><b>Pincode:</b> ' + pincode + '</p>';
            }

            if (gender != '') {
                filter_selected.push('gender');
                
                var gender_name = $("#gender option:selected").map(function() {
                    return $(this).text();
                }).get();
                filter_obj['gender'] = gender_name;
                filter_by_html += '<p><b>Gender:</b> ' + gender_name.join(', ') + '</p>';
            }

            if (locality != '') {
                filter_selected.push('locality');
                filter_obj['locality'] = locality;
                filter_by_html += '<p><b>Locality:</b> ' + locality + '</p>';
            }


            if (profession != '') {
                filter_selected.push('profession');
                filter_obj['profession'] = profession;
                var profession_name = $("#profession option:selected").map(function() {
                    return $(this).text();
                }).get().join(', ');
                filter_by_html += '<p><b>Profession:</b> ' + profession_name + '</p>';

            }


            if (salary != '') {
                filter_selected.push('salary');
                filter_obj['salary'] = salary;
                filter_by_html += '<p><b>Salary:</b> ' + salary + '</p>';

            }

            if (source != '') {
                filter_selected.push('source');
                filter_obj['source'] = source;
                var source_name = $("#source option:selected").map(function() {
                    return $(this).text();
                }).get().join(', ');
                filter_by_html += '<p><b>Source:</b> ' + source_name + '</p>';
            }

            if (operator_name != '') {
                filter_selected.push('operator name');
                filter_obj['operator_name'] = operator_name;
                var operator_text = $("#operator_name option:selected").map(function() {
                    return $(this).text();
                }).get().join(', ');
                filter_by_html += '<p><b>Operator Name:</b> ' + operator_text + '</p>';
            }


            if (lead_from_date != '') {
                filter_selected.push('lead from date');
                filter_obj['lead_from_date'] = lead_from_date;
                filter_by_html += '<p><b>Lead From Date:</b> ' + lead_from_date + '</p>';

            }


            if (lead_to_date != '') {
                filter_selected.push('lead to date');
                filter_obj['lead_to_date'] = lead_to_date;
                filter_by_html += '<p><b>Lead To Date:</b> ' + lead_to_date + '</p>';

            }

            /* Filters - STOP */


            //console.log('filter_obj');         
            //console.log(filter_obj);


            var filter_by = filter_selected.join();
            //$("#filter-by-val").text(filter_by);

            $("#campaing-val").text(campaign_name);
            $(".category-val").text(category_id);
            $("#campaing-content-val").text($("#content_label_name").val());
            $("#sms_content_val").text(message);
            $("#detination_url_val").text(detination_url);
            $("#test_sms_content_val").text(message);
            $("#test_detination_url_val").text(detination_url);

            $('#scheduled-campaing-val').text(campaign_name);
            $('#scheduled-category-val').text(category_id);
            $("#schedule_sms_content_val").text(message);
            $("#schedule_detination_url_val").text(detination_url);
            var filter_category_id = $("#filter_category_id").val();
            var filter_topper_id         = $("#filter_topper_id").val();

            if (filter_category_id.length > 0) {
                var filter_category_name = $("#filter_category_id option:selected").map(function() {
                    return $(this).text();
                }).get().join(', ');
                filter_by_html += '<p><b>Filter Category:</b> ' + filter_category_name + '</p>';
            }

            filter_by_html += '<p><b>Topper:</b> ' + filter_topper_id + '</p>';

            /*  include and exclude campaign multiple filters data starts */
           
            //var mode_step4          = '<p><b>Mode: ';

            var include_camp_ids     = [];
            var exclude_camp_ids     = [];
            var include_cont_lbl_ids = [];
            var exclude_cont_lbl_ids = [];
            if (filter_topper_id !== 'non-sender') {
              for (i = 1; i <= $('.multiple_filters_search').length; i++) {
                
                if ($("#filter_mode_" + i).prop("checked") == true) {
                  var filter_mode            = 'include';
                } else {
                  var filter_mode            = 'exclude';
                }


                var filter_campaign_id_value = $("#filter_campaign_id_" + i).val();
                var filter_content_lbl_id_value = $("#filter_content_label_id_" + i).val();

                if (Array.isArray(filter_campaign_id_value) && filter_campaign_id_value.length) {
                  var filter_campaign_name_txt = $("#filter_campaign_id_" + i + " option:selected").map(function() {
                    return $(this).text();
                  }).get().join(', ');

                  filter_by_html        += '<p><b>Filter ' + i + ' : </b><br/>Mode : ' + filter_mode + '<br/>';

                  if (filter_mode == 'include') {
                    if (filter_campaign_id_value.includes("0")) {
                      if (filter_topper_id == 'clicker') {
                        $('#filter_campaign_id_' + i + ' option:not(:first)').each(function() { 
                          if (include_camp_ids.indexOf($(this).attr('value')) === -1) {
                            include_camp_ids.push($(this).attr('value'));
                          }
                        });
                      } else {
                        include_camp_ids = filter_campaign_id_value;
                        filter_content_lbl_id_value = [];
                      }
                    } else if (Array.isArray(filter_campaign_id_value)) {
                      for (var j = 0, len = filter_campaign_id_value.length; j < len; j++) {
                        if (include_camp_ids.indexOf(filter_campaign_id_value[j].toString()) === -1) {
                          include_camp_ids.push(filter_campaign_id_value[j]);
                        }
                      }
                    } else {
                      include_camp_ids.push(filter_campaign_id_value[j]);
                    } 
                   
                  } else {
                    if (filter_campaign_id_value.includes("0")) {
                      if (filter_topper_id == 'clicker') {
                        $('#filter_campaign_id_' + i + ' option:not(:first)').each(function() { 
                          if (exclude_camp_ids.indexOf($(this).attr('value')) === -1) {
                            exclude_camp_ids.push( $(this).attr('value') );
                          }
                        });
                      } else {
                        exclude_camp_ids = filter_campaign_id_value;
                        filter_content_lbl_id_value = [];
                      }
                    } else if (Array.isArray(filter_campaign_id_value)) {

                      for (var j = 0, len = filter_campaign_id_value.length; j < len; j++) {
                        if (exclude_camp_ids.indexOf(filter_campaign_id_value[j].toString()) === -1) {
                          if (exclude_camp_ids.indexOf($(this).attr('value')) === -1) {
                            exclude_camp_ids.push(filter_campaign_id_value[j]);
                          }
                        }
                      }
                    } else {
                      exclude_camp_ids.push(filter_campaign_id_value[j]);
                    } 
                  }

                  filter_by_html        += 'Filter Campaign : ' + filter_campaign_name_txt;

                }

                
                if (Array.isArray(filter_content_lbl_id_value) && filter_content_lbl_id_value.length) {
                  var filter_content_label_name_txt = $('#filter_content_label_id_' + i + ' option:selected').map(function() {
                    return $(this).text();
                  }).get().join(', ');

                  if (filter_mode == 'include') {
                    if (filter_content_lbl_id_value.includes("0")) {
                      if (filter_topper_id == 'clicker') {
                        $('#filter_content_label_id_' + i + ' option:not(:first)').each(function() { 
                          if (include_cont_lbl_ids.indexOf($(this).attr('value')) === -1) {
                            include_cont_lbl_ids.push( $(this).attr('value') );
                          }
                        });
                      }
                    } else if (Array.isArray(filter_content_lbl_id_value)) {
                      var fil_con_data_check = [];
                      $('#filter_content_label_id_' + i + ' option:selected').each(function() { 
                        fil_con_data_check.push( $(this).attr('con_camp_id') ); //getting campaign id values
                      });

                      if (fil_con_data_check) {
                        var res = include_camp_ids.filter( function(n) { return this.has(n) }, new Set(fil_con_data_check) );
                        if (res.length > 0) {
                          include_camp_ids = include_camp_ids.filter(function(i) {return res.indexOf(i) < 0;});
                        }
                      }
                      for (var j = 0, len = filter_content_lbl_id_value.length; j < len; j++) {
                        if (include_cont_lbl_ids.indexOf(filter_content_lbl_id_value[j].toString()) === -1) {
                          include_cont_lbl_ids.push(filter_content_lbl_id_value[j]);
                        }
                      }
                    } else {
                      include_cont_lbl_ids.push(filter_content_lbl_id_value[j]);
                    } 

                  } else {
                   if (filter_content_lbl_id_value.includes("0")) {
                     if (filter_topper_id == 'clicker') {
                      $('#filter_content_label_id_' + i + ' option:not(:first)').each(function() { 
                        if (exclude_cont_lbl_ids.indexOf($(this).attr('value')) === -1) {
                          exclude_cont_lbl_ids.push( $(this).attr('value') );
                        }
                      });
                    }
                    } else if (Array.isArray(filter_content_lbl_id_value)) {

                      for (var j = 0, len = filter_content_lbl_id_value.length; j < len; j++) {
                        if (exclude_cont_lbl_ids.indexOf(filter_content_lbl_id_value[j].toString()) === -1) {
                          
                          exclude_cont_lbl_ids.push(filter_content_lbl_id_value[j]);
                        }
                      }

                      var fil_con_data_check = [];
                      $('#filter_content_label_id_' + i + ' option:selected').each(function() { 
                        fil_con_data_check.push( $(this).attr('con_camp_id') ); //getting campaign id values
                      });

                      if (fil_con_data_check) {
                        var res = exclude_camp_ids.filter( function(n) { return this.has(n) }, new Set(fil_con_data_check) );
                        if (res.length > 0) {
                          exclude_camp_ids = exclude_camp_ids.filter(function(i) {return res.indexOf(i) < 0;});
                        }
                      }
                    } else {
                      exclude_cont_lbl_ids.push(filter_content_lbl_id_value[j]);
                    } 
                  }

                  filter_by_html        += '<br/>Filter Content Label : ' + filter_content_label_name_txt + '</p>';
                }
                
              }

              filter_by_html        += '</p>';
            } 

            

            var minRecord = $("#minRecord").val();
            var maxRecord = $("#maxRecord").val();

            if ($("#filter_mode").prop("checked") == true) {
                var filter_mode = 'include';
            } else {
                var filter_mode = 'exclude';
            }

            var minAge = $("#minAge").val();
            var maxAge = $("#maxAge").val();


            
            
            
            if (minAge > 0 || maxAge < 100) {
                filter_by_html += '<p><b>Age:</b> ' + minAge + ' to ' + maxAge + '</p>';
            }
           
            var check_empty = jQuery.isEmptyObject(filter_obj);

            if (check_empty) {
                filter_obj['empty'] = 'null';
            }

            $("#filter-by-val").html(filter_by_html);

            var step_no = $("li.current .step-number").text();
            var content_view_page = $("#page_name").val();
            var campaign_id_filter = $("#stored_camp_id").val();
            var smpp_name = $("#smpp_name").val();
            var smpp_sender_name = $("#sender_addr").val();

            if (content_view_page == 'add-content') {
              
            }

            var total_contacts_val = parseInt($('#total_contacts').val()); 
             $('#from_rec_val').val("");
             $('#to_rec_val').val("");
             $("#max_err_msg").hide();
             $("#errmsg").hide();

               var $remaining = $('#remaining'),
                   $messages = $remaining.next();
           
           
                var chars = $("#sent_messages").val().length,
                    messages = Math.ceil(chars / 140),
                    remaining = messages * 140 - (chars % (messages * 140) || messages * 140);
           
                    $remaining.text(remaining);
                    $(".preview_remaining").text(remaining);
                         
            $(".total-contacts-val").text(0);
           
            if ((step_no == 'Step 3' &&  content_view_page == 'view') || (step_no == 'Step 4' && content_view_page == 'add')) {
                
                var cat_id=$('#category_id').val();
                $.ajax({
                    url: '/sms/uni_submit',
                    type: 'POST',
                    dataType: 'json',
                    cache: false,
                    data: {
                        
                        'category_id':cat_id,
                        'filter_category_id': filter_category_id,
                        'filter_topper_id': filter_topper_id,
                        'filter_search': filter_obj,
                        'filter_mode': filter_mode,
                        'minRecord': minRecord,
                        'maxRecord': maxRecord,
                        'stored_camp_id':campaign_id_filter,
                        'minAge': minAge,
                        'maxAge': maxAge,
                        'page_name':content_view_page,
                        'smpp_name':smpp_name,
                        'smpp_sender_name':smpp_sender_name,
                        'include_camp_ids'     : include_camp_ids,
                        'exclude_camp_ids'     : exclude_camp_ids,
                        'include_cont_lbl_ids' : include_cont_lbl_ids,
                        'exclude_cont_lbl_ids' : exclude_cont_lbl_ids,
                        'button_type':'search'
                    },
                    timeout: 1000000,
                    success: function(data) {
                    

                            $("#campaing-content-val").text($("#content_label_name").val());
                            
                            $("#total-contacts-val").text(data.total_contacts);
                            $("#total_contacts").val(data.total_contacts);
                            $("#record_loader").hide();

                            
                            if (data.total_contacts=='0') {

                                jQuery('#preview').attr("style", "display:none");
                                jQuery('#scheduled_sms').attr("style", "display:none");

                                 $('#from_rec_val').prop("disabled", true);
                                 $('#to_rec_val').prop("disabled", true);
                                 $('#from_rec_val').val("");
                                 $('#to_rec_val').val("");
                                 $("#max_err_msg").hide();
                                 $("#errmsg").hide();
                                 $('#zero_notification').show();

                            }else {
                                 jQuery('#preview').attr("style", "display:block");
                                 jQuery('#preview').attr("style", "width:30%");

                                 jQuery('#scheduled_sms').attr("style", "display:block");
                                 jQuery('#scheduled_sms').attr("style", "width:30%");

                                 $('#from_rec_val').prop("disabled", false);
                            }



                    }
                });
            }


        } // onstepchanged end

    });
});