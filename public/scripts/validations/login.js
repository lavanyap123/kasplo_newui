$(document).on('click', '#login_btn', function() {
  var validator = $("#login_form").validate({
    rules : {
      email : {
        required : true,
        email : true
      },
      password : {
        required : true
      }
    },

    messages : {
      email : {
        required  : "Email is required",
        email     : "Please enter valid email"
      },
      password : {
        required : "Password is required"
      }

    },

    submitHandler: function(form) {
      
      $("#login_btn").prop('disabled', true);
      var email    = $("#email").val();
      var password = $("#password").val();

      $.ajax({
        type     : 'POST',
        url      : '/users/login',
        data     : {'email' : email, 'password' : password},
        dataType : 'json',
        success  : function (data) {
          
          if (data.status == 200) {
            //$("#login_form")[0].reset();
            $('#notif').html('<p>' + data.message + '</p>');
            window.location.href = data.redirect;
          } else {
            $("#login_btn").prop('disabled', false);
            $('#notif').html('<p>' + data.message + '</p>');
          }             

        },
     
      });
    }
    
  });            
}); //document ready  