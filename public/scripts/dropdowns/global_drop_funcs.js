function getSelect2format(data, table_fields) {
    var data_ar = [];
    if (data.length > 0 ) {
        for (i = 0; i < data.length; i++) {
            data_ar.push({'id' : data[i][table_fields._id], 'text' : data[i][table_fields.name]})
        }
    }
    return data_ar;
} //getSelect2format


function getPaginationResult(total_len, limit, offset) {
  
  var fetched_records = total_len - (offset + limit);
  var is_page_next  = false; 

  if (fetched_records > 0 && fetched_records >= total_len) {
    is_page_next  = false;
  } else if (fetched_records > 0 && fetched_records < total_len) {
    is_page_next = true;
  } 
    return is_page_next;
} //getPaginationResult

function getNestedChildren(list, table_fields) {
    var roots = []; console.log(list);
    for (i = 0; i < list.length; i += 1) {
      node = list[i];
      //console.log('children_length' + node[table_fields.child_id].length);
      var child_length = node[table_fields.child_id].length;
      var obj = {
          
          text: node[table_fields.parent_name],
          children : [],
          "element": HTMLOptGroupElement
        };
      for (j = 0; j < child_length; j += 1) {
        var child_node_id = node[table_fields.child_id][j];
        var child_node_name = node[table_fields.child_name][j];

        console.log(child_node_name)
      
      // if (roots.length === 0) {
      //   //console.log(node[table_fields.child_id]);
      //   var obj = {
      //     text: node[table_fields.parent_name],
      //     children: [{ id: node[table_fields.child_id], text: node[table_fields.child_name] }]
      //   };
      //   roots.push(obj);
      // } else {
        //console.log('else   ' + node[table_fields.parent_name]);
        obj['children'].push({ id: child_node_id, text: child_node_name });
        }
        
          roots.push(obj);

      
    }
    console.log(roots);
   
    return roots;
} //getNestedChildren
