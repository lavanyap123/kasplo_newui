$(document).ready(function () {
    var limit = 5;
    $('#category_id').select2({
      placeholder: "Please select category",
      ajax: {
        url: "/category/get_all_category",
        dataType: "json",
        global: false,
        cache: true,
        delay: 250,
        //allowClear: true,
        minimumInputLength: 2,
        data: function(params) {
          return {
            q: params.term, // search term
            _page: params.page || 1,
            _limit: limit // page size
          };
        },
        processResults: function(data, params) {
        
          var total_len      = data.total_count; 
          params.page        = params.page || 1;
         
          var total_pages    = Math.ceil(total_len / limit); 

          var offset         = parseInt(((params.page - 1)) * limit); 

          var get_data       = getSelect2format(data.category_data); 
          
          var has_page_next  = getPaginationResult(total_len, limit, offset)

            return {
              results    : get_data,
              pagination : {
                more  : has_page_next
              }
            }
        } //end of process results
      } // end of ajax
    });

    $('#source_id').select2({
      placeholder: "Please select source",
      ajax: {
        url: "/source/get_source_dropdown",
        dataType: "json",
        global: false,
        cache: false,
        delay: 250,
        //allowClear: true,
        minimumInputLength: 2,
        data: function(params) {
          return {
            q: params.term, // search term
            _page: params.page || 1,
            _limit: limit // page size
          };
        },
        processResults: function(data, params) {
         
          var total_len      = data.total_count + 1; 
          var total_pages    = Math.ceil(total_len / limit); 

          params.page        = params.page || 1;
          
          var offset         = parseInt(((params.page - 1)) * limit); 
          
         
          console.log(data.source_data);
          var get_data       = getSelect2format(data.source_data); 
          
          var has_page_next  = getPaginationResult(total_len, limit, offset)

          return {
            results: get_data,
            pagination: {
              more: has_page_next
            }
          };
        } //end of process results
      } // end of ajax
    });
}); //document ready


function getDropdownByAjax () {

} //getDropdownByAjax

function getSelect2format(data, is_child_node = false) {
    var data_ar = [];
    if (data.length > 0 && !is_child_node) {
        for (i = 0; i < data.length; i++) {
            data_ar.push({'id' : data[i]._id, 'text' : data[i].name})
        }
    }
    return data_ar;
} //getSelect2format


function getPaginationResult(total_len, limit, offset) {
  
  var fetched_records = total_len - (offset + limit);
  var is_page_next  = false; 

  if (fetched_records > 0 && fetched_records >= total_len) {
    is_page_next  = false;
  } else if (fetched_records > 0 && fetched_records < total_len) {
    is_page_next = true;
  } 
    return is_page_next;
} //getPaginationResult