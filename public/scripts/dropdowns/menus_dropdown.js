$(document).ready(function () {
  $('#menu_type').trigger('change');
    var limit = 5;
    $('#menu_type').select2({
      placeholder: $(this).data('value'),
      multiple: true,
      ajax: {
        url: "/menus/get_menutype_dropdown",
        dataType: "json",
        global: false,
        cache: true,
        delay: 250,
        //allowClear: true,
        minimumInputLength: 2,
        data: function(params) {
          return {
            q: params.term, // search term
            _page: params.page || 1,
            _limit: limit // page size
          };
        },
        processResults: function(data, params) {
          var id     = $(this).data('id');
          console.log('id ===' + id)
          var total_len      = data.total_count; 
          params.page        = params.page || 1;
         
          var total_pages    = Math.ceil(total_len / limit); 

          var offset         = parseInt(((params.page - 1)) * limit); 
          var table_fields   = {
            '_id'  : '_id',
            'name' : 'menu_type_name'
          }
          var get_data       = getSelect2format(data.result_data, table_fields); 
          
          var has_page_next  = getPaginationResult(total_len, limit, offset)

            return {
              results    : get_data,
              pagination : {
                more  : has_page_next
              }
            }
        } //end of process results
      } // end of ajax
    }); //menu_type
    
    $('#menu_level2').select2({
      placeholder: $(this).data('value'),
      ajax: {
        url: "/menus/get_menutype_dropdown",
        dataType: "json",
        global: false,
        cache: true,
        delay: 250,
        //allowClear: true,
        minimumInputLength: 2,
        data: function(params) {
          return {
            type : 'level2',
            q: params.term, // search term
            _page: params.page || 1,
            _limit: limit // page size
          };
        },
        processResults: function(data, params) {
          
          var total_len      = data.total_count; 
          params.page        = params.page || 1;
         
          var total_pages    = Math.ceil(total_len / limit); 

          var offset         = parseInt(((params.page - 1)) * limit); 
          var table_fields   = {
            'parent_id'   : '_id',
            'parent_name' : 'menu_name',
            'child_id'    : 'menu_type_id',
            'child_name'  : 'menu_type_name',
          }
          var get_data       = getNestedChildren(data.result_data, table_fields); 
          
          var has_page_next  = getPaginationResult(total_len, limit, offset)
            console.log(has_page_next);

            return {
              results    : get_data,
 
              pagination : {
                more  : has_page_next
              }
            }
        }, //end of process results
        templateResult: function(data) {
         if (typeof data.children != 'undefined') {
          $(".select2-results__group").each(function() {
           if (data.text == $(this).text()) {
            return data.text = '';
           }
          });
         }
         return data.text;
        }
      } // end of ajax
    }); //menu_type
}); //document ready




