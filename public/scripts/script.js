
$(".goToHome").click(function(){
  location.href = "dashboard.html";
});

$(".createNewCampaign").click( function(){
    location.href = "add-new-campaign";
});

$(".createNewUser").click( function(){
    location.href = "add-user.html";
});

$(".goToViewCampaign").click( function(){
  location.href = "campaign-view.html";
});

$(".toggle-password").click(function(){

    var inputPassword = document.getElementById("password");

    if (inputPassword.type === "password") {
        inputPassword.type = "text";
        $(".fa").removeClass("fa-eye-slash");
    } else {
        inputPassword.type = "password";
        $(".fa").addClass("fa-eye-slash");
    }
});

$('.toggledClass').click(function(e){
    e.preventDefault();
    $(this).find('.fa-chevron-down').toggleClass('displayNone');
    $(this).find('.fa-chevron-up').toggleClass('displayBlock');
});

// Script For Leftbar Width Changes 

var expandLeftBar = $("#expandLeftBarWidth")

$(".widthChangeIconSec").click(function(){

    $(".leftBarParent").removeClass("expandLeftBarWidth").addClass("minimisedLeftBarWidth");
    $(".contentRightParentSec").removeClass("compressedContentWidth").addClass("expandedContentWidth");
    $(this).hide();
    $(".smallWidthChangeIconSec").show();

});

$(".smallWidthChangeIconSec").click(function(){

  $(".leftBarParent").removeClass("minimisedLeftBarWidth").addClass("expandLeftBarWidth");
  $(".contentRightParentSec").removeClass("expandedContentWidth").addClass("compressedContentWidth");
  $(this).hide();
  $(".widthChangeIconSec").show();
  
});