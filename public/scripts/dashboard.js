
        var options = {
            chart: {
                height: 370,
                type: 'bar',
            },
            colors : ['#4E8DF9'],
            plotOptions: {
                bar: {
                    horizontal: false,
                    columnWidth: '20%',
                },
            },
            dataLabels: {
                enabled: false
            },
            stroke: {
                show: true,
                width: 2,
                colors: ['transparent']
            },
            series: [{
                name: 'Clicks',
                data: [<%= last_seven_days_count %>]
            }],
            xaxis: {
                categories: [],
            },
            yaxis: {
                categories: ['0', '1K', '2K', '3K', '4k', '5K', '6K'],
            },
            fill: {
                opacity: 1

            },
            tooltip: {
                y: {
                    formatter: function (val) {
                        return  val 
                    }
                }
            }
        }

        var chart = new ApexCharts(
            document.querySelector("#chart"),
            options
        );

        chart.render();
       

        
        var categoryOptions = {
            chart: {
                width: 360,
                height: 400,
                type: 'donut',
                offsetX: -20,
                offsetY: 20,
            },
            colors: ["#04386C","#0349A0","#003BA0","#0044B6","#0355BD","#0355BD","#4E8DF9","#5A97FF","#82B0FF","#82B0FF"],
            stroke: {
                show: false,
                curve: 'smooth',
                width: 0,
                dashArray: 0,      
            },
            legend: {
                show: true,
                showForSingleSeries: false,
                showForNullSeries: true,
                showForZeroSeries: true,
                position: 'bottom',
                horizontalAlign: 'left', 
                floating: screenLeft,
                fontSize: '11px',
                formatter: undefined,
                inverseOrder: false,
                width: undefined,
                height: undefined,
                tooltipHoverFormatter: undefined,
                offsetX: 0,
                offsetY: -10,
                labels: {
                    colors: undefined,
                    useSeriesColors: false
                },
                markers: {
                    width: 12,
                    height: 12,
                    strokeWidth: 0,
                    strokeColor: '#fff',
                    fillColors: undefined,
                    radius: 12,
                    customHTML: undefined,
                    onClick: undefined,
                    offsetX: 0,
                    offsetY: 0
                },
                itemMargin: {
                    horizontal: 3,
                    vertical: 8
                },
                onItemClick: {
                    toggleDataSeries: true
                },
                onItemHover: {
                    highlightDataSeries: true
                },
            },
            dataLabels: {
                enabled: false
            },
            labels: ['BFSI_CreditScore', 'Health', 'BFSI - Personal Loan', 'BFSI-Car Insurance', 'BFSI-Health Insurance', 'BFSI - Credit Card', 'Real Estate', 'Vending Machines', 'BFSI_Demat', 'Design and Architecture'],
            series: [362238,97271,52601,29005,27477,23173,18650,17955,15803,14122],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        }

        var chart = new ApexCharts(
            document.querySelector("#piechart"),
            categoryOptions
        );

        chart.render();



        var campaignOptions = {
            chart: {
                width: 360,
                height: 400,
                type: 'donut',
                offsetX: -20,
                offsetY: 20,
                colors: ['#546E7A', '#E91E63']
            },
            colors: ["#04386C","#0349A0","#003BA0","#0044B6","#0355BD","#0355BD","#4E8DF9","#5A97FF","#82B0FF","#82B0FF"],
            stroke: {
                show: false,
                curve: 'smooth',
                width: 0,
                dashArray: 0,      
            },
            legend: {
                show: true,
                showForSingleSeries: false,
                showForNullSeries: true,
                showForZeroSeries: true,
                position: 'bottom',
                horizontalAlign: 'left', 
                floating: screenLeft,
                fontSize: '11px',
                formatter: undefined,
                inverseOrder: false,
                width: undefined,
                height: undefined,
                tooltipHoverFormatter: undefined,
                offsetX: 0,
                offsetY: -10,
                labels: {
                    colors: undefined,
                    useSeriesColors: false
                },
                markers: {
                    width: 12,
                    height: 12,
                    strokeWidth: 0,
                    strokeColor: '#fff',
                    fillColors: undefined,
                    radius: 12,
                    customHTML: undefined,
                    onClick: undefined,
                    offsetX: 0,
                    offsetY: 0
                },
                itemMargin: {
                    horizontal: 3,
                    vertical: 8
                },
                onItemClick: {
                    toggleDataSeries: true
                },
                onItemHover: {
                    highlightDataSeries: true
                },
            },
            dataLabels: {
                enabled: false
            },
            labels: ['BFSI_CreditScore', 'BFSI - Personal Loan', 'BFSI-Car Insurance', 'Health',   'BFSI-Health Insurance', 'BFSI - Credit Card', 'Real Estate', 'Vending Machines', 'BFSI_Demat', 'Design and Architecture'],
            series: [362238,97271,52601,29005,27477,23173,18650,17955,15803,14122],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 200
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        }

        var chart = new ApexCharts(
            document.querySelector("#donoughtchart"),
            campaignOptions
        );

        chart.render();



        // SMS Log Charts




        var smsLogOptions = {
            chart: {
                width: 450,
                height: 450,
                type: 'donut',
                offsetX: 150,
                offsetY: 20,
                colors: ['#546E7A', '#E91E63']
            },
            colors: ["#48FF85","#FF4848","#4E8DF9"],
            stroke: {
                show: false,
                curve: 'smooth',
                width: 0,
                dashArray: 0,      
            },
            legend: {
                show: true,
                showForSingleSeries: false,
                showForNullSeries: true,
                showForZeroSeries: true,
                position: 'bottom',
                horizontalAlign: 'center', 
                floating: screenLeft,
                fontSize: '11px',
                formatter: undefined,
                inverseOrder: false,
                width: undefined,
                height: undefined,
                tooltipHoverFormatter: undefined,
                offsetX: 0,
                offsetY: -10,
                labels: {
                    colors: undefined,
                    useSeriesColors: false
                },
                markers: {
                    width: 12,
                    height: 12,
                    strokeWidth: 0,
                    strokeColor: '#fff',
                    fillColors: undefined,
                    radius: 10,
                    customHTML: undefined,
                    onClick: undefined,
                    offsetX: 0,
                    offsetY: 0
                },
                itemMargin: {
                    horizontal: 3,
                    vertical: 8
                },
                onItemClick: {
                    toggleDataSeries: true
                },
                onItemHover: {
                    highlightDataSeries: true
                },
            },
            dataLabels: {
                enabled: false
            },
            labels: ['Delivered', 'Un-Delivered', 'Click'],
            series: [362238,97271,52601,],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 400
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        }

        var chart = new ApexCharts(
            document.querySelector("#smslogdonoughtchart"),
            smsLogOptions
        );

        chart.render();


        //SMPP Line Chart

        var smpplinechartoptions = {
            series: [{
              name: "Session Duration",
              data: [45, 52, 38, 24, 33, 26, 21, 20, 6, 8]
            },
            {
              name: "Page Views",
              data: [35, 41, 62, 42, 13, 18, 29, 37, 36, 51]
            },
            {
              name: 'Total Visits',
              data: [87, 57, 74, 99, 75, 38, 62, 47, 82, 56]
            }
          ],
            chart: {
            height: 350,
            type: 'line',
            colors: ['#48FF85','#4E8DF9','#FF4848'],
            zoom: {
              enabled: false
            },
          },
          colors: ['#48FF85','#4E8DF9','#FF4848'],
          dataLabels: {
            enabled: false
          },
          stroke: {
            width: [2, 2, 2],
            curve: 'smooth',
            dashArray: [0, 0, 0]
          },
          title: {
            text: 'Page Statistics',
            align: 'left'
          },
          legend: {
            tooltipHoverFormatter: function(val, opts) {
              return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
            }
          },
          markers: {
            size: 0,
            hover: {
              sizeOffset: 6
            }
          },
          xaxis: {
            categories: ['01 Jan', '02 Jan', '03 Jan', '04 Jan', '05 Jan', '06 Jan', '07 Jan', '08 Jan', '09 Jan',
              '10 Jan'
            ],
          },
          tooltip: {
            y: [
              {
                title: {
                  formatter: function (val) {
                    return val + " (mins)"
                  }
                }
              },
              {
                title: {
                  formatter: function (val) {
                    return val + " per session"
                  }
                }
              },
              {
                title: {
                  formatter: function (val) {
                    return val;
                  }
                }
              }
            ]
          },
          grid: {
            borderColor: '#f1f1f1',
          }
          };
  
          var smpplinechart = new ApexCharts(document.querySelector("#smpplinechart"), smpplinechartoptions);
          smpplinechart.render();


        // Smpp 1st Donought Chart
        
        var smppfirstDonoughtOptions = {
            chart: {
                width: 300,
                height: 300,
                type: 'donut',
                offsetX: 50,
                offsetY: 30,
                colors: ['#48FF85','#4E8DF9','#FF4848']
            },
            colors: ['#48FF85','#4E8DF9','#FF4848'],
            stroke: {
                show: false,
                curve: 'smooth',
                width: 0,
                dashArray: 0,      
            },
            legend: {
                show: true,
                showForSingleSeries: false,
                showForNullSeries: true,
                showForZeroSeries: true,
                position: 'bottom',
                horizontalAlign: 'center', 
                floating: screenLeft,
                fontSize: '11px',
                formatter: undefined,
                inverseOrder: false,
                width: undefined,
                height: undefined,
                tooltipHoverFormatter: undefined,
                offsetX: 0,
                offsetY: -10,
                labels: {
                    colors: undefined,
                    useSeriesColors: false
                },
                markers: {
                    width: 12,
                    height: 12,
                    strokeWidth: 0,
                    strokeColor: '#fff',
                    fillColors: undefined,
                    radius: 10,
                    customHTML: undefined,
                    onClick: undefined,
                    offsetX: 0,
                    offsetY: 0
                },
                itemMargin: {
                    horizontal: 3,
                    vertical: 8
                },
                onItemClick: {
                    toggleDataSeries: true
                },
                onItemHover: {
                    highlightDataSeries: true
                },
            },
            dataLabels: {
                enabled: false
            },
            labels: ['Delivered', 'Sent', 'Click'],
            series: [362238,97271,52601],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 400
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        }

        var smppFirstDonoughtchart = new ApexCharts(document.querySelector("#smppdonoughtfirstchart"),smppfirstDonoughtOptions);

        smppFirstDonoughtchart.render();


        // Smpp 2nd Donought Chart
        
        
        var smppOptions = {
            chart: {
                width: 450,
                height: 450,
                type: 'donut',
                offsetX: 150,
                offsetY: 20,
                colors: ['#0044B6', '#0C66D8', '#5A97FF', '#B7D1FF']
            },
            colors: ['#0044B6', '#0C66D8', '#848484', '#5A97FF', '#B7D1FF', '#848484', '#848484', '#848484' ],
            stroke: {
                show: false,
                curve: 'smooth',
                width: 0,
                dashArray: 0,      
            },
            legend: {
                show: true,
                showForSingleSeries: false,
                showForNullSeries: true,
                showForZeroSeries: true,
                position: 'bottom',
                horizontalAlign: 'center', 
                floating: screenLeft,
                fontSize: '11px',
                formatter: undefined,
                inverseOrder: false,
                width: undefined,
                height: undefined,
                tooltipHoverFormatter: undefined,
                offsetX: 0,
                offsetY: -10,
                labels: {
                    colors: undefined,
                    useSeriesColors: false
                },
                markers: {
                    width: 12,
                    height: 12,
                    strokeWidth: 0,
                    strokeColor: '#fff',
                    fillColors: undefined,
                    radius: 10,
                    customHTML: undefined,
                    onClick: undefined,
                    offsetX: 0,
                    offsetY: 0
                },
                itemMargin: {
                    horizontal: 3,
                    vertical: 8
                },
                onItemClick: {
                    toggleDataSeries: true
                },
                onItemHover: {
                    highlightDataSeries: true
                },
            },
            dataLabels: {
                enabled: false
            },
            labels: ['Total Sent', 'DLR Pending', 'Expired', 'Delivered',  'Failed', 'DND Failed', 'Blocked', 'Others'],
            series: [362238,97271,0,52601,42601,0,0,0],
            responsive: [{
                breakpoint: 480,
                options: {
                    chart: {
                        width: 400
                    },
                    legend: {
                        position: 'bottom'
                    }
                }
            }]
        }

        var chart = new ApexCharts(document.querySelector("#smppdonoughtchart"),smppOptions);

        chart.render();