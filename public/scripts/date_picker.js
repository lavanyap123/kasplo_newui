$(document).ready(function() {       

       $('.from_date').daterangepicker({
         singleDatePicker: true,
         showDropdowns: true,
         //autoUpdateInput: false,
         opens: "right",
         locale: {
           format: 'YYYY-MM-DD'
         },
         maxDate: new Date()
       });
           
       var from_date_val = '';
        $('.from_date').on('change', function() {

          from_date_val = $(".from_date").val(); 
          $('.to_date').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            //autoUpdateInput: false,
            opens: "right",
            locale: {
            format: 'YYYY-MM-DD'
          },
          minDate: new Date(from_date_val),
          maxDate: new Date()
        });
          $('.to_date').val('');
      });
        $('.from_date').val('');
    });