const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  name: {
    type: String
  },
  first_name: {
    type: String,
    required: true
  },
  last_name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  mobile: {
    type: String,
    required: true
  },
  country_id: {
    type: String,
    required: true
  },
  im_id: {
    type: String
  },
  im_account: {
    type : String
  },
  address: {
    type : String
  },
  team_id: {
    type : String,
    required: true
  },
  team_leader: {
    type : String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  role_id : {
    type : String,
    required : true
  },
  role_name : {
    type : String,
    required : true
  },
  release_read_status: {
    type: String,
  },
  pcrypto: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  },
  permission_access: {
    type: Object
  },
  role_based_access : {
    type: Array
  }
});

const User = mongoose.model('user', UserSchema);

module.exports = User;
