const express                = require('express');
const app2                   = express();
const passport2              = require('passport');
var cookieParser             = require('cookie-parser');
const flash                  = require('connect-flash');
const mongoose               = require('mongoose');
var nodemailer               = require('nodemailer');
require('./config/passport')(passport2);

// DB Config
const db                     = require('./config/keys').mongoURI;
const db_options             = require('./config/keys').db_options;
const database_name          = require('./config/keys').database_name;
const mode                   = require('./config/keys').mode;
const base_URL               = require('./config/keys').base_URL;
const mail_from              = require('./config/keys').mail_from;
const to_mail_ids            = require('./config/keys').to_mail_ids;
const smtp_credentials       = require('./config/keys').smtp_credentials;

mongoose.connect(db,db_options)
.then(() => console.log('APP.js = MongoDB Connected from='+db))
.catch(err => console.log(err));


mongoose.connection.on('error', (err) => {
  //console.log(err);
  sendEmail(err,'MongoDB error');
});

process.on('uncaughtException', (err) => {
  console.log(err);

  sendEmail(err,'Uncaught Exception thrown');
});

process.on('unhandledRejection', (err) => {
  console.log(err);

  sendEmail(err,'Unhandled Rejection at Promise');
});


function sendEmail (err, subject) {
  var transporter = nodemailer.createTransport(smtp_credentials);

  if (mode == 'localhost') { 
    var subject   = "This error is "+ mode + "(" + base_URL + ")";
  } else {
    var subject   = mode + "- Kasplo - Bulk SMS[upload.js] - Critical alert -" + subject;
  }

  var mailOptions = {
    from    : mail_from, 
    to      : 'vinodhtamilvanan@adcanopus.com',        
    subject : subject,
    text    : err.stack.toString(),
    html    : "<p><b style='color:red;'>" + err.stack.toString() + "</b></p>"
  };

  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      console.log(error);
    } else {
      console.log("Message sent: %s", info.messageId);
      console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
    }
  });    
}

app2.set('view engine', 'ejs');
app2.use("/public", express.static(__dirname + '/public'));

// Express body parser
app2.use(express.urlencoded({
    extended: true
}));

// Passport middleware
app2.use(passport2.initialize());
app2.use(passport2.session());

app2.use(cookieParser());

// Connect flash
app2.use(flash());

app2.use('/upload', require('./routes/upload.js'));


app2.use(function(req, res) {
 res.render('404');
});

process.on('SIGINT', function() {
  mongoose.connection.close(function () {
    console.log('Mongoose disconnected on app termination');
    process.exit(0);
  });
});

const server   = app2.listen(5001, console.log(`Server started on port 5001`));
// increase the timeout to 4 minutes
server.timeout = 3600000;  
/* ##################### UPLOAD PORT-2 Configuration - (Stop) ####################### */
